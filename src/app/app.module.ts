import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {Routes, RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AgmCoreModule} from '@agm/core';
import {DataTablesModule} from 'angular-datatables';
import {Ng2SearchPipeModule} from 'ng2-search-filter';
import {ToastrModule} from 'ngx-toastr';
import {FeatherModule} from 'angular-feather';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { NgCircleProgressModule } from 'ng-circle-progress';
import {
  Camera,
  Heart,
  Github,
  Activity,
  Airplay,
  AlertCircle,
  AlertOctagon,
  AlertTriangle,
  AlignCenter,
  AlignJustify,
  AlignLeft,
  AlignRight,
  Anchor,
  Aperture,
  Archive,
  ArrowDown,
  ArrowDownCircle,
  ArrowDownLeft,
  ArrowDownRight,
  ArrowLeftCircle,
  ArrowLeft,
  ArrowRight,
  ArrowRightCircle,
  ArrowUp,
  ArrowUpCircle,
  ArrowUpLeft,
  ArrowUpRight,
  AtSign,
  Award,
  BarChart2,
  BarChart,
  BatteryCharging,
  Battery,
  BellOff,
  Bell,
  Bluetooth,
  Bold,
  BookOpen,
  Book,
  Bookmark,
  Box,
  Briefcase,
  Calendar,
  CameraOff,
  Cast,
  CheckCircle,
  CheckSquare,
  Check,
  ChevronDown,
  ChevronLeft,
  ChevronRight,
  ChevronUp,
  ChevronsDown,
  ChevronsLeft,
  ChevronsRight,
  ChevronsUp,
  Chrome,
  Circle,
  Clipboard,
  Clock,
  CloudDrizzle,
  CloudLightning,
  CloudOff,
  CloudRain,
  Cloud,
  CloudSnow,
  Code,
  Codepen,
  Codesandbox,
  Coffee,
  Columns,
  Command,
  Compass,
  Copy,
  CornerDownLeft,
  CornerDownRight,
  CornerLeftDown,
  CornerLeftUp,
  CornerRightDown,
  CornerRightUp,
  CornerUpLeft,
  CornerUpRight,
  Cpu,
  CreditCard,
  Crop,
  Crosshair,
  Database,
  Delete,
  Disc,
  DollarSign,
  DownloadCloud,
  Download,
  Droplet,
  Edit,
  Edit2,
  Edit3,
  ExternalLink,
  EyeOff,
  Eye,
  Facebook,
  FastForward,
  Feather,
  Figma,
  FileMinus,
  FilePlus,
  FileText,
  File,
  Film,
  Filter,
  Flag,
  Folder,
  FolderMinus,
  FolderPlus,
  Framer,
  Frown,
  Gift,
  GitBranch,
  GitCommit,
  GitMerge,
  GitPullRequest,
  Gitlab,
  Globe,
  Grid,
  HardDrive,
  Hash,
  Headphones,
  HelpCircle,
  Hexagon,
  Home,
  MoreHorizontal,
  Image,
  Inbox,
  Info,
  Instagram,
  Italic,
  Key,
  Layers,
  Layout,
  LifeBuoy,
  Link,
  Link2,
  Linkedin,
  List,
  Loader,
  Lock,
  LogIn,
  LogOut,
  Mail,
  MapPin,
  Map,
  Maximize,
  Maximize2,
  Meh,
  Menu,
  MessageCircle,
  MessageSquare,
  Mic,
  MicOff,
  Minimize,
  Minimize2,
  MinusCircle,
  MinusSquare,
  Minus,
  Monitor,
  Moon,
  MoreVertical,
  MousePointer,
  Move,
  Music,
  Navigation,
  Navigation2,
  Octagon,
  Package,
  Paperclip,
  PauseCircle,
  Pause,
  PenTool,
  Percent,
  PhoneCall,
  PhoneForwarded,
  PhoneIncoming,
  PhoneMissed,
  Phone,
  PhoneOff,
  PhoneOutgoing,
  PieChart,
  Play,
  PlayCircle,
  Plus,
  PlusCircle,
  PlusSquare,
  Pocket,
  Power,
  Printer,
  Radio,
  RefreshCcw,
  RefreshCw,
  Repeat,
  Rewind,
  RotateCcw,
  RotateCw,
  Rss,
  Save,
  Scissors,
  Search,
  Send,
  Server,
  Settings,
  Share,
  Share2,
  Shield,
  ShieldOff,
  ShoppingBag,
  ShoppingCart,
  Shuffle,
  Sidebar,
  SkipBack,
  SkipForward,
  Slack,
  Slash,
  Sliders,
  Smartphone,
  Smile,
  Speaker,
  Square,
  Star,
  StopCircle,
  Sun,
  Sunrise,
  Sunset,
  Tablet,
  Tag,
  Target,
  Terminal,
  Thermometer,
  ThumbsDown,
  ThumbsUp,
  ToggleLeft,
  ToggleRight,
  Tool,
  Trash,
  Trash2,
  Trello,
  TrendingDown,
  TrendingUp,
  Triangle,
  Truck,
  Tv,
  Twitch,
  Twitter,
  Type,
  Umbrella,
  Underline,
  Unlock,
  Upload,
  UploadCloud,
  User,
  UserCheck,
  UserMinus,
  UserPlus,
  UserX,
  Users,
  Video,
  VideoOff,
  Voicemail,
  Volume,
  Volume1,
  Volume2,
  VolumeX,
  Watch,
  Wifi,
  WifiOff,
  Wind,
  XCircle,
  XOctagon,
  XSquare,
  X,
  Youtube,
  Zap,
  ZapOff,
  ZoomIn,
  ZoomOut
} from 'angular-feather/icons';
import {FullComponent} from './layouts/full/full.component';
import {BlankComponent} from './layouts/blank/blank.component';
import {MatTableModule} from '@angular/material/table';
import {MatCardModule} from '@angular/material/card';
/*import { VerticalNavigationComponent } from './shared/vertical-header/vertical-navigation.component';*/
import {VerticalSidebarComponent} from './shared/vertical-sidebar/vertical-sidebar.component';
import {BreadcrumbComponent} from './shared/breadcrumb/breadcrumb.component';
import {HorizontalNavigationComponent} from './shared/horizontal-header/horizontal-navigation.component';
import {HorizontalSidebarComponent} from './shared/horizontal-sidebar/horizontal-sidebar.component';
import {AppComponent} from './app.component';
import {SpinnerComponent} from './shared/spinner.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {UriService} from "./uri.service";
import {VerticalNavigationComponent} from "./shared/vertical-header/vertical-navigation.component";
import { FileUtilityComponent } from './file-utility/file-utility.component';
import {AngularMultiSelectModule} from "angular2-multiselect-dropdown";
import { MatListModule } from '@angular/material/list'
import {MastersComponent} from "./masters/masters.component";
import {RscDtItemCodeComponent} from "./masters/rsc-dt-item-code/rsc-dt-item-code.component";
import {RscDtDivisionComponent} from "./masters/rsc-dt-division/rsc-dt-division.component";
import {RscDtUomComponent} from "./masters/rsc-dt-uom/rsc-dt-uom.component";
import {RscDtStockTypeComponent} from "./masters/rsc-dt-stock-type/rsc-dt-stock-type.component";
import {RSCDTItemCategoryComponent} from "./masters/rsc-dt-item-category/rsc-dt-item-category.component";
import {RscDtSailingDaysComponent} from "./masters/rsc-dt-sailing-days/rsc-dt-sailing-days.component";
import {RscDtCoverDaysComponent} from "./masters/rsc-dt-cover-days/rsc-dt-cover-days.component";
import {RscDtTechnicalSeriesComponent} from "./masters/rsc-dt-technical-series/rsc-dt-technical-series.component";
import {RscDtSupplierComponent} from "./masters/rsc-dt-supplier/rsc-dt-supplier.component";
import {RscDtLinesComponent} from "./masters/rsc-dt-lines/rsc-dt-lines.component";
import {RscDtSafetyStockComponent} from "./masters/rsc-dt-safety-stock/rsc-dt-safety-stock.component";
import {RscDtMoqComponent} from "./masters/rsc-dt-moq/rsc-dt-moq.component";
import {RscDtProductionTypeComponent} from "./masters/rsc-dt-production-type/rsc-dt-production-type.component";
import {RscDtTransportTypeComponent} from "./masters/rsc-dt-transport-type/rsc-dt-transport-type.component";
import {RscDtItemClassComponent} from "./masters/rsc-dt-item-class/rsc-dt-item-class.component";
import {RscDtTransportModeComponent} from "./masters/rsc-dt-transport-mode/rsc-dt-transport-mode.component";
import {RscDtMouldComponent} from "./masters/rsc-dt-mould/rsc-dt-mould.component";
import {MatInputModule} from "@angular/material/input";
import {MatFormFieldModule} from "@angular/material/form-field";
import {NgxOrgChartModule} from "ngx-org-chart";
import { RscDtStdPriceComponent} from './masters/rsc-dt-std-price/rsc-dt-std-price.component';
import {HorizontalHeaderService} from "./shared/horizontal-header/horizontal-header.service";
import { RscDtMaterialCategoryComponent } from './masters/rsc-dt-material-category/rsc-dt-material-category.component';
import { RscDtMapComponent } from './masters/rsc-dt-map/rsc-dt-map.component';
import { PlanSummaryComponent } from './plan-summary/plan-summary.component';
import { RscDtSkidsComponent } from './masters/rsc-dt-skids/rsc-dt-skids.component';
import { RscDtReasonTypeComponent } from './masters/rsc-dt-reason-type/rsc-dt-reason-type.component';
import { RscDtReasonComponent } from './masters/rsc-dt-reason/rsc-dt-reason.component';
import { RscDtTagComponent } from './masters/rsc-dt-tag/rsc-dt-tag.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NewExecutionComponent } from './new-execution/new-execution.component';
import { NewPmComponent } from './new-execution/new-pm/new-pm.component';
import { MpsNewComponent } from './new-execution/new-pm/mps-new/mps-new.component';
import { ConsumptionNewComponent } from './new-execution/new-pm/consumption-new/consumption-new.component';
import { MouldSaturationNewComponent } from './new-execution/new-pm/mould-saturation-new/mould-saturation-new.component';
import { SlobNewComponent } from './new-execution/new-pm/slob-new/slob-new.component';
import { PurchasePlanNewComponent } from './new-execution/new-pm/purchase-plan-new/purchase-plan-new.component';
import { LogicalConsumptionNewComponent } from './new-execution/new-pm/consumption-new/logical-consumption-new/logical-consumption-new.component';
import { GrossConsumptionNewComponent } from './new-execution/new-pm/consumption-new/gross-consumption-new/gross-consumption-new.component';
import { SupplierWiseNewComponent } from './new-execution/new-pm/mould-saturation-new/supplier-wise-new/supplier-wise-new.component';
import { MouldWiseNewComponent } from './new-execution/new-pm/mould-saturation-new/mould-wise-new/mould-wise-new.component';
import { SlobTabNewComponent } from './new-execution/new-pm/slob-new/slob-tab-new/slob-tab-new.component';
import { CoverdaysTabNewComponent } from './new-execution/new-pm/slob-new/coverdays-tab-new/coverdays-tab-new.component';
import { SummaryTabNewComponent } from './new-execution/new-pm/slob-new/summary-tab-new/summary-tab-new.component';
import { NewRmComponent } from './new-execution/new-rm/new-rm.component';
import { NewBulkConsumptionComponent } from './new-execution/new-rm/new-bulk-consumption/new-bulk-consumption.component';
import { NewRmConsumptionComponent } from './new-execution/new-rm/new-rm-consumption/new-rm-consumption.component';
import { NewRmMpsComponent } from './new-execution/new-rm/new-rm-mps/new-rm-mps.component';
import { NewRmPurchasePlanComponent } from './new-execution/new-rm/new-rm-purchase-plan/new-rm-purchase-plan.component';
import { NewBulkLogicalComponent } from './new-execution/new-rm/new-bulk-consumption/new-bulk-logical/new-bulk-logical.component';
import { NewBulkGrossComponent } from './new-execution/new-rm/new-bulk-consumption/new-bulk-gross/new-bulk-gross.component';
import { NewRmLogicalComponent } from './new-execution/new-rm/new-rm-consumption/new-rm-logical/new-rm-logical.component';
import { NewRmGrossComponent } from './new-execution/new-rm/new-rm-consumption/new-rm-gross/new-rm-gross.component';
import {RmServiceService} from "./new-execution/new-rm/rm-service.service";
import {PmServiceService} from "./new-execution/new-pm/pm-service.service";
import {ExecutionFlowPageServiceService} from "./new-execution/execution-flow-page-service.service";
import { SlobComponent } from './new-execution/new-rm/slob/slob.component';
import { CoverDaysComponent } from './new-execution/new-rm/cover-days/cover-days.component';
import { SummaryComponent } from './new-execution/new-rm/summary/summary.component';
import { FgModuleComponent } from './new-execution/fg-module/fg-module.component';
import { FgMpsComponent } from './new-execution/fg-module/fg-mps/fg-mps.component';
import { LineSaturationComponent } from './new-execution/fg-module/line-saturation/line-saturation.component';
import { SkidSaturationComponent } from './new-execution/fg-module/skid-saturation/skid-saturation.component';
import { FgSlobComponent } from './new-execution/fg-module/fg-slob/fg-slob.component';
import {PmExportService} from "./new-execution/new-pm/pm-export.service";
import {RmExportService} from "./new-execution/new-rm/rm-export.service";
import { AdministratorPageComponent } from './new-execution/Administrator/administrator-page/administrator-page.component';
import { OrganisationComponent } from './new-execution/Administrator/administrator-page/organisation/organisation.component';
import { UsersComponent } from './new-execution/Administrator/administrator-page/users/users.component';
import { ExportFlowComponent } from './export-flow/export-flow.component';
import { UserManagementComponent } from './new-execution/Administrator/administrator-page/user-management/user-management.component';
import { PlanManagementComponent } from './new-execution/Administrator/administrator-page/plan-management/plan-management.component';
import { AnalyzerComponent } from './new-execution/analyzer/analyzer/analyzer.component';
import { AllReportsComponent } from './new-execution/analyzer/analyzer/all-reports/all-reports.component';
import { PmReportsComponent } from './new-execution/new-pm/pm-reports/pm-reports.component';
import { RmReportsComponent } from './new-execution/new-rm/rm-reports/rm-reports.component';
import { FgReportsComponent } from './new-execution/fg-module/fg-reports/fg-reports.component';
import { PmBackupComponent } from './new-execution/new-pm/pm-backup/pm-backup.component';
import { AllBackupComponent } from './new-execution/analyzer/analyzer/all-backup/all-backup.component';
import { RmBackupComponent } from './new-execution/new-rm/rm-backup/rm-backup.component';
import { FgBackupComponent } from './new-execution/fg-module/fg-backup/fg-backup.component';
import { PmStockMesComponent } from './new-execution/new-pm/Stock/pm-stock-mes/pm-stock-mes.component';
import { PmStockReceiptComponent } from './new-execution/new-pm/Stock/pm-stock-receipt/pm-stock-receipt.component';
import { RmStockMesComponent } from './new-execution/new-rm/Stock/rm-stock-mes/rm-stock-mes.component';
import { RmStockReceiptComponent } from './new-execution/new-rm/Stock/rm-stock-receipt/rm-stock-receipt.component';
import { RmTransferStockComponent } from './new-execution/new-rm/Stock/rm-transfer-stock/rm-transfer-stock.component';
import { BulkOpRmStockComponent } from './new-execution/new-rm/Stock/bulk-op-rm-stock/bulk-op-rm-stock.component';
import { StockRejectionComponent } from './new-execution/new-rm/Stock/stock-rejection/stock-rejection.component';
import { NewRmSummaryComponent } from './new-execution/new-rm/new-rm-summary/new-rm-summary.component';
import { FgSlobSummaryComponent } from './new-execution/fg-module/fg-slob-summary/fg-slob-summary.component';
import { FgDashboardComponent } from './new-execution/fg-module/fg-dashboard/fg-dashboard.component';
import { MpsDashTabComponent } from './new-execution/fg-module/fg-dashboard/mps-dash-tab/mps-dash-tab.component';
import { SaturationDashTabComponent } from './new-execution/fg-module/fg-dashboard/saturation-dash-tab/saturation-dash-tab.component';
import { SlobDashTabComponent } from './new-execution/fg-module/fg-dashboard/slob-dash-tab/slob-dash-tab.component';
import { LineSaturationTabComponent } from './new-execution/fg-module/fg-dashboard/saturation-dash-tab/line-saturation-tab/line-saturation-tab.component';
import { SkidsSaturationTabComponent } from './new-execution/fg-module/fg-dashboard/saturation-dash-tab/skids-saturation-tab/skids-saturation-tab.component';
import { FgPlanningHorizonComponent } from './new-execution/fg-module/fg-planning-horizon/fg-planning-horizon.component';
import { DeltaAnalyticsComponent } from './new-execution/fg-module/fg-planning-horizon/delta-analytics/delta-analytics.component';
import { PmMpsDashTabComponent } from './new-execution/new-pm/analytics-dashboard/pm-mps-dash-tab/pm-mps-dash-tab.component';
import { PmSaturationDashTabComponent } from './new-execution/new-pm/analytics-dashboard/pm-saturation-dash-tab/pm-saturation-dash-tab.component';
import { PmSlobDashTabComponent } from './new-execution/new-pm/analytics-dashboard/pm-slob-dash-tab/pm-slob-dash-tab.component';
import { PmPlanningHorizonComponent } from './new-execution/new-pm/pm-planning-horizon/pm-planning-horizon.component';
import { PmOtifComponent } from './new-execution/new-pm/pm-planning-horizon/pm-otif/pm-otif.component';
import { PmOtifDashComponent } from './new-execution/new-pm/pm-planning-horizon/pm-otif/pm-otif-dash/pm-otif-dash.component';
import { PmDataDashComponent } from './new-execution/new-pm/pm-planning-horizon/pm-otif/pm-data-dash/pm-data-dash.component';
import {PmAnalyticsDashboardComponent} from "./new-execution/new-pm/analytics-dashboard/pm-analytics-dashboard.component";
import { RmAnalyticsDashboardComponent } from './new-execution/new-rm/rm-analytics-dashboard/rm-analytics-dashboard.component';
import { RmMpsDashTabComponent } from './new-execution/new-rm/rm-analytics-dashboard/rm-mps-dash-tab/rm-mps-dash-tab.component';
import { RmSlobDashTabComponent } from './new-execution/new-rm/rm-analytics-dashboard/rm-slob-dash-tab/rm-slob-dash-tab.component';
import { RmPlanningHorizonComponent } from './new-execution/new-rm/rm-planning-horizon/rm-planning-horizon.component';
import { RmOtifComponent } from './new-execution/new-rm/rm-planning-horizon/rm-otif/rm-otif.component';
import { PurchasePlanRmCoverDaysComponent } from './new-execution/new-rm/purchase-plan-rm-cover-days/purchase-plan-rm-cover-days.component';
import { RmPurchaseCoverdaysComponent } from './new-execution/new-rm/purchase-plan-rm-cover-days/rm-purchase-coverdays/rm-purchase-coverdays.component';
import { RmPurchaseStockEquationComponent } from './new-execution/new-rm/purchase-plan-rm-cover-days/rm-purchase-stock-equation/rm-purchase-stock-equation.component';
import { PurchasePlanPmCoverDaysComponent } from './new-execution/new-pm/purchase-plan-pm-cover-days/purchase-plan-pm-cover-days.component';
import { PmPurchaseCoverDaysComponent } from './new-execution/new-pm/purchase-plan-pm-cover-days/pm-purchase-cover-days/pm-purchase-cover-days.component';
import { PmPurchaseStockEquationComponent } from './new-execution/new-pm/purchase-plan-pm-cover-days/pm-purchase-stock-equation/pm-purchase-stock-equation.component';
import { PmSlobMovementDashComponent } from './new-execution/new-pm/pm-planning-horizon/pm-otif/pm-slob-movement-dash/pm-slob-movement-dash.component';
import { PmInventoryQualityIndexDashComponent } from './new-execution/new-pm/pm-planning-horizon/pm-otif/pm-inventory-quality-index-dash/pm-inventory-quality-index-dash.component';
import { RmSlobMovementDashComponent } from './new-execution/new-rm/rm-planning-horizon/rm-slob-movement-dash/rm-slob-movement-dash.component';
import { RmInventoryQualityIndexDashComponent } from './new-execution/new-rm/rm-planning-horizon/rm-inventory-quality-index-dash/rm-inventory-quality-index-dash.component';
import { FgSlobMovementDashComponent } from './new-execution/fg-module/fg-planning-horizon/fg-slob-movement-dash/fg-slob-movement-dash.component';
import { FgInventoryQualityIndexDashComponent } from './new-execution/fg-module/fg-planning-horizon/fg-inventory-quality-index-dash/fg-inventory-quality-index-dash.component';
import { PmMasterDataComponent } from './new-execution/new-pm/pm-master-data/pm-master-data.component';
import { RmMasterDataComponent } from './new-execution/new-rm/rm-master-data/rm-master-data.component';
import { FgMasterDataComponent } from './new-execution/fg-module/fg-master-data/fg-master-data.component';
import { PmMaterialModalComponent } from './new-execution/new-pm/pm-master-data/pm-material-modal/pm-material-modal.component';
import { MaterialCategoryModalComponent } from './new-execution/new-pm/pm-master-data/material-category-modal/material-category-modal.component';
import { MouldsModalComponent } from './new-execution/new-pm/pm-master-data/moulds-modal/moulds-modal.component';
import { PmSupplierModalComponent } from './new-execution/new-pm/pm-master-data/pm-supplier-modal/pm-supplier-modal.component';
import { PurchaseRemarkModalComponent } from './new-execution/new-pm/pm-master-data/purchase-remark-modal/purchase-remark-modal.component';
import { SlobReasonModalComponent } from './new-execution/new-pm/pm-master-data/slob-reason-modal/slob-reason-modal.component';
import { SlobRemarkModalComponent } from './new-execution/new-pm/pm-master-data/slob-remark-modal/slob-remark-modal.component';
import { SlobMapModalComponent } from './new-execution/new-pm/pm-master-data/slob-map-modal/slob-map-modal.component';
import { SlobStdpriceModalComponent } from './new-execution/new-pm/pm-master-data/slob-stdprice-modal/slob-stdprice-modal.component';
import { StockMidNightModalComponent } from './new-execution/new-pm/pm-master-data/stock-mid-night-modal/stock-mid-night-modal.component';
import { StockReceiptModalComponent } from './new-execution/new-pm/pm-master-data/stock-receipt-modal/stock-receipt-modal.component';
import { RmMaterialModalComponent } from './new-execution/new-rm/rm-master-data/rm-material-modal/rm-material-modal.component';
import { RmSupplierModalComponent } from './new-execution/new-rm/rm-master-data/rm-supplier-modal/rm-supplier-modal.component';
import { RmSlobReasonModalComponent } from './new-execution/new-rm/rm-master-data/rm-slob-reason-modal/rm-slob-reason-modal.component';
import { RmSlobRemarkModalComponent } from './new-execution/new-rm/rm-master-data/rm-slob-remark-modal/rm-slob-remark-modal.component';
import { RmSlobMapModalComponent } from './new-execution/new-rm/rm-master-data/rm-slob-map-modal/rm-slob-map-modal.component';
import { RmSlobStdpriceModalComponent } from './new-execution/new-rm/rm-master-data/rm-slob-stdprice-modal/rm-slob-stdprice-modal.component';
import { RmStockMidNightModalComponent } from './new-execution/new-rm/rm-master-data/rm-stock-mid-night-modal/rm-stock-mid-night-modal.component';
import { RmStockReceiptModalComponent } from './new-execution/new-rm/rm-master-data/rm-stock-receipt-modal/rm-stock-receipt-modal.component';
import { RmBulkoprmModalComponent } from './new-execution/new-rm/rm-master-data/rm-bulkoprm-modal/rm-bulkoprm-modal.component';
import { RmTransferStockModalComponent } from './new-execution/new-rm/rm-master-data/rm-transfer-stock-modal/rm-transfer-stock-modal.component';
import { RmRejectionModalComponent } from './new-execution/new-rm/rm-master-data/rm-rejection-modal/rm-rejection-modal.component';
import { FgMaterialModalComponent } from './new-execution/fg-module/fg-master-data/fg-material-modal/fg-material-modal.component';
import { FgLinesModalComponent } from './new-execution/fg-module/fg-master-data/fg-lines-modal/fg-lines-modal.component';
import { FgSkidModalComponent } from './new-execution/fg-module/fg-master-data/fg-skid-modal/fg-skid-modal.component';
import { FgSlobReasonModalComponent } from './new-execution/fg-module/fg-master-data/fg-slob-reason-modal/fg-slob-reason-modal.component';
import { FgSlobRemarkModalComponent } from './new-execution/fg-module/fg-master-data/fg-slob-remark-modal/fg-slob-remark-modal.component';
import { FgSlobMapModalComponent } from './new-execution/fg-module/fg-master-data/fg-slob-map-modal/fg-slob-map-modal.component';
import { FgSlobStdpriceModalComponent } from './new-execution/fg-module/fg-master-data/fg-slob-stdprice-modal/fg-slob-stdprice-modal.component';
import { RmPivotComponent } from './new-execution/new-rm/rm-pivot/rm-pivot.component';
import { PmDeltaAnalyticsComponent } from './new-execution/new-pm/pm-planning-horizon/pm-otif/pm-delta-analytics/pm-delta-analytics.component';
import { CommonAnalyticsComponent } from './new-execution/analyzer/common-analytics/common-analytics.component';
import { AnalyzerMpsDashComponent } from './new-execution/analyzer/common-analytics/analyzer-mps-dash/analyzer-mps-dash.component';
import { PmOtifCummulativeScoreComponent } from './new-execution/new-pm/pm-planning-horizon/pm-otif-cummulative-score/pm-otif-cummulative-score.component';
import { LineDeltaAnalyticsComponent } from './new-execution/fg-module/fg-planning-horizon/line-delta-analytics/line-delta-analytics.component';
import { SkidDeltaAnalyticsComponent } from './new-execution/fg-module/fg-planning-horizon/skid-delta-analytics/skid-delta-analytics.component';
import { RmBulkMaterialModalComponent } from './new-execution/new-rm/rm-master-data/rm-bulk-material-modal/rm-bulk-material-modal.component';
import { RmSubBasesMaterialModalComponent } from './new-execution/new-rm/rm-master-data/rm-sub-bases-material-modal/rm-sub-bases-material-modal.component';
import { FgWipMaterialModalComponent } from './new-execution/fg-module/fg-master-data/fg-wip-material-modal/fg-wip-material-modal.component';
import { HomePageComponent } from './new-execution/home-page/home-page.component';
import { PlanAModuleComponent } from './new-execution/plan-a-module/plan-a-module.component';
import { PlanPageComponent } from './new-execution/plan-a-module/plan-page/plan-page.component';
import { GoToPlanModuleComponent } from './new-execution/plan-a-module/go-to-plan-module/go-to-plan-module.component';
import { GoToValidationComponent } from './new-execution/plan-a-module/go-to-validation/go-to-validation.component';
import { GoToExecutionComponent } from './new-execution/plan-a-module/go-to-execution/go-to-execution.component';
import { RmDeltaAnalyticsComponent } from './new-execution/new-rm/rm-planning-horizon/rm-delta-analytics/rm-delta-analytics.component';
import { PlanningHorizonAnalyzerComponent } from './new-execution/analyzer/planning-horizon-analyzer/planning-horizon-analyzer.component';
import { CommonMpsDeltaAnalyticsComponent } from './new-execution/analyzer/planning-horizon-analyzer/common-mps-delta-analytics/common-mps-delta-analytics.component';
import {NgFileDragDropModule} from 'ng-file-drag-drop';
import { FgReportsPageComponent } from './new-execution/fg-module/fg-reports-page/fg-reports-page.component';
import { AllReportsPageComponent } from './new-execution/analyzer/analyzer/all-reports-page/all-reports-page.component';
import { AllOtifComponent } from './new-execution/analyzer/analyzer/all-otif/all-otif.component';
import { PmReportsPageComponent } from './new-execution/new-pm/pm-reports-page/pm-reports-page.component';
import { PmReportsOtifComponent } from './new-execution/new-pm/pm-reports-otif/pm-reports-otif.component';
import { RmReportsPageComponent } from './new-execution/new-rm/rm-reports-page/rm-reports-page.component';
import { MatRadioModule } from '@angular/material/radio';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSliderModule} from '@angular/material/slider';
import { BackupPmOtifComponent } from './new-execution/analyzer/analyzer/backup-pm-otif/backup-pm-otif.component';
import { BackupAnnualAnalysisComponent } from './new-execution/analyzer/analyzer/backup-annual-analysis/backup-annual-analysis.component';
import { BackupPlansTabComponent } from './new-execution/analyzer/analyzer/backup-plans-tab/backup-plans-tab.component';
import { AnalyzerBackupPageComponent } from './new-execution/analyzer/analyzer/analyzer-backup-page/analyzer-backup-page.component';
import { PmBackupPageComponent } from './new-execution/new-pm/pm-backup/pm-backup-page/pm-backup-page.component';
import { PmBackupPlansComponent } from './new-execution/new-pm/pm-backup/pm-backup-plans/pm-backup-plans.component';
import { PmBackupOtifComponent } from './new-execution/new-pm/pm-backup/pm-backup-otif/pm-backup-otif.component';
import { PmBackupAnnualAnalysisComponent } from './new-execution/new-pm/pm-backup/pm-backup-annual-analysis/pm-backup-annual-analysis.component';
import { FgBackupPlansComponent } from './new-execution/fg-module/fg-backup/fg-backup-plans/fg-backup-plans.component';
import { FgBackupAnnualAnalysisComponent } from './new-execution/fg-module/fg-backup/fg-backup-annual-analysis/fg-backup-annual-analysis.component';
import { FgBackupPageComponent } from './new-execution/fg-module/fg-backup/fg-backup-page/fg-backup-page.component';
import { RmBackupPageComponent } from './new-execution/new-rm/rm-backup/rm-backup-page/rm-backup-page.component';
import { RmBackupPlansComponent } from './new-execution/new-rm/rm-backup/rm-backup-plans/rm-backup-plans.component';
import { RmBackupAnnualAnalysisComponent } from './new-execution/new-rm/rm-backup/rm-backup-annual-analysis/rm-backup-annual-analysis.component';
import {AppRoutingModule} from "./app-routing.module";
import {AuthService} from "./auth.service";
import {AuthGuard} from "./auth-guard.service";
import {TokenInterceptor} from "./token.interceptor";
import {LoginService} from "./sign-in/login.service";
import {PartyService} from "./party/party.service";
import {SignInComponent} from "./sign-in/sign-in.component";
import {NgIdleKeepaliveModule} from "@ng-idle/keepalive";
import { RmStockMesWithRejectionComponent } from './new-execution/new-rm/rm-master-data/rm-stock-mes-with-rejection/rm-stock-mes-with-rejection.component';
import { PmStockMesWithRejectionComponent } from './new-execution/new-pm/pm-master-data/pm-stock-mes-with-rejection/pm-stock-mes-with-rejection.component';
import { RmHomePageComponent } from './new-execution/home-page/rm-home-page/rm-home-page.component';
import { PmHomePageComponent } from './new-execution/home-page/pm-home-page/pm-home-page.component';
import { FgHomePageComponent } from './new-execution/home-page/fg-home-page/fg-home-page.component';
import { AnalystHomePageComponent } from './new-execution/home-page/analyst-home-page/analyst-home-page.component';
import { AdminHomePageComponent } from './new-execution/home-page/admin-home-page/admin-home-page.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 1,
  wheelPropagation: true,
  minScrollbarLength: 20
};

// Select some icons (use an object, not an array)
const icons = {
  Camera,
  Heart,
  Github,
  Activity,
  Airplay,
  AlertCircle,
  AlertOctagon,
  AlertTriangle,
  AlignCenter,
  AlignJustify,
  AlignLeft,
  AlignRight,
  Anchor,
  Aperture,
  Archive,
  ArrowDown,
  ArrowDownCircle,
  ArrowDownLeft,
  ArrowDownRight,
  ArrowLeftCircle,
  ArrowLeft,
  ArrowRight,
  ArrowRightCircle,
  ArrowUp,
  ArrowUpCircle,
  ArrowUpLeft,
  ArrowUpRight,
  AtSign,
  Award,
  BarChart2,
  BarChart,
  BatteryCharging,
  Battery,
  BellOff,
  Bell,
  Bluetooth,
  Bold,
  BookOpen,
  Book,
  Bookmark,
  Box,
  Briefcase,
  Calendar,
  CameraOff,
  Cast,
  CheckCircle,
  CheckSquare,
  Check,
  ChevronDown,
  ChevronLeft,
  ChevronRight,
  ChevronUp,
  ChevronsDown,
  ChevronsLeft,
  ChevronsRight,
  ChevronsUp,
  Chrome,
  Circle,
  Clipboard,
  Clock,
  CloudDrizzle,
  CloudLightning,
  CloudOff,
  CloudRain,
  Cloud,
  CloudSnow,
  Code,
  Codepen,
  Codesandbox,
  Coffee,
  Columns,
  Command,
  Compass,
  Copy,
  CornerDownLeft,
  CornerDownRight,
  CornerLeftDown,
  CornerLeftUp,
  CornerRightDown,
  CornerRightUp,
  CornerUpLeft,
  CornerUpRight,
  Cpu,
  CreditCard,
  Crop,
  Crosshair,
  Database,
  Delete,
  Disc,
  DollarSign,
  DownloadCloud,
  Download,
  Droplet,
  Edit,
  Edit2,
  Edit3,
  ExternalLink,
  EyeOff,
  Eye,
  Facebook,
  FastForward,
  Feather,
  Figma,
  FileMinus,
  FilePlus,
  FileText,
  File,
  Film,
  Filter,
  Flag,
  Folder,
  FolderMinus,
  FolderPlus,
  Framer,
  Frown,
  Gift,
  GitBranch,
  GitCommit,
  GitMerge,
  GitPullRequest,
  Gitlab,
  Globe,
  Grid,
  HardDrive,
  Hash,
  Headphones,
  HelpCircle,
  Hexagon,
  Home,
  MoreHorizontal,
  Image,
  Inbox,
  Info,
  Instagram,
  Italic,
  Key,
  Layers,
  Layout,
  LifeBuoy,
  Link,
  Link2,
  Linkedin,
  List,
  Loader,
  Lock,
  LogIn,
  LogOut,
  Mail,
  MapPin,
  Map,
  Maximize,
  Maximize2,
  Meh,
  Menu,
  MessageCircle,
  MessageSquare,
  Mic,
  MicOff,
  Minimize,
  Minimize2,
  MinusCircle,
  MinusSquare,
  Minus,
  Monitor,
  Moon,
  MoreVertical,
  MousePointer,
  Move,
  Music,
  Navigation,
  Navigation2,
  Octagon,
  Package,
  Paperclip,
  PauseCircle,
  Pause,
  PenTool,
  Percent,
  PhoneCall,
  PhoneForwarded,
  PhoneIncoming,
  PhoneMissed,
  Phone,
  PhoneOff,
  PhoneOutgoing,
  PieChart,
  Play,
  PlayCircle,
  Plus,
  PlusCircle,
  PlusSquare,
  Pocket,
  Power,
  Printer,
  Radio,
  RefreshCcw,
  RefreshCw,
  Repeat,
  Rewind,
  RotateCcw,
  RotateCw,
  Rss,
  Save,
  Scissors,
  Search,
  Send,
  Server,
  Settings,
  Share,
  Share2,
  Shield,
  ShieldOff,
  ShoppingBag,
  ShoppingCart,
  Shuffle,
  Sidebar,
  SkipBack,
  SkipForward,
  Slack,
  Slash,
  Sliders,
  Smartphone,
  Smile,
  Speaker,
  Square,
  Star,
  StopCircle,
  Sun,
  Sunrise,
  Sunset,
  Tablet,
  Tag,
  Target,
  Terminal,
  Thermometer,
  ThumbsDown,
  ThumbsUp,
  ToggleLeft,
  ToggleRight,
  Tool,
  Trash,
  Trash2,
  Trello,
  TrendingDown,
  TrendingUp,
  Triangle,
  Truck,
  Tv,
  Twitch,
  Twitter,
  Type,
  Umbrella,
  Underline,
  Unlock,
  Upload,
  UploadCloud,
  User,
  UserCheck,
  UserMinus,
  UserPlus,
  UserX,
  Users,
  Video,
  VideoOff,
  Voicemail,
  Volume,
  Volume1,
  Volume2,
  VolumeX,
  Watch,
  Wifi,
  WifiOff,
  Wind,
  XCircle,
  XOctagon,
  XSquare,
  X,
  Youtube,
  Zap,
  ZapOff,
  ZoomIn,
  ZoomOut
};

@NgModule({
  declarations: [
    AppComponent,
    SpinnerComponent,
    FullComponent,
    BlankComponent,
    VerticalNavigationComponent,
    BreadcrumbComponent,
    VerticalSidebarComponent,
    HorizontalNavigationComponent,
    HorizontalSidebarComponent,
    FileUtilityComponent,
    MastersComponent,
    RscDtItemCodeComponent,
    RSCDTItemCategoryComponent,
    RscDtCoverDaysComponent,
    RscDtUomComponent,
    RscDtDivisionComponent,
    RscDtSafetyStockComponent,
    RscDtItemClassComponent,
    RscDtTechnicalSeriesComponent,
    RscDtMoqComponent,
    RscDtTransportModeComponent,
    RscDtTransportTypeComponent,
    RscDtMouldComponent,
    RscDtSupplierComponent,
    RscDtProductionTypeComponent,
    RscDtLinesComponent,
    RscDtStockTypeComponent,
    RscDtSailingDaysComponent,
    RscDtStdPriceComponent,
    RscDtMaterialCategoryComponent,
    RscDtMapComponent,
    PlanSummaryComponent,
    RscDtSkidsComponent,
    RscDtReasonTypeComponent,
    RscDtReasonComponent,
    RscDtTagComponent,
    NewExecutionComponent,
    NewPmComponent,
    MpsNewComponent,
    ConsumptionNewComponent,
    MouldSaturationNewComponent,
    SlobNewComponent,
    PurchasePlanNewComponent,
    LogicalConsumptionNewComponent,
    GrossConsumptionNewComponent,
    SupplierWiseNewComponent,
    MouldWiseNewComponent,
    SlobTabNewComponent,
    CoverdaysTabNewComponent,
    SummaryTabNewComponent,
    NewRmComponent,
    NewBulkConsumptionComponent,
    NewRmConsumptionComponent,
    NewRmMpsComponent,
    NewRmPurchasePlanComponent,
    NewBulkLogicalComponent,
    NewBulkGrossComponent,
    NewRmLogicalComponent,
    NewRmGrossComponent,
    SlobComponent,
    CoverDaysComponent,
    SummaryComponent,
    FgModuleComponent,
    FgMpsComponent,
    LineSaturationComponent,
    SkidSaturationComponent,
    FgSlobComponent,
    AdministratorPageComponent,
    OrganisationComponent,
    UsersComponent,
    ExportFlowComponent,
    UserManagementComponent,
    PlanManagementComponent,
    AnalyzerComponent,
    AllReportsComponent,
    PmReportsComponent,
    RmReportsComponent,
    FgReportsComponent,
    PmBackupComponent,
    AllBackupComponent,
    RmBackupComponent,
    FgBackupComponent,
    PmStockMesComponent,
    PmStockReceiptComponent,
    RmStockMesComponent,
    RmStockReceiptComponent,
    RmTransferStockComponent,
    BulkOpRmStockComponent,
    StockRejectionComponent,
    NewRmSummaryComponent,
    FgSlobSummaryComponent,
    FgDashboardComponent,
    MpsDashTabComponent,
    SaturationDashTabComponent,
    SlobDashTabComponent,
    LineSaturationTabComponent,
    SkidsSaturationTabComponent,
    FgPlanningHorizonComponent,
    DeltaAnalyticsComponent,
    PmMpsDashTabComponent,
    PmSaturationDashTabComponent,
    PmSlobDashTabComponent,
    PmPlanningHorizonComponent,
    PmOtifComponent,
    PmOtifDashComponent,
    PmDataDashComponent,
    PmAnalyticsDashboardComponent,
    RmAnalyticsDashboardComponent,
    RmMpsDashTabComponent,
    RmSlobDashTabComponent,
    RmPlanningHorizonComponent,
    RmOtifComponent,
    PurchasePlanRmCoverDaysComponent,
    RmPurchaseCoverdaysComponent,
    RmPurchaseStockEquationComponent,
    PurchasePlanPmCoverDaysComponent,
    PmPurchaseCoverDaysComponent,
    PmPurchaseStockEquationComponent,
    PmSlobMovementDashComponent,
    PmInventoryQualityIndexDashComponent,
    RmSlobMovementDashComponent,
    RmInventoryQualityIndexDashComponent,
    FgSlobMovementDashComponent,
    FgInventoryQualityIndexDashComponent,
    PmMasterDataComponent,
    RmMasterDataComponent,
    FgMasterDataComponent,
    PmMaterialModalComponent,
    MaterialCategoryModalComponent,
    MouldsModalComponent,
    PmSupplierModalComponent,
    PurchaseRemarkModalComponent,
    SlobReasonModalComponent,
    SlobRemarkModalComponent,
    SlobMapModalComponent,
    SlobStdpriceModalComponent,
    StockMidNightModalComponent,
    StockReceiptModalComponent,
    RmMaterialModalComponent,
    RmSupplierModalComponent,
    RmSlobReasonModalComponent,
    RmSlobRemarkModalComponent,
    RmSlobMapModalComponent,
    RmSlobStdpriceModalComponent,
    RmStockMidNightModalComponent,
    RmStockReceiptModalComponent,
    RmBulkoprmModalComponent,
    RmTransferStockModalComponent,
    RmRejectionModalComponent,
    FgMaterialModalComponent,
    FgLinesModalComponent,
    FgSkidModalComponent,
    FgSlobReasonModalComponent,
    FgSlobRemarkModalComponent,
    FgSlobMapModalComponent,
    FgSlobStdpriceModalComponent,
    RmPivotComponent,
    PmDeltaAnalyticsComponent,
    CommonAnalyticsComponent,
    AnalyzerMpsDashComponent,
    PmOtifCummulativeScoreComponent,
    LineDeltaAnalyticsComponent,
    SkidDeltaAnalyticsComponent,
    RmBulkMaterialModalComponent,
    RmSubBasesMaterialModalComponent,
    FgWipMaterialModalComponent,
    HomePageComponent,
    PlanAModuleComponent,
    PlanPageComponent,
    GoToPlanModuleComponent,
    GoToValidationComponent,
    GoToExecutionComponent,
    RmDeltaAnalyticsComponent,
    PlanningHorizonAnalyzerComponent,
    CommonMpsDeltaAnalyticsComponent,
    FgReportsPageComponent,
    AllReportsPageComponent,
    AllOtifComponent,
    PmReportsPageComponent,
    PmReportsOtifComponent,
    RmReportsPageComponent,
    BackupPmOtifComponent,
    BackupAnnualAnalysisComponent,
    BackupPlansTabComponent,
    AnalyzerBackupPageComponent,
    PmBackupPageComponent,
    PmBackupPlansComponent,
    PmBackupOtifComponent,
    PmBackupAnnualAnalysisComponent,
    FgBackupPlansComponent,
    FgBackupAnnualAnalysisComponent,
    FgBackupPageComponent,
    RmBackupPageComponent,
    RmBackupPlansComponent,
    RmBackupAnnualAnalysisComponent,
    SignInComponent,
    RmStockMesWithRejectionComponent,
    PmStockMesWithRejectionComponent,
    RmHomePageComponent,
    PmHomePageComponent,
    FgHomePageComponent,
    AnalystHomePageComponent,
    AdminHomePageComponent
  ],
  imports: [
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    NgFileDragDropModule,
    NgxSpinnerModule,
    NgxOrgChartModule,
    MatTableModule,
    MatCardModule,
    MatListModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    DataTablesModule,
    HttpClientModule,
    NgbModule,
    MatCheckboxModule,
    FeatherModule.pick(icons),
    FeatherModule,
    Ng2SearchPipeModule,
    PerfectScrollbarModule,
    AngularMultiSelectModule,
    MatInputModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyDoliAneRffQDyA7Ul9cDk3tLe7vaU4yP8'}),
    NgIdleKeepaliveModule.forRoot(),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 10000,
      showSubtitle: true,
      subtitleColor: "#000000",
    }), MatRadioModule,
    MatProgressSpinnerModule,MatSliderModule
  ],
  providers: [AuthService, LoginService, PartyService, AuthGuard, UriService,RmServiceService,PmServiceService ,HorizontalHeaderService,
     ExecutionFlowPageServiceService, PmExportService, RmExportService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  exports: [MatCardModule, MatFormFieldModule],
  bootstrap: [AppComponent]
})
export class AppModule {
}
