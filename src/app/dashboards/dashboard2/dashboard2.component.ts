import {Component, AfterViewInit, OnInit} from '@angular/core';
import * as $ from 'jquery';
import {Router} from "@angular/router";

@Component({
  templateUrl: './dashboard2.component.html',
  styleUrls: ['./dashboard2.component.css']
})
export class Dashboard2Component implements AfterViewInit,OnInit {
  currentJustify = 'start';

  currentOrientation = 'horizontal';
  selectedName: any;
  queueRecords = [{
    id:0,
    application: 'first',
    queueType: 'good',
    startTime: '12:30',
    endTime: '5:30'
  },
    {
      id:1,
      application: 'first',
      queueType: 'fgg',
      startTime: '12:30',
      endTime: '5:30'
    },
    {
      id:2,
      application: 'first',
      queueType: 'good',
      startTime: '12:440',
      endTime: '5:440'
    }
  ]
  rowsControls: any[] = [];
  rows = [{
    id: 0,
    name: 'First row'
  }, {
    id: 1,
    name: 'Second row'
  }, {
    id: 2,
    name: 'Third row'
  }]

  accordionSamples = [
    {
      id: 1,
      title: 'Simple',
      content: `Simple content sample`
    },
    {
      id: 2,
      title: 'Fancy',
      content: `Fancy content interesting`
    },
    {
      id: 3,
      title: 'third',
      content: `third content interesting`
    }
  ]

  games = [{
    'date': 12/12/12, 'label': 'one', 'score':3
  },
    {
      'date': 3/12/12, 'label': 'two', 'score':5
    },
    {
      'date': 4/12/12, 'label': 'three', 'score':10
    }
  ]

  subtitle: string;

  constructor(private router: Router) {
    this.subtitle = 'This is some text within a card block.';
  }
  ngOnInit(): void {
    this.rows.forEach(row => {
      this.rowsControls.push({
        isCollapsed: true
      })
    });




    this.highlightRow(-1);


  }

  pmPage(){
    this.router.navigateByUrl('/dashboard/pm')
  }
  rmPage(){
    this.router.navigateByUrl('/dashboard/rm')
  }


  ngAfterViewInit() { }
  public highlightRow(emp:any) {

    console.log(emp);
    console.log(emp);
    this.selectedName = emp;
  }
}
