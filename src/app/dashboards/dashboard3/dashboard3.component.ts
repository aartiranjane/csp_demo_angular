import {Component, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {Router} from "@angular/router";


@Component({
  templateUrl: './dashboard3.component.html',
  styleUrls: ['./dashboard3.component.css']
})
export class Dashboard3Component implements AfterViewInit, OnInit {

  public config: PerfectScrollbarConfigInterface = {};


  subtitle: string;

  constructor(private router: Router) {
    this.subtitle = 'This is some text within a card block.';
  }

  ngOnInit() {

  }
  ngAfterViewInit(){}





}
