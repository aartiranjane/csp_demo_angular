import {Component, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {Router} from "@angular/router";
@Component({
    templateUrl: './dashboard1.component.html',
    styleUrls: ['./dashboard1.component.css']
})
export class Dashboard1Component implements AfterViewInit, OnInit {

  finalProList:any= [];
  isConsumption1 = 'active enabled';
  isConsumption2 = '';
  isConsumption3 = 'disabled';
  showTab = 1;
  blinkLogicalTab ={
    'pulse2' : true
  }
  blinkGrossTab ={
    'pulse2' : false
  }
    public config: PerfectScrollbarConfigInterface = {};

    subtitle: string;
    constructor(private router: Router ) {
        this.subtitle = 'This is some text within a card block.';   
    }

    ngOnInit(){
      /*modal open code*/
   /*   document.getElementById('executionFlowModal')!.click();*/

    }

  tab(index:any){
    if(index == 1){
      this.isConsumption1 = 'active enabled';
      this.isConsumption2 = '';
      this.isConsumption3 = '';
      this.showTab = 1;

    }
    else if(index ==  2){

      this.isConsumption2 = 'active enabled';
      this.isConsumption1 = '';
      this.isConsumption3 = '';
      this.blinkLogicalTab.pulse2 = false;
      this.blinkGrossTab.pulse2 = true;
      this.showTab = 2;

    }
    else if(index ==  3){
      this.isConsumption3 = 'active enabled';
      this.isConsumption1 = '';
      this.isConsumption2 = '';
      this.blinkGrossTab.pulse2 = false;
      this.showTab = 3;

    }

  }



    ngAfterViewInit() { }
}
