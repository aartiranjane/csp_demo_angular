import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';
import 'rxjs/add/observable/of';
import {Router} from '@angular/router';
import 'rxjs/add/observable/throw';
import {LoginService} from './sign-in/login.service';
import {UriService} from './uri.service';
import {IntervalObservable} from 'rxjs-compat/observable/IntervalObservable';
import {interval} from 'rxjs';
import {DEFAULT_INTERRUPTSOURCES, Idle} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';


class UserDetailsView {
    userDetails: any;
    username: any;
}

@Injectable()
export class AuthService {
    helper = new JwtHelperService();
    currentUserDetailsView: UserDetailsView;
    isAuthenticatedEvent: Subject<boolean> = new Subject<boolean>();
    currentUserObservable: Subject<any> = new Subject<any>();
    public idleState = 'Not started.';
    public timedOut = '';
    public timeoutTime = 180;
    public idleTime = 600;                              // in seconds - after these many idle dialog will open
    public refreshTokenExpTime = 21400;                   // in seconds - refresh token expire time
    public showWarning = new Subject();
    public relogSub: any;
    private loggedInTime = new Subject();
    private un;
    private pw;

    constructor(private loginService: LoginService,
                private router: Router,
                private uriService: UriService,
                public idle: Idle,
                public keepAlive: Keepalive) {
    }

    isAuthenticated() {
        const promise = new Promise((resolve, reject) => {
            resolve(this.isLoggedIn());
        });
        return promise;
    }

    login(name: string, pw: string): Promise<boolean> {
        this.timedOut = '';
        this.currentUserDetailsView = new UserDetailsView();
        this.un = name;
        this.pw = pw;
        debugger
        return new Promise((resolve, reject) => {
            this.loginService.getUserUsingXhr(name, pw).subscribe((res) => {
                if (res.error !== undefined) {
                    if (res.error === 'unauthorized') {
                        reject('Please enter valid credentials');
                    }
                    this.isAuthenticatedEvent.next(false);
                } else {
                    sessionStorage.setItem('username', name);
                    this.currentUserDetailsView.userDetails = res;
                    this.currentUserDetailsView.username = name;
                    sessionStorage.setItem('userToken', res.access_token);
                    sessionStorage.setItem('refresh_token', res.refresh_token);
                    sessionStorage.setItem('currentUser', JSON.stringify(res));
                    this.currentUserObservable.next(this.currentUserDetailsView);
                    this.isAuthenticatedEvent.next(true);
                    const relogin = interval(1000);
                    this.idle.watch();
                    const sub = relogin.subscribe(value1 => {
                        this.relogSub = sub;
                        this.loggedInTime.next(value1);
                        if (value1 === this.refreshTokenExpTime) {
                            sub.unsubscribe();
                        }
                    });
                    IntervalObservable.create((res.expires_in - 15) * 1000).subscribe(() => {
                        if (this.isLoggedIn()) {
                            this.usedRefreshToken();
                        }
                    });
                    resolve(true);
                }
            });
        });
    }

    reLogin() {
        this.currentUserDetailsView = null;
        this.relogSub.unsubscribe();
        sessionStorage.clear();
        this.idle.stop();
        this.timedOut = '';
        this.currentUserDetailsView = new UserDetailsView();
        this.loginService.getUserUsingXhr(this.un, this.pw).subscribe((res) => {
            if (res.error !== undefined) {
                if (res.error === 'unauthorized') {
                    console.log(res.error);
                }
                this.isAuthenticatedEvent.next(false);
            } else {
                sessionStorage.setItem('username', this.un);
                this.currentUserDetailsView.userDetails = res;
                this.currentUserDetailsView.username = this.un;
                sessionStorage.setItem('userToken', res.access_token);
                sessionStorage.setItem('refresh_token', res.refresh_token);
                sessionStorage.setItem('currentUser', JSON.stringify(res));
                this.currentUserObservable.next(this.currentUserDetailsView);
                this.isAuthenticatedEvent.next(true);
                const relogin = interval(1000);
                this.idle.watch();
                const sub = relogin.subscribe(value1 => {
                    this.relogSub = sub;
                    this.loggedInTime.next(value1);
                    if (value1 === this.refreshTokenExpTime) {
                        sub.unsubscribe();
                    }
                });
                IntervalObservable.create((res.expires_in - 15) * 1000).subscribe(() => {
                    if (this.isLoggedIn()) {
                        this.usedRefreshToken();
                    }
                });
            }
        });
    }

    getLoggedInTime() {
        return this.loggedInTime;
    }

    idleConfig() {
        this.idle.setIdle(this.idleTime);
        // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
        this.idle.setTimeout(this.timeoutTime);

        // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
        this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

        this.idle.onIdleEnd.subscribe(() => this.idleState = 'No longer idle.');
        this.idle.onTimeout.subscribe(() => {
        });
        this.idle.onIdleStart.subscribe(() => this.idleState = 'You\'ve gone idle!');
        this.idle.onTimeoutWarning.subscribe((countdown) => {
            this.idleState = 'You will time out in ' + (countdown - 1) + ' seconds!';
            this.showWarning.next(countdown);
            if (countdown === 1) {
                this.timedOut = 'Your session has been expired !!! Please log in again.';
                this.logout();
            }
        });

        // sets the ping interval to 15 seconds
        this.keepAlive.interval(15);
        this.keepAlive.start();
        this.keepAlive.onPing.subscribe(() => {
        });
    }

    getToken(): string {
        return sessionStorage.getItem('userToken');
    }

    usedRefreshToken() {
        const data = new FormData();
        data.append('grant_type', 'refresh_token');
        data.append('refresh_token', sessionStorage.getItem('refresh_token'));
        const xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open('POST', this.uriService.getAuthServerUri() + '/oauth/token');
        xhr.setRequestHeader('Authorization', 'Basic ' + btoa('USER_CLIENT_APP:password'));
        xhr.send(data);
        xhr.onreadystatechange = () => {
            if (xhr.response !== '') {
                const newToken = xhr.response;
                sessionStorage.setItem('currentUser', newToken);
                const response = JSON.parse(newToken);
                sessionStorage.setItem('userToken', response.access_token);
                sessionStorage.setItem('refresh_token', response.refresh_token);
            }
        };
    }

    isLoggedIn() {
        return this.currentUserDetailsView !== null;
    }

    logout() {
        this.isAuthenticatedEvent.next(false);
        this.currentUserDetailsView = undefined;
        this.relogSub.unsubscribe();
        sessionStorage.clear();
        this.idle.stop();
        this.router.navigate(['/login']);
    }
}
