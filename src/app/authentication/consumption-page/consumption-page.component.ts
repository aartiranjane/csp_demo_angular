import {Component, HostListener, OnInit} from '@angular/core';
import {PerfectScrollbarConfigInterface} from "ngx-perfect-scrollbar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-consumption-page',
  templateUrl: './consumption-page.component.html',
  styleUrls: ['./consumption-page.component.css']
})
export class ConsumptionPageComponent implements OnInit  {
 /* public config: PerfectScrollbarConfigInterface = {};*/
  finalProList:any= [];
  isConsumption1 = 'active enabled';
  isConsumption2 = '';
  isConsumption3 = 'disabled';
  showTab = 1;
  blinkLogicalTab ={
    'pulse2' : true
  }
  blinkGrossTab ={
    'pulse2' : false
  }


  constructor(public router: Router) { }

  tabStatus = 'justified';

  public isCollapsed = false;

  public innerWidth: any;
  public defaultSidebar: any;
  public showSettings = false;
  public showMobileMenu = false;
  public expandLogo = false;

  options = {
    theme: 'light', // two possible values: light, dark
    dir: 'ltr', // two possible values: ltr, rtl
    layout: 'horizontal', // two possible values: vertical, horizontal
    sidebartype: 'full', // four possible values: full, iconbar, overlay, mini-sidebar
    sidebarpos: 'fixed', // two possible values: fixed, absolute
    headerpos: 'fixed', // two possible values: fixed, absolute
    boxed: 'full', // two possible values: full, boxed
    navbarbg: 'skin1', // six possible values: skin(1/2/3/4/5/6)
    sidebarbg: 'skin1', // six possible values: skin(1/2/3/4/5/6)
    logobg: 'skin6' // six possible values: skin(1/2/3/4/5/6)
  };

  Logo() {
    this.expandLogo = !this.expandLogo;
  }

  ngOnInit() {
    if (this.router.url === '/') {
      this.router.navigate(['/dashboard/dashboard1']);
    }
    this.defaultSidebar = this.options.sidebartype;
    this.handleSidebar();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: string) {
    this.handleSidebar();
  }

  handleSidebar() {
    this.innerWidth = window.innerWidth;
    switch (this.defaultSidebar) {
      case 'full':
      case 'iconbar':
        if (this.innerWidth < 1170) {
          this.options.sidebartype = 'mini-sidebar';
        } else {
          this.options.sidebartype = this.defaultSidebar;
        }
        break;

      case 'overlay':
        if (this.innerWidth < 767) {
          this.options.sidebartype = 'mini-sidebar';
        } else {
          this.options.sidebartype = this.defaultSidebar;
        }
        break;

      default:
    }
  }

  toggleSidebarType() {
    switch (this.options.sidebartype) {
      case 'full':
      case 'iconbar':
        this.options.sidebartype = 'mini-sidebar';
        break;

      case 'overlay':
        this.showMobileMenu = !this.showMobileMenu;
        break;

      case 'mini-sidebar':
        if (this.defaultSidebar === 'mini-sidebar') {
          this.options.sidebartype = 'full';
        } else {
          this.options.sidebartype = this.defaultSidebar;
        }
        break;

      default:
    }
  }
  tab(index:any){
    if(index == 1){
      this.isConsumption1 = 'active enabled';
      this.isConsumption2 = '';
      this.isConsumption3 = '';
      this.showTab = 1;

    }
    else if(index ==  2){
      this.isConsumption2 = 'active enabled';
      this.isConsumption1 = '';
      this.isConsumption3 = '';
      this.blinkLogicalTab.pulse2 = false;
      this.blinkGrossTab.pulse2 = true;
      this.showTab = 2;

    }
    else if(index ==  3){
      this.isConsumption3 = 'active enabled';
      this.isConsumption1 = '';
      this.isConsumption2 = '';
      this.blinkGrossTab.pulse2 = false;
      this.showTab = 3;

    }

  }
}
