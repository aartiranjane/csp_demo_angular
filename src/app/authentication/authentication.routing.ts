import { Routes } from '@angular/router';
import { NotfoundComponent } from './404/not-found.component';
import {ConsumptionPageComponent} from "./consumption-page/consumption-page.component";

export const AuthenticationRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '404',
        component: NotfoundComponent
      },
      {
        path: 'consumption',
        component: ConsumptionPageComponent
      }
    ]
  }
];
