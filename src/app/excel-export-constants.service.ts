import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ExcelExportConstantsService {

  constructor() {
  }

  pmMpsPlan: string = "MPS";

  pmLogicalConsumption: string = "PM Logical (FG based) Consumption";
  pmGrossConsumption: string = "PM Gross (PM based) Consumption";
  pmMesStock: string = "PM Midnight 'A+Q' Stock";
  pmMesRejectionStock: string = 'PM Midnight Rejection Stock';
  pmReceiptStock: string = 'PM Receipts';
  pmPurchasePlan: string = "PM Purchase Plan";
  pmMoudWiseMouldSaturation: string = "PM Mould Wise Mould Saturation";
  pmSupplierWiseMouldSaturation: string = "PM Supplier Wise Mould Saturation";
  pmSlob: string = "PM SLOB";
  pmSlobSummary: string = "PM SLOB Summary";
  pmCoverDays: string = "PM Cover Days";
  pmPurchaseCoverdays: string = "PM Purchase Cover Days";
  pmPurchaseStockEquation: string = "PM Purchase Stock Equation";
  pmCoverDaysSummary: string = "PM Cover Days Summary";
  pmMasterDataMould = "PM Mould Master Data";
  pmMaterialMasterData = "PM Material Master Data";
  pmSupplierMasterData = "PM Supplier Master Data";
  pmOtifSummary = "PM Otif Summary ";
  pmMaterialCategoryModal = "PM Material Category ";
  pmMpsTriangleAnalysis = "MPS Triangle Analysis ";
  pmInventoryQualityIndex = "PM Inventory Quality Index ";
  pmSlobMovement = "PM Slob Movement ";

  rmLogicalConsumption: string = "RM Logical (Bulk based) Consumption";
  rmGrossConsumption: string = "RM Gross (RM based) Consumption";
  rmMesStock: string = "RM Midnight 'A+Q' Stock";
  rmMesRejectionStock: string = 'RM Midnight Rejection Stock';
  rmReceiptStock: string = 'RM Receipts';
  rmBulkOpRmStock: string = 'RM Bulk OP RM Stock';
  rmTransferStock: string = 'RM Transfer Stock';
  rmRejectionStock: string = 'RM Rejection Stock';
  rmPurchasePlan: string = "RM Purchase Plan";
  rmBulkLogicalConsumption: string = "Bulk Logical (FG based) Consumption";
  rmBulkGrossConsumption: string = "Bulk Gross (Bulk based) Consumption";
  rmSlobExcel: string = "RM SLOB";
  rmSlobCoverDaysSummary: string = "RM Cover Days Summary";
  rmCoverDaysExcel: string = "RM Cover Days";
  rmPurchaseCoverdays: string = "RM Purchase Cover Days";
  rmPurchaseStockEquation: string = "RM Purchase Stock Equation";
  rmSLOBSummary: string = "RM SLOB Summary";
  rmCoverDaysSummary: string = "RM Cover Days Summary";
  rmPivotConsumption: string = "Rm Pivot (RM based) Consumption";
  rmMaterialMasterData = "RM Material Master Data";
  rmBulkMaterialMasters = "RM Bulk Material Master Data";
  rmBaseSubBaseMaterialMasters = "RM Base SubBase Material Master Data";
  rmSupplierMasterData = "RM Supplier Master Data";
  rmMpsTriangleAnalysis = "MPS Triangle Analysis ";
  rmInventoryQualityIndex = "RM Inventory Quality Index ";
  rmSlobMovement = "RM Slob Movement ";

  fgLineSaturation: string = "FG Line Saturation";
  fgSkidSaturation: string = "FG Skid Saturation";
  fgSlobSaturation: string = "FG SLOB";
  fgSlobSummary: string = "FG SLOB Summary";
  fgMasterDataSkid: string = "FG Skid Master Data";
  fgMasterDataLine = "FG Line Master Data";
  fgSFWIPMaterialsMasters = "SF WIP Materials Masters Data";
  fgMaterialMasters = "FG Material Master Data";
  fgMpsTriangleAnalysis = "MPS Triangle Analysis ";
  fgInventoryQualityIndex = "FG Inventory Quality Index ";
  fgSlobMovement = "FG Slob Movement ";
}
