import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {PartyService} from '../party.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
//import {OrganizationData} from '../party-type-organization-list/party-type-organization-list.component';

export interface UserData {
    firstName: any;
    lastName: any;
    options: any;
}

export interface OrganizationData {
    organizationName: any;
    relation: any;
    serviceType: any;
    options: any;
}

@Component({
    selector: 'app-party-type-user-list',
    templateUrl: './party-type-user-list.component.html',
    styleUrls: ['./party-type-user-list.component.css']
})
export class PartyTypeUserListComponent implements OnInit, OnDestroy {
    /*Data table Org related code*/
    displayedColumnsOrg: string[] = ['options', 'organizationName'];
    dataSourceOrg: MatTableDataSource<OrganizationData>;

    /*end of Data table related code*/
    /*Data user table related code*/
    displayedColumns: string[] = ['firstName', 'lastName', 'options'];
    dataSource: MatTableDataSource<UserData>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    /*end of Data table user related code*/
    orgList = [];
    userList = [];
    currentOrgForUser: any;
    successMessage = '';
    errorMessage = '';
    userIdToReset: any;

    constructor(private partyService: PartyService, private router: Router,
                private route: ActivatedRoute, private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.spinnerService.show();
        this.route.fragment.subscribe((res: any) => {
            if (res !== undefined && res === 'true') {
                this.successMessage = 'User saved successfully';
            }
        });
        /***Data table Organization initialization code***/

        this.partyService.partyTypeOrganizationListChanged.subscribe((res: any) => {
            this.spinnerService.hide();
            this.orgList = res;
            this.partyService.partyTypeOrganizationList = res;
            const OrganizationsView: OrganizationData[] = [];
            res.forEach((organization) => {
                OrganizationsView.push({
                    organizationName: organization.party.partyName.organizationName,
                    relation: organization.partyRelationship.relationshipType.name,
                    serviceType: organization.party.partyTypeOrganization.serviceType,
                    options: organization
                });
            });
            this.dataSourceOrg.data = [];
            this.dataSourceOrg.data = OrganizationsView;
            this.partyService.currentOrgIdForUser = this.dataSourceOrg.filteredData[0].options.party.id;
            this.usersList(this.dataSourceOrg.filteredData[0].options.party.id);
            /*  this.paginator._changePageSize(this.paginator.pageSize);*/
        }, (error) => {
            console.log(error);
        });
        /***Data table Organization initialization code***/

        this.partyService.getPartyTypeOrganizationList().switchMap((res: any) => {
            this.spinnerService.hide();
            this.orgList = res;
            this.partyService.partyTypeOrganizationList = res;
            const OrganizationsView: OrganizationData[] = [];
            res.forEach((organization) => {
                OrganizationsView.push({
                    organizationName: organization.party.partyName !== undefined && organization.party.partyName !== null
                        ? organization.party.partyName.organizationName : '',
                    relation: organization.partyRelationship !== undefined && organization.partyRelationship !== null &&
                    organization.partyRelationship.relationshipType !== undefined &&
                    organization.partyRelationship.relationshipType !== null
                        ? organization.partyRelationship.relationshipType.name : '',
                    serviceType: organization.party.partyTypeOrganization !== undefined && organization.party.partyTypeOrganization !== null
                        ? organization.party.partyTypeOrganization.serviceType : '',
                    options: organization
                });
            });
            this.dataSourceOrg = new MatTableDataSource(OrganizationsView);
            this.usersList(this.dataSourceOrg.filteredData[0].options.party.id);
            this.partyService.currentOrgIdForUser = this.dataSourceOrg.filteredData[0].options.party.id;
            return this.partyService.getPartyById(this.partyService.currentOrgIdForUser);
        }).switchMap((res: any) => {
            this.currentOrgForUser = res;
            return this.partyService.getOrganizationWisePartyTypeUserList(this.partyService.currentOrgIdForUser);
        }).subscribe((res: any) => {
            this.spinnerService.hide();
            this.userList = res;
            this.partyService.partyTypeUserList = res;
            const usersView: UserData[] = [];
            res.forEach((user) => {
                usersView.push({
                    firstName: user.partyTypeUser !== undefined ? user.partyTypeUser.firstName : '',
                    lastName: user.partyTypeUser !== undefined ? user.partyTypeUser.lastName : '',
                    options: user
                });
            });
            this.dataSource = new MatTableDataSource(usersView);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        }, (error) => {
            console.log(error);
        });
        this.partyService.partyTypeUserListChanged.subscribe((res: any) => {
            this.spinnerService.hide();
            this.userList = res;
            this.partyService.partyTypeUserList = res;
            const usersView: UserData[] = [];
            res.forEach((user) => {
                usersView.push({
                    firstName: user.partyTypeUser !== undefined ? user.partyTypeUser.firstName : '',
                    lastName: user.partyTypeUser !== undefined ? user.partyTypeUser.lastName : '',
                    options: user
                });
            });
            this.dataSource.data = [];
            this.dataSource.data = usersView;
            //this.paginator._changePageSize(this.paginator.pageSize);
        }, (error) => {
            console.log(error);
        });
        /***Data table initialization code***/
        //this.addUserorg(this.dataSourceOrg.filteredData[0].options.party.id);

    }

    usersList(id) {
        this.spinnerService.show();
        this.partyService.currentOrgIdForUser = id;
        this.partyService.getPartyById(id)
            .switchMap((res: any) => {
                this.currentOrgForUser = res;
                console.log(this.currentOrgForUser);
                //this.partyService.currentOrgIdForUser = this.dataSourceOrg.filteredData[0].options.id;
                return this.partyService.getOrganizationWisePartyTypeUserList(this.currentOrgForUser.id);
            }).subscribe((res: any) => {
            this.spinnerService.hide();
            this.userList = res;
            this.partyService.partyTypeUserList = res;
            const usersView: UserData[] = [];
            res.forEach((user) => {
                usersView.push({
                    firstName: user.partyTypeUser !== undefined ? user.partyTypeUser.firstName : '',
                    lastName: user.partyTypeUser !== undefined ? user.partyTypeUser.lastName : '',
                    options: user
                });
            });
            this.dataSource = new MatTableDataSource(usersView);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.spinnerService.hide();
        }, (error) => {
            console.log(error);
        });
    }

    userPage() {
        this.router.navigate(['userList/user/new']);
    }

    ngOnDestroy() {
        this.partyService.currentOrgIdForUser = undefined;
        console.log(this.partyService.currentOrgIdForUser);
    }

    setUserIdToReset(id: any) {
        this.userIdToReset = id;
    }


    addUserorg(partyId: number) {
        this.partyService.currentOrgIdForUser = partyId;
        console.log(this.partyService.currentOrgIdForUser);
        this.router.navigate(['userList/user/new']);
    }

    addUser() {
        this.router.navigate(['user/new'], {relativeTo: this.route});
    }

    editUser(partyId: any) {
        this.router.navigate(['userList/user/' + partyId]);
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    applyFilterorg(filterValue: string) {
        this.dataSourceOrg.filter = filterValue.trim().toLowerCase();
    }

    backButton() {
        this.router.navigate(['organizationList']);
    }

    resetPassword(id: any) {
        this.spinnerService.show();
        this.partyService.recoverPasswordByAdmin(id).subscribe((res: any) => {
            console.log(res);
            if (res.status === 'RECOVERED') {
                this.successMessage = 'New Credentialshas been send to registered email Id';
            } else {
                this.errorMessage = 'Unable to resend the password';
            }
            this.spinnerService.hide();
        }, () => {
            this.errorMessage = 'Error while resetting the password';
            this.spinnerService.hide();
        });
    }
}
