import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PartyService} from '../../party.service';
import {BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import {MastersService} from '../../../shared/masters.service';
import {of} from 'rxjs';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

@Component({
    selector: 'app-party-type-user',
    templateUrl: './party-type-user.component.html',
    styleUrls: ['./party-type-user.component.css']
})
export class PartyTypeUserComponent implements OnInit {
    submitButtonClicked = false;
    currentUsername: any;
    partyForm: FormGroup;
    partyRelationshipForm: FormGroup;
    cityList = [];
    roleList = [];
    selectedRole = [];
    currentPartyTypeUserId: any;
    currentPartyTypeUser: any;
    secondPartyId: any;
    currentOrgForUser: any;
    singleSelectDropdownSettings: any;
    disabledSingleSelectDropdownSettings: any;
    public dpConfig: Partial<BsDatepickerConfig> = new BsDatepickerConfig();
    isUsernameExists = false;
    isValidmobileNumber = false;
    isValidAlternameMobileNumber = true;
    isValidLandlineNumber = true;
    isValidAlternateLandlineNumber = true;
    isFaxNumberValid = true;
    isAlternateFaxNumberValid = true;

    constructor(private partyService: PartyService,
                private mastersService: MastersService,
                private route: ActivatedRoute,
                private router: Router, private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.spinnerService.show();
        this.singleSelectDropdownSettings = {
            text: 'Select',
            enableSearchFilter: true,
            showCheckbox: false,
            singleSelection: true,
            enableFilterSelectAll: false,
            classes: 'myclass custom-class'
        };
        this.disabledSingleSelectDropdownSettings = {
            text: 'Select',
            enableSearchFilter: true,
            showCheckbox: false,
            singleSelection: true,
            enableFilterSelectAll: false,
            disabled: true,
            classes: 'myclass custom-class'
        };
        this.currentUsername = sessionStorage.getItem('username');
        this.secondPartyId = this.partyService.currentOrgIdForUser;
        this.route.params.switchMap((params) => {
            console.log(params);
            if (params['userId'] !== undefined && params['userId'] !== 'new') {
                this.currentPartyTypeUserId = params['userId'];
            }
            this.initAllForms();
            if (this.partyService.currentOrgIdForUser !== null && this.partyService.currentOrgIdForUser !== undefined) {
                return this.partyService.getPartyById(this.partyService.currentOrgIdForUser);
            }
            return of(null);
        }).switchMap((res) => {
            if (res !== null && res !== undefined) {
                this.currentOrgForUser = res;
            }
            return this.mastersService.getRoleList();
        }).switchMap((res: any) => {
            if (res !== null && res !== undefined && res.length !== 0) {
                this.roleList = [];
                res.forEach((role) => {
                    this.roleList.push({id: role.id, itemName: role.name});
                });
            }
            return this.mastersService.getCityList();
        }).switchMap((res: any) => {
            if (res.length !== 0) {
                this.cityList = res;
            }
            this.initAllForms();
            if (this.currentPartyTypeUserId !== undefined) {
                return this.partyService.getPartyTypeUserView(+this.currentPartyTypeUserId);
            } else {
                this.spinnerService.hide();
                return of();
            }
        }).subscribe((res: any) => {
            this.spinnerService.hide();
            if (res !== null && res !== undefined && res.id !== null) {
                this.currentPartyTypeUser = res;
                this.editUser(res);
            }
        }, (error) => {
            this.spinnerService.hide();
            console.log(error);
        });
    }

    private initAllForms() {
        this.partyForm = new FormGroup({
            id: new FormControl(null),
            type: new FormControl('USER'),
            createdAt: new FormControl(new Date()),
            createdBy: new FormControl(this.currentUsername),
            updatedAt: new FormControl(null),
            modifiedBy: new FormControl(null),
            partyTypeUser: new FormGroup({
                id: new FormControl(null),
                username: new FormControl('', Validators.required),
                password: new FormControl(''),
                firstName: new FormControl('', Validators.required),
                middleName: new FormControl(''),
                lastName: new FormControl('', Validators.required),
                enabled: new FormControl(true),
                createdAt: new FormControl(new Date()),
                updatedAt: new FormControl(null),
                modifiedBy: new FormControl(null)
            }),
            partyName: new FormGroup({
                id: new FormControl(null),
                party: new FormControl(null),
                salutation: new FormControl(''),
                firstName: new FormControl(''),
                lastName: new FormControl(''),
                createdAt: new FormControl(new Date()),
                updatedAt: new FormControl(null),
                modifiedBy: new FormControl(null)
            }),
            partyContactDetails: new FormGroup({
                id: new FormControl(null),
                party: new FormControl(null),
                mobileNoOne: new FormControl('', Validators.required),
                mobileNoTwo: new FormControl(''),
                landLineOne: new FormControl(''),
                landLineTwo: new FormControl(''),
                faxOne: new FormControl(''),
                faxTwo: new FormControl(''),
                emailIdOne: new FormControl('', [Validators.required, Validators.email]),
                emailIdTwo: new FormControl('', Validators.email),
                createdAt: new FormControl(new Date()),
                updatedAt: new FormControl(null),
                modifiedBy: new FormControl(null)
            }),
            partyAddressDetails: new FormArray([]),
            roles: new FormControl([])
        });
        const partyAddressDetailsForm = new FormGroup({
            party: new FormControl(''),
            addressType: new FormControl('OFFICE'),
            addressLineOne: new FormControl('', Validators.required),
            addressLineTwo: new FormControl(''),
            area: new FormControl(''),
            pinCode: new FormControl(''),
            city: new FormControl(null),
            createdAt: new FormControl(new Date())
        });
        (<FormArray>this.partyForm.get('partyAddressDetails')).push(partyAddressDetailsForm);
        this.partyRelationshipForm = new FormGroup({
            id: new FormControl(null),
            firstParty: new FormControl(''),
            secondParty: new FormControl(''),
            relationshipType: new FormControl(null),
            beginDate: new FormControl(new Date()),
            endDate: new FormControl(''),
            createdAt: new FormControl(new Date()),
            updatedAt: new FormControl(null),
            modifiedBy: new FormControl(null)
        });
    }

    editUser(userView: any) {
        this.isValidmobileNumber = true;
        this.partyForm.reset();
        this.partyRelationshipForm.reset();
        this.partyForm.setControl('partyAddressDetails', new FormArray([]));
        this.partyForm.patchValue({
            id: userView.party.id,
            type: userView.party.type,
            createdAt: userView.party.createdAt,
            createdBy: userView.party.createdBy,
            updatedAt: new Date(),
            modifiedBy: this.currentUsername,
            partyTypeUser: {
                id: userView.party.partyTypeUser.id,
                username: userView.party.partyTypeUser.username,
                password: userView.party.partyTypeUser.password,
                firstName: userView.party.partyTypeUser.firstName,
                middleName: userView.party.partyTypeUser.middleName,
                lastName: userView.party.partyTypeUser.lastName,
                enabled: userView.party.partyTypeUser.enabled,
                createdAt: userView.party.partyTypeUser.createdAt,
                updatedAt: new Date(),
                modifiedBy: this.currentUsername,
            },
            partyName: {
                id: userView.party.partyName.id,
                salutation: userView.party.partyName.salutation,
                firstName: userView.party.partyName.firstName,
                lastName: userView.party.partyName.lastName,
                createdAt: userView.party.partyName.createdAt,
                updatedAt: new Date(),
                modifiedBy: this.currentUsername,
            },
            partyContactDetails: {
                id: userView.party.partyContactDetails.id,
                mobileNoOne: userView.party.partyContactDetails.mobileNoOne,
                mobileNoTwo: userView.party.partyContactDetails.mobileNoTwo,
                landLineOne: userView.party.partyContactDetails.landLineOne,
                landLineTwo: userView.party.partyContactDetails.landLineTwo,
                faxOne: userView.party.partyContactDetails.faxOne,
                faxTwo: userView.party.partyContactDetails.faxTwo,
                emailIdOne: userView.party.partyContactDetails.emailIdOne,
                emailIdTwo: userView.party.partyContactDetails.emailIdTwo,
                createdAt: userView.party.partyContactDetails.createdAt,
                updatedAt: new Date(),
                modifiedBy: this.currentUsername,
            }
        });
        for (let i = 0; i < userView.party.partyAddressDetails.length; i++) {
            const partyAddressDetailsForm = new FormGroup({
                id: new FormControl(userView.party.partyAddressDetails[i].id),
                party: new FormControl(userView.party.partyAddressDetails[i].party),
                addressType: new FormControl(userView.party.partyAddressDetails[i].addressType),
                addressLineOne: new FormControl(userView.party.partyAddressDetails[i].addressLineOne, Validators.required),
                addressLineTwo: new FormControl(userView.party.partyAddressDetails[i].addressLineTwo),
                area: new FormControl(userView.party.partyAddressDetails[i].area),
                pinCode: new FormControl(userView.party.partyAddressDetails[i].pinCode),
                city: new FormControl(userView.party.partyAddressDetails[i].city !== undefined &&
                userView.party.partyAddressDetails[i].city !== null ? userView.party.partyAddressDetails[i].city.id : null),
                createdAt: new FormControl(userView.party.partyAddressDetails[i].createdAt),
                updatedAt: new FormControl(new Date()),
                modifiedBy: new FormControl(this.currentUsername)
            });
            (<FormArray>this.partyForm.get('partyAddressDetails')).push(partyAddressDetailsForm);
        }
        this.partyRelationshipForm.patchValue({
            id: userView.partyRelationship.id,
            firstParty: userView.partyRelationship.firstParty,
            secondParty: userView.partyRelationship.secondParty,
            relationshipType: userView.partyRelationship.relationshipType,
            beginDate: new Date(userView.partyRelationship.beginDate),
            endDate: new Date(userView.partyRelationship.endDate),
            createdAt: new Date(userView.partyRelationship.createdAt),
            updatedAt: new Date(),
            modifiedBy: this.currentUsername
        });
        this.selectedRole = [];
        if (this.currentOrgForUser.partyTypeOrganization !== undefined
            && this.currentOrgForUser.partyTypeOrganization !== null && !this.currentOrgForUser.partyTypeOrganization.service) {
            if (userView != null && userView.party != null && userView.party.roles[0] !== undefined && userView.party.roles[0] !== null) {
                this.selectedRole.push(this.roleList.find(role => role.id === userView.party.roles[0].id));
            }
        }
    }


    usernameValidator(event: any) {
        this.partyService.getPartyByUsername(event.target.value).subscribe((res) => {
            this.isUsernameExists = res !== null;
        }, (error) => {
            console.log(error);
        });
    }

    addUser() {
        this.spinnerService.show();
        this.secondPartyId = this.partyService.currentOrgIdForUser;
        if (this.selectedRole !== null && this.selectedRole.length !== 0) {
            const roles = [];
            roles.push({id: this.selectedRole[0].id});
            this.partyForm.value.roles = roles;
        } else {
            this.partyForm.value.roles = [];
        }
        for (let i = 0; i < this.partyForm.value.partyAddressDetails.length; i++) {
            if (this.partyForm.value.partyAddressDetails[i].city !== null && this.partyForm.value.partyAddressDetails[i].city !== undefined) {
                const cityTobeSaved = {
                    id: this.partyForm.value.partyAddressDetails[i].city
                };
                this.partyForm.value.partyAddressDetails[i].city = cityTobeSaved;
            }
        }
        if (this.currentPartyTypeUserId !== undefined) {
            this.partyService.editPartyWithUserType(this.partyForm.value)
                .switchMap((res: any) => this.partyService.editPartyRelationship(this.partyRelationshipForm.value))
                .switchMap((res: any) => {
                    this.spinnerService.hide();
                    //this.router.navigate(['../../'], {relativeTo: this.route, fragment: 'true'});
                    this.partyForm.reset();
                    this.router.navigate(['userList/user/new']);
                    return this.partyService.getOrganizationWisePartyTypeUserList(this.secondPartyId);
                }).subscribe((res: any) => {
                this.partyService.partyTypeUserListChanged.next(res);
            }, (error) => {
                console.log(error);
            });
        } else {
            this.partyService.addPartyWithUserType(this.partyForm.value)
                .switchMap((res: any) => {
                    this.partyRelationshipForm.value.firstParty = {
                        id: res.id
                    };
                    this.partyRelationshipForm.value.secondParty = {
                        id: this.secondPartyId
                    };
                    return this.partyService.addPartyRelationship(this.partyRelationshipForm.value);
                }).switchMap((res: any) => {
                this.spinnerService.hide();
                this.partyForm.reset();
                //this.router.navigate(['../../'], {relativeTo: this.route, fragment: 'true'});
                this.router.navigate(['userList/user/new']);
                return this.partyService.getOrganizationWisePartyTypeUserList(this.secondPartyId);
            }).subscribe((res: any) => {
                this.partyService.partyTypeUserListChanged.next(res);
            }, (error) => {
                console.log(error);
            });
        }
    }

    mobileNumberValidation(mobileNumber: any) {
        if (isNaN(mobileNumber)) {
            this.isValidmobileNumber = false;
        } else {
            this.isValidmobileNumber = true;
        }
    }

    alternateMobileNumberValidation(alternameMobileNumber: any) {
        if (isNaN(alternameMobileNumber)) {
            this.isValidAlternameMobileNumber = false;
        } else {
            this.isValidAlternameMobileNumber = true;
        }
    }

    landlineNumberValidation(landlineNumber: any) {
        if (isNaN(landlineNumber)) {
            this.isValidLandlineNumber = false;
        } else {
            this.isValidLandlineNumber = true;
        }
    }

    alternateLandlineNumberValidation(alternameLandlineNumber: any) {
        if (isNaN(alternameLandlineNumber)) {
            this.isValidAlternateLandlineNumber = false;
        } else {
            this.isValidAlternameMobileNumber = true;
        }
    }

    faxNumberValidation(faxNumber: any) {
        if (isNaN(faxNumber)) {
            this.isFaxNumberValid = false;
        } else {
            this.isFaxNumberValid = true;
        }
    }

    alternateFaxNumberValidaiton(alternateFaxNumber: any) {
        if (isNaN(alternateFaxNumber)) {
            this.isAlternateFaxNumberValid = false;
        } else {
            this.isAlternateFaxNumberValid = true;
        }
    }
}
