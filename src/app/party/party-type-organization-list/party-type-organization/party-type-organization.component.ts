import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validator, Validators} from '@angular/forms';
import {BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import {PartyService} from '../../party.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PartyRelationshipTypeService} from '../../../masters/party-relationship-type.service';
import {EnumsService} from '../../../shared/enums.service';
import {MastersService} from '../../../shared/masters.service';
import {of} from 'rxjs';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

@Component({
    selector: 'app-party-type-organization',
    templateUrl: './party-type-organization.component.html',
    styleUrls: ['./party-type-organization.component.css']
})
export class PartyTypeOrganizationComponent implements OnInit {
    submitButtonClicked = false;
    currentUsername: any;
    partyForm: FormGroup;
    partyRelationshipForm: FormGroup;
    partyContactDetails: FormGroup;
    cityList = [];
    currentOrganization: any;
    currentOrgId: any;
    secondPartyList = [];
    serviceTypeList = [];
    selectedSecondParty = [];
    selectedServiceType = [];
    singleSelectDropdownSettings: any;
    config: any;
    isCheckboxChecked = false;
    errorMessage = '';
    public dpConfig: Partial<BsDatepickerConfig> = new BsDatepickerConfig();
    isSupplier = false;
    isValidmobileNumber = true;
    isValidAlternateMobileNumber = true;
    isValidLandlineNumber = true;
    isValidAlternateLandlineNumber = true;
    isFaxNumberValid = true;
    isAlternateFaxNumberValid = true;
    email='/^[a-z0-9!#$%&*+.\/=?^_`{|}~.~]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i';

    constructor(private partyService: PartyService, private mastersService: MastersService, private route: ActivatedRoute,
                private partyRelationshipTypeService: PartyRelationshipTypeService, private enumsService: EnumsService,
                private router: Router, private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.spinnerService.show();
        this.currentUsername = sessionStorage.getItem('username');
        this.config = {
            displayKey: 'name',
            height: '200px',
            placeholder: 'Select',
            search: true,
            limitTo: 10
        };
        this.singleSelectDropdownSettings = {
            text: 'Select',
            enableSearchFilter: true,
            showCheckbox: false,
            singleSelection: true,
            enableFilterSelectAll: false,
            classes: 'myclass custom-class'
        };
        this.route.params.switchMap((params) => {
            if (params['partyId'] !== undefined && params['partyId'] !== 'new') {
                this.currentOrgId = params['partyId'];
            }
            return this.mastersService.getCityList();
        }).switchMap((res: any) => {
            if (res.length !== 0) {
                this.cityList = res;
            }
            return this.enumsService.getServiceTypes();
        }).switchMap((res: any) => {
            if (res.length !== 0) {
                res.forEach((serviceType, index) => {
                    this.serviceTypeList.push({id: index, itemName: serviceType});
                });
            }
            return this.partyService.getPartyTypeOrganizationListWithNoService();
        }).switchMap((res: any) => {
            if (res.length !== 0) {
                res.forEach((partyView) => {
                    this.secondPartyList.push({
                        id: partyView.party.id,
                        itemName: partyView.party.partyName.organizationName
                    });
                });
                if (this.secondPartyList.length === 1) {
                    this.selectedSecondParty = this.secondPartyList;
                }
            }
            this.initAllForms();
            if (this.currentOrgId !== undefined) {
                return this.partyService.getPartyTypeOrganizationView(this.currentOrgId);
            } else {
                return of(null);
            }
        }).subscribe((res: any) => {
            this.spinnerService.hide();
            if (res !== undefined && res !== null && res.id !== null) {
                this.currentOrganization = res;
                this.editOrganization(res);
            }
        }, (error) => {
            console.log(error);
        });
    }

    checkValidConditions() {
        if (!this.partyForm.valid || !this.isValidmobileNumber ||
            !this.isValidAlternateMobileNumber || !this.isFaxNumberValid || !this.isAlternateFaxNumberValid ||
            !this.isValidLandlineNumber || !this.isValidAlternateLandlineNumber) {
            return true;
        }
    }

    private initAllForms() {
        this.partyForm = new FormGroup({
            id: new FormControl(null),
            type: new FormControl('ORGANIZATION'),
            createdAt: new FormControl(new Date()),
            createdBy: new FormControl(this.currentUsername),
            partyTypeOrganization: new FormGroup({
                id: new FormControl(null),
                organizationType: new FormControl('ORGANIZATION', Validators.required),
                serviceType: new FormControl('', Validators.required),
                vendorCode: new FormControl(''),
                service: new FormControl(false),
                createdAt: new FormControl(new Date())
            }),
            partyName: new FormGroup({
                id: new FormControl(null),
                party: new FormControl(''),
                salutation: new FormControl(''),
                organizationName: new FormControl(''),
                createdAt: new FormControl(new Date())
            }),
            partyContactDetails: new FormGroup({
                id: new FormControl(null),
                party: new FormControl(''),
                mobileNoOne: new FormControl('', Validators.required),
                mobileNoTwo: new FormControl(''),
                landLineOne: new FormControl(''),
                landLineTwo: new FormControl(''),
                faxOne: new FormControl(''),
                faxTwo: new FormControl(''),
                emailIdOne: new FormControl('', [Validators.required]),
                emailIdTwo: new FormControl('', Validators.email),
                createdAt: new FormControl(new Date())
            }),
            partyAddressDetails: new FormArray([])
        });
        (<FormArray>this.partyForm.get('partyAddressDetails')).push(this.addPartyAddressDetailFormGroup('OFFICE'));
        this.partyRelationshipForm = new FormGroup({
            id: new FormControl(null),
            firstParty: new FormControl(''),
            secondParty: new FormControl(this.secondPartyList.length === 1 &&
            this.selectedSecondParty[0] !== undefined ? {id: this.selectedSecondParty[0].id} : null),
            relationshipType: new FormControl(null),
            beginDate: new FormControl(new Date()),
            endDate: new FormControl(''),
            createdAt: new FormControl(new Date())
        });
    }

    editOrganization(organizationView: any) {
        this.partyForm.reset();
        this.partyRelationshipForm.reset();
        this.partyForm.setControl('partyAddressDetails', new FormArray([]));
        this.partyForm.patchValue({
            id: organizationView.party.id,
            type: organizationView.party.type,
            createdAt: organizationView.party.createdAt,
            createdBy: organizationView.party.createdBy,
            partyTypeOrganization: {
                id: organizationView.party.partyTypeOrganization.id,
                organizationType: organizationView.party.partyTypeOrganization.organizationType,
                serviceType: organizationView.party.partyTypeOrganization.serviceType,
                vendorCode: organizationView.party.partyTypeOrganization.vendorCode,
                service: organizationView.party.partyTypeOrganization.service,
                createdAt: organizationView.party.partyTypeOrganization.createdAt
            },
            partyName: {
                id: organizationView.party.partyName.id,
                salutation: organizationView.party.partyName.salutation,
                organizationName: organizationView.party.partyName.organizationName,
                createdAt: organizationView.party.partyName.createdAt
            },
            partyContactDetails: {
                id: organizationView.party.partyContactDetails.id,
                mobileNoOne: organizationView.party.partyContactDetails.mobileNoOne,
                mobileNoTwo: organizationView.party.partyContactDetails.mobileNoTwo,
                landLineOne: organizationView.party.partyContactDetails.landLineOne,
                landLineTwo: organizationView.party.partyContactDetails.landLineTwo,
                faxOne: organizationView.party.partyContactDetails.faxOne,
                faxTwo: organizationView.party.partyContactDetails.faxTwo,
                emailIdOne: organizationView.party.partyContactDetails.emailIdOne,
                emailIdTwo: organizationView.party.partyContactDetails.emailIdTwo,
                createdAt: organizationView.party.partyContactDetails.createdAt
            }
        });
        for (let i = 0; i < organizationView.party.partyAddressDetails.length; i++) {
            (<FormArray>this.partyForm.get('partyAddressDetails')).push(this.editPartyAddressDetailFormGroup(organizationView.party.partyAddressDetails[i]));
        }
        this.partyRelationshipForm.patchValue({
            id: organizationView.partyRelationship.id,
            firstParty: organizationView.partyRelationship.firstParty,
            secondParty: organizationView.partyRelationship.secondParty,
            relationshipType: organizationView.partyRelationship.relationshipType,
            beginDate: new Date(organizationView.partyRelationship.beginDate),
            endDate: new Date(organizationView.partyRelationship.endDate),
            createdAt: new Date(organizationView.partyRelationship.createdAt)
        });
        this.selectedServiceType = [];
        this.selectedSecondParty = [];
        if (organizationView.party.partyTypeOrganization.service
            && organizationView.party.partyTypeOrganization.organizationType === 'ORGANIZATION') {
            var selectedServiceType = this.serviceTypeList.find(serviceType => serviceType.itemName === organizationView.party.partyTypeOrganization.serviceType);
            var selectedSecondParty = this.secondPartyList.find(secondParty => secondParty.id === organizationView.partyRelationship.secondParty.id);
            this.selectedServiceType.push(selectedServiceType);
            this.selectedSecondParty.push(selectedSecondParty);
        }
    }

    setCheckboxId(index): string {
        return 'custom' + index;
    }

    addPartyAddressDetailFormGroup(type: string): FormGroup {
        const partyAddressDetailsFormGroup = new FormGroup({
            party: new FormControl(''),
            addressType: new FormControl(type),
            addressLineOne: new FormControl('', Validators.required),
            addressLineTwo: new FormControl(''),
            area: new FormControl(''),
            pinCode: new FormControl(''),
            city: new FormControl(null),
            createdAt: new FormControl(new Date())
        });
        return partyAddressDetailsFormGroup;
    }

    editPartyAddressDetailFormGroup(addressDetails): FormGroup {
        const partyAddressDetailsFormGroup = new FormGroup({
            id: new FormControl(addressDetails.id),
            party: new FormControl(addressDetails.party),
            addressType: new FormControl(addressDetails.addressType),
            addressLineOne: new FormControl(addressDetails.addressLineOne, Validators.required),
            addressLineTwo: new FormControl(addressDetails.addressLineTwo),
            area: new FormControl(addressDetails.area),
            pinCode: new FormControl(addressDetails.pinCode),
            city: new FormControl(addressDetails.city.id),
            createdAt: new FormControl(addressDetails.createdAt)
        });
        return partyAddressDetailsFormGroup;
    }

    copyAddressIfChecked(index: any, isChecked: boolean) {
        const formArray = this.partyForm.get('partyAddressDetails') as FormArray;
        const tempAddress = formArray.at(index);
        if (isChecked) {
            this.isCheckboxChecked = true;
            for (const control of formArray.controls) {
                if (control instanceof FormGroup) {
                    if (control.get('addressType').value === 'OFFICE') {
                        tempAddress.patchValue({
                            addressLineOne: control.get('addressLineOne').value,
                            addressLineTwo: control.get('addressLineTwo').value,
                            area: control.get('area').value,
                            pinCode: control.get('pinCode').value,
                            city: control.get('city').value,
                        });
                        tempAddress.markAsTouched();
                        tempAddress.updateValueAndValidity();
                        tempAddress.disable();
                    }
                }
            }
        } else {
            this.isCheckboxChecked = false;
            tempAddress.enable();
            tempAddress.patchValue({
                addressLineOne: '',
                addressLineTwo: '',
                area: '',
                pinCode: '',
                city: null,
            });
            tempAddress.updateValueAndValidity();
        }
    }

    addOrganization() {
        this.spinnerService.show();
        console.log(this.partyForm.value);
        this.partyForm.value.partyTypeOrganization.service = this.selectedServiceType.length !== 0;
        if (this.selectedSecondParty.length !== 0 && this.secondPartyList.length > 1) {
            this.partyRelationshipForm.value.secondParty = {
                id: this.selectedSecondParty[0].id
            };
        }
        if (this.selectedServiceType.length !== 0) {
            this.partyForm.value.partyTypeOrganization.serviceType = this.selectedServiceType[0].itemName;
        }
        this.partyForm.value.partyAddressDetails = this.partyForm.getRawValue().partyAddressDetails;
        for (let i = 0; i < this.partyForm.value.partyAddressDetails.length; i++) {
            if (this.partyForm.value.partyAddressDetails[i].city !== null &&
                this.partyForm.value.partyAddressDetails[i].city !== undefined) {
                const city = {
                    id: this.partyForm.value.partyAddressDetails[i].city
                };
                this.partyForm.value.partyAddressDetails[i].city = city;
            }
        }
        if (this.currentOrgId !== undefined) {
            this.partyService.editPartyWithOrganizationType(this.partyForm.value)
                .switchMap((res: any) => this.partyService.editPartyRelationship(this.partyRelationshipForm.value))
                .switchMap((res: any) => {
                    return this.partyService.getPartyTypeOrganizationList();
                }).subscribe((res: any) => {
                this.spinnerService.hide();
                this.partyService.partyTypeOrganizationListChanged.next(res);
                this.router.navigate(['../../'], {relativeTo: this.route, fragment: 'true'});
            }, (error) => {
                console.log(error);
            });
        } else {
            this.partyService.addPartyWithOrganizationType(this.partyForm.value).switchMap((res: any) => {
                this.partyRelationshipForm.value.firstParty = {
                    id: res.id
                };
                return this.partyService.addPartyRelationship(this.partyRelationshipForm.value);
            }).switchMap((res: any) => {
                this.router.navigate(['../../'], {relativeTo: this.route, fragment: 'true'});
                this.spinnerService.hide();
                return this.partyService.getPartyTypeOrganizationList();
            }).subscribe((res: any) => {
                this.partyService.partyTypeOrganizationListChanged.next(res);
            }, (error) => {
                console.log(error);
            });
        }
    }

    vendorCodeValidationsOnSuplier(service: any) {
        this.isSupplier = false;
        if (service.itemName === 'SUP') {
            this.isSupplier = true;
            this.partyForm.get('partyTypeOrganization.vendorCode').setValidators([Validators.required]);
            this.partyForm.get('partyTypeOrganization.vendorCode').updateValueAndValidity();
        } else {
            this.partyForm.get('partyTypeOrganization.vendorCode').clearValidators();
            this.partyForm.get('partyTypeOrganization.vendorCode').updateValueAndValidity();
            this.isSupplier = false;
        }
    }

    mobileNumberValidation(mobileNumber: any) {

        if (isNaN(mobileNumber)) {
            this.isValidmobileNumber = false;
        } else {
            this.isValidmobileNumber = true;
        }
    }

    alternateMobileNumberValidation(alternateMobileNumber: any) {
        if (isNaN(alternateMobileNumber)) {
            this.isValidAlternateMobileNumber = false;
        } else {
            this.isValidAlternateMobileNumber = true;
        }
    }

    landlineNumberValidation(landlineNumber: any) {
        if (isNaN(landlineNumber)) {
            this.isValidLandlineNumber = false;
        } else {
            this.isValidLandlineNumber = true;
        }
    }

    alternateLandlineNumberValidation(alternateLandlineNumber: any) {
        if (isNaN(alternateLandlineNumber)) {
            this.isValidAlternateLandlineNumber = false;
        } else {
            this.isValidAlternateLandlineNumber = true;
        }
    }

    faxNumberValidation(faxNumber: any) {
        if (isNaN(faxNumber)) {
            this.isFaxNumberValid = false;
        } else {
            this.isFaxNumberValid = true;
        }
    }

    alternateFaxNumberValidaiton(alternateFaxNumber: any) {
        if (isNaN(alternateFaxNumber)) {
            this.isAlternateFaxNumberValid = false;
        } else {
            this.isAlternateFaxNumberValid = true;
        }
    }
}
