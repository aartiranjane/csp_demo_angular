import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {PartyService} from '../party.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

export interface OrganizationData {
    organizationName: any;
    relation: any;
    serviceType: any;
    options: any;
}

@Component({
    selector: 'app-party-type-organization-list',
    templateUrl: './party-type-organization-list.component.html',
    styleUrls: ['./party-type-organization-list.component.css']
})
export class PartyTypeOrganizationListComponent implements OnInit {

    orgEditCloseBtn = false;
    /*Data table related code*/
    displayedColumns: string[] = ['organizationName', 'relation', 'serviceType', 'options'];
    dataSource: MatTableDataSource<OrganizationData>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    /*end of Data table related code*/
    orgList = [];
    successMessage = '';
    errorMessage = '';
    selectedName: any;
    SelectionBgColor = 'white';


    constructor(private partyService: PartyService, private router: Router,
                private route: ActivatedRoute, private spinnerService: Ng4LoadingSpinnerService) {
    }

    ngOnInit() {
        this.spinnerService.show();
        this.route.fragment.subscribe((res: any) => {
            if (res !== undefined && res === 'true') {
                this.successMessage = 'Organization saved successfully';
                this.addOrganization();
                this.orgEditCloseBtn = false;
                this.SelectionBgColor = 'white';
            }
        });
        /***Data table initialization code***/
        this.partyService.getPartyTypeOrganizationList().subscribe((res: any) => {
            this.spinnerService.hide();
            this.orgList = res;
            this.partyService.partyTypeOrganizationList = res;
            const OrganizationsView: OrganizationData[] = [];
            res.forEach((organization) => {
                OrganizationsView.push({
                    organizationName: organization.party.partyName !== undefined && organization.party.partyName !== null
                        ? organization.party.partyName.organizationName : '',
                    relation: organization.partyRelationship !== undefined && organization.partyRelationship !== null &&
                    organization.partyRelationship.relationshipType !== undefined &&
                    organization.partyRelationship.relationshipType !== null
                        ? organization.partyRelationship.relationshipType.name : '',
                    serviceType: organization.party.partyTypeOrganization !== undefined && organization.party.partyTypeOrganization !== null
                        ? organization.party.partyTypeOrganization.serviceType : '',
                    options: organization
                });
            });
            this.dataSource = new MatTableDataSource(OrganizationsView);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        }, (error) => {
            console.log(error);
        });
        this.partyService.partyTypeOrganizationListChanged.subscribe((res: any) => {
            this.spinnerService.hide();
            this.orgList = res;
            this.partyService.partyTypeOrganizationList = res;
            const OrganizationsView: OrganizationData[] = [];
            res.forEach((organization) => {
                OrganizationsView.push({
                    organizationName: organization.party.partyName.organizationName,
                    relation: organization.partyRelationship.relationshipType.name,
                    serviceType: organization.party.partyTypeOrganization.serviceType,
                    options: organization
                });
            });
            this.dataSource.data = [];
            this.dataSource.data = OrganizationsView;
            this.paginator._changePageSize(this.paginator.pageSize);
        }, (error) => {
            console.log(error);
        });
        /***Data table initialization code***/
        this.addOrganization();

    }

    closeEditOrg() {

        this.SelectionBgColor = 'white';
        this.addOrganization();

        this.orgEditCloseBtn = false;
        this.highlightRow(-1);
    }

    public highlightRow(emp) {

        console.log(emp);
        console.log(emp);
        this.selectedName = emp;
    }

    userPage() {
        this.router.navigate(['userList/user/new']);
    }

    addOrganization() {
        this.router.navigate(['organization/new'], {relativeTo: this.route});
    }

    editOrganization(partyId: any) {
        this.SelectionBgColor = 'aliceblue';
        this.orgEditCloseBtn = true;
        this.router.navigate(['organization/' + partyId], {relativeTo: this.route});
    }

    addUser(partyId: number) {
        this.partyService.currentOrgIdForUser = partyId;
        this.router.navigate(['userList/user/new']);
    }

    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
}

