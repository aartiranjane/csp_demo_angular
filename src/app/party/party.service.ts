import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Subject, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {UriService} from '../uri.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable()
export class PartyService {
    partyTypeUserList = [];
    partyTypeUserListChanged = new Subject();
    partyTypeOrganizationList = [];
    partyTypeOrganizationListChanged = new Subject();
    currentOrgIdForUser: any;
    
    constructor(private http: HttpClient, private uriService: UriService) {
    }
    
    addPartyWithUserType(party: any) {
        return this.http.post(this.uriService.getResourceServerUri() + '/party/user',
            JSON.stringify(party), httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    editPartyWithUserType(party: any) {
        return this.http.put(this.uriService.getResourceServerUri() + '/party/user',
            JSON.stringify(party), httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getOrganizationWisePartyTypeUserList(organizationId: any) {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/user',
            {params: {organizationId: organizationId}}).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeUserView(id: any) {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/user/' + id,
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyByUsername(username: any) {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/username',
            {params: {username: username}}).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    addPartyWithOrganizationType(party: any) {
        return this.http.post(this.uriService.getResourceServerUri() + '/party/organization',
            JSON.stringify(party), httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    editPartyWithOrganizationType(party: any) {
        return this.http.put(this.uriService.getResourceServerUri() + '/party/organization',
            JSON.stringify(party), httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    addPartyRelationship(partyRelationship: any) {
        return this.http.post(this.uriService.getResourceServerUri() + '/partyRelationship',
            JSON.stringify(partyRelationship), httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    editPartyRelationship(partyRelationship: any) {
        return this.http.put(this.uriService.getResourceServerUri() + '/partyRelationship',
            JSON.stringify(partyRelationship), httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeOrganizationList() {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/organization',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeOrganizationView(id: any) {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/organization/' + id,
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeOrganizationListWithNoService() {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/organization/NoService',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeOrganizationListWithServiceTypeFF() {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/organization/ff',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeOrganizationListWithServiceTypeSUP() {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/organization/sup',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeOrganizationListWithServiceTypeCHA() {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/organization/cha',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeOrganizationListWithServiceTypeCBW() {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/organization/cbw',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyTypeOrganizationListWithServiceTypeLP() {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/organization/lp',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    getPartyById(partyId: any) {
        return this.http.get(this.uriService.getResourceServerUri() + '/party/' + partyId,
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    changeUserPassword(password: any) {
        return this.http.post(this.uriService.getResourceServerUri() + '/user/savePassword',
            JSON.stringify(password), httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    recoverPassword(password: any) {
        return this.http.post(this.uriService.getResourceServerUri() + '/forgetUsernamePassword',
            JSON.stringify(password), httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    recoverPasswordByAdmin(id: any) {
        return this.http.get(this.uriService.getResourceServerUri() + '/forgetUsernamePassword/' + id,
            httpOptions).pipe(
            map((response: Response) => response),
            catchError(this.errorHandler));
    }
    getApplicationOwner() {
        return this.http.get(this.uriService.getResourceServerUri() + '/orgnization-by-service',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    errorHandler(error: Response) {
        return throwError(error || 'Server Error');
    }
}
