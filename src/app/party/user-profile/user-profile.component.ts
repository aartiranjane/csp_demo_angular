import {Component, OnInit} from '@angular/core';
import {PartyService} from '../party.service';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
    private currentUsername = '';
    userForm: FormGroup;
    passwordForm: FormGroup;
    confirmPassword = '';
    editUser = 'view';
    editAddress = 'view';
    editContact = 'view';
    contactForm: FormGroup;
    userId = undefined;
    constructor(private partyService: PartyService,
                private spinnerService: Ng4LoadingSpinnerService) {
    }
    ngOnInit() {
        this.currentUsername = sessionStorage.getItem('username');
        this.initAllForms();
        this.partyService.getPartyByUsername(this.currentUsername).subscribe((res: any) => {
            console.log(res);
            this.userId = res.partyTypeUser.id;
            this.initAllForms();
            const view = {
                party: res
            };
            this.editUserForm(view);
        });
    }
    saveUserDetails() {
    }
    editUserForm(userView: any) {
        this.userForm.patchValue({
            id: userView.party.partyTypeUser.id,
            username: userView.party.partyTypeUser.username,
            password: userView.party.partyTypeUser.password,
            firstName: userView.party.partyTypeUser.firstName,
            middleName: userView.party.partyTypeUser.middleName,
            lastName: userView.party.partyTypeUser.lastName,
            enabled: userView.party.partyTypeUser.enabled,
            createdAt: userView.party.partyTypeUser.createdAt,
            updatedAt: new Date(),
            modifiedBy: this.currentUsername
        });
        this.contactForm.patchValue({
            id: userView.party.partyContactDetails.id,
            mobileNoOne: userView.party.partyContactDetails.mobileNoOne,
            mobileNoTwo: userView.party.partyContactDetails.mobileNoTwo,
            landLineOne: userView.party.partyContactDetails.landLineOne,
            landLineTwo: userView.party.partyContactDetails.landLineTwo,
            faxOne: userView.party.partyContactDetails.faxOne,
            faxTwo: userView.party.partyContactDetails.faxTwo,
            emailIdOne: userView.party.partyContactDetails.emailIdOne,
            emailIdTwo: userView.party.partyContactDetails.emailIdTwo,
            createdAt: userView.party.partyContactDetails.createdAt,
            updatedAt: new Date(),
            modifiedBy: this.currentUsername,
        });
        // for (let i = 0; i < userView.party.partyAddressDetails.length; i++) {
        //     const partyAddressDetailsForm = new FormGroup({
        //         id: new FormControl(userView.party.partyAddressDetails[i].id),
        //         party: new FormControl(userView.party.partyAddressDetails[i].party),
        //         addressType: new FormControl(userView.party.partyAddressDetails[i].addressType),
        //         addressLineOne: new FormControl(userView.party.partyAddressDetails[i].addressLineOne, Validators.required),
        //         addressLineTwo: new FormControl(userView.party.partyAddressDetails[i].addressLineTwo),
        //         area: new FormControl(userView.party.partyAddressDetails[i].area),
        //         pinCode: new FormControl(userView.party.partyAddressDetails[i].pinCode),
        //         city: new FormControl(userView.party.partyAddressDetails[i].city !== undefined &&
        //         userView.party.partyAddressDetails[i].city !== null ? userView.party.partyAddressDetails[i].city.id : null),
        //         createdAt: new FormControl(userView.party.partyAddressDetails[i].createdAt),
        //         updatedAt: new FormControl(new Date()),
        //         modifiedBy: new FormControl(this.currentUsername)
        //     });
        //     (<FormArray>this.userForm.get('partyAddressDetails')).push(partyAddressDetailsForm);
        // }
    }
    private initAllForms() {
        // this.userForm = new FormGroup({
        //     id: new FormControl(null),
        //     type: new FormControl('USER'),
        //     createdAt: new FormControl(new Date()),
        //     createdBy: new FormControl(this.currentUsername),
        //     updatedAt: new FormControl(null),
        //     modifiedBy: new FormControl(null),
        //     partyTypeUser: ,
        //     partyName: new FormGroup({
        //         id: new FormControl(null),
        //         party: new FormControl(null),
        //         salutation: new FormControl(''),
        //         firstName: new FormControl(''),
        //         lastName: new FormControl(''),
        //         createdAt: new FormControl(new Date()),
        //         updatedAt: new FormControl(null),
        //         modifiedBy: new FormControl(null)
        //     }),
        //     partyContactDetails: new FormGroup({
        //         id: new FormControl(null),
        //         party: new FormControl(null),
        //         mobileNoOne: new FormControl('', Validators.required),
        //         mobileNoTwo: new FormControl(''),
        //         landLineOne: new FormControl(''),
        //         landLineTwo: new FormControl(''),
        //         faxOne: new FormControl(''),
        //         faxTwo: new FormControl(''),
        //         emailIdOne: new FormControl('', [Validators.required, Validators.email]),
        //         emailIdTwo: new FormControl('', Validators.email),
        //         createdAt: new FormControl(new Date()),
        //         updatedAt: new FormControl(null),
        //         modifiedBy: new FormControl(null)
        //     }),
        //     partyAddressDetails: new FormArray([]),
        //     roles: new FormControl([])
        // });
        this.userForm = new FormGroup({
            id: new FormControl(null),
            username: new FormControl('', Validators.required),
            password: new FormControl(''),
            firstName: new FormControl('', Validators.required),
            middleName: new FormControl(''),
            lastName: new FormControl('', Validators.required),
            enabled: new FormControl(true),
            createdAt: new FormControl(new Date()),
            updatedAt: new FormControl(null),
            modifiedBy: new FormControl(null)
        });
        this.contactForm = new FormGroup({
            id: new FormControl(null),
            party: new FormControl(null),
            mobileNoOne: new FormControl('', Validators.required),
            mobileNoTwo: new FormControl(''),
            landLineOne: new FormControl(''),
            landLineTwo: new FormControl(''),
            faxOne: new FormControl(''),
            faxTwo: new FormControl(''),
            emailIdOne: new FormControl('', [Validators.required, Validators.email]),
            emailIdTwo: new FormControl('', Validators.email),
            createdAt: new FormControl(new Date()),
            updatedAt: new FormControl(null),
            modifiedBy: new FormControl(null)
        });
        this.resetChangePasswordForm();
    }
    changePassword() {
        this.spinnerService.show();
        this.partyService.changeUserPassword(this.passwordForm.value).subscribe((res: any) => {
            this.passwordForm.setValue(res);
            this.spinnerService.hide();
        }, (error) => {
            this.passwordForm.patchValue({
                message: 'could not change the password',
                status: 'NOT_RECOVERED'
            });
            this.spinnerService.hide();
        });
    }
    checkConfirmPassword() {
        if (this.passwordForm !== null && this.passwordForm !== undefined &&
            this.passwordForm.value.newPassword !== this.confirmPassword || this.confirmPassword === '') {
            return true;
        } else {
            return false;
        }
    }
    checkPasswordChanged() {
        if (this.passwordForm.value.status === 'RECOVERING') {
            return 0;
        } else if (this.passwordForm.value.status === 'RECOVERED') {
            return 1;
        } else {
            return 2;
        }
    }
    resetChangePasswordForm() {
        this.passwordForm = new FormGroup({
            id: new FormControl(this.userId),
            oldPassword: new FormControl('', Validators.required),
            newPassword: new FormControl('', [Validators.required, Validators.email]),
            message: new FormControl('not a response'),
            status: new FormControl('RECOVERING')
        });
        this.confirmPassword = '';
    }
}
