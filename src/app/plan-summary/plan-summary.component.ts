import { Component, OnInit } from '@angular/core';
import {switchMap} from "rxjs/operators";
import {NgxSpinnerService} from "ngx-spinner";
import {PmServiceService} from "../new-execution/new-pm/pm-service.service";
import * as $ from 'jquery';
import {ExecutionFlowPageServiceService} from "../new-execution/execution-flow-page-service.service";

@Component({
  selector: 'app-plan-summary',
  templateUrl: './plan-summary.component.html',
  styleUrls: ['./plan-summary.component.css']
})
export class PlanSummaryComponent implements OnInit {
  headerList = [
    {code: "code"},
    {description: "Description"},
    {supplier: "Supplier"},
    {supplierCode: "SupplierCode"},
    {rtd: "RTD"},
    {confirmedSchedule: "Confirmed Schedule"},
    {tentativeSchedule: "Tentative Schedule"},
    {total: "Total"},
    {priority: "Priority"},
    {remark:"RMK"},
    {safetyStock: "SS"},
    {moq:"MOQ"}

  ];
  monthNamesList?: any = [];
  supplyDataList: any =[];
  constructor(private spinner: NgxSpinnerService, private executionFlowPageService: ExecutionFlowPageServiceService, private pmService: PmServiceService) { }



  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getPmSupplyData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((pmSupplyResponse: any) => {
        console.log(pmSupplyResponse);
        if (pmSupplyResponse !== null && pmSupplyResponse !== undefined) {
          this.supplyDataList = pmSupplyResponse.itemCodes;
          console.log(this.supplyDataList);
        }
        this.spinner.hide();
      }, (error) => {
        console.log(error);
      });
    })

  }



}
