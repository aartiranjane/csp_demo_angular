import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {switchMap} from "rxjs/operators";
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../auth.service';
import {reject} from 'q';
import {PartyService} from '../party/party.service';
import {ExecutionFlowPageServiceService} from "../new-execution/execution-flow-page-service.service";
import {LoginService} from "./login.service";
import {AnalyzerService} from "../new-execution/analyzer/analyzer/analyzer.service";

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
    currentUser:any;
    loginForm: FormGroup;
    passwordForm: FormGroup;
    @Input() error: string;
    forgetPassword = false;
    mpsHeaderList:any=[];
    moduleNamesArray=[];
    monthYearList:any=[];
  username='';
  password='';

  private _chrome = navigator.userAgent.indexOf('Chrome') > -1;


  constructor(public authService: AuthService,
                private partyService: PartyService, private analyzerService: AnalyzerService,
                private executionFlowPageService: ExecutionFlowPageServiceService,
                private router: Router,
                private route: ActivatedRoute,
                private execution: ExecutionFlowPageServiceService,
                private loginService: LoginService) {
    }

    ngOnInit() {
        this.initForm();
    }

    private initForm() {
        const username = '';
        const password = '';
        this.loginForm = new FormGroup({
          username: new FormControl('', [Validators.required]),
          password: new FormControl('', [Validators.required])
        });

        this.passwordForm =  new FormGroup({
          emailId: new FormControl(''),
          status: new FormControl('RECOVERING'),
          message: new FormControl(''),
          username: new FormControl('', [Validators.required]),
        });

    }

    onSubmit() {
        this.authService.login(this.loginForm.value.username.trim(), this.loginForm.value.password).then(success => {
            if (success) {
              try {
                this.error='';
                this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
                const myArray = this.currentUser.scope.split(" ");
                console.log(myArray);
                this.executionFlowPageService.roleModuleList = myArray.reverse();
                this.router.navigateByUrl('/homePage');
                /* this.getHeaderAndMonthData();*/
             /*   console.log('ModuleName = '+this.moduleArray);*/
                this.getHeaderAndMonthData();
                // this.loginService.checkActiveLicence();
              /*  this.username='';
                this.password='';*/
              this.executionFlowPageService.userName =  this.loginForm.value.username;
                console.log(this.loginForm.value.username);
                this.loginForm.reset();
              }catch (e) {
                alert('Please re-new Licence or Enter valid credentials');
              }
            }
            else {
              this.error = 'Please enter valid credentials';
            }
        }, fail => {
          this.error = 'Please enter valid credentials';
        });
    }

    getCredentials() {
        this.partyService.recoverPassword(this.passwordForm.value).subscribe((res: any) => {
            console.log(res);
        });
    }
  getHeaderAndMonthData(){
    this.getHeaderData();
    this.getMonthHeaderData();
  }

  getMonthHeaderData() {
    //getCurrentMonthsMpsHeaderData
    //getMpsHeaderData
    this.analyzerService.getBackMonthNamesList().subscribe((bkMonthRes: any) => {
      console.log(bkMonthRes);
      if (bkMonthRes !== null && bkMonthRes !== undefined && bkMonthRes.length !== 0) {
        this.monthYearList = bkMonthRes.reverse();
        this.executionFlowPageService.backupMonthList= this.monthYearList.reverse();
        console.log(this.monthYearList);
        console.log(this.executionFlowPageService.backupMonthList);

        if (this.monthYearList.length > 0) {
          this.analyzerService.selectedMonth = this.monthYearList[0].monthNameAlias;
          this.analyzerService.monthNameOnZip = this.monthYearList[0].monthName;
          this.analyzerService.setselectedMonth(this.analyzerService.selectedMonth);

        }

      }
    }, (error) => {
      console.log(error);
    });
  }

  getHeaderData() {
    // getMpsHeaderData
    this.executionFlowPageService.getPlanMpsDataWithoutBackup().subscribe((headerResponse: any) => {
      console.log(headerResponse);
      if (headerResponse !== null && headerResponse !== undefined) {
        /*   this.mpsHeaderList = headerResponse;
           this.mpsHeaderList.reverse();
         */
        const mpsHeaderListOfData = headerResponse;

        this.mpsHeaderList=mpsHeaderListOfData.reverse();
        this.executionFlowPageService.mpsPlanHeader =this.mpsHeaderList;
        this.mpsHeaderList = mpsHeaderListOfData;
        if (this.mpsHeaderList.length > 0) {
          this.executionFlowPageService.selectedHeaderId = this.mpsHeaderList[0].id;
          this.executionFlowPageService.selectedPlanDate = this.mpsHeaderList[0].mpsDate;
          this.executionFlowPageService.selectedPlanName =   this.mpsHeaderList[0].mpsName;
          this.executionFlowPageService.setselectedHeaderId(this.executionFlowPageService.selectedHeaderId);
        }
        this.getModuleName();

      }
    }, (error) => {
      console.log(error);
    });
  }

  getModuleName() {
    this.mpsHeaderList.forEach((res: any) => {
      this.moduleNamesArray = [...new Set(this.mpsHeaderList.map(item => item.moduleName))];
      /* this.lineGraphData.push({label: res.mouldName, data: res.m1Value});*/
      /*if (res.moduleName === "ALL") {
        this.moduleNamesArray.push("ALL");
      } else {
        this.moduleNamesArray = [...new Set(this.mpsHeaderList.map(item => item.moduleName))];
      }*/
    });
   if( this.mpsHeaderList.length===0){
     this.executionFlowPageService.moduleNameDisabled = false;
   }

    /*
    if (this.moduleNamesArray.includes('ALL')) {
      this.executionFlowPageService.moduleNameDisabled = false;
    } else if (this.executionFlowPageService.loginUsers === 'mayur' && this.moduleNamesArray.length > 0) {
      this.executionFlowPageService.moduleNameDisabled = false;
    } else if (this.executionFlowPageService.loginUsers === 'pratap' && this.moduleNamesArray.includes('PM')) {
      this.executionFlowPageService.moduleNameDisabled = false;
    } else if (this.executionFlowPageService.loginUsers === 'vaibhav' && this.moduleNamesArray.includes('RM')) {
      this.executionFlowPageService.moduleNameDisabled = false;
    }*/
  }

}
