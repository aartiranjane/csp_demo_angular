import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {UriService} from '../uri.service';
import {LicenceService} from '../licence.service';
import {PartyService} from '../party/party.service';
import {Router} from '@angular/router';

@Injectable()
export class LoginService {
    loginOptionsClicked = new Subject();
    licenceDays = 0;
    constructor(private http: HttpClient,
                private partyService: PartyService,
                private licence: LicenceService,
                private uriService: UriService,
                private router: Router) {
    }

    getUserUsingXhr(username, password): Observable<any> {
        return Observable.create(observer => {
            const data = new FormData();
            data.append('grant_type', 'password');
            data.append('username', username);
            data.append('password', password);

            const xhr = new XMLHttpRequest();
            xhr.withCredentials = true;
            xhr.open('POST', this.uriService.getAuthServerUri() + '/oauth/token');
            xhr.setRequestHeader('Authorization', 'Basic ' + btoa('USER_CLIENT_APP:password'));
            xhr.send(data);
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    observer.next(JSON.parse(xhr.response));
                    observer.complete();
                }
            };
        });
    }
    checkActiveLicence() {
        this.partyService.getApplicationOwner().switchMap((res: any) => {
            if (res && res[0]) {
                return this.licence.checkLicenceValidity(res[0].orgnizationName);
            } else {
                return this.licence.checkLicenceValidity('');
            }
        }).subscribe((res: any) => {
          try {
            if (res < 5 && res >= 0) {
              this.licenceDays = res;
              document.getElementById('licenceExpireSoonBtn').click();
            } else if (res === -2) {
              document.getElementById('licenceDemoModalBtn').click();
            } else if (res === -1) {
              document.getElementById('licenceModalBtn').click();
            }
            //this.router.navigate(['/homePage']);
          }catch (e) {
            alert('Licence module is not working correctly...Please contact Administrator!');
          }

        });
    }
}
