import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UriService} from './uri.service';
import {throwError} from 'rxjs';
const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};
@Injectable({
    providedIn: 'root'
})
export class LicenceService {
    appId = 'CTNSAN1AP';
    appName = 'C2NET';
    constructor(private http: HttpClient,
                private uriService: UriService) {
    }
    
    checkLicenceValidity(owner: string) {
        const nameArr = owner.split(' ');
        let tempName = '';
        nameArr.forEach(a=> tempName+= a.toUpperCase());
        const subString = this.appId + ':' + this.appName + ':' + tempName;
        return this.http.get(this.uriService.getResourceServerUri() + '/check-valid-licence',
            {params: {subString: subString}}).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }
    
    updateLicenceFile(file) {
        const url = this.uriService.getResourceServerUri() + '/upload-licence-file';
        const formdata: FormData = new FormData();
        formdata.append('file', file);
        return this.http.post(url, formdata);
    }
    
    errorHandler(error: Response) {
        return throwError(error || 'Server Error');
    }
}
