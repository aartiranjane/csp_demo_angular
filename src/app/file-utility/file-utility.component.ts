import {Component, OnInit} from '@angular/core';
import {FileUtilityService} from './file-utility.service';
import {NgForm} from '@angular/forms';
declare const Compare: any;

@Component({
  selector: 'app-file-utility',
  templateUrl: './file-utility.component.html',
  styleUrls: ['./file-utility.component.css']
})
export class FileUtilityComponent implements OnInit {

  dropdownList: any = [];
  selectedItems: any = [];
  selectedData: any = [];
  dropdownSettings = {};
  selectedDataList:any=[];
  constructor(private fileUtilityService: FileUtilityService) {
  }

  ngOnInit() {
    this.fileUtilityService.getFileUtility().subscribe((fileUtilityResponse: any) => {
        if (fileUtilityResponse != null) {

          fileUtilityResponse.forEach((response: any) => {
            this.dropdownList.push({
              id: response.id,
              itemName: response.testTaskName,
            });
          });

          this.dropdownSettings = {
            singleSelection: false,
            idField: 'id',
            textField: 'itemName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            allowSearchFilter: true,
            enableSearchFilter: true,
            classes:"myclass custom-class"
          }
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }


  setValueName() {
    this.selectedDataList=this.selectedData;
  }
  onItemSelect(item:any){
    console.log(item);
    console.log(this.selectedItems);
    this.selectedData.push(item.itemName);
  }
  OnItemDeSelect(item:any){
    console.log(item);
    console.log(this.selectedItems);
  }
  onSelectAll(items: any){
    console.log(items);
  }
  onDeSelectAll(items: any) {
    console.log(items);
  }

}
