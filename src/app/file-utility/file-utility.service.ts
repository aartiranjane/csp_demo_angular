import  { Injectable } from '@angular/core';
import {HttpClient, HttpHandler, HttpHeaders} from "@angular/common/http";
import {UriService} from "../uri.service";
import {catchError, map} from "rxjs/operators";
import {throwError} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class FileUtilityService {
  endpoint = this.uriService.getResourceServerUri();

  constructor(private http: HttpClient,
              private uriService: UriService) {
  }
  getFileUtility() {
    return this.http.get(this.endpoint + '/api/rsc-dt-test-task', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }
  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }

}
