import {Component, Input, OnInit, ViewChild,ElementRef} from '@angular/core';
import {AuthService} from "./auth.service";
import {LicenceService} from "./licence.service";
import {LoginService} from "./sign-in/login.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ExecutionFlowPageServiceService} from "./new-execution/execution-flow-page-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @Input() isAuthenticated = false;
  @ViewChild('inputFile') myInputVariable: ElementRef;
  @ViewChild('inputFileForUpload') myInputVariableForUpload: ElementRef;
  loginOptionsClicked = false;
  idleState = '';
  showmodel = true;
  longActive = '';
  fileTemp = undefined;
  licenceUploaded = false;
  reactivateBtn=true;
  uploadBtn=true;

  constructor(private authService: AuthService, private execution:ExecutionFlowPageServiceService,
              private router: Router,
              private route: ActivatedRoute,
              private licence: LicenceService,
              public loginService: LoginService) {
  }

  ngOnInit() {
    this.authService.idleConfig();
    this.authService.isAuthenticatedEvent.subscribe((authenticated: boolean) => {
      this.isAuthenticated = authenticated;
    });
    this.loginService.loginOptionsClicked.subscribe((res: any) => {
      this.loginOptionsClicked = res === true;
    }, (error) => {
      console.log(error);
    });
    this.authService.showWarning.subscribe((res: any) => {
      if (this.showmodel) {
        this.showmodel = false;
        document.getElementById('sessionwarningmodelbutton').click();
      }
      const min = (res - 1) / 60;
      const minutesToDisplay = +min.toFixed(1).charAt(0);
      this.idleState = 'You will logged out in ' + min.toFixed(1).charAt(0) + ' : ' + (res + 1) % 60 + (minutesToDisplay == 0 ? ' sec' : ' mins');
      if (res === 1) {
        document.getElementById('closemodelbuttonnowatch').click();
      }
    });
    this.authService.getLoggedInTime().subscribe((res: any) => {
      const timelaps = res;
      const refresstime = this.authService.refreshTokenExpTime;
      const minuteToDisplay=+((refresstime - timelaps - 2) / 60).toFixed(1).charAt(0);
      this.longActive = 'Session active too long, please click continue else it will auto logout in ' + ((refresstime - timelaps - 2) / 60).toFixed(1).charAt(0) + ' : ' + (refresstime - timelaps) % 60 + (minuteToDisplay === 0 ? ' secs' :' mins ');
      if (refresstime - timelaps < this.authService.timeoutTime && this.idleState === '' && this.showmodel) {
        this.showmodel = false;
        document.getElementById('toolongactivesessionmodelbutton').click();
      }
      if (refresstime - timelaps === 0) {
        document.getElementById('logoutbutton').click();
      }
    });
    this.router.navigate([''])
  }

  closeModelNoWatch() {
    this.showmodel = true;
    this.idleState = '';
    this.authService.idle.stop();
  }

  closeModel() {
    this.showmodel = true;
    this.idleState = '';
    this.authService.idle.stop();
    this.authService.idle.watch();
  }

  reLogin() {
    this.showmodel = true;
    this.longActive = '';
    this.authService.reLogin();
  }

  logout(messsage?) {
    this.reactivateBtn = true;
    this.uploadBtn =true;
    this.myInputVariable.nativeElement.value = '';
    this.myInputVariableForUpload.nativeElement.value = '';
    this.showmodel = true;
    this.longActive = '';
    this.authService.timedOut = messsage !== undefined ? messsage :
      'Your session has been expired !!! Please log in again.';
    this.authService.logout();
  }

  buyNewLicence() {
    this.licence.updateLicenceFile(this.fileTemp).subscribe((res) => {
      this.licenceUploaded = true;
    });
    this.logout('');
  }

  enterIntoApp() {
    if(this.execution.loginUsers === 'rucha' ||  this.execution.loginUsers === 'nayan'){
      this.router.navigate(['/newExecutionflow']);
    }
    else if(this.execution.loginUsers === 'admin'){
      this.router.navigate(['/administrator/userManagement/organisation']);
    }
    else{
      this.router.navigate(['/homePage']);
    }

  }

  selectTempFile(event) {
    this.fileTemp = event.target.files.item(0);
   /* this.reactivateBtn =false;*/
    this.reactivateBtn = false;
    this.uploadBtn =false;
  }

  clickedOk() {
    setTimeout(() => {
      this.licenceUploaded = false;
    }, 1000);
    this.logout('');
  }

}
