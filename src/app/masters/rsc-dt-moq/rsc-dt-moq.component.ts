import { Component, OnInit } from '@angular/core';
import {RscDtMoqService} from './rsc-dt-moq.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-moq',
  templateUrl: './rsc-dt-moq.component.html',
  styleUrls: ['./rsc-dt-moq.component.css']
})
export class RscDtMoqComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtMoqList :any = [];
  displayedColumns: string[] = ['moq_value'];

  constructor(private rscDtMoqService: RscDtMoqService) { }

  ngOnInit() {
    this.rscDtMoqService.getRscDtMoqList().subscribe((rscDtMoqResponse: any) => {
        this.rscDtMoqList = rscDtMoqResponse;
        if (rscDtMoqResponse != null) {
          const rscDtMoqView :any= [];
          rscDtMoqResponse.forEach((response :any) => {
              {
                rscDtMoqView.push({
                  moq_value: response.moqValue
                });
                this.dataSource = new MatTableDataSource(rscDtMoqView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
