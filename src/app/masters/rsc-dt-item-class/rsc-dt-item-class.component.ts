import { Component, OnInit } from '@angular/core';
import {RscDtItemClassService} from './rsc-dt-item-class.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-item-class',
  templateUrl: './rsc-dt-item-class.component.html',
  styleUrls: ['./rsc-dt-item-class.component.css']
})
export class RscDtItemClassComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtItemClassList = [];
  displayedColumns: string[] = ['alias_name', 'full_name'];

  constructor(private rscDtItemClassService: RscDtItemClassService) {
  }

  ngOnInit() {
    this.rscDtItemClassService.getRscDtItemClassList().subscribe((rscDtItemClassResponse: any) => {
        this.rscDtItemClassList = rscDtItemClassResponse;
        if (rscDtItemClassResponse != null) {
          const rscDtItemClassView :any = [];
          rscDtItemClassResponse.forEach((response :any) => {
              {
                rscDtItemClassView.push({
                  alias_name: response.aliasName,
                  full_name: response.fullName
                });
                this.dataSource = new MatTableDataSource(rscDtItemClassView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
