import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RscDtItemCategoryService} from './rsc-dt-item-category.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-item-category',
  templateUrl: './rsc-dt-item-category.component.html',
  styleUrls: ['./rsc-dt-item-category.component.css']
})
export class RSCDTItemCategoryComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtItemCategoryList = [];
  displayedColumns: string[] = ['category', 'description'];

  constructor(private rscDtItemCategoryService: RscDtItemCategoryService) { }

  ngOnInit() {
    this.rscDtItemCategoryService.getRscDtItemCategoryList().subscribe((rscDtItemCategoryResponse: any) => {
        this.rscDtItemCategoryList = rscDtItemCategoryResponse;
        if (rscDtItemCategoryResponse != null) {
          const rscDtItemCategoryView:any = [];
          rscDtItemCategoryResponse.forEach((response:any) => {
              {
                rscDtItemCategoryView.push({
                  category: response.category,
                  description: response.description
                });
                this.dataSource = new MatTableDataSource(rscDtItemCategoryView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
