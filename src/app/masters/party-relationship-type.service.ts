import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UriService} from '../uri.service';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable()
export class PartyRelationshipTypeService {
    constructor(private http: HttpClient, private uriService: UriService) {
    }

    getPartyRelationshipTypeList() {
        return this.http.get(this.uriService.getResourceServerUri() + '/partyRelationshipType',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    errorHandler(error: Response) {
        return throwError(error || 'Server Error');
    }
}
