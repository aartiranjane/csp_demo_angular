import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {RscDtReasonTypeService} from "./rsc_dt_reason_type.service";

@Component({
  selector: 'app-rsc-dt-reason-type',
  templateUrl: './rsc-dt-reason-type.component.html',
  styleUrls: ['./rsc-dt-reason-type.component.css']
})
export class RscDtReasonTypeComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtReasonTypeList = [];
  displayedColumns: string[] = ['name'];

  constructor(private rscDtReasonTypeService: RscDtReasonTypeService) {
  }

  ngOnInit() {
    this.rscDtReasonTypeService.getRscDtReasonTypeList().subscribe((rscDtReasonTypeResponse: any) => {
        this.rscDtReasonTypeList = rscDtReasonTypeResponse;
        if (rscDtReasonTypeResponse != null) {
          const rscDtReasonTypeView :any = [];
          rscDtReasonTypeResponse.forEach((response :any) => {
              {
                rscDtReasonTypeView.push({
                  name: response.name
                });
                this.dataSource = new MatTableDataSource(rscDtReasonTypeView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
