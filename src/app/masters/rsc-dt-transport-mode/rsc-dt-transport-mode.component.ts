import { Component, OnInit } from '@angular/core';
import {RscDtTransportModeService} from './rsc-dt-transport-mode.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-transport-mode',
  templateUrl: './rsc-dt-transport-mode.component.html',
  styleUrls: ['./rsc-dt-transport-mode.component.css']
})
export class RscDtTransportModeComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtTransportModeList :any = [];
  displayedColumns: string[] = ['code', 'description'];

  constructor(private rscDtTransportModeService: RscDtTransportModeService) {
  }

  ngOnInit() {
    this.rscDtTransportModeService.getRscDtTransportModeList().subscribe((rscDtTransportModeResponse: any) => {
        this.rscDtTransportModeList = rscDtTransportModeResponse;
        if (rscDtTransportModeResponse != null) {
          const rscDtTransportModeView :any = [];
          rscDtTransportModeResponse.forEach((response:any) => {
              {
                rscDtTransportModeView.push({
                  code: response.code,
                  description: response.description
                });
                this.dataSource = new MatTableDataSource(rscDtTransportModeView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
