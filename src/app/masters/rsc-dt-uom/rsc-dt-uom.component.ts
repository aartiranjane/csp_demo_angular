import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RscDtUomService} from './rsc-dt-uom.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-uom',
  templateUrl: './rsc-dt-uom.component.html',
  styleUrls: ['./rsc-dt-uom.component.css']
})
export class RscDtUomComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtUomList = [];
  displayedColumns: string[] = ['alias_name', 'full_name'];

  constructor(private rscDtUomService: RscDtUomService) {
  }

  ngOnInit() {
    this.rscDtUomService.getRscDtUomList().subscribe((rscDtUomResponse: any) => {
        this.rscDtUomList = rscDtUomResponse;
        if (rscDtUomResponse != null) {
          const rscDtUomView: any = [];
          rscDtUomResponse.forEach((response :any) => {
              {
                rscDtUomView.push({
                  alias_name: response.aliasName,
                  full_name: response.fullName
                });
                this.dataSource = new MatTableDataSource(rscDtUomView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
