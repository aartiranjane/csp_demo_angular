import { Component, OnInit } from '@angular/core';
import {RscDtLinesService} from './rsc-dt-lines.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-lines',
  templateUrl: './rsc-dt-lines.component.html',
  styleUrls: ['./rsc-dt-lines.component.css']
})
export class RscDtLinesComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtLinesList = [];
  displayedColumns: string[] = ['name','capacityPerShift','shifts','speed','gUpTime','modPerLinePerShift','hrsPerShift'];

  constructor(private rscDtLinesService: RscDtLinesService) {
  }

  ngOnInit() {
    this.rscDtLinesService.getRscDtLinesList().subscribe((rscDtLinesResponse: any) => {
        this.rscDtLinesList = rscDtLinesResponse;
        if (rscDtLinesResponse != null) {
          const rscDtLinesView :any = [];
          rscDtLinesResponse.forEach((response :any) => {
              {
      rscDtLinesView.push({
        name: response.name,
        capacityPerShift: response.capacityPerShift,
        shifts: response.shifts,
        speed: response.speed,
        gUpTime: response.gUpTime,
        modPerLinePerShift: response.modPerLinePerShift,
        hrsPerShift: response.hrsPerShift,
      });
      this.dataSource = new MatTableDataSource(rscDtLinesView);
    }

  }
);
}
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
