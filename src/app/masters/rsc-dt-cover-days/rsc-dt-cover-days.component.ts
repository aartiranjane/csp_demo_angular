import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RscDtCoverDaysService} from './rsc-dt-cover-days.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-cover-days',
  templateUrl: './rsc-dt-cover-days.component.html',
  styleUrls: ['./rsc-dt-cover-days.component.css']
})
export class RscDtCoverDaysComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtCoverDaysList:any = [];
  displayedColumns: string[] = ['cover_days'];

  constructor(private rscDtCoverDaysService: RscDtCoverDaysService) { }

  ngOnInit() {
    this.rscDtCoverDaysService.getRscDtCoverDaysList().subscribe((rscDtCoverDaysResponse: any) => {
        this.rscDtCoverDaysList = rscDtCoverDaysResponse;
        if (rscDtCoverDaysResponse != null) {
          const rscDtCoverDaysView:any = [];
          rscDtCoverDaysResponse.forEach((response:any) => {
              {
                rscDtCoverDaysView.push({
                  cover_days: response.coverDays
                });
                this.dataSource = new MatTableDataSource(rscDtCoverDaysView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
