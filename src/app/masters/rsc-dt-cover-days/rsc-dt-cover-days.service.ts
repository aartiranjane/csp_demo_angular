import { Injectable } from '@angular/core';
import {Subject, throwError} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {UriService} from '../../uri.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RscDtCoverDaysService {
  endpoint = this.uriService.getResourceServerUri();

  constructor(private http: HttpClient,
              private uriService: UriService) {
  }

  getRscDtCoverDaysList() {
    return this.http.get(this.endpoint + '/api/rsc-dt-cover-days', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
