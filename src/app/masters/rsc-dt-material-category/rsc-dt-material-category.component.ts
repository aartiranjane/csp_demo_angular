import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {RscDtMaterialCategoryService} from './rsc_dt_material_category.service';

@Component({
  selector: 'app-rsc-dt-material-category',
  templateUrl: './rsc-dt-material-category.component.html',
  styleUrls: ['./rsc-dt-material-category.component.css']
})
export class RscDtMaterialCategoryComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtMaterialCategoryList = [];
  displayedColumns: string[] = ['name'];

  constructor(private rscDtMaterialCategoryService: RscDtMaterialCategoryService) {
  }

  ngOnInit() {
    this.rscDtMaterialCategoryService.getRscDtMaterialCategoryList().subscribe((rscDtMaterialCategoryResponse: any) => {
        this.rscDtMaterialCategoryList = rscDtMaterialCategoryResponse;
        if (rscDtMaterialCategoryResponse != null) {
          const rscDtMaterialCategoryView :any = [];
          rscDtMaterialCategoryResponse.forEach((response :any) => {
              {
                rscDtMaterialCategoryView.push({
                  name: response.name
                });
                this.dataSource = new MatTableDataSource(rscDtMaterialCategoryView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
