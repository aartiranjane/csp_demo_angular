import { Component, OnInit } from '@angular/core';
import {RscDtTechnicalSeriesService} from './rsc-dt-technical-series.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-technical-series',
  templateUrl: './rsc-dt-technical-series.component.html',
  styleUrls: ['./rsc-dt-technical-series.component.css']
})
export class RscDtTechnicalSeriesComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtTechnicalSeriesList = [];
  displayedColumns: string[] = ['series_value'];

  constructor(private rscDtTechnicalSeriesService: RscDtTechnicalSeriesService) { }

  ngOnInit() {
    this.rscDtTechnicalSeriesService.getRscDtTechnicalSeriesList().subscribe((rscDtTechnicalSeriesResponse: any) => {
        this.rscDtTechnicalSeriesList = rscDtTechnicalSeriesResponse;
        if (rscDtTechnicalSeriesResponse != null) {
          const rscDtTechnicalSeriesView :any = [];
          rscDtTechnicalSeriesResponse.forEach((response:any) => {
              {
                rscDtTechnicalSeriesView.push({
                  series_value: response.seriesValue
                });
                this.dataSource = new MatTableDataSource(rscDtTechnicalSeriesView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
