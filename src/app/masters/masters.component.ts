import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-masters',
  templateUrl: './masters.component.html',
  styleUrls: ['./masters.component.css']
})
export class MastersComponent implements OnInit {
  mastersList = [
    ['Rsc DT ItemCode', 'rscDtItemCode'],
    ['Rsc DT ItemCategory', 'rscDtItemCategory'],
    ['Rsc DT CoverDays', 'rscDtCoverDays'],
    ['Rsc DT Uom', 'rscDtUom'],
    ['Rsc DT Division', 'rscDtDivision'],
    ['Rsc DT Safety Stock', 'rscDtSafetyStock'],
    ['Rsc DT Item Class', 'rscDtItemClass'],
    ['Rsc DT Technical Series', 'rscDtTechnicalSeries'],
    ['Rsc DT Moq', 'rscDtMoq'],
    ['Rsc DT Transport Mode', 'rscDtTransportMode'],
    ['Rsc DT Transport Type', 'rscDtTransportType'],
    ['Rsc DT Mould', 'rscDtMould'],
    ['Rsc DT Supplier', 'rscDtSupplier'],
    ['Rsc DT Production Type', 'rscDtProductionType'],
    ['Rsc DT Lines', 'rscDtLines'],
    ['Rsc DT Stock Type', 'rscDtStockType'],
    ['Rsc DT Sailing Days', 'rscDtSailingDays'],
    ['Rsc DT Std Price', 'rscDtStdPrice'],
    ['Rsc Dt Material Category', 'rscDtMaterialCategory'],
    ['Rsc Dt Map', 'rscDtMap'],
    ['Rsc Dt Skids', 'rscDtSkids'],
    ['Rsc Dt Reason Type', 'rscDtReasonType'],
    ['Rsc Dt Reason', 'rscDtReason'],
    ['Rsc Dt Tag', 'rscDtTag']

  ];
  currentMasterName!: string;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  loadMasterData(masterName :any, masterUrl :any ) {
    this.currentMasterName = masterName;
    this.router.navigate(['administrator/masters/' + masterUrl]);
  }
}
