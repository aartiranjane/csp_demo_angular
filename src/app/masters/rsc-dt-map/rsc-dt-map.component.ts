import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {RscDtMapService} from './rsc_dt_map.service';

@Component({
  selector: 'app-rsc-dt-map',
  templateUrl: './rsc-dt-map.component.html',
  styleUrls: ['./rsc-dt-map.component.css']
})
export class RscDtMapComponent implements OnInit {

  dataSource!: MatTableDataSource<any>;
  rscDtMapList = [];
  displayedColumns: string[] = ['price'];

  constructor(private rscDtMapService: RscDtMapService) {
  }

  ngOnInit() {
    this.rscDtMapService.getRscDtMapList().subscribe((rscDtMapResponse: any) => {
        this.rscDtMapList = rscDtMapResponse;
        if (rscDtMapResponse != null) {
          const rscDtMapView: any = [];
          rscDtMapResponse.forEach((response: any) => {
              {
                rscDtMapView.push({
                  price: response.price
                });
                this.dataSource = new MatTableDataSource<any>(rscDtMapView);
              }
            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
