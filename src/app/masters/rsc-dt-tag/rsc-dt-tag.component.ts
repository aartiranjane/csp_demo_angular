import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {RscDtTagService} from "./rsc_dt_tag.service";

@Component({
  selector: 'app-rsc-dt-tag',
  templateUrl: './rsc-dt-tag.component.html',
  styleUrls: ['./rsc-dt-tag.component.css']
})
export class RscDtTagComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtTagList = [];
  displayedColumns: string[] = ['name'];

  constructor(private rscDtTagService: RscDtTagService) {
  }

  ngOnInit() {
    this.rscDtTagService.getRscDtTagList().subscribe((rscDtTagResponse: any) => {
        this.rscDtTagList = rscDtTagResponse;
        if (rscDtTagResponse != null) {
          const rscDtTagView :any = [];
          rscDtTagResponse.forEach((response :any) => {
              {
                rscDtTagView.push({
                  name: response.name
                });
                this.dataSource = new MatTableDataSource(rscDtTagView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
