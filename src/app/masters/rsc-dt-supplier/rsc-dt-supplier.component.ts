import { Component, OnInit } from '@angular/core';
import {RscDtSupplierService} from './rsc-dt-supplier.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-supplier',
  templateUrl: './rsc-dt-supplier.component.html',
  styleUrls: ['./rsc-dt-supplier.component.css']
})
export class RscDtSupplierComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtSupplierList = [];
  displayedColumns: string[] = ['supplier_name', 'active'];

  constructor(private rscDtSupplierService: RscDtSupplierService) {
  }

  ngOnInit() {
    this.rscDtSupplierService.getRscDtSupplierList().subscribe((rscDtSupplierResponse: any) => {
        this.rscDtSupplierList = rscDtSupplierResponse;
        if (rscDtSupplierResponse != null) {
          const rscDtSupplierView :any= [];
          rscDtSupplierResponse.forEach((response :any) => {
              {
                rscDtSupplierView.push({
                  supplier_name: response.supplierName,
                  active: response.active
                });
                this.dataSource = new MatTableDataSource(rscDtSupplierView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
