import { Component, OnInit } from '@angular/core';
import {RscDtStockTypeService} from './rsc-dt-stock-type.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-stock-type',
  templateUrl: './rsc-dt-stock-type.component.html',
  styleUrls: ['./rsc-dt-stock-type.component.css']
})
export class RscDtStockTypeComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtStockTypeList = [];
  displayedColumns: string[] = ['alias_name', 'full_name'];

  constructor(private rscDtStockTypeService: RscDtStockTypeService) {
  }

  ngOnInit() {
    this.rscDtStockTypeService.getRscDtStockTypeList().subscribe((rscDtStockTypeResponse: any) => {
        this.rscDtStockTypeList = rscDtStockTypeResponse;
        if (rscDtStockTypeResponse != null) {
          const rscDtStockTypeView :any = [];
          rscDtStockTypeResponse.forEach((response :any) => {
              {
                rscDtStockTypeView.push({
                  alias_name: response.aliasName,
                  full_name: response.fullName
                });
                this.dataSource = new MatTableDataSource(rscDtStockTypeView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
