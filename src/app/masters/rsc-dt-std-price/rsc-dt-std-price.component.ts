import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {RscDtStdPriceService} from "./rsc_dt_std_price.service";

@Component({
  selector: 'app-rsc-dt-std-price',
  templateUrl: './rsc-dt-std-price.component.html',
  styleUrls: ['./rsc-dt-std-price.component.css']
})
export class RscDtStdPriceComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtStdPriceList = [];
  displayedColumns: string[] = ['price'];

  constructor(private rscDtStdPriceService: RscDtStdPriceService) {
  }

  ngOnInit() {
    this.rscDtStdPriceService.getRscDtStdPriceList().subscribe((rscDtStdPriceResponse: any) => {
        this.rscDtStdPriceList = rscDtStdPriceResponse;
        if (rscDtStdPriceResponse != null) {
          const rscDtStdPriceView: any = [];
          rscDtStdPriceResponse.forEach((response: any) => {
              {
                rscDtStdPriceView.push({
                  price: response.price
                });
                this.dataSource = new MatTableDataSource<any>(rscDtStdPriceView);
              }
            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
