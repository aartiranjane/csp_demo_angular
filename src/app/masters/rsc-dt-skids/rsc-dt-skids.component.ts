import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {RscDtSkidsService} from "./rsc_dt_skids.service";

@Component({
  selector: 'app-rsc-dt-skids',
  templateUrl: './rsc-dt-skids.component.html',
  styleUrls: ['./rsc-dt-skids.component.css']
})
export class RscDtSkidsComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtSkidsList = [];
  displayedColumns: string[] = ['name', 'shifts', 'batchesPerShift', 'hrsPerShift'];

  constructor(private rscDtSkidsService: RscDtSkidsService) {
  }

  ngOnInit() {
    this.rscDtSkidsService.getRscDtSkidsList().subscribe((rscDtSkidsResponse: any) => {
        this.rscDtSkidsList = rscDtSkidsResponse;
        if (rscDtSkidsResponse != null) {
          const rscDtSkidsView: any = [];
          rscDtSkidsResponse.forEach((response: any) => {
              {
                rscDtSkidsView.push({
                  name: response.name,
                  shifts: response.shifts,
                  batchesPerShift: response.batchesPerShift,
                  hrsPerShift: response.hrsPerShift,
                });
                this.dataSource = new MatTableDataSource(rscDtSkidsView);
              }
            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
