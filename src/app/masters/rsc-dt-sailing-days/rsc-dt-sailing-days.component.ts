import { Component, OnInit } from '@angular/core';
import {RscDtSailingDaysService} from './rsc-dt-sailing-days.service';
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-rsc-dt-sailing-days',
  templateUrl: './rsc-dt-sailing-days.component.html',
  styleUrls: ['./rsc-dt-sailing-days.component.css']
})
export class RscDtSailingDaysComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtSailingDaysList = [];
  displayedColumns: string[] = ['sailing_days'];

  constructor(private rscDtSailingDaysService: RscDtSailingDaysService) { }

  ngOnInit() {
    this.rscDtSailingDaysService.getRscDtSailingDaysList().subscribe((rscDtSailingDaysResponse: any) => {
        this.rscDtSailingDaysList = rscDtSailingDaysResponse;
        if (rscDtSailingDaysResponse != null) {
          const rscDtSailingDaysView:any = [];
          rscDtSailingDaysResponse.forEach((response:any) => {
              {
                rscDtSailingDaysView.push({
                  sailing_days: response.sailingDays
                });
                this.dataSource = new MatTableDataSource(rscDtSailingDaysView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
