import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {RscDtReasonService} from "./rsc_dt_reason.service";

@Component({
  selector: 'app-rsc-dt-reason',
  templateUrl: './rsc-dt-reason.component.html',
  styleUrls: ['./rsc-dt-reason.component.css']
})
export class RscDtReasonComponent implements OnInit {
  dataSource!: MatTableDataSource<any>;
  rscDtReasonList = [];
  displayedColumns: string[] = ['reason','description'];

  constructor(private rscDtReasonService:  RscDtReasonService) {
  }

  ngOnInit() {
    this.rscDtReasonService.getRscDtReasonList().subscribe((rscDtReasonResponse: any) => {
        this.rscDtReasonList = rscDtReasonResponse;
        if (rscDtReasonResponse != null) {
          const rscDtReasonView :any = [];
          rscDtReasonResponse.forEach((response :any) => {
              {
                rscDtReasonView.push({
                  reason: response.reason,
                  description: response.description
                });
                this.dataSource = new MatTableDataSource(rscDtReasonView);
              }

            }
          );
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
