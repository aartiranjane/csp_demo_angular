import {
  AfterViewChecked,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnInit,
  Output, HostListener
} from '@angular/core';
import {Location} from '@angular/common';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HorizontalHeaderService} from "./horizontal-header.service";
import {ExecutionFlowPageServiceService} from "../../new-execution/execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {AnalyzerService} from "../../new-execution/analyzer/analyzer/analyzer.service";
import {AuthService} from "../../auth.service";


declare var $: any;

@Component({
  selector: 'app-horizontal-navigation',
  templateUrl: './horizontal-navigation.component.html'
})

export class HorizontalNavigationComponent implements AfterViewInit, OnInit, AfterViewChecked {
  mpsHeaderList: any = [];
  selectedPlan: any;
  selectedPlanId: any;
  selectedPlanDate: any;
  planName: any;
  selectedPlanSubmit: boolean = true;
  monthYearList:any=[];
  monthName:any;
  singleselectedItems: any = [];
  singledropdownSettings = {};
  monthSelectList:any=[];
  selectedMonthName:any;
  selectedMonth:any;
  selectedMonthSubmit: boolean = true;
  modulePage:any;
  planHeaderList:any=[];
  backMonthList:any=[];


  @Output() toggleSidebar = new EventEmitter<void>();

  public config: PerfectScrollbarConfigInterface = {};

  public showSearch = false;

  public isCollapsed = false;
  public showMobileMenu = false;
  loginModuleName: any;

  userName:any;
  moduleName:any;
  adminName:any;


  // This is for Mymessages


  constructor(private authService: AuthService,
              private modalService: NgbModal, private translate: TranslateService,
              private route: ActivatedRoute,private location: Location,
              private analyzerService:AnalyzerService,
              private ngxSpinnerService: NgxSpinnerService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              public router: Router, public horizontalHeaderService: HorizontalHeaderService,
              private changeRef: ChangeDetectorRef) {

    translate.setDefaultLang('en');

  }


  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit() {
     this.modulePage = this.executionFlowPageService.moduleWisePage;

    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };

    this.userName = this.executionFlowPageService.userName;
    this.moduleName = this.executionFlowPageService.selectedModuleName;
    this.executionFlowPageService.msgHideShow = this.moduleName;


    this.ngxSpinnerService.show();
    this.getHeaderData();
    if(this.executionFlowPageService.moduleWisePage === 'FGBkup'
      || this.executionFlowPageService.moduleWisePage === 'PMBkup' ||
      this.executionFlowPageService.moduleWisePage === 'RMBkup'||
      this.executionFlowPageService.moduleWisePage === 'ALLBkup'){
      this.getMonthHeaderData();
    }
    /*this.getMonthHeaderData();*/
  }


  /*backClicked() {
    this._location.back();
  }
*/

  selectMonthYr(row:any){
    this.selectedMonthName= row.monthNameAlias;
    this.selectedMonth = row.monthNameAlias;
    this.selectedMonthSubmit = false;
  }
  submitMonth(){
    this.monthName = this.selectedMonth;
    this.analyzerService.selectedMonth = this.selectedMonthName;
    this.analyzerService.setselectedMonth(this.selectedMonthName);
    this.monthName = this.selectedMonth;
    this.analyzerService.monthNameOnZip = this.selectedMonth;
    this.selectedMonthSubmit = true;
  }

  getMonthHeaderData() {
    this.backMonthList=this.executionFlowPageService.backupMonthList;
    this.monthYearList = this.backMonthList;
    if (this.monthYearList.length > 0) {
      this.selectMonthYr(this.monthYearList[0]);
      this.selectedMonthSubmit = true;
      this.analyzerService.selectedMonth = this.monthYearList[0].monthNameAlias;
      this.monthName = this.monthYearList[0].monthNameAlias;
      this.analyzerService.monthNameOnZip = this.monthName;
      this.analyzerService.setselectedMonth(this.analyzerService.selectedMonth);
      this.selectedMonthSubmit = true;
     /* if (this.analyzerService.selectedMonth === null || this.analyzerService.selectedMonth === undefined) {
        this.selectMonthYr(this.monthYearList[0]);
        this.analyzerService.selectedMonth = this.monthYearList[0].monthNameAlias;
        this.monthName = this.monthYearList[0].monthNameAlias;
        this.analyzerService.monthNameOnZip = this.monthName;
      }*/
    }
  }

  getHeaderData() {
    this.planHeaderList = this.executionFlowPageService.mpsPlanHeader;
    console.log(this.planHeaderList);
    this.mpsHeaderList=this.planHeaderList;
    console.log(this.mpsHeaderList);
    if (this.mpsHeaderList.length > 0) {
      this.selectPlan(this.mpsHeaderList[0]);
      this.selectedPlanSubmit = true;
     this.planName = this.mpsHeaderList[0].mpsName;
      this.selectedPlanDate = this.mpsHeaderList[0].mpsDate;
      this.executionFlowPageService.selectedHeaderId = this.mpsHeaderList[0].id;
      this.executionFlowPageService.selectedPlanDate = this.mpsHeaderList[0].mpsDate;
      this.executionFlowPageService.selectedPlanName = this.planName;
      this.selectedPlanSubmit = true;
      this.executionFlowPageService.setselectedHeaderId(this.executionFlowPageService.selectedHeaderId);
     /* if (this.executionFlowPageService.selectedHeaderId === null || this.executionFlowPageService.selectedHeaderId === undefined) {
        this.selectPlan(this.mpsHeaderList[0]);
        this.executionFlowPageService.selectedHeaderId = this.mpsHeaderList[0].id;
        this.executionFlowPageService.selectedPlanDate = this.mpsHeaderList[0].mpsDate;
        this.planName = this.mpsHeaderList[0].mpsName;
        this.executionFlowPageService.selectedPlanName = this.planName;
      }*/
    }

  }


  selectPlan(row: any) {
    this.selectedPlanId = row.id;
    this.selectedPlan = row.mpsName;
    this.selectedPlanDate = row.mpsDate;
    this.selectedPlanSubmit = false;
  }

  submitPlan() {
    this.executionFlowPageService.selectedHeaderId = this.selectedPlanId;
    this.executionFlowPageService.selectedPlanDate = this.selectedPlanDate;
    this.executionFlowPageService.setselectedHeaderId(this.selectedPlanId);
    this.planName = this.selectedPlan;
    this.executionFlowPageService.selectedPlanName = this.selectedPlan;

    this.selectedPlanSubmit = true;
  }

  ngAfterViewInit() {
  }


  logOut() {
    if (this.mpsHeaderList.length > 0) {
      this.executionFlowPageService.selectedHeaderId = this.mpsHeaderList[this.mpsHeaderList.length - 1].id;
      this.executionFlowPageService.selectedPlanDate = this.mpsHeaderList[this.mpsHeaderList.length - 1].mpsDate;
    }
    this.router.navigateByUrl('/login');
    this.authService.idle.stop();
    this.executionFlowPageService.selectedModuleName = undefined;
  }
}
