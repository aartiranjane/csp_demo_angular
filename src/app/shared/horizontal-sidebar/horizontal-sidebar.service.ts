import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RouteInfo } from './horizontal-sidebar.metadata';
import { ROUTES } from './horizontal-menu-items';
import {RMROUTES} from "./rm-horizontal-menu-item";
import {FGROUTES} from "./fg-horizontal-menu-item";
import {ADMINROUTES} from "./administrator-horizontal-menu-item";
import {ANALYZERROUTES} from "./analyzer-horizontal-menu-item";
import {FGYEARLY} from "./fg-yearly-horizontal-menu-items";
import {PMYEARLY} from "./pm-yearly-horizontal-menu-items";
import {RMYEARLY} from "./rm-yearly-horizontal-menu-items";
import {FGBACKUPROUTES} from "./fg-backup-menu-items";
import {RMBACKUPROUTES} from "./rm-backup-menu-items";
import {PMBACKUPROUTES} from "./pm-backup-menu-items";
import {YEARLYANALYZERROUTES} from "./analyzer-yearly-menu-items";
import {ANALYZERBACKUPROUTES} from "./analyzer-backup-menu-items";


@Injectable({
    providedIn: 'root'
})
export class HorizontalSidebarService {

    public screenWidth: any;
    public collapseSidebar: boolean = false;
    public fullScreen: boolean = false;

    rmMenuItems: RouteInfo[] = RMROUTES;
    fgMenuItems:  RouteInfo[] = FGROUTES;
    adminMenuItems:  RouteInfo[] = ADMINROUTES;
    analyzerItems: RouteInfo[]= ANALYZERROUTES;
    MENUITEMS: RouteInfo[] = ROUTES;
    fgYearlyItems: RouteInfo[]=FGYEARLY;
    pmYearlyItems: RouteInfo[]=PMYEARLY;
    rmYearlyItems: RouteInfo[]= RMYEARLY;
    fgBackUpItems: RouteInfo[]= FGBACKUPROUTES;
    rmBackUpItems: RouteInfo[]= RMBACKUPROUTES;
    pmBackUpItems: RouteInfo[]= PMBACKUPROUTES;
    analyzerYearlyItems: RouteInfo[]= YEARLYANALYZERROUTES;
    analyzerBackupItems: RouteInfo[]= ANALYZERBACKUPROUTES;

    items = new BehaviorSubject<RouteInfo[]>(this.MENUITEMS);
    rmitems = new BehaviorSubject<RouteInfo[]>(this.rmMenuItems);
    fgitems = new BehaviorSubject<RouteInfo[]>(this.fgMenuItems);
    adminitems = new BehaviorSubject<RouteInfo[]>(this.adminMenuItems);
    analyzeritems = new BehaviorSubject<RouteInfo[]>(this.analyzerItems);
    fgyearly = new BehaviorSubject<RouteInfo[]>(this.fgYearlyItems);
    pmyearly = new BehaviorSubject<RouteInfo[]>(this.pmYearlyItems);
    rmyearly = new BehaviorSubject<RouteInfo[]>(this.rmYearlyItems);
    fgbackup = new BehaviorSubject<RouteInfo[]>(this.fgBackUpItems);
    rmbackup = new BehaviorSubject<RouteInfo[]>(this.rmBackUpItems);
    pmbackup = new BehaviorSubject<RouteInfo[]>(this.pmBackUpItems);
    analyzeryearly = new BehaviorSubject<RouteInfo[]>(this.analyzerYearlyItems);
   analyzerbackup = new BehaviorSubject<RouteInfo[]>(this.analyzerBackupItems);


  constructor() {
    }
}
