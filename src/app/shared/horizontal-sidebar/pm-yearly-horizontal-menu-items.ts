import { RouteInfo } from './horizontal-sidebar.metadata';



export const PMYEARLY: RouteInfo[] = [
  {
    path: "",
    title: 'MPS Triangle Analytics',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/pmYearlyModule/pmDeltaAnalytics",
        title: "MPS Triangle Analytics",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'5px',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },


  {
    path: "",
    title: 'SLOB Movement',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/pmYearlyModule/pmSlobMovementTab",
        title: "SLOB Movement",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },
  {
    path: "",
    title: 'Inventory Quality Index',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/pmYearlyModule/pmInventoryQuality",
        title: "Inventory Quality Index",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },

  {
    path: "",
    title: 'OTIF Cummulative Score',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/pmYearlyModule/pmOtifCummulativeScore",
        title: "OTIF Cummulative Score",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },


];


