import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { RouteInfo } from './horizontal-sidebar.metadata';
import { HorizontalSidebarService } from './horizontal-sidebar.service';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {PerfectScrollbarConfigInterface} from "ngx-perfect-scrollbar";
import {ExecutionFlowPageServiceService} from "../../new-execution/execution-flow-page-service.service";
import {switchMap} from "rxjs/operators";
import {FormControl} from "@angular/forms";
import {Location} from "@angular/common";


@Component({
  selector: 'app-horizontal-sidebar',
  templateUrl: './horizontal-sidebar.component.html'
})
export class HorizontalSidebarComponent implements OnInit {
  modulePage:any;
 /* mpsHeaderList: any = [];
  selectedPlan: any;
  selectedPlanId: any;
  planName: any;

  selectedPlanSubmit: boolean = true;*/
  showMenu = '';
  showSubMenu = '';
  public sidebarnavItems: RouteInfo[] = [];
  public sidebarnavItemsRm: RouteInfo[] = [];
  public sidebarnavItemsFg: RouteInfo[] = [];
  public sidebarnavItemsAdmin: RouteInfo[] = [];
  public sidebarnavItemsAnalyzer: RouteInfo[] = [];
  public sidebarnavItemsFgYearly:RouteInfo[] = [];
  public sidebarnavItemsPmYearly:RouteInfo[] = [];
  public sidebarnavItemsRmYearly:RouteInfo[] = [];
  public sidebarnavItemsFgBackup:RouteInfo[] = [];
  public sidebarnavItemsRmBackup:RouteInfo[] = [];
  public sidebarnavItemsPmBackup:RouteInfo[] = [];
  public sidebarnavItemsAnalyzerYr :RouteInfo[] = [];
  public sidebarnavItemsAnalyzerBk :RouteInfo[] = [];
  path = '';
  submenuPath: any='';
  public config: PerfectScrollbarConfigInterface = {};
  constructor(private menuServise: HorizontalSidebarService,private location: Location,
              public router: Router, private executionFlowService: ExecutionFlowPageServiceService) {
   /* this.menuServise.items.pipe(
      switchMap((menuItems: any) => {
        this.sidebarnavItems = menuItems;
        // Active menu
        this.sidebarnavItems.filter(m => m.submenu.filter(
          (s) => {
            if (s.path === this.router.url) {
              this.path = m.title;
              this.submenuPath= m.submenu[0].title;
            }
          }
        ));
        this.addExpandClass(this.path);
        this.addActiveClass(this.submenuPath);
        return  this.menuServise.rmitems;
      }))
      .pipe(
      switchMap((rmmenuItems: any) => {
        this.sidebarnavItemsRm = rmmenuItems;

        // Active menu
        this.sidebarnavItemsRm.filter(m => m.submenu.filter(
          (s) => {
            if (s.path === this.router.url) {
              this.path = m.title;
              this.submenuPath= m.submenu[0].title;
            }
          }
        ));
        this.addExpandClass(this.path);
        this.addActiveClass(this.submenuPath);
        return  this.menuServise.fgitems;
      })).pipe(
      switchMap((fgmenuItems: any) => {
        this.sidebarnavItemsFg = fgmenuItems;

        // Active menu
        this.sidebarnavItemsFg.filter(m => m.submenu.filter(
          (s) => {
            if (s.path === this.router.url) {
              this.path = m.title;
              this.submenuPath= m.submenu[0].title;
            }
          }
        ));
        this.addExpandClass(this.path);
        this.addActiveClass(this.submenuPath);
        return this.menuServise.adminitems;
      }))
      .pipe(
        switchMap((adminMenuItems: any) => {
          this.sidebarnavItemsAdmin = adminMenuItems;

          // Active menu
          this.sidebarnavItemsAdmin.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
              }
            }
          ));
          this.addExpandClass(this.path);
          return   this.menuServise.analyzeritems;
        }))
      .pipe(
        switchMap((analyzerMenuItems: any) => {
          this.sidebarnavItemsAnalyzer = analyzerMenuItems;

          // Active menu
          this.sidebarnavItemsAnalyzer.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
              }
            }
          ));
          this.addExpandClass(this.path);
          return this.menuServise.fgyearly;
        }))
      .pipe(
        switchMap((fgYearlyItems: any) => {
          this.sidebarnavItemsFgYearly = fgYearlyItems;

          // Active menu
          this.sidebarnavItemsFgYearly.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
                this.submenuPath= m.submenu[0].title;
              }
            }
          ));
          this.addExpandClass(this.path);
          this.addActiveClass(this.submenuPath);
          return this.menuServise.pmyearly;
        }))
      .pipe(
        switchMap((pmYearlyItems: any) => {
          this.sidebarnavItemsPmYearly = pmYearlyItems;

          // Active menu
          this.sidebarnavItemsPmYearly.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
                this.submenuPath= m.submenu[0].title;
              }
            }
          ));
          this.addExpandClass(this.path);
          this.addActiveClass(this.submenuPath);
          return  this.menuServise.rmyearly;
        }))
      .pipe(
        switchMap((rmYearlyItems: any) => {
          this.sidebarnavItemsRmYearly = rmYearlyItems;

          // Active menu
          this.sidebarnavItemsRmYearly.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
                this.submenuPath= m.submenu[0].title;
              }
            }
          ));
          this.addExpandClass(this.path);
          this.addActiveClass(this.submenuPath);
          return  this.menuServise.fgbackup;
        }))
      .pipe(
        switchMap((fgBackup: any) => {
          this.sidebarnavItemsFgBackup = fgBackup;

          // Active menu
          this.sidebarnavItemsFgBackup.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
              }
            }
          ));
          this.addExpandClass(this.path);
          return  this.menuServise.rmbackup;
        }))
      .pipe(
        switchMap((rmBackup: any) => {
          this.sidebarnavItemsRmBackup = rmBackup;

          // Active menu
          this.sidebarnavItemsRmBackup.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
              }
            }
          ));
          this.addExpandClass(this.path);
          return  this.menuServise.pmbackup;
        }))
      .pipe(
        switchMap((pmBackup: any) => {
          this.sidebarnavItemsPmBackup = pmBackup;

          // Active menu
          this.sidebarnavItemsPmBackup.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
              }
            }
          ));
          this.addExpandClass(this.path);
          return this.menuServise.analyzeryearly;
        }))
      .pipe(
        switchMap((yr: any) => {
          this.sidebarnavItemsAnalyzerYr = yr;

          // Active menu
          this.sidebarnavItemsAnalyzerYr.filter(m => m.submenu.filter(
            (s) => {
              if (s.path === this.router.url) {
                this.path = m.title;
              }
            }
          ));
          this.addExpandClass(this.path);
          return   this.menuServise.analyzerbackup;
        }))
      .subscribe((bk: any) => {
      this.sidebarnavItemsAnalyzerBk = bk;

      // Active menu
      this.sidebarnavItemsAnalyzerBk.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    }, (error) => {
      console.log(error);
    });*/

    this.menuServise.items.subscribe(menuItems => {
      this.sidebarnavItems = menuItems;
      // Active menu
      this.sidebarnavItems.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
        this.submenuPath= m.submenu[0].title;
          }
        }
      ));
      this.addExpandClass(this.path);
     this.addActiveClass(this.submenuPath);
    });

    this.menuServise.rmitems.subscribe(rmmenuItems => {
      this.sidebarnavItemsRm = rmmenuItems;

      // Active menu
      this.sidebarnavItemsRm.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
            this.submenuPath= m.submenu[0].title;
          }
        }
      ));
      this.addExpandClass(this.path);
      this.addActiveClass(this.submenuPath);
    });

    this.menuServise.fgitems.subscribe(fgmenuItems => {
      this.sidebarnavItemsFg = fgmenuItems;

      // Active menu
      this.sidebarnavItemsFg.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
            this.submenuPath= m.submenu[0].title;
          }
        }
      ));
      this.addExpandClass(this.path);
      this.addActiveClass(this.submenuPath);
    });

    this.menuServise.adminitems.subscribe(adminMenuItems => {
      this.sidebarnavItemsAdmin = adminMenuItems;

      // Active menu
      this.sidebarnavItemsAdmin.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    });

    this.menuServise.analyzeritems.subscribe(analyzerMenuItems => {
      this.sidebarnavItemsAnalyzer = analyzerMenuItems;

      // Active menu
      this.sidebarnavItemsAnalyzer.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    });

    this.menuServise.fgyearly.subscribe(fgYearlyItems => {
      this.sidebarnavItemsFgYearly = fgYearlyItems;

      // Active menu
      this.sidebarnavItemsFgYearly.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
            this.submenuPath= m.submenu[0].title;
          }
        }
      ));
      this.addExpandClass(this.path);
      this.addActiveClass(this.submenuPath);
    });

    this.menuServise.pmyearly.subscribe(pmYearlyItems => {
      this.sidebarnavItemsPmYearly = pmYearlyItems;

      // Active menu
      this.sidebarnavItemsPmYearly.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
            this.submenuPath= m.submenu[0].title;
          }
        }
      ));
      this.addExpandClass(this.path);
      this.addActiveClass(this.submenuPath);
    });

    this.menuServise.rmyearly.subscribe(rmYearlyItems => {
      this.sidebarnavItemsRmYearly = rmYearlyItems;

      // Active menu
      this.sidebarnavItemsRmYearly.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
            this.submenuPath= m.submenu[0].title;
          }
        }
      ));
      this.addExpandClass(this.path);
      this.addActiveClass(this.submenuPath);
    });

    this.menuServise.fgbackup.subscribe(fgBackup => {
      this.sidebarnavItemsFgBackup = fgBackup;

      // Active menu
      this.sidebarnavItemsFgBackup.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    });

    this.menuServise.rmbackup.subscribe(rmBackup => {
      this.sidebarnavItemsRmBackup = rmBackup;

      // Active menu
      this.sidebarnavItemsRmBackup.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    });

    this.menuServise.pmbackup.subscribe(pmBackup => {
      this.sidebarnavItemsPmBackup = pmBackup;

      // Active menu
      this.sidebarnavItemsPmBackup.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    });

    this.menuServise.analyzeryearly.subscribe(yr => {
      this.sidebarnavItemsAnalyzerYr = yr;

      // Active menu
      this.sidebarnavItemsAnalyzerYr.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    });

    this.menuServise.analyzerbackup.subscribe(bk => {
      this.sidebarnavItemsAnalyzerBk = bk;

      // Active menu
      this.sidebarnavItemsAnalyzerBk.filter(m => m.submenu.filter(
        (s) => {
          if (s.path === this.router.url) {
            this.path = m.title;
          }
        }
      ));
      this.addExpandClass(this.path);
    });


  }

  ngOnInit(){
    this.modulePage=this.executionFlowService.moduleWisePage;
    /* this.executionFlowService.getMpsHeaderData().subscribe((headerResponse:any)=>{
       if(headerResponse !== null && headerResponse !== undefined){
       this.mpsHeaderList = headerResponse;
      /!* this.selectPlan(this.mpsHeaderList[0]);*!/
       this.executionFlowService.selectedHeaderId = this.mpsHeaderList[0].id;
       this.planName = this.mpsHeaderList[0].mpsName;
       }
     }, (error) => {
       console.log(error);
     });*/
  }


  backClicked() {
    this.location.back();
  }
  homePage(){
    this.router.navigateByUrl('/homePage');

  }

  /*selectPlan(row:any){
    this.selectedPlanId = row.id;
      this.selectedPlan =row.mpsName;
      this.selectedPlanSubmit = false;
  }

  submitPlan(){
    this.planName =this.selectedPlan;
    this.executionFlowService.selectedHeaderId =this.selectedPlanId;
    this.selectedPlanSubmit = true;
  }*/

  addExpandClass(element: any) {
    if (element === this.showMenu) {
      this.showMenu = element;
    } else {
      this.showMenu = element;
    }
  }


  addActiveClass(element: any) {
    if (element === this.showSubMenu) {
      this.showSubMenu = element;
    } else {
      this.showSubMenu = element;
    }
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  logOutPage(){
    this.router.navigateByUrl('authentication/login');
  }
}
