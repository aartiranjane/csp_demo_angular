import { RouteInfo } from './horizontal-sidebar.metadata';



export const RMYEARLY: RouteInfo[] = [
  {
    path: "",
    title: 'MPS Triangle Analytics',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/rmYearlyModule/rmDeltaTabs",
        title: "MPS Triangle Analytics",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'5px',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },


  {
    path: "",
    title: 'SLOB Movement',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/rmYearlyModule/rmSlobMovementTab",
        title: "SLOB Movement",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },
  {
    path: "",
    title: 'Inventory Quality Index',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/rmYearlyModule/rmInventoryQuality",
        title: "Inventory Quality Index",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },



];


