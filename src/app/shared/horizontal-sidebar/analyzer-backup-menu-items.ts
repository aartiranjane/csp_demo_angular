import { RouteInfo } from './horizontal-sidebar.metadata';



export const ANALYZERBACKUPROUTES: RouteInfo[] = [
  {
    path: "",
    title: 'Backup',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/analyzerBk/allBackup/bkPlan",
        title: "Backup",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'5px',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },

];



