import { RouteInfo } from './horizontal-sidebar.metadata';

export const ADMINROUTES: RouteInfo[] = [

  {
    path: "",
    title: 'User Management',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/administrator/userManagement/organisation",
        title: "User Management",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'',
        fontSize:'',
        fontWeight:'',
        height:'',
        submenu: [],
      },
      /* {
         path: "",
         title: "BOM",
         icon: "mdi mdi-adjust",
         class: "",
         ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
         submenu: [],
       },*/

    ]
  },
  /*{
    path: "",
    title: 'Plan Management',
    icon: 'mdi mdi-cart',
    class: 'has-arrow',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/administrator/planManagement",
        title: "Plan Management",
        icon: "mdi mdi-adjust",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'',
        fontSize:'',
        fontWeight:'',
        height:'',
        submenu: [],
      }
    ]
  },
  {
    path: "",
    title: 'Masters Management',
    icon: 'mdi mdi-cart',
    class: 'has-arrow',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/administrator/masters",
        title: "Masters",
        icon: "mdi mdi-adjust",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'',
        fontSize:'',
        fontWeight:'',
        height:'',
        submenu: [],
      }
    ]
  },*/
];

/*export const ADMINROUTES: RouteInfo[] = [
  {
    path: "",
    title: 'Plan Management',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/administrator/planManagement",
        title: "Plan Amendment",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'5px',
       color:'grey',
        fontSize:'14px',
        fontWeight:'400',
       height:'28px',
        submenu: [],
      }
    ]
  },
  {
    path: "",
    title: 'Masters Management',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "",
        title: "FG Masters",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
       color:'grey',
        fontSize:'14px',
        fontWeight:'400',
       height:'28px',
        submenu: [],
      },
      {
        path: "/administrator/masters",
        title: "PM Masters",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
       color:'grey',
        fontSize:'14px',
        fontWeight:'400',
       height:'28px',
        submenu: [],
      },
      {
        path: "",
        title: "RM Masters",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
       height:'28px',
        submenu: [],
      },


    ]
  }
];*/


/*export const ADMINROUTES: RouteInfo[] = [
  {
    path: "",
    title: 'User Management',
    icon: 'mdi mdi-calendar-text',
    class: 'has-arrow',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    submenu: [
      {
        path: "/administrator/userManagement/organisation",
        title: "User Management",
        icon: "mdi mdi-adjust",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        submenu: [],
      },
      /!* {
         path: "",
         title: "BOM",
         icon: "mdi mdi-adjust",
         class: "",
         ddclass: '',
         extralink: false,
         bgNavColor:'',
         submenu: [],
       },*!/

    ]
  },
  {
    path: "",
    title: 'Plan Management',
    icon: 'mdi mdi-cart',
    class: 'has-arrow',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    submenu: [
      {
        path: "/administrator/planManagement",
        title: "Plan Management",
        icon: "mdi mdi-adjust",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        submenu: [],
      }
    ]
  },
  {
    path: "",
    title: 'Masters Management',
    icon: 'mdi mdi-cart',
    class: 'has-arrow',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    submenu: [
      {
        path: "/administrator/masters",
        title: "Masters",
        icon: "mdi mdi-adjust",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        submenu: [],
      }
    ]
  },
];*/
