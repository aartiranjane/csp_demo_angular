// Sidebar route metadata
export interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  ddclass: string;
  extralink: boolean;
  bgNavColor: any;
  color:any;
  fontSize:any;
  fontWeight:any;
  height:any;
  submenu: RouteInfo[];
}
