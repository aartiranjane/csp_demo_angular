import { RouteInfo } from './horizontal-sidebar.metadata';



export const PMBACKUPROUTES: RouteInfo[] = [
  {
    path: "",
    title: 'Backup',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/pmBackupPage/pmBackup/pmPlan",
        title: "Backup",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'5px',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },

];



