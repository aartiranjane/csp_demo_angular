import { RouteInfo } from './horizontal-sidebar.metadata';



export const FGYEARLY: RouteInfo[] = [
  {
    path: "",
    title: 'MPS Triangle Analytics',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/fgYearlyModule/deltaTab",
        title: "MPS Triangle Analytics",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'5px',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },
  {
    path: "",
    title: 'Line Triangle Analytics',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/fgYearlyModule/lineDeltaAnalytics",
        title: "Line Triangle Analytics",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },

  {
    path: "",
    title: 'Skid Triangle Analytics',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/fgYearlyModule/skidDeltaAnalytics",
        title: "Skid Triangle Analytics",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },

  {
    path: "",
    title: 'SLOB Movement',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/fgYearlyModule/fgSlobMovementTab",
        title: "SLOB Movement",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  },
  {
    path: "",
    title: 'Inventory Quality Index',
    icon: '',
    class: '',
    ddclass: '',
    extralink: false,
    bgNavColor:'',
    color:'',
    fontSize:'',
    fontWeight:'',
    height:'',
    submenu: [
      {
        path: "/fgYearlyModule/fgInventoryQuality",
        title: "Inventory Quality Index",
        icon: "",
        class: "",
        ddclass: '',
        extralink: false,
        bgNavColor:'',
        color:'grey',
        fontSize:'14px',
        fontWeight:'400',
        height:'28px',
        submenu: [],
      }
    ]
  }
];


