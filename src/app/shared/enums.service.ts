import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {UriService} from '../uri.service';
import {throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable()
export class EnumsService {
    constructor(private http: HttpClient, private uriService: UriService) {
    }

    getServiceTypes() {
        return this.http.get(this.uriService.getResourceServerUri() + '/serviceTypeList', httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getOrganizationTypes() {
        return this.http.get(this.uriService.getResourceServerUri() + '/organizationTypeList', httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getShipmentDivisions() {
        return this.http.get(this.uriService.getResourceServerUri() + '/shipmentDivision', httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }


    getDocumentTypes() {
        return this.http.get(this.uriService.getResourceServerUri() + '/documentTypes', httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getLabelPriority() {
        return this.http.get(this.uriService.getResourceServerUri() + '/labelPriority', httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    errorHandler(error: Response) {
        return throwError(error || 'Server Error');
    }

}
