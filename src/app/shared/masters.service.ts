import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {UriService} from '../uri.service';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable()
export class MastersService {
    constructor(private http: HttpClient, private uriService: UriService) {
    }

    getDocumentsWithDocumentTypeShipment() {
        return this.http.get(this.uriService.getResourceServerUri() + '/document/documentType/shipment',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getTransportModeList() {
        return this.http.get(this.uriService.getResourceServerUri() + '/transportMode',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getCityList() {
        return this.http.get(this.uriService.getResourceServerUri() + '/city',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getRoleList() {
        return this.http.get(this.uriService.getResourceServerUri() + '/roles',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getShipmentTypeList() {
        return this.http.get(this.uriService.getResourceServerUri() + '/shipmentType',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getContainerTypeList() {
        return this.http.get(this.uriService.getResourceServerUri() + '/containerType',
            httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getShipmentType(id: number) {
        return this.http.get(this.uriService.getResourceServerUri() + '/shipmentType/' + id, httpOptions).pipe(map((response: Response) => response),
            catchError(this.errorHandler));
    }

    getAllCurrency() {
        return this.http.get(this.uriService.getResourceServerUri() + '/currency')
            .pipe(map((response: Response) => response),
                catchError(this.errorHandler));
    }

    errorHandler(error: Response) {
        return throwError(error || 'Server Error');
    }
}
