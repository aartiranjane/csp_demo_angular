import {RouterModule, Routes} from "@angular/router";
import {FullComponent} from "./layouts/full/full.component";
import {MastersComponent} from "./masters/masters.component";
import {RscDtDivisionComponent} from "./masters/rsc-dt-division/rsc-dt-division.component";
import {RscDtItemCodeComponent} from "./masters/rsc-dt-item-code/rsc-dt-item-code.component";
import {RscDtUomComponent} from "./masters/rsc-dt-uom/rsc-dt-uom.component";
import {RscDtStockTypeComponent} from "./masters/rsc-dt-stock-type/rsc-dt-stock-type.component";
import {RSCDTItemCategoryComponent} from "./masters/rsc-dt-item-category/rsc-dt-item-category.component";
import {RscDtSailingDaysComponent} from "./masters/rsc-dt-sailing-days/rsc-dt-sailing-days.component";
import {RscDtCoverDaysComponent} from "./masters/rsc-dt-cover-days/rsc-dt-cover-days.component";
import {RscDtTechnicalSeriesComponent} from "./masters/rsc-dt-technical-series/rsc-dt-technical-series.component";
import {RscDtSupplierComponent} from "./masters/rsc-dt-supplier/rsc-dt-supplier.component";
import {RscDtLinesComponent} from "./masters/rsc-dt-lines/rsc-dt-lines.component";
import {RscDtSafetyStockComponent} from "./masters/rsc-dt-safety-stock/rsc-dt-safety-stock.component";
import {RscDtMoqComponent} from "./masters/rsc-dt-moq/rsc-dt-moq.component";
import {RscDtProductionTypeComponent} from "./masters/rsc-dt-production-type/rsc-dt-production-type.component";
import {RscDtTransportTypeComponent} from "./masters/rsc-dt-transport-type/rsc-dt-transport-type.component";
import {RscDtItemClassComponent} from "./masters/rsc-dt-item-class/rsc-dt-item-class.component";
import {RscDtTransportModeComponent} from "./masters/rsc-dt-transport-mode/rsc-dt-transport-mode.component";
import {RscDtMouldComponent} from "./masters/rsc-dt-mould/rsc-dt-mould.component";
import {RscDtStdPriceComponent} from "./masters/rsc-dt-std-price/rsc-dt-std-price.component";
import {RscDtMaterialCategoryComponent} from "./masters/rsc-dt-material-category/rsc-dt-material-category.component";
import {RscDtMapComponent} from "./masters/rsc-dt-map/rsc-dt-map.component";
import {PlanSummaryComponent} from "./plan-summary/plan-summary.component";
import {RscDtSkidsComponent} from "./masters/rsc-dt-skids/rsc-dt-skids.component";
import {RscDtReasonTypeComponent} from "./masters/rsc-dt-reason-type/rsc-dt-reason-type.component";
import {RscDtReasonComponent} from "./masters/rsc-dt-reason/rsc-dt-reason.component";
import {RscDtTagComponent} from "./masters/rsc-dt-tag/rsc-dt-tag.component";
import {NewExecutionComponent} from "./new-execution/new-execution.component";
import {NewPmComponent} from "./new-execution/new-pm/new-pm.component";
import {MpsNewComponent} from "./new-execution/new-pm/mps-new/mps-new.component";
import {ConsumptionNewComponent} from "./new-execution/new-pm/consumption-new/consumption-new.component";
import {LogicalConsumptionNewComponent} from "./new-execution/new-pm/consumption-new/logical-consumption-new/logical-consumption-new.component";
import {GrossConsumptionNewComponent} from "./new-execution/new-pm/consumption-new/gross-consumption-new/gross-consumption-new.component";
import {PurchasePlanNewComponent} from "./new-execution/new-pm/purchase-plan-new/purchase-plan-new.component";
import {MouldSaturationNewComponent} from "./new-execution/new-pm/mould-saturation-new/mould-saturation-new.component";
import {MouldWiseNewComponent} from "./new-execution/new-pm/mould-saturation-new/mould-wise-new/mould-wise-new.component";
import {SupplierWiseNewComponent} from "./new-execution/new-pm/mould-saturation-new/supplier-wise-new/supplier-wise-new.component";
import {SlobNewComponent} from "./new-execution/new-pm/slob-new/slob-new.component";
import {SlobTabNewComponent} from "./new-execution/new-pm/slob-new/slob-tab-new/slob-tab-new.component";
import {CoverdaysTabNewComponent} from "./new-execution/new-pm/slob-new/coverdays-tab-new/coverdays-tab-new.component";
import {SummaryTabNewComponent} from "./new-execution/new-pm/slob-new/summary-tab-new/summary-tab-new.component";
import {NewRmComponent} from "./new-execution/new-rm/new-rm.component";
import {NewRmMpsComponent} from "./new-execution/new-rm/new-rm-mps/new-rm-mps.component";
import {NewBulkConsumptionComponent} from "./new-execution/new-rm/new-bulk-consumption/new-bulk-consumption.component";
import {NewBulkLogicalComponent} from "./new-execution/new-rm/new-bulk-consumption/new-bulk-logical/new-bulk-logical.component";
import {NewBulkGrossComponent} from "./new-execution/new-rm/new-bulk-consumption/new-bulk-gross/new-bulk-gross.component";
import {NewRmConsumptionComponent} from "./new-execution/new-rm/new-rm-consumption/new-rm-consumption.component";
import {NewRmLogicalComponent} from "./new-execution/new-rm/new-rm-consumption/new-rm-logical/new-rm-logical.component";
import {NewRmGrossComponent} from "./new-execution/new-rm/new-rm-consumption/new-rm-gross/new-rm-gross.component";
import {NewRmPurchasePlanComponent} from "./new-execution/new-rm/new-rm-purchase-plan/new-rm-purchase-plan.component";
import {SlobComponent} from "./new-execution/new-rm/slob/slob.component";
import {CoverDaysComponent} from "./new-execution/new-rm/cover-days/cover-days.component";
import {SummaryComponent} from "./new-execution/new-rm/summary/summary.component";
import {FgModuleComponent} from "./new-execution/fg-module/fg-module.component";
import {LineSaturationComponent} from "./new-execution/fg-module/line-saturation/line-saturation.component";
import {SkidSaturationComponent} from "./new-execution/fg-module/skid-saturation/skid-saturation.component";
import {FgSlobComponent} from "./new-execution/fg-module/fg-slob/fg-slob.component";
import {FgMpsComponent} from "./new-execution/fg-module/fg-mps/fg-mps.component";
import {AdministratorPageComponent} from "./new-execution/Administrator/administrator-page/administrator-page.component";
import {OrganisationComponent} from "./new-execution/Administrator/administrator-page/organisation/organisation.component";
import {UsersComponent} from "./new-execution/Administrator/administrator-page/users/users.component";
import {ExportFlowComponent} from "./export-flow/export-flow.component";
import {UserManagementComponent} from "./new-execution/Administrator/administrator-page/user-management/user-management.component";
import {PlanManagementComponent} from "./new-execution/Administrator/administrator-page/plan-management/plan-management.component";
import {AnalyzerComponent} from "./new-execution/analyzer/analyzer/analyzer.component";
import {AllReportsComponent} from "./new-execution/analyzer/analyzer/all-reports/all-reports.component";
import {PmReportsComponent} from "./new-execution/new-pm/pm-reports/pm-reports.component";
import {RmReportsComponent} from "./new-execution/new-rm/rm-reports/rm-reports.component";
import {FgReportsComponent} from "./new-execution/fg-module/fg-reports/fg-reports.component";
import {PmBackupComponent} from "./new-execution/new-pm/pm-backup/pm-backup.component";
import {AllBackupComponent} from "./new-execution/analyzer/analyzer/all-backup/all-backup.component";
import {RmBackupComponent} from "./new-execution/new-rm/rm-backup/rm-backup.component";
import {FgBackupComponent} from "./new-execution/fg-module/fg-backup/fg-backup.component";
import {PmStockMesComponent} from "./new-execution/new-pm/Stock/pm-stock-mes/pm-stock-mes.component";
import {PmStockReceiptComponent} from "./new-execution/new-pm/Stock/pm-stock-receipt/pm-stock-receipt.component";
import {RmStockMesComponent} from "./new-execution/new-rm/Stock/rm-stock-mes/rm-stock-mes.component";
import {RmStockReceiptComponent} from "./new-execution/new-rm/Stock/rm-stock-receipt/rm-stock-receipt.component";
import {BulkOpRmStockComponent} from "./new-execution/new-rm/Stock/bulk-op-rm-stock/bulk-op-rm-stock.component";
import {RmTransferStockComponent} from "./new-execution/new-rm/Stock/rm-transfer-stock/rm-transfer-stock.component";
import {StockRejectionComponent} from "./new-execution/new-rm/Stock/stock-rejection/stock-rejection.component";
import {NewRmSummaryComponent} from "./new-execution/new-rm/new-rm-summary/new-rm-summary.component";
import {FgSlobSummaryComponent} from "./new-execution/fg-module/fg-slob-summary/fg-slob-summary.component";
import {FgDashboardComponent} from "./new-execution/fg-module/fg-dashboard/fg-dashboard.component";
import {MpsDashTabComponent} from "./new-execution/fg-module/fg-dashboard/mps-dash-tab/mps-dash-tab.component";
import {SlobDashTabComponent} from "./new-execution/fg-module/fg-dashboard/slob-dash-tab/slob-dash-tab.component";
import {LineSaturationTabComponent} from "./new-execution/fg-module/fg-dashboard/saturation-dash-tab/line-saturation-tab/line-saturation-tab.component";
import {SkidsSaturationTabComponent} from "./new-execution/fg-module/fg-dashboard/saturation-dash-tab/skids-saturation-tab/skids-saturation-tab.component";
import {FgPlanningHorizonComponent} from "./new-execution/fg-module/fg-planning-horizon/fg-planning-horizon.component";
import {DeltaAnalyticsComponent} from "./new-execution/fg-module/fg-planning-horizon/delta-analytics/delta-analytics.component";
import {PmMpsDashTabComponent} from "./new-execution/new-pm/analytics-dashboard/pm-mps-dash-tab/pm-mps-dash-tab.component";
import {PmSaturationDashTabComponent} from "./new-execution/new-pm/analytics-dashboard/pm-saturation-dash-tab/pm-saturation-dash-tab.component";
import {PmSlobDashTabComponent} from "./new-execution/new-pm/analytics-dashboard/pm-slob-dash-tab/pm-slob-dash-tab.component";
import {PmPlanningHorizonComponent} from "./new-execution/new-pm/pm-planning-horizon/pm-planning-horizon.component";
import {PmOtifDashComponent} from "./new-execution/new-pm/pm-planning-horizon/pm-otif/pm-otif-dash/pm-otif-dash.component";
import {PmDataDashComponent} from "./new-execution/new-pm/pm-planning-horizon/pm-otif/pm-data-dash/pm-data-dash.component";
import {PmAnalyticsDashboardComponent} from "./new-execution/new-pm/analytics-dashboard/pm-analytics-dashboard.component";
import {RmAnalyticsDashboardComponent} from "./new-execution/new-rm/rm-analytics-dashboard/rm-analytics-dashboard.component";
import {RmMpsDashTabComponent} from "./new-execution/new-rm/rm-analytics-dashboard/rm-mps-dash-tab/rm-mps-dash-tab.component";
import {RmSlobDashTabComponent} from "./new-execution/new-rm/rm-analytics-dashboard/rm-slob-dash-tab/rm-slob-dash-tab.component";
import {RmPlanningHorizonComponent} from "./new-execution/new-rm/rm-planning-horizon/rm-planning-horizon.component";
import {RmOtifComponent} from "./new-execution/new-rm/rm-planning-horizon/rm-otif/rm-otif.component";
import {PurchasePlanRmCoverDaysComponent} from "./new-execution/new-rm/purchase-plan-rm-cover-days/purchase-plan-rm-cover-days.component";
import {RmPurchaseCoverdaysComponent} from "./new-execution/new-rm/purchase-plan-rm-cover-days/rm-purchase-coverdays/rm-purchase-coverdays.component";
import {RmPurchaseStockEquationComponent} from "./new-execution/new-rm/purchase-plan-rm-cover-days/rm-purchase-stock-equation/rm-purchase-stock-equation.component";
import {PurchasePlanPmCoverDaysComponent} from "./new-execution/new-pm/purchase-plan-pm-cover-days/purchase-plan-pm-cover-days.component";
import {PmPurchaseCoverDaysComponent} from "./new-execution/new-pm/purchase-plan-pm-cover-days/pm-purchase-cover-days/pm-purchase-cover-days.component";
import {PmPurchaseStockEquationComponent} from "./new-execution/new-pm/purchase-plan-pm-cover-days/pm-purchase-stock-equation/pm-purchase-stock-equation.component";
import {PmSlobMovementDashComponent} from "./new-execution/new-pm/pm-planning-horizon/pm-otif/pm-slob-movement-dash/pm-slob-movement-dash.component";
import {PmInventoryQualityIndexDashComponent} from "./new-execution/new-pm/pm-planning-horizon/pm-otif/pm-inventory-quality-index-dash/pm-inventory-quality-index-dash.component";
import {RmSlobMovementDashComponent} from "./new-execution/new-rm/rm-planning-horizon/rm-slob-movement-dash/rm-slob-movement-dash.component";
import {RmInventoryQualityIndexDashComponent} from "./new-execution/new-rm/rm-planning-horizon/rm-inventory-quality-index-dash/rm-inventory-quality-index-dash.component";
import {FgSlobMovementDashComponent} from "./new-execution/fg-module/fg-planning-horizon/fg-slob-movement-dash/fg-slob-movement-dash.component";
import {FgInventoryQualityIndexDashComponent} from "./new-execution/fg-module/fg-planning-horizon/fg-inventory-quality-index-dash/fg-inventory-quality-index-dash.component";
import {PmMasterDataComponent} from "./new-execution/new-pm/pm-master-data/pm-master-data.component";
import {RmMasterDataComponent} from "./new-execution/new-rm/rm-master-data/rm-master-data.component";
import {FgMasterDataComponent} from "./new-execution/fg-module/fg-master-data/fg-master-data.component";
import {PmMaterialModalComponent} from "./new-execution/new-pm/pm-master-data/pm-material-modal/pm-material-modal.component";
import {MaterialCategoryModalComponent} from "./new-execution/new-pm/pm-master-data/material-category-modal/material-category-modal.component";
import {MouldsModalComponent} from "./new-execution/new-pm/pm-master-data/moulds-modal/moulds-modal.component";
import {PmSupplierModalComponent} from "./new-execution/new-pm/pm-master-data/pm-supplier-modal/pm-supplier-modal.component";
import {StockReceiptModalComponent} from "./new-execution/new-pm/pm-master-data/stock-receipt-modal/stock-receipt-modal.component";
import {StockMidNightModalComponent} from "./new-execution/new-pm/pm-master-data/stock-mid-night-modal/stock-mid-night-modal.component";
import {SlobReasonModalComponent} from "./new-execution/new-pm/pm-master-data/slob-reason-modal/slob-reason-modal.component";
import {SlobRemarkModalComponent} from "./new-execution/new-pm/pm-master-data/slob-remark-modal/slob-remark-modal.component";
import {SlobMapModalComponent} from "./new-execution/new-pm/pm-master-data/slob-map-modal/slob-map-modal.component";
import {SlobStdpriceModalComponent} from "./new-execution/new-pm/pm-master-data/slob-stdprice-modal/slob-stdprice-modal.component";
import {PurchaseRemarkModalComponent} from "./new-execution/new-pm/pm-master-data/purchase-remark-modal/purchase-remark-modal.component";
import {RmMaterialModalComponent} from "./new-execution/new-rm/rm-master-data/rm-material-modal/rm-material-modal.component";
import {RmSupplierModalComponent} from "./new-execution/new-rm/rm-master-data/rm-supplier-modal/rm-supplier-modal.component";
import {RmSlobReasonModalComponent} from "./new-execution/new-rm/rm-master-data/rm-slob-reason-modal/rm-slob-reason-modal.component";
import {RmSlobRemarkModalComponent} from "./new-execution/new-rm/rm-master-data/rm-slob-remark-modal/rm-slob-remark-modal.component";
import {RmSlobMapModalComponent} from "./new-execution/new-rm/rm-master-data/rm-slob-map-modal/rm-slob-map-modal.component";
import {RmSlobStdpriceModalComponent} from "./new-execution/new-rm/rm-master-data/rm-slob-stdprice-modal/rm-slob-stdprice-modal.component";
import {RmStockMidNightModalComponent} from "./new-execution/new-rm/rm-master-data/rm-stock-mid-night-modal/rm-stock-mid-night-modal.component";
import {RmStockReceiptModalComponent} from "./new-execution/new-rm/rm-master-data/rm-stock-receipt-modal/rm-stock-receipt-modal.component";
import {RmBulkoprmModalComponent} from "./new-execution/new-rm/rm-master-data/rm-bulkoprm-modal/rm-bulkoprm-modal.component";
import {RmTransferStockModalComponent} from "./new-execution/new-rm/rm-master-data/rm-transfer-stock-modal/rm-transfer-stock-modal.component";
import {RmRejectionModalComponent} from "./new-execution/new-rm/rm-master-data/rm-rejection-modal/rm-rejection-modal.component";
import {FgMaterialModalComponent} from "./new-execution/fg-module/fg-master-data/fg-material-modal/fg-material-modal.component";
import {FgLinesModalComponent} from "./new-execution/fg-module/fg-master-data/fg-lines-modal/fg-lines-modal.component";
import {FgSkidModalComponent} from "./new-execution/fg-module/fg-master-data/fg-skid-modal/fg-skid-modal.component";
import {FgSlobReasonModalComponent} from "./new-execution/fg-module/fg-master-data/fg-slob-reason-modal/fg-slob-reason-modal.component";
import {FgSlobRemarkModalComponent} from "./new-execution/fg-module/fg-master-data/fg-slob-remark-modal/fg-slob-remark-modal.component";
import {FgSlobMapModalComponent} from "./new-execution/fg-module/fg-master-data/fg-slob-map-modal/fg-slob-map-modal.component";
import {FgSlobStdpriceModalComponent} from "./new-execution/fg-module/fg-master-data/fg-slob-stdprice-modal/fg-slob-stdprice-modal.component";
import {RmPivotComponent} from "./new-execution/new-rm/rm-pivot/rm-pivot.component";
import {PmDeltaAnalyticsComponent} from "./new-execution/new-pm/pm-planning-horizon/pm-otif/pm-delta-analytics/pm-delta-analytics.component";
import {AnalyzerMpsDashComponent} from "./new-execution/analyzer/common-analytics/analyzer-mps-dash/analyzer-mps-dash.component";
import {CommonAnalyticsComponent} from "./new-execution/analyzer/common-analytics/common-analytics.component";
import {PmOtifCummulativeScoreComponent} from "./new-execution/new-pm/pm-planning-horizon/pm-otif-cummulative-score/pm-otif-cummulative-score.component";
import {LineDeltaAnalyticsComponent} from "./new-execution/fg-module/fg-planning-horizon/line-delta-analytics/line-delta-analytics.component";
import {SkidDeltaAnalyticsComponent} from "./new-execution/fg-module/fg-planning-horizon/skid-delta-analytics/skid-delta-analytics.component";
import {RmBulkMaterialModalComponent} from "./new-execution/new-rm/rm-master-data/rm-bulk-material-modal/rm-bulk-material-modal.component";
import {RmSubBasesMaterialModalComponent} from "./new-execution/new-rm/rm-master-data/rm-sub-bases-material-modal/rm-sub-bases-material-modal.component";
import {FgWipMaterialModalComponent} from "./new-execution/fg-module/fg-master-data/fg-wip-material-modal/fg-wip-material-modal.component";
import {HomePageComponent} from "./new-execution/home-page/home-page.component";
import {PlanAModuleComponent} from "./new-execution/plan-a-module/plan-a-module.component";
import {GoToPlanModuleComponent} from "./new-execution/plan-a-module/go-to-plan-module/go-to-plan-module.component";
import {PlanPageComponent} from "./new-execution/plan-a-module/plan-page/plan-page.component";
import {GoToValidationComponent} from "./new-execution/plan-a-module/go-to-validation/go-to-validation.component";
import {GoToExecutionComponent} from "./new-execution/plan-a-module/go-to-execution/go-to-execution.component";
import {RmDeltaAnalyticsComponent} from "./new-execution/new-rm/rm-planning-horizon/rm-delta-analytics/rm-delta-analytics.component";
import {PlanningHorizonAnalyzerComponent} from "./new-execution/analyzer/planning-horizon-analyzer/planning-horizon-analyzer.component";
import {CommonMpsDeltaAnalyticsComponent} from "./new-execution/analyzer/planning-horizon-analyzer/common-mps-delta-analytics/common-mps-delta-analytics.component";
import {FgReportsPageComponent} from "./new-execution/fg-module/fg-reports-page/fg-reports-page.component";
import {AllReportsPageComponent} from "./new-execution/analyzer/analyzer/all-reports-page/all-reports-page.component";
import {AllOtifComponent} from "./new-execution/analyzer/analyzer/all-otif/all-otif.component";
import {PmReportsPageComponent} from "./new-execution/new-pm/pm-reports-page/pm-reports-page.component";
import {PmReportsOtifComponent} from "./new-execution/new-pm/pm-reports-otif/pm-reports-otif.component";
import {RmReportsPageComponent} from "./new-execution/new-rm/rm-reports-page/rm-reports-page.component";
import {BackupPlansTabComponent} from "./new-execution/analyzer/analyzer/backup-plans-tab/backup-plans-tab.component";
import {BackupPmOtifComponent} from "./new-execution/analyzer/analyzer/backup-pm-otif/backup-pm-otif.component";
import {BackupAnnualAnalysisComponent} from "./new-execution/analyzer/analyzer/backup-annual-analysis/backup-annual-analysis.component";
import {AnalyzerBackupPageComponent} from "./new-execution/analyzer/analyzer/analyzer-backup-page/analyzer-backup-page.component";
import {PmBackupPageComponent} from "./new-execution/new-pm/pm-backup/pm-backup-page/pm-backup-page.component";
import {PmBackupPlansComponent} from "./new-execution/new-pm/pm-backup/pm-backup-plans/pm-backup-plans.component";
import {PmBackupOtifComponent} from "./new-execution/new-pm/pm-backup/pm-backup-otif/pm-backup-otif.component";
import {PmBackupAnnualAnalysisComponent} from "./new-execution/new-pm/pm-backup/pm-backup-annual-analysis/pm-backup-annual-analysis.component";
import {FgBackupPageComponent} from "./new-execution/fg-module/fg-backup/fg-backup-page/fg-backup-page.component";
import {FgBackupPlansComponent} from "./new-execution/fg-module/fg-backup/fg-backup-plans/fg-backup-plans.component";
import {FgBackupAnnualAnalysisComponent} from "./new-execution/fg-module/fg-backup/fg-backup-annual-analysis/fg-backup-annual-analysis.component";
import {RmBackupPageComponent} from "./new-execution/new-rm/rm-backup/rm-backup-page/rm-backup-page.component";
import {RmBackupPlansComponent} from "./new-execution/new-rm/rm-backup/rm-backup-plans/rm-backup-plans.component";
import {RmBackupAnnualAnalysisComponent} from "./new-execution/new-rm/rm-backup/rm-backup-annual-analysis/rm-backup-annual-analysis.component";
import {SignInComponent} from "./sign-in/sign-in.component";
import {AuthGuard} from "./auth-guard.service";
import {NgModule} from "@angular/core";
import {RmStockMesWithRejectionComponent} from "./new-execution/new-rm/rm-master-data/rm-stock-mes-with-rejection/rm-stock-mes-with-rejection.component";
import {PmStockMesWithRejectionComponent} from "./new-execution/new-pm/pm-master-data/pm-stock-mes-with-rejection/pm-stock-mes-with-rejection.component";
import {RmHomePageComponent} from "./new-execution/home-page/rm-home-page/rm-home-page.component";
import {PmHomePageComponent} from "./new-execution/home-page/pm-home-page/pm-home-page.component";
import {FgHomePageComponent} from "./new-execution/home-page/fg-home-page/fg-home-page.component";
import {AnalystHomePageComponent} from "./new-execution/home-page/analyst-home-page/analyst-home-page.component";
import {AdminHomePageComponent} from "./new-execution/home-page/admin-home-page/admin-home-page.component";


const routes: Routes = [
  {path: '', redirectTo: "/login", pathMatch: "full"},
  {path: 'login', component: SignInComponent},
  {path:'', component: FullComponent, canActivate: [AuthGuard], children:[
     /* {path: 'homePage', component: HomePageComponent},*/
      {path: 'homePage', component: HomePageComponent,canActivate: [AuthGuard], children: [
          {path: 'rmRole', component: RmHomePageComponent , canActivate: [AuthGuard]},
          {path: 'pmRole', component: PmHomePageComponent , canActivate: [AuthGuard]},
          {path: 'fgRole', component: FgHomePageComponent , canActivate: [AuthGuard]},
          {path: 'analystRole', component: AnalystHomePageComponent , canActivate: [AuthGuard]},
          {path: 'AdminRole', component: AdminHomePageComponent , canActivate: [AuthGuard]},
          {path: '', redirectTo: 'rmRole', pathMatch: 'full'},
          ]},

      // All Modules Common Components
      {path: 'planSummary', component: PlanSummaryComponent, canActivate: [AuthGuard]},
      {path: 'exportFlow', component: ExportFlowComponent, canActivate: [AuthGuard]},
      {path: 'newExecutionflow', component: NewExecutionComponent, canActivate: [AuthGuard]},

      /*******DPM Components**********/
      {path: 'planModule', component: PlanAModuleComponent, canActivate: [AuthGuard], children: [
          {path: 'planPage', component: PlanPageComponent, canActivate: [AuthGuard]},
          {path: 'planModulePage', component: GoToPlanModuleComponent, canActivate: [AuthGuard]},
          {path: 'validationPage', component: GoToValidationComponent, canActivate: [AuthGuard]},
          {path: 'executionPage', component: GoToExecutionComponent, canActivate: [AuthGuard]},
          {path: '', redirectTo: 'planPage', pathMatch: 'full'},

        ]},

      /*******DVM Components**********/

      // FG Module Components
      {path: 'fgModule', children: [
          {path: '', component: FgModuleComponent, canActivate: [AuthGuard]},
          {path: 'fgDashboard', children: [
              {path:'',  component: FgDashboardComponent, canActivate: [AuthGuard]},
              {path: 'mpsTab', component: MpsDashTabComponent, canActivate: [AuthGuard]},
              {path: 'lines', component: LineSaturationTabComponent, canActivate: [AuthGuard]},
              {path: 'skids', component: SkidsSaturationTabComponent, canActivate: [AuthGuard]},
              /*{path: 'saturationTab', component: SaturationDashTabComponent, children: [
                  {path: 'lines', component: LineSaturationTabComponent},
                  {path: 'skids', component: SkidsSaturationTabComponent},
                  {path: "", redirectTo: "lines", pathMatch: "full"}
                ]},*/
              {path: 'slobTab', component: SlobDashTabComponent, canActivate: [AuthGuard]},
            ]
          },
          {
            path: 'planHorizon', children: [
              {path:'',  component: FgPlanningHorizonComponent, canActivate: [AuthGuard]},
              {path: 'deltaTab', component: DeltaAnalyticsComponent, canActivate: [AuthGuard]},
              {path: 'fgSlobMovementTab', component: FgSlobMovementDashComponent, canActivate: [AuthGuard]},
              {path: 'fgInventoryQuality', component: FgInventoryQualityIndexDashComponent, canActivate: [AuthGuard]},
              {path: 'lineDeltaAnalytics', component: LineDeltaAnalyticsComponent, canActivate: [AuthGuard]},
              {path: 'skidDeltaAnalytics', component: SkidDeltaAnalyticsComponent, canActivate: [AuthGuard]},
            ]
          },
          {path: 'fgMps', component: FgMpsComponent, canActivate: [AuthGuard]},
          {path: 'lineSaturation', component: LineSaturationComponent, canActivate: [AuthGuard]},
          {path: 'skidSaturation', component: SkidSaturationComponent, canActivate: [AuthGuard]},
          {path: 'fgSlob', component: FgSlobComponent, canActivate: [AuthGuard]},
          {path: 'fgSlobSummary', component: FgSlobSummaryComponent, canActivate: [AuthGuard]},
         /* {path: 'fgReports', component: FgReportsComponent, canActivate: [AuthGuard]},*/
          {path: 'fgReportsPage', component: FgReportsPageComponent, children: [{path: 'fgReports', component: FgReportsComponent}, {path: "", redirectTo: "fgReports", pathMatch: "full"}]},
          {path: 'fgMasterData', component: FgMasterDataComponent, canActivate: [AuthGuard], children: [
              {path: 'fgMaterialPopup', component: FgMaterialModalComponent, canActivate: [AuthGuard]},
              {path: 'fgWipMaterialPopup', component: FgWipMaterialModalComponent, canActivate: [AuthGuard]},
              {path: 'fgLinesPopup', component: FgLinesModalComponent, canActivate: [AuthGuard]},
              {path: 'fgSkidPopup', component: FgSkidModalComponent, canActivate: [AuthGuard]},
              {path: 'fgSlobReasonPopup', component: FgSlobReasonModalComponent, canActivate: [AuthGuard]},
              {path: 'fgSlobRemarkPopup', component: FgSlobRemarkModalComponent, canActivate: [AuthGuard]},
              {path: 'fgMapPopup', component: FgSlobMapModalComponent, canActivate: [AuthGuard]},
              {path: 'fgStdPricePopup', component: FgSlobStdpriceModalComponent, canActivate: [AuthGuard]}
            ]},
        ]},
      // PM Module Components
      {path: 'newPm',  canActivate: [AuthGuard], children: [
          {path: '', component: NewPmComponent},
          {
            path: 'analyticsDashboard' , children: [
              {path: '', component: PmAnalyticsDashboardComponent },
              {path: 'pmMpsTab', component: PmMpsDashTabComponent},
              {path: 'pmSaturationTab', component: PmSaturationDashTabComponent},
              {path: 'pmSlobTab', component: PmSlobDashTabComponent},
              {path: "", redirectTo: "pmMpsTab", pathMatch: "full"}
            ]
          },
          {
            path: 'pmPlanHorizon', children: [
              {path: '', component: PmPlanningHorizonComponent},
              {path: 'pmOtifTab', component: PmOtifDashComponent},
              {path: 'pmDataTab', component: PmDataDashComponent},
              {path: 'pmSlobMovementTab', component: PmSlobMovementDashComponent},
              {path: 'pmInventoryQuality', component: PmInventoryQualityIndexDashComponent},
              {path: 'pmDeltaAnalytics', component: PmDeltaAnalyticsComponent},
              {path: 'pmOtifCummulativeScore', component: PmOtifCummulativeScoreComponent},
            ]
          },
          {path: 'newMps', component: MpsNewComponent},
          {
            path: 'newConsumption', children: [
              {path: '', component: ConsumptionNewComponent},
              {path: 'newLogicalTab', component: LogicalConsumptionNewComponent},
              {path: 'newGrossTab', component: GrossConsumptionNewComponent},
            ]
          },
          {path: 'purchasePlan', component: PurchasePlanNewComponent},
          {
            path: 'newMouldSaturation', children: [
              {path: '', component: MouldSaturationNewComponent},
              {path: 'mouldWiseTab', component: MouldWiseNewComponent},
              {path: 'supplierWiseTab', component: SupplierWiseNewComponent},
            ]
          },

          {
            path: 'Coverdays&StockEquation', children: [
              {path: '', component: PurchasePlanPmCoverDaysComponent},
              {path: 'pmPurchaseCoverdays', component: PmPurchaseCoverDaysComponent},
              {path: 'pmPurchaseStockEquation', component: PmPurchaseStockEquationComponent},
            ]
          },
          {
            path: 'newSlob', children: [
              {path: '', component: SlobNewComponent},
              {path: 'slobTab', component: SlobTabNewComponent},
              {path: 'coverDaysTab', component: CoverdaysTabNewComponent},
              {path: 'summaryTab', component: SummaryTabNewComponent},
            ]
          },
          /*{path: 'pmReports', component: PmReportsComponent, canActivate: [AuthGuard]},*/
          {path: 'pmReportsPage', component: PmReportsPageComponent,
            children: [{path: 'pmReports', component: PmReportsComponent},
              {path: 'pmOtif', component: PmReportsOtifComponent},
              {path: "", redirectTo: "pmReports", pathMatch: "full"}]},
          {path: 'pmMasterData', component: PmMasterDataComponent, children: [
              {path: 'pmMaterialPopup', component: PmMaterialModalComponent, canActivate: [AuthGuard]},
              {path: 'materialCategoryPopup', component: MaterialCategoryModalComponent, canActivate: [AuthGuard]},
              {path: 'mouldsPopup', component: MouldsModalComponent, canActivate: [AuthGuard]},
              {path: 'pmSupplierPopup', component: PmSupplierModalComponent, canActivate: [AuthGuard]},

              {path: 'purchaseRemarkPopup', component: PurchaseRemarkModalComponent, canActivate: [AuthGuard]},

              {path: 'slobReasonPopup', component: SlobReasonModalComponent, canActivate: [AuthGuard]},
              {path: 'slobRemarkPopup', component: SlobRemarkModalComponent, canActivate: [AuthGuard]},
              {path: 'mapPopup', component: SlobMapModalComponent, canActivate: [AuthGuard]},
              {path: 'stdPricePopup', component: SlobStdpriceModalComponent, canActivate: [AuthGuard]},

              {path: 'midNightPopup', component: StockMidNightModalComponent, canActivate: [AuthGuard]},
              {path: 'receiptPopup', component: StockReceiptModalComponent, canActivate: [AuthGuard]},
              {path: 'pmMesWithRejectionPopup', component: PmStockMesWithRejectionComponent, canActivate: [AuthGuard]},
            ]},
          {path: 'mesPm', component: PmStockMesComponent, canActivate: [AuthGuard]},
          {path: 'receiptPm', component: PmStockReceiptComponent, canActivate: [AuthGuard]},
        ]},
      // RM Module Components
      {path: 'newRm', children: [
          {path: '', component: NewRmComponent, canActivate: [AuthGuard]},
          {
            path: 'rmAnalyticsDashboard', children: [
              {path: '', component: RmAnalyticsDashboardComponent, canActivate: [AuthGuard]},
              {path: 'rmMpsTab', component: RmMpsDashTabComponent, canActivate: [AuthGuard]},
              {path: 'rmSlobTab', component: RmSlobDashTabComponent, canActivate: [AuthGuard]},
              {path: "", redirectTo: "rmMpsTab", pathMatch: "full"}
            ]
          },
          {
            path: 'rmPlanHorizon' , children: [
              {path: '', component: RmPlanningHorizonComponent, canActivate: [AuthGuard]},
              {path: 'rmOtifTab', component: RmOtifComponent, canActivate: [AuthGuard]},
              {path: 'rmSlobMovementTab', component: RmSlobMovementDashComponent, canActivate: [AuthGuard]},
              {path: 'rmInventoryQuality', component: RmInventoryQualityIndexDashComponent, canActivate: [AuthGuard]},


            ]
          },

          {path: 'newRmMps', component: NewRmMpsComponent, canActivate: [AuthGuard]},
          {
            path: 'newBulkConsumption', children: [
              {path: '', component: NewBulkConsumptionComponent, canActivate: [AuthGuard]},
              {path: 'newBulkLogicalTab', component: NewBulkLogicalComponent, canActivate: [AuthGuard]},
              {path: 'newBulkGrossTab', component: NewBulkGrossComponent, canActivate: [AuthGuard]},
            ]
          },
          {path: 'rmPivot', component: RmPivotComponent, canActivate: [AuthGuard]},
          {
            path: 'newRmConsumption', children: [
              {path: '', component: NewRmConsumptionComponent, canActivate: [AuthGuard]},
              {path: 'newRmLogicalTab', component: NewRmLogicalComponent, canActivate: [AuthGuard]},
              {path: 'newRmGrossTab', component: NewRmGrossComponent, canActivate: [AuthGuard]},
            ]
          },
          {path: 'purchasePlanRm', component: NewRmPurchasePlanComponent, canActivate: [AuthGuard]},
          {
            path: 'newMouldSaturation', children: [
              {path: '', component: MouldSaturationNewComponent, canActivate: [AuthGuard]},
              {path: 'mouldWiseTab', component: MouldWiseNewComponent, canActivate: [AuthGuard]},
              {path: 'supplierWiseTab', component: SupplierWiseNewComponent, canActivate: [AuthGuard]},
            ]
          },

          {
            path: 'Coverdays&StockEquation', children: [
              {path: '', component: PurchasePlanRmCoverDaysComponent, canActivate: [AuthGuard]},
              {path: 'rmPurchaseCoverdays', component: RmPurchaseCoverdaysComponent, canActivate: [AuthGuard]},
              {path: 'rmPurchaseStockEquation', component: RmPurchaseStockEquationComponent, canActivate: [AuthGuard]},
            ]
          },
          {path: 'rmSlob', component: SlobComponent, canActivate: [AuthGuard]},
          {path: 'rmCoverDays', component: CoverDaysComponent, canActivate: [AuthGuard]},
          {path: 'rmSummary', component: SummaryComponent, canActivate: [AuthGuard]},
          {path: 'newRmSummary', component: NewRmSummaryComponent, canActivate: [AuthGuard]},
          /*{path: 'rmReports', component: RmReportsComponent, canActivate: [AuthGuard]},*/
          {path: 'rmReportsPage', component: RmReportsPageComponent, children: [{path: 'rmReports', component: RmReportsComponent}, {path: "", redirectTo: "rmReports", pathMatch: "full"}]},
          {path: 'rmMasterData', component: RmMasterDataComponent, children: [
              {path: 'rmMaterialPopup', component: RmMaterialModalComponent, canActivate: [AuthGuard]},
              {path: 'rmBulkMaterialPopup', component: RmBulkMaterialModalComponent, canActivate: [AuthGuard]},
              {path: 'rmSubBasesMaterialPopup', component: RmSubBasesMaterialModalComponent, canActivate: [AuthGuard]},

              {path: 'rmSupplierPopup', component: RmSupplierModalComponent, canActivate: [AuthGuard]},

              {path: 'rmSlobReasonPopup', component: RmSlobReasonModalComponent, canActivate: [AuthGuard]},
              {path: 'rmSlobRemarkPopup', component: RmSlobRemarkModalComponent, canActivate: [AuthGuard]},
              {path: 'rmMapPopup', component: RmSlobMapModalComponent, canActivate: [AuthGuard]},
              {path: 'rmStdPricePopup', component: RmSlobStdpriceModalComponent, canActivate: [AuthGuard]},

              {path: 'rmMidNightPopup', component: RmStockMidNightModalComponent, canActivate: [AuthGuard]},
              {path: 'rmReceiptPopup', component: RmStockReceiptModalComponent, canActivate: [AuthGuard]},
              {path: 'bulkOprmPopup', component: RmBulkoprmModalComponent, canActivate: [AuthGuard]},
              {path: 'transferStockPopup', component: RmTransferStockModalComponent, canActivate: [AuthGuard]},
              {path: 'rejectionPopup', component: RmRejectionModalComponent, canActivate: [AuthGuard]},
              {path: 'mesWithRejectionPopup', component: RmStockMesWithRejectionComponent, canActivate: [AuthGuard]},

            ]},
          {path: 'mesRm', component: RmStockMesComponent, canActivate: [AuthGuard]},
          {path: 'receiptRm', component: RmStockReceiptComponent, canActivate: [AuthGuard]},
          {path: 'bulkOpRm', component: BulkOpRmStockComponent, canActivate: [AuthGuard]},
          {path: 'transferStock', component: RmTransferStockComponent, canActivate: [AuthGuard]},
          {path: 'rejection', component: StockRejectionComponent, canActivate: [AuthGuard]},
        ]},
      // Analyzer Module Components
      {path: 'analyzer', children: [
          {path: '', component: AnalyzerComponent, canActivate: [AuthGuard]},
          {path: 'commonAnalytics', children: [
              {path:'',  component: CommonAnalyticsComponent, canActivate: [AuthGuard]},
              {path: 'mpsTab', component: PmMpsDashTabComponent, canActivate: [AuthGuard]},
              {path: 'fgLinePage', component: LineSaturationTabComponent, canActivate: [AuthGuard]},
              {path: 'fgSkidPage', component: SkidsSaturationTabComponent, canActivate: [AuthGuard]},
              {path: 'fgSlobPage', component: SlobDashTabComponent, canActivate: [AuthGuard]},
              {path: 'pmMouldPage', component: PmSaturationDashTabComponent, canActivate: [AuthGuard]},
              {path: 'pmOtifDataPage', component: PmOtifDashComponent, canActivate: [AuthGuard]},
              {path: 'pmSlobPage', component: PmSlobDashTabComponent, canActivate: [AuthGuard]},
              {path: 'rmSlobPage', component: RmSlobDashTabComponent, canActivate: [AuthGuard]},
            ]},
          /*{path: 'allReports', component: AllReportsComponent, canActivate: [AuthGuard]},*/
          {path: 'allReportsPage', component: AllReportsPageComponent, children: [
              {path: 'allReports', component: AllReportsComponent},
              {path: 'allOtif', component: AllOtifComponent},
              {path: "", redirectTo: "allReports", pathMatch: "full"}
            ]},
          {path: 'allBackup', component: AllBackupComponent, canActivate: [AuthGuard]}
        ]},
      // Administrator Module Components
      {path: 'administrator', children: [
          {path: '', component: AdministratorPageComponent, canActivate: [AuthGuard]},
          {
            path: 'userManagement', component: UserManagementComponent, children: [
              {path: 'organisation', component: OrganisationComponent, canActivate: [AuthGuard]},
              {path: 'users', component: UsersComponent, canActivate: [AuthGuard]},
              {path: "", redirectTo: "organisation", pathMatch: "full"}
            ]
          },
          {path: 'planManagement', component: PlanManagementComponent, canActivate: [AuthGuard]},
          {
            path: 'masters', component: MastersComponent, canActivate: [AuthGuard], children: [
              {path: 'rscDtItemCode', component: RscDtItemCodeComponent, canActivate: [AuthGuard]},
              {path: 'rscDtItemCategory', component: RSCDTItemCategoryComponent, canActivate: [AuthGuard]},
              {path: 'rscDtCoverDays', component: RscDtCoverDaysComponent, canActivate: [AuthGuard]},
              {path: 'rscDtUom', component: RscDtUomComponent, canActivate: [AuthGuard]},
              {path: 'rscDtDivision', component: RscDtDivisionComponent, canActivate: [AuthGuard]},
              {path: 'rscDtSafetyStock', component: RscDtSafetyStockComponent, canActivate: [AuthGuard]},
              {path: 'rscDtItemClass', component: RscDtItemClassComponent, canActivate: [AuthGuard]},
              {path: 'rscDtTechnicalSeries', component: RscDtTechnicalSeriesComponent, canActivate: [AuthGuard]},
              {path: 'rscDtMoq', component: RscDtMoqComponent, canActivate: [AuthGuard]},
              {path: 'rscDtTransportMode', component: RscDtTransportModeComponent, canActivate: [AuthGuard]},
              {path: 'rscDtTransportType', component: RscDtTransportTypeComponent, canActivate: [AuthGuard]},
              {path: 'rscDtMould', component: RscDtMouldComponent, canActivate: [AuthGuard]},
              {path: 'rscDtSupplier', component: RscDtSupplierComponent, canActivate: [AuthGuard]},
              {path: 'rscDtProductionType', component: RscDtProductionTypeComponent, canActivate: [AuthGuard]},
              {path: 'rscDtLines', component: RscDtLinesComponent, canActivate: [AuthGuard]},
              {path: 'rscDtStockType', component: RscDtStockTypeComponent, canActivate: [AuthGuard]},
              {path: 'rscDtSailingDays', component: RscDtSailingDaysComponent, canActivate: [AuthGuard]},
              {path: 'rscDtStdPrice', component: RscDtStdPriceComponent, canActivate: [AuthGuard]},
              {path: 'rscDtMaterialCategory', component: RscDtMaterialCategoryComponent, canActivate: [AuthGuard]},
              {path: 'rscDtMap', component: RscDtMapComponent, canActivate: [AuthGuard]},
              {path: 'rscDtSkids', component: RscDtSkidsComponent, canActivate: [AuthGuard]},
              {path: 'rscDtReasonType', component: RscDtReasonTypeComponent, canActivate: [AuthGuard]},
              {path: 'rscDtReason', component: RscDtReasonComponent, canActivate: [AuthGuard]},
              {path: 'rscDtTag', component: RscDtTagComponent, canActivate: [AuthGuard]},
            ]
          },
        ]
      },

      /*******APA Components**********/

      // FG Module Components
      {path: 'fgYearlyModule', children: [
          {path:'',  component: FgPlanningHorizonComponent, canActivate: [AuthGuard]},
          {path: 'deltaTab', component: DeltaAnalyticsComponent, canActivate: [AuthGuard]},
          {path: 'fgSlobMovementTab', component: FgSlobMovementDashComponent, canActivate: [AuthGuard]},
          {path: 'fgInventoryQuality', component: FgInventoryQualityIndexDashComponent, canActivate: [AuthGuard]},
          {path: 'lineDeltaAnalytics', component: LineDeltaAnalyticsComponent, canActivate: [AuthGuard]},
          {path: 'skidDeltaAnalytics', component: SkidDeltaAnalyticsComponent, canActivate: [AuthGuard]},
        ]},
      // PM Module Components
      {path: 'pmYearlyModule', children: [
          {path: '', component: PmPlanningHorizonComponent, canActivate: [AuthGuard]},
          {path: 'pmDeltaAnalytics', component: PmDeltaAnalyticsComponent, canActivate: [AuthGuard]},
          {path: 'pmSlobMovementTab', component: PmSlobMovementDashComponent, canActivate: [AuthGuard]},
          {path: 'pmInventoryQuality', component: PmInventoryQualityIndexDashComponent, canActivate: [AuthGuard]},
          {path: 'pmOtifCummulativeScore', component: PmOtifCummulativeScoreComponent, canActivate: [AuthGuard]},
        ]},
      // RM Module Components
      {path: 'rmYearlyModule' , children: [
          {path: '', component: RmPlanningHorizonComponent, canActivate: [AuthGuard]},
          {path: 'rmDeltaTabs', component: RmDeltaAnalyticsComponent, canActivate: [AuthGuard]},
          {path: 'rmSlobMovementTab', component: RmSlobMovementDashComponent, canActivate: [AuthGuard]},
          {path: 'rmInventoryQuality', component: RmInventoryQualityIndexDashComponent, canActivate: [AuthGuard]},


        ]},
      // Analyzer Module Components
      {path: 'planningHorizonAnalyzer', children: [
          {path: '', component: PlanningHorizonAnalyzerComponent, canActivate: [AuthGuard]},
          {path: 'commonMpsDelta', component: PmDeltaAnalyticsComponent, canActivate: [AuthGuard]},
          {path: 'fgLineTriangle', component: LineDeltaAnalyticsComponent, canActivate: [AuthGuard]},
          {path: 'fgSkidTriangle', component: SkidDeltaAnalyticsComponent, canActivate: [AuthGuard]},
          {path: 'slob', component: FgSlobMovementDashComponent, canActivate: [AuthGuard]},
          {path: 'inventory', component: FgInventoryQualityIndexDashComponent, canActivate: [AuthGuard]},

          {path: 'pmSlob', component: PmSlobMovementDashComponent, canActivate: [AuthGuard]},
          {path: 'pmInventory', component: PmInventoryQualityIndexDashComponent, canActivate: [AuthGuard]},
          {path: 'cummulativeScore', component: PmOtifCummulativeScoreComponent, canActivate: [AuthGuard]},

          {path: 'rmSlob', component: RmSlobMovementDashComponent, canActivate: [AuthGuard]},
          {path: 'rmInventory', component: RmInventoryQualityIndexDashComponent, canActivate: [AuthGuard]},
        ]},

      /*******APB Components**********/
      // FG Module Components
      {
    path: 'fgBackupPage', children: [
      {path: '', component: FgBackupComponent},
      {path: 'fgBackup', component: FgBackupPageComponent, canActivate: [AuthGuard], children: [
          {path: 'fgPlan', component: FgBackupPlansComponent, canActivate: [AuthGuard]},
          {path: 'fgAnnual', component: FgBackupAnnualAnalysisComponent, canActivate: [AuthGuard]},
          {path: "", redirectTo: "fgPlan", pathMatch: "full"}
        ]}
    ]},
      // RM Module Components
      {path: 'rmBackupPage', children: [
      {path: '', component: RmBackupComponent},
      {path: 'rmBackup', component: RmBackupPageComponent, canActivate: [AuthGuard], children: [
          {path: 'rmPlan', component: RmBackupPlansComponent, canActivate: [AuthGuard]},
          {path: 'rmAnnual', component: RmBackupAnnualAnalysisComponent, canActivate: [AuthGuard]},
          {path: "", redirectTo: "rmPlan", pathMatch: "full"}
        ]}
    ]},
      // PM Module Components
      {
    path: 'pmBackupPage', children: [
      {path: '', component: PmBackupComponent},
      {path: 'pmBackup', component: PmBackupPageComponent, canActivate: [AuthGuard], children: [
          {path: 'pmPlan', component: PmBackupPlansComponent, canActivate: [AuthGuard]},
          {path: 'pmOtif', component: PmBackupOtifComponent, canActivate: [AuthGuard]},
          {path: 'pmAnnual', component: PmBackupAnnualAnalysisComponent, canActivate: [AuthGuard]},
          {path: "", redirectTo: "pmPlan", pathMatch: "full"}
        ]}
    ]},
      // Analyzer Module Components
      {
    path: 'analyzerBk', children: [
      {path: '', component: AnalyzerBackupPageComponent},
      {path: 'allBackup', component: AllBackupComponent, canActivate: [AuthGuard], children: [
          {path: 'bkPlan', component: BackupPlansTabComponent, canActivate: [AuthGuard]},
          {path: 'bkPmOtif', component: BackupPmOtifComponent, canActivate: [AuthGuard]},
          {path: 'bkAnnual', component: BackupAnnualAnalysisComponent, canActivate: [AuthGuard]},
          {path: "", redirectTo: "bkPlan", pathMatch: "full"}
        ]}
    ]},
  ]}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
