import {Component, OnInit} from '@angular/core';
import {FgModuleService} from "../fg-module.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";

@Component({
  selector: 'app-fg-slob-summary',
  templateUrl: './fg-slob-summary.component.html',
  styleUrls: ['./fg-slob-summary.component.css']
})
export class FgSlobSummaryComponent implements OnInit {
  totalSlobSummaryValues:any =[];
  slobSummaryList:any=[];
  totalSlobDeviation:any=[];
  divsionSlobDeviation:any =[];
  constructor(private fgModuleService: FgModuleService, private spinner: NgxSpinnerService,
              private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
     this.fgModuleService.getFgSlobSummaryTableData().pipe(
      switchMap((slobSummaryResponse: any) => {
        console.log(slobSummaryResponse);
        if (slobSummaryResponse !== null && slobSummaryResponse!== undefined) {
          this.totalSlobSummaryValues = slobSummaryResponse;
          this.slobSummaryList = slobSummaryResponse?.typeWiseFGSlobSummaryDTOS;
          console.log(this.slobSummaryList);
        }
        return this.fgModuleService.getFgSlobDeviationData();
      })).pipe(
      switchMap((totalSlobDeviationResponse: any) => {
        console.log(totalSlobDeviationResponse);
        if (totalSlobDeviationResponse !== null && totalSlobDeviationResponse!== undefined) {
          this.totalSlobDeviation = totalSlobDeviationResponse;
          console.log(this.totalSlobDeviation);
        }
        return this.fgModuleService.getFgSlobDivisionDeviationData();
      })).subscribe((divsionSlobDeviationResponse: any) => {
       console.log(divsionSlobDeviationResponse);
      if (divsionSlobDeviationResponse !== null && divsionSlobDeviationResponse!== undefined) {
        this.divsionSlobDeviation = divsionSlobDeviationResponse;
        console.log(this.divsionSlobDeviation);
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
    });
  }

}
