import {Component, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {FgModuleService} from "../fg-module.service";
import {switchMap} from "rxjs/operators";
import {saveAs} from "file-saver";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-skid-saturation',
  templateUrl: './skid-saturation.component.html',
  styleUrls: ['./skid-saturation.component.css']
})
export class SkidSaturationComponent implements OnInit {
  $subs:Subscription;
  monthNamesList?: any = [];
  skidSaturationList: any = [];
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;
  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private spinner: NgxSpinnerService, private fgModuleService: FgModuleService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.fgModuleService.getFgSkidSaturationData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((skidSaturationResponse: any) => {
        if (skidSaturationResponse !== null && skidSaturationResponse !== undefined) {
          this.skidSaturationList = skidSaturationResponse?.fgSkidSaturationDTO;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  exportAsXLSX() {
    this.fgModuleService.getFgSkidExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.fgSkidSaturation + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = this.excelExportConstantsService.fgSkidSaturation + ' '+ 'Downloaded successfully';
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

}
