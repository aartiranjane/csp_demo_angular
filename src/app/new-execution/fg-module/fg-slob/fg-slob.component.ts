import {Component, OnDestroy, OnInit} from '@angular/core';
import {switchMap} from "rxjs/operators";
import {NgxSpinnerService} from "ngx-spinner";
import {FgModuleService} from "../fg-module.service";
import {saveAs} from "file-saver";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-fg-slob',
  templateUrl: './fg-slob.component.html',
  styleUrls: ['./fg-slob.component.css']
})
export class FgSlobComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  slobTotalValues: any = [];
  slobDataList: any = [];
  successMessage = '';
  errorMessage = '';
  overAllDeviationList:any=[];
  totalSlobSummaryValues:any =[];
  slobSummaryList:any=[];
  divsionSlobDeviation:any =[];
  isExcelDisable=false;
  successSLOBSummaryMessage = '';
  errorSLOBSummaryMessage = '';
  constructor(private spinner: NgxSpinnerService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.fgModuleService.getFgSlobTotalValuesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((slobTotalValuesDataResponse: any) => {
          console.log(slobTotalValuesDataResponse);
          if (slobTotalValuesDataResponse !== null && slobTotalValuesDataResponse !== undefined) {
            this.slobTotalValues = slobTotalValuesDataResponse;
          }
          return this.fgModuleService.getFgSlobData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((slobDataResponse: any) => {
          if (slobDataResponse !== null && slobDataResponse !== undefined) {
            this.slobDataList = slobDataResponse;
          }
          return this.fgModuleService.getFgSlobSummaryTableData();
        })).pipe(
        switchMap((slobSummaryResponse: any) => {
          console.log(slobSummaryResponse);
          if (slobSummaryResponse !== null && slobSummaryResponse!== undefined) {
            this.totalSlobSummaryValues = slobSummaryResponse;
            this.slobSummaryList = slobSummaryResponse?.typeWiseFGSlobSummaryDTOS;
            console.log(this.slobSummaryList);
          }
          return this.fgModuleService.getFgSlobDeviationData();
        })).pipe(
        switchMap((overAllDeviationResponse: any) => {
          console.log(overAllDeviationResponse);
          if (overAllDeviationResponse !== null && overAllDeviationResponse!== undefined) {
            this.overAllDeviationList = overAllDeviationResponse;
            console.log(this.overAllDeviationList)
          }
          return this.fgModuleService.getFgSlobDivisionDeviationData();
        })).subscribe((divsionSlobDeviationResponse: any) => {
        console.log(divsionSlobDeviationResponse);
        if (divsionSlobDeviationResponse !== null && divsionSlobDeviationResponse!== undefined) {
          this.divsionSlobDeviation = divsionSlobDeviationResponse;
          console.log(this.divsionSlobDeviation);
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  exportAsXLSX() {
    this.fgModuleService.getSlobExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.fgSlobSaturation + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
  fgSlObSummaryexportAsXLSX() {
    this.fgModuleService.getSlobSummaryExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.fgSlobSummary + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successSLOBSummaryMessage = 'Download Success!';
      setTimeout(() => {
        this.successSLOBSummaryMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorSLOBSummaryMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorSLOBSummaryMessage = '';
      }, 2000);
    });
  }

}
