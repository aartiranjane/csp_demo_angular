import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fg-reports-page',
  templateUrl: './fg-reports-page.component.html',
  styleUrls: ['./fg-reports-page.component.css']
})
export class FgReportsPageComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('nav-org-tabb').click();
  }

}
