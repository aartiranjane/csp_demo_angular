import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {FgModuleService} from "../../fg-module.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {Chart} from "chart.js";

@Component({
  selector: 'app-skid-delta-analytics',
  templateUrl: './skid-delta-analytics.component.html',
  styleUrls: ['./skid-delta-analytics.component.css']
})
export class SkidDeltaAnalyticsComponent implements OnInit, AfterViewChecked {
  monthNamesList?: any = [];
  skidList:any=[];
  skidDeltaId:any;
  singleselectedItems: any = [];
  singledropdownSettings = {};
  deltaAnalyticsList?:any;
  deviationGraphData={
    labels:[],
    data:[]
  };
  monthRows:any =[];
  constructor(public executionFlowPageService:ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService,
              private spinner: NgxSpinnerService, private changeRef: ChangeDetectorRef) { }
  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }


  ngOnInit(): void {
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };

    this.spinner.show();
      this.fgModuleService.getAllRscDtSkidsList().subscribe((skidListResponse: any) => {
        console.log(skidListResponse);
        if (skidListResponse !== null && skidListResponse !== undefined) {
       /*   skidListResponse?.skidsDTOList.forEach((res: any) => {
            this.skidList.push({id: res.id, itemName: res.skidName});
            this.singleselectedItems[0] = this.skidList[0];
            this.getskidWiseData(this.skidList[0]);
          });*/


          this.skidList = skidListResponse;
          if(this.skidList.length >0){
            this.getskidWiseData(this.skidList[0]);
          }
          console.log( this.skidList);



        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });



  }

  onItemDeSelect(){}
  getskidWiseData(row:any){
    this.skidDeltaId = row.id;
    this.spinner.show();
    this.executionFlowPageService.getDeltaAnalyticsMonthNames().pipe(
      switchMap((monthNamesDataResponse: any) => {
        if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
          this.monthNamesList = monthNamesDataResponse?.columnNames;
          this.monthRows = monthNamesDataResponse?.rowNames;
          console.log(this.monthNamesList);
          for (let i = 0; i < monthNamesDataResponse?.columnNames.length; i++) {
            this.deviationGraphData.labels[i] = monthNamesDataResponse?.columnNames[i];
          }
        }
        return this.fgModuleService.getFgSkidDeltaAnalyticsData(this.skidDeltaId);
      })).subscribe((skidDeltaAnalyticsResponse:any)=>{
      if(skidDeltaAnalyticsResponse!==undefined && skidDeltaAnalyticsResponse!==null){
        console.log(skidDeltaAnalyticsResponse);
        this.deltaAnalyticsList =skidDeltaAnalyticsResponse;
        this.deviationBarChart();
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  }
  skidDeviationBar:any;
  deviationBarChart() {
    const canvas : any = document.getElementById("skid_Deviation_graph");
    const ctx = canvas.getContext("2d");
    if(this.skidDeviationBar !== undefined)
      this.skidDeviationBar.destroy();
    this.skidDeviationBar = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.deviationGraphData.labels,
        datasets: [{
          label: "Deviation",
          type: "bar",
          backgroundColor: [
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',
            'rgba(199, 51, 51, 0.3)',

          ],
          borderColor: [
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',
            'rgba(199, 51, 51, 1)',

          ],
          borderWidth: 1,
          data:[
            this.deltaAnalyticsList?.deviationDetails[0],
            this.deltaAnalyticsList?.deviationDetails[1],
            this.deltaAnalyticsList?.deviationDetails[2],
            this.deltaAnalyticsList?.deviationDetails[3],
            this.deltaAnalyticsList?.deviationDetails[4],
            this.deltaAnalyticsList?.deviationDetails[5],
            this.deltaAnalyticsList?.deviationDetails[6],
            this.deltaAnalyticsList?.deviationDetails[7],
            this.deltaAnalyticsList?.deviationDetails[8],
            this.deltaAnalyticsList?.deviationDetails[9],
            this.deltaAnalyticsList?.deviationDetails[10],
            this.deltaAnalyticsList?.deviationDetails[11],
            this.deltaAnalyticsList?.deviationDetails[12],
            this.deltaAnalyticsList?.deviationDetails[13],
            this.deltaAnalyticsList?.deviationDetails[14],
            this.deltaAnalyticsList?.deviationDetails[15],
            this.deltaAnalyticsList?.deviationDetails[16],
            this.deltaAnalyticsList?.deviationDetails[17],
            this.deltaAnalyticsList?.deviationDetails[18],
            this.deltaAnalyticsList?.deviationDetails[19],
            this.deltaAnalyticsList?.deviationDetails[20],
            this.deltaAnalyticsList?.deviationDetails[21],
            this.deltaAnalyticsList?.deviationDetails[22]
          ],
        }
        ]
      },
      options: {
        animation: {

          onComplete() {
            const chartInstance = this.chart,
              ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = this.chart.config.options.defaultFontColor;
            this.data.datasets.forEach((dataset, i) =>{
              const meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach( (bar, index)=> {
                const data = dataset.data[index];
                ctx.fillText(data, bar._model.x, bar._model.y +2);
              });
            });
          }
        },
        responsive: true,
        maintainAspectRatio: true,
        legend: {display: true},
        scales: {
          xAxes: [{
            offset: true,
            scaleLabel: {
              display: true,
              labelString: 'Months'
            },
            ticks: {fontSize: 10, beginAtZero: true,}
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Deviation',
            },
            ticks: {
              fontSize: 10,
              beginAtZero: true
            }
          }]
        },
      }
    });
  }


}
