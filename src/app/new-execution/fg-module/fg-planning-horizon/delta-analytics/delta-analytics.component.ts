import {Component, OnInit} from '@angular/core';
import {PerfectScrollbarConfigInterface} from "ngx-perfect-scrollbar";
import {Router} from "@angular/router";
import {PmServiceService} from "../../../new-pm/pm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {Chart} from "chart.js";
import {saveAs} from "file-saver";
import {FgModuleService} from "../../fg-module.service";

type array = any[];

@Component({
  selector: 'app-delta-analytics',
  templateUrl: './delta-analytics.component.html',
  styleUrls: ['./delta-analytics.component.css']
})
export class DeltaAnalyticsComponent implements OnInit {
  isExcelDisable = false;
  public config: PerfectScrollbarConfigInterface = {};
  public showSettings = false;
  mpsPlanArray: any;
  mpsDeltaAnalysisDataList?: any;
  successMessage = '';
  errorMessage = '';
  mpsSearchTerm: any;
  bpiSearch: any;
  bpiCodeList = [];
  checked: boolean = false;
  monthNamesList?: any = [];
  filteredByKeyList: any = [];
  filterlist = [];
  description: any = [];
  monthValuelist: any = [];
  code: any = [];
  total: any = [];
  lineName: any = [];
  filter: any = [];
  filterData: any = [];
  filterKey: any = "";
  mpsFla: any;
  isShow = true;
  isDisplay = false;
  isSelected: any;
  value: any;
  masterIndeterminate: boolean = false;
  checkbox: any = {
    code: [],
    description: [],
    divisionName: [],
    signatureName: [],
    brandName: [],
    month1: [],
    month2: [],
    month3: [],
    month4: [],
    month5: [],
    month6: [],
    month7: [],
    month8: [],
    month9: [],
    month10: [],
    month11: [],
    month12: [],
    total: [],
    materialCategoryName: [],
    materialSubCategoryName: [],
    lineName: [],
    skidName: [],
    fla: []
  };
  checkboxClone: any = {};
  filterCheckboxStateMaintain: number[] = [];
  displayedColumns: string[] = ['code', 'description', 'total', 'monthValues', 'lineName'];
  selectedFilterList = [];
  masterChecked = true;
  filteredList: string[] = [];
  seletedDropdownIndex;
  isMasterCheckboxSelected = false;
  isMasterIntermediate = false;
  filteredDataList = [];
  uniqueDivisionList: any = [];
  uniqueDivisionListsSize: any;
  uniqueSignatureList: any = [];
  uniqueSignatureListsSize: any;
  uniqueBrandList: any = [];
  uniqueBrandListsSize: any;
  uniqueCtg1List: any = [];
  uniqueCtg1ListsSize: any;
  uniqueCtg2List: any = [];
  uniqueCtg2ListsSize: any;
  uniqueLinesList: any = [];
  uniqueLinesListsSize: any;
  uniqueFgList: any = [];
  uniqueFgListsSize: any;
  isExecute = true;
  deviationGraphData = {
    labels: [],
    data: []
  };
  monthRows: any = [];

  constructor(private router: Router, private pmService: PmServiceService,
              private fgModuleService: FgModuleService,
              private excelExportConstantsService: ExcelExportConstantsService,
              public executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) {


  }

  ngOnInit() {
    for (let i = 0; i < 23; i++) {
      this.selectedFilterList.push({
        masterChecked: true,
        selected: false,
        masterIndeterminate: false
      })
    }
    this.filter = {
      code: {content: []},
      description: {content: []},
      divisionName: {content: []},
      signatureName: {content: []},
      brandName: {content: []},
      month1: {content: []},
      month2: {content: []},
      month3: {content: []},
      month4: {content: []},
      month5: {content: []},
      month6: {content: []},
      month7: {content: []},
      month8: {content: []},
      month9: {content: []},
      month10: {content: []},
      month11: {content: []},
      month12: {content: []},
      total: {content: []},
      materialCategoryName: {content: []},
      materialSubCategoryName: {content: []},
      lineName: {content: []},
      skidName: {content: []},
      fla: {content: []}
    };
  /*  this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }*/
      this.spinner.show();
   /*   this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: object) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
            this.pmService.monthNameList = monthNamesDataResponse;
          }
          return*/ this.executionFlowPageService.getLatestMpsData().subscribe((mpsDataResponse: any) => {
        this.bpiCodeList = mpsDataResponse.bpiCodes;
        this.filteredDataList = mpsDataResponse.bpiCodes;
        this.planSummaryByBrand();
        if (mpsDataResponse.bpiCodes != null) {
          /*mpsDataResponse.bpiCodes.forEach((response: any) => {
            this.monthValuelist.push({
              monthValues: response.monthValues,
            });
          });*/
          this.checkbox.code = this.getUniqueElements(this.bpiCodeList, 'code');
          this.checkbox.total = this.getUniqueElements(this.bpiCodeList, 'total');
          this.checkbox.description = this.getUniqueElements(this.bpiCodeList, 'description');
          this.checkbox.divisionName = this.getUniqueElements(this.bpiCodeList, 'divisionName');
          this.checkbox.signatureName = this.getUniqueElements(this.bpiCodeList, 'signatureName');
          this.checkbox.brandName = this.getUniqueElements(this.bpiCodeList, 'brandName');
          this.checkbox.materialCategoryName = this.getUniqueElements(this.bpiCodeList, 'materialCategoryName');
          this.checkbox.materialSubCategoryName = this.getUniqueElements(this.bpiCodeList, 'materialSubCategoryName');
          this.checkbox.lineName = this.getUniqueElements(this.bpiCodeList, 'lineName');
          this.checkbox.skidName = this.getUniqueElements(this.bpiCodeList, 'skidName');
          this.checkbox.fla = this.getUniqueElements(this.bpiCodeList, 'fla');
          for (let i = 1; i <= 12; i++) {
            const monthKey = 'month' + i;
            this.checkbox[monthKey] = this.getUniqueElements(this.bpiCodeList, monthKey, true);
          }
          this.createCheckBoxArray();
        }
        if (mpsDataResponse !== null && mpsDataResponse !== undefined) {
          this.bpiCodeList = mpsDataResponse?.bpiCodes;
        }
        this.checkboxClone = Object.assign({}, this.checkbox);
       /* this.spinner.hide();*/
      }, (error) => {
       /* this.spinner.hide();*/
        console.log(error);
      });
  /*  })*/
  }

  getUniqueElements(bpiCodeList: array, uniqueBy: string, isMonth?: boolean) {
    if (isMonth) {
      return [...new Set(bpiCodeList.map(value => value.monthValues[uniqueBy].value).sort())];
    } else {
      return [...new Set(bpiCodeList.map(value => value[uniqueBy]).sort())];
    }
  }

  createCheckBoxArray() {
    this.checkbox.code.forEach((value: any, index: number) => {
      this.checkbox.code[index] = this.setFilterData('code', this.checkbox.code[index]);

      if (this.checkbox.total.length > index) {
        this.checkbox.total[index] = this.setFilterData('total', this.checkbox.total[index]);
      }
      if (this.checkbox.description.length > index) {
        this.checkbox.description[index] = this.setFilterData('description', this.checkbox.description[index]);
      }
      if (this.checkbox.divisionName.length > index) {
        this.checkbox.divisionName[index] = this.setFilterData('divisionName', this.checkbox.divisionName[index]);
      }
      if (this.checkbox.signatureName.length > index) {
        this.checkbox.signatureName[index] = this.setFilterData('signatureName', this.checkbox.signatureName[index]);
      }
      if (this.checkbox.brandName.length > index) {
        this.checkbox.brandName[index] = this.setFilterData('brandName', this.checkbox.brandName[index]);
      }
      if (this.checkbox.materialCategoryName.length > index) {
        this.checkbox.materialCategoryName[index] = this.setFilterData('materialCategoryName', this.checkbox.materialCategoryName[index]);
      }
      if (this.checkbox.materialSubCategoryName.length > index) {
        this.checkbox.materialSubCategoryName[index] = this.setFilterData('materialSubCategoryName', this.checkbox.materialSubCategoryName[index]);
      }
      if (this.checkbox.lineName.length > index) {
        this.checkbox.lineName[index] = this.setFilterData('lineName', this.checkbox.lineName[index]);
      }
      if (this.checkbox.skidName.length > index) {
        this.checkbox.skidName[index] = this.setFilterData('skidName', this.checkbox.skidName[index]);
      }
      if (this.checkbox.fla.length > index) {
        this.checkbox.fla[index] = this.setFilterData('fla', this.checkbox.fla[index]);
      }
      for (let i = 1; i <= 12; i++) {
        const monthKey = 'month' + i;
        if (this.checkbox[monthKey][index] !== null && this.checkbox[monthKey][index] !== undefined) {
          this.checkbox[monthKey][index] = this.setFilterData(monthKey, this.checkbox[monthKey][index]);
        }
      }
    });
    console.log(this.checkbox);

  }

  setFilterData(name: any, nameValue) {
    if (nameValue !== null && nameValue !== undefined) {
      return {
        [name]: nameValue,
        disabled: false,
        checked: true,
        labelPosition: "after"
      };
    } else if (nameValue == null || nameValue == '') {
      return {
        [name]: 'blank',
        disabled: false,
        checked: true,
        labelPosition: "after"
      }
    }
  }

  setFilterKey(event: any, seletedDropdownIndex: any) {
    this.bpiSearch = '';
    this.filterKey = event.target.value;
    this.seletedDropdownIndex = seletedDropdownIndex;
    this.filterCheckboxStateMaintain = [];
    for (let i = 0; i < this.checkbox[this.filterKey].length; i++) {
      if (this.checkbox[this.filterKey][i]?.checked == true) {
        this.filterCheckboxStateMaintain.push(i);
      }
    }
    this.isMasterCheckboxSelected = this.selectedFilterList[seletedDropdownIndex].masterChecked;
    this.isMasterIntermediate = this.selectedFilterList[seletedDropdownIndex].masterIndeterminate;

    const index = this.checkbox[this.filterKey].findIndex(value => value.checked === true);

    const filteredCheckboxArray = this.checkbox[this.filterKey];

    this.checkbox[this.filterKey] = filteredCheckboxArray.filter(
      (thing, i, arr) => arr.findIndex(t => thing !== undefined && t[this.filterKey] === thing[this.filterKey]) === i
    ).sort((a, b) => a[this.filterKey] - b[this.filterKey]);
  }

  toggleDisplay() {
    this.spinner.show();
    this.isShow = !this.isShow;
    this.isDisplay = false;
    console.log(this.selectedFilterList);
    console.log(this.bpiCodeList);
    const indexList = [];
    this.filteredList = [];
    for (let i = 0; i < this.selectedFilterList.length; i++) {
      //if (this.selectedFilterList[i].masterIndeterminate == true) {
      this.selectedFilterList[i].masterIndeterminate = false;
      this.selectedFilterList[i].masterChecked = true;
      indexList.push(i);
      //}
      console.log(this.mpsPlanArray);

    }

    const objectKeyList = Object.keys(this.checkbox);
    for (let i = 0; i < indexList.length; i++) {
      this.checkbox[objectKeyList[indexList[i]]] = this.checkboxClone[objectKeyList[indexList[i]]];
      const a = this.checkbox[objectKeyList[indexList[i]]];
      const array = this.checkbox[objectKeyList[indexList[i]]];
      for (let j = 0; j < array.length; j++) {
        array[j].checked = true;
      }
    }
    this.planSummaryByBrand();
    /*  this.planSummaryByBrand();*/
    this.isExecute = true;
    this.spinner.hide();
  }

  selectAll(index) {
    this.selectedFilterList[index].masterChecked = !this.selectedFilterList[index].masterChecked;
    for (let i = 0; i < this.checkbox[this.filterKey].length; i++) {
      this.checkbox[this.filterKey][i].checked = this.selectedFilterList[index].masterChecked;
    }
  }

  listChange(index: number, skidname?: any) {
    console.log(skidname);
    let checkedCount = 0;
    for (const checkbox of this.checkbox[this.filterKey]) {
      if (checkbox?.checked) {
        checkedCount++;
      }
    }
    if (checkedCount > 0 && checkedCount < this.checkbox[this.filterKey].length) {
      this.selectedFilterList[index].masterIndeterminate = true;
    } else if (checkedCount === this.checkbox[this.filterKey].length) {
      this.selectedFilterList[index].masterIndeterminate = false;
      this.selectedFilterList[index].masterChecked = true;
    } else {
      this.selectedFilterList[index].masterIndeterminate = false;
      this.selectedFilterList[index].masterChecked = false;
    }
  }

  cancel() {
    //this.checkbox[this.filterKey] = this.filterCheckboxStateMaintain[0];
    for (let i = 0; i < this.checkbox[this.filterKey].length; i++) {
      this.checkbox[this.filterKey][i].checked = this.filterCheckboxStateMaintain.findIndex(value1 => value1 == i) !== -1;
    }
    this.selectedFilterList[this.seletedDropdownIndex].masterChecked = this.isMasterCheckboxSelected;
    this.selectedFilterList[this.seletedDropdownIndex].masterIndeterminate = this.isMasterIntermediate;
  }

  submit(columnIndex: any) {
    if (this.bpiSearch.toString().trim() !== '') {
      this.setCheckBoxArrayAsPerSearch(columnIndex);
    }
    this.isDisplay = true;
    this.filterData = [];
    this.filteredByKeyList = [];
    this.filteredByKeyList = this.checkbox[this.filterKey];
    const isExist = this.filteredList.findIndex(value1 => value1 === this.filterKey);
    if (isExist === -1) {
      this.filteredList.push(this.filterKey);
    }
    this.filterDataList();
    this.makeEmptyArray();
    this.fillArray();


    /* this.planSummaryByBrand();*/
    this.isExecute = false;
  }

  setCheckBoxArrayAsPerSearch(columnIndex: number) {
    let checkboxArrayByFilterKey = [];
    checkboxArrayByFilterKey = this.checkbox[this.filterKey];
    checkboxArrayByFilterKey.filter((value: any) => {
      const value1 = value[this.filterKey];
      const isPresent = value1.toString().toUpperCase().includes(this.bpiSearch.toString().toUpperCase());
      if (isPresent) {
        value.checked = true;
      } else {
        value.checked = false;
      }
    });
    this.selectedFilterList[columnIndex].masterChecked = false;
    this.selectedFilterList[columnIndex].masterIndeterminate = true;
  }

  makeEmptyArray() {
    let isFilterAppliedOnKey;
    for (let k = 0; k < Object.keys(this.checkbox).length; k++) {
      const key = Object.keys(this.checkbox)[k];
      isFilterAppliedOnKey = this.filteredList.findIndex(value1 => value1 === key);
      if (isFilterAppliedOnKey === -1) {
        this.checkbox[key] = []
      }
    }
  }

  fillArray() {
    let isFilterAppliedOnKey;
    for (let j = 0; j < this.filterData.length; j++) {
      for (let k = 0; k < Object.keys(this.checkbox).length; k++) {
        const key = Object.keys(this.checkbox)[k];
        isFilterAppliedOnKey = this.filteredList.findIndex(value1 => value1 === key);
        if (isFilterAppliedOnKey == -1) {
          const splicedKey = key.slice(0, 5);
          this.setConditionalFilterListData(key, j, splicedKey === 'month');
        }
      }
    }
  }

  filterDataList() {
    const splittedKey = this.filterKey.toString().slice(0, 5);
    if (this.filteredList[0] == this.filterKey) {
      if (splittedKey == 'month') {
        for (let i = 0; i < this.filteredByKeyList.length; i++) {
          this.filterlist = this.bpiCodeList.filter((value: any) =>
            this.filteredByKeyList[i].checked === true && value.monthValues[this.filterKey].value == this.filteredByKeyList[i][this.filterKey]
          );
          this.filterData = this.filterData.concat(this.filterlist);
        }
      } else {
        for (let i = 0; i < this.filteredByKeyList.length; i++) {
          this.filterlist = this.bpiCodeList.filter((value: any) =>
            (this.filteredByKeyList[i].checked === true && value[this.filterKey] == this.filteredByKeyList[i][this.filterKey])
          );
          this.filterData = this.filterData.concat(this.filterlist);
        }
      }
      this.filteredDataList = [];
      this.filteredDataList[0] = this.filterData;
    } else {
      const filterKeyIndex = this.filteredList.findIndex(value1 => value1 == this.filterKey);
      const index = this.filteredList.length == this.filteredDataList.length ? filterKeyIndex - 1 : this.filteredDataList.length - 1;
      if (splittedKey == 'month') {
        for (let i = 0; i < this.filteredByKeyList.length; i++) {
          this.filterlist = this.filteredDataList[index].filter((value: any) =>
            this.filteredByKeyList[i].checked === true && value.monthValues[this.filterKey].value == this.filteredByKeyList[i][this.filterKey]
          );
          this.filterData = this.filterData.concat(this.filterlist);
        }
      } else {
        for (let i = 0; i < this.filteredByKeyList.length; i++) {
          this.filterlist = this.filteredDataList[index].filter((value: any) =>
              (this.filteredByKeyList[i].checked === true && value[this.filterKey] == this.filteredByKeyList[i][this.filterKey])
            /*this.filteredByKeyList[i][this.filterKey] == 'blank'*/
          );
          this.filterData = this.filterData.concat(this.filterlist);
        }
      }
      this.filteredDataList[this.filteredDataList.length] = this.filterData;
    }

  }

  setConditionalFilterListData(key: string, j: number, isMonth: boolean) {
    let index;
    if (isMonth) {
      index = this.checkboxClone[key].findIndex(value1 => value1[key] == this.filterData[j].monthValues[key].value);
    } else {
      index = this.checkboxClone[key].findIndex(value1 => value1 != null && (value1[key] == this.filterData[j][key]));
    }
    this.checkbox[key].push(this.checkboxClone[key][index]);
  }

  deviationBar: any;

  planSummaryByBrand() {
    if (this.isDisplay === false) {
      this.mpsPlanArray = this.bpiCodeList;
      this.getLengthOfUniqueList(this.mpsPlanArray);
    } else {
      this.mpsPlanArray = this.filterData;
    }
    this.fillUniqueItemsList(this.mpsPlanArray);
    this.getAllItemsList();
    this.fillDivisionBrandandSignatureWiseDataList(this.mpsPlanArray);
    const canvas: any = document.getElementById("Deviation_graph");
    const ctx = canvas.getContext("2d");
    if (this.deviationBar !== undefined)
      this.deviationBar.destroy();
    this.deviationBar = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.deviationGraphData.labels,
        datasets: [{
          data: this.deviationGraphData.data,
          type: 'bar',
          backgroundColor: [
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',

          ],
          borderColor: [
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',

          ],
          borderWidth: 1,
          label: "Deviation",

        },
        ]
      },
      options: {
        /* responsive: true,
         maintainAspectRatio: true,*/
        legend: {display: true},
        scales: {
          xAxes: [{
            offset: true,
            scaleLabel: {
              display: true,
              labelString: 'Months'
            },
            ticks: {fontSize: 10, beginAtZero: false,}
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Plan Qty',
            },
            ticks: {
              fontSize: 10,
              beginAtZero: false,
              /*min: 0,
              max: 100,*/
              /* stepSize: 1000000*/
            }
          }]
        },
      }
    });
  }

  executeBtn() {
    this.planSummaryByBrand();
  }

  fillDivisionBrandandSignatureWiseDataList(mpsPlanArray: any) {

    const rscMtPPDetailsCriteria = {
      /* itemIds: [],*/
      divisionIds: [],
      brandIds: [],
      signatureIds: [],
      categoryIds: [],
      subCategoryIds: [],
      lineIds: [],
      skidIds: [],
    };
    /* rscMtPPDetailsCriteria.mpsId = this.executionFlowPageService.selectedHeaderId;*/
    rscMtPPDetailsCriteria.divisionIds = [...new Set(mpsPlanArray.map(item => item.otherInfo.divisionId))];
    rscMtPPDetailsCriteria.brandIds = [...new Set(mpsPlanArray.map(item => item.otherInfo.brandId))];
    rscMtPPDetailsCriteria.signatureIds = [...new Set(mpsPlanArray.map(item => item.otherInfo.signatureId))];
    rscMtPPDetailsCriteria.categoryIds = [...new Set(mpsPlanArray.map(item => item.otherInfo.materialCategoryId))];
    rscMtPPDetailsCriteria.subCategoryIds = [...new Set(mpsPlanArray.map(item => item.otherInfo.materialSubCategoryId))];
    rscMtPPDetailsCriteria.lineIds = [...new Set(mpsPlanArray.map(item => item.otherInfo.linesId))];
    rscMtPPDetailsCriteria.skidIds = [...new Set(mpsPlanArray.map(item => item.otherInfo.skidsId))];
    /*  rscMtPPDetailsCriteria.itemIds = [...new Set(mpsPlanArray.map(item => item.id))];*/
    console.log(rscMtPPDetailsCriteria);
    this.spinner.show();
    this.executionFlowPageService.getDeltaAnalyticsMonthNames().pipe(
      switchMap((monthNamesDataResponse: any) => {
        if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
          this.monthNamesList = monthNamesDataResponse?.columnNames;
          this.monthRows = monthNamesDataResponse?.rowNames;
          console.log(monthNamesDataResponse);
          for (let i = 0; i < monthNamesDataResponse?.columnNames.length; i++) {
            this.deviationGraphData.labels[i] = monthNamesDataResponse?.columnNames[i];
          }
        }
        return this.pmService.getPmDeltaAnalyticsData(rscMtPPDetailsCriteria);
      })).subscribe((res: any) => {
      if (res !== null && res !== undefined ){
        console.log(res);
        this.mpsDeltaAnalysisDataList = res;
        for (let i = 0; i < res?.deviationValues.length; i++) {
          this.deviationGraphData.data[i] = res?.deviationValues[i];
        }
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });

  }

  fillUniqueItemsList(mpsPlanArray: any) {
    this.uniqueDivisionList = [...new Set(mpsPlanArray.map(item => item.divisionName))];
    this.uniqueSignatureList = [...new Set(mpsPlanArray.map(item => item.signatureName))];
    this.uniqueBrandList = [...new Set(mpsPlanArray.map(item => item.brandName))];
    this.uniqueCtg1List = [...new Set(mpsPlanArray.map(item => item.materialCategoryName))];
    this.uniqueCtg2List = [...new Set(mpsPlanArray.map(item => item.materialSubCategoryName))];
    this.uniqueLinesList = [...new Set(mpsPlanArray.map(item => item.lineName))];
    this.uniqueFgList = [...new Set(mpsPlanArray.map(item => item.code))];
  }

  getLengthOfUniqueList(mpsPlanArray: any) {
    this.uniqueDivisionListsSize = [...new Set(mpsPlanArray.map(item => item.divisionName))].length;
    this.uniqueSignatureListsSize = [...new Set(mpsPlanArray.map(item => item.signatureName))].length;
    this.uniqueBrandListsSize = [...new Set(mpsPlanArray.map(item => item.brandName))].length;
    this.uniqueCtg1ListsSize = [...new Set(mpsPlanArray.map(item => item.materialCategoryName))].length;
    this.uniqueCtg2ListsSize = [...new Set(mpsPlanArray.map(item => item.materialSubCategoryName))].length;
    this.uniqueLinesListsSize = [...new Set(mpsPlanArray.map(item => item.lineName))].length;
    this.uniqueFgListsSize = [...new Set(mpsPlanArray.map(item => item.code))].length;
  }

  getAllItemsList() {
    if (this.uniqueDivisionList.length.toString() === this.uniqueDivisionListsSize.toString()) {
      this.uniqueDivisionList = [];
      this.uniqueDivisionList.push("All");
    }
    if (this.uniqueSignatureList.length.toString() === this.uniqueSignatureListsSize.toString()) {
      this.uniqueSignatureList = [];
      this.uniqueSignatureList.push("All");
    }
    if (this.uniqueBrandList.length.toString() === this.uniqueBrandListsSize.toString()) {
      this.uniqueBrandList = [];
      this.uniqueBrandList.push("All");
    }
    if (this.uniqueCtg1List.length.toString() === this.uniqueCtg1ListsSize.toString()) {
      this.uniqueCtg1List = [];
      this.uniqueCtg1List.push("All");
    }
    if (this.uniqueCtg2List.length.toString() === this.uniqueCtg2ListsSize.toString()) {
      this.uniqueCtg2List = [];
      this.uniqueCtg2List.push("All");
    }
    if (this.uniqueLinesList.length.toString() === this.uniqueLinesListsSize.toString()) {
      this.uniqueLinesList = [];
      this.uniqueLinesList.push("All");
    }
    if (this.uniqueFgList.length.toString() === this.uniqueFgListsSize.toString()) {
      this.uniqueFgList = [];
      this.uniqueFgList.push("All");
    }
  }

  exportAsXLSX() {
    this.fgModuleService.getfgMPSTriangleAnalysisExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.fgMpsTriangleAnalysis + " " + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

  /*getRequest() {
    this.pmService.getPmDeltaAnalyticsData().subscribe((res: any) => {
      console.log("delta:", res);
    })
  }*/

}
