import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {FgModuleService} from "../../fg-module.service";
import {switchMap} from "rxjs/operators";
import {NgxSpinnerService} from "ngx-spinner";
import {Chart} from "chart.js";

@Component({
  selector: 'app-line-delta-analytics',
  templateUrl: './line-delta-analytics.component.html',
  styleUrls: ['./line-delta-analytics.component.css']
})
export class LineDeltaAnalyticsComponent implements OnInit, AfterViewChecked {
  monthNamesList?: any = [];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  lineList:any=[];
  lineDeltaId:any;
  deltaAnalyticsList?:any;
  deviationGraphData={
    labels:[],
    data:[]
  };
  deviationList:any=[];
  finalPlanList:any=[];
  monthRows:any =[];


  constructor(public executionFlowPageService:ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService,
              private spinner: NgxSpinnerService, private changeRef: ChangeDetectorRef) { }
  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }


  ngOnInit(): void {

    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };


    this.spinner.show();
      this.fgModuleService.getAllRscDtLinesList().subscribe((lineListResponse: any) => {
        console.log(lineListResponse);
        if (lineListResponse !== null && lineListResponse !== undefined) {/*
          lineListResponse?.linesDTOList.forEach((res: any) => {
            this.lineList.push({id: res.id, itemName: res.lineName});

          });*/
        /*  this.singleselectedItems[0] = this.lineList[0];*/
         this.lineList = lineListResponse;
         if(this.lineList.length > 0) {
           this.getlineWiseData(this.lineList[0]);
         }

          console.log( this.lineList);


        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });



  }
  onItemDeSelect(){}
  getlineWiseData(row:any){
    this.lineDeltaId = row.id;
    this.spinner.show();

    this.executionFlowPageService.getDeltaAnalyticsMonthNames().pipe(
      switchMap((monthNamesDataResponse: any) => {
        if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
          this.monthNamesList = monthNamesDataResponse?.columnNames;
          this.monthRows = monthNamesDataResponse?.rowNames;
          console.log(this.monthNamesList);
          for (let i = 0; i < monthNamesDataResponse?.columnNames.length; i++) {
            this.deviationGraphData.labels[i] = monthNamesDataResponse?.columnNames[i];
          }
        }
        return  this.fgModuleService.getFgLineDeltaAnalyticsData(this.lineDeltaId);
      })).subscribe((lineDeltaAnalyticsResponse:any)=>{
       if(lineDeltaAnalyticsResponse!==undefined && lineDeltaAnalyticsResponse!==null){
         console.log(lineDeltaAnalyticsResponse);
         this.deltaAnalyticsList =lineDeltaAnalyticsResponse;
         this.deviationBarChart();
       }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  }
  deviationBar:any;
  deviationBarChart() {
    const canvas : any = document.getElementById("line_Deviation_graph");
    const ctx = canvas.getContext("2d");
    if(this.deviationBar !== undefined)
      this.deviationBar.destroy();
    this.deviationBar = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: this.deviationGraphData.labels,
        datasets: [{
          label: "Deviation",
          type: "bar",
          backgroundColor: [
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',
            'rgba(210,105,30,0.6)',

          ],
          borderColor: [
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',
            'rgba(210,105,30,1)',

          ],
          borderWidth: 1,
          data:[
            this.deltaAnalyticsList?.deviationDetails[0],
            this.deltaAnalyticsList?.deviationDetails[1],
            this.deltaAnalyticsList?.deviationDetails[2],
            this.deltaAnalyticsList?.deviationDetails[3],
            this.deltaAnalyticsList?.deviationDetails[4],
            this.deltaAnalyticsList?.deviationDetails[5],
            this.deltaAnalyticsList?.deviationDetails[6],
            this.deltaAnalyticsList?.deviationDetails[7],
            this.deltaAnalyticsList?.deviationDetails[8],
            this.deltaAnalyticsList?.deviationDetails[9],
            this.deltaAnalyticsList?.deviationDetails[10],
            this.deltaAnalyticsList?.deviationDetails[11],
            this.deltaAnalyticsList?.deviationDetails[12],
            this.deltaAnalyticsList?.deviationDetails[13],
            this.deltaAnalyticsList?.deviationDetails[14],
            this.deltaAnalyticsList?.deviationDetails[15],
            this.deltaAnalyticsList?.deviationDetails[16],
            this.deltaAnalyticsList?.deviationDetails[17],
            this.deltaAnalyticsList?.deviationDetails[18],
            this.deltaAnalyticsList?.deviationDetails[19],
            this.deltaAnalyticsList?.deviationDetails[20],
            this.deltaAnalyticsList?.deviationDetails[21],
            this.deltaAnalyticsList?.deviationDetails[22],
          ],
        }
        ]
      },
      options: {
        animation: {

          onComplete() {
            const chartInstance = this.chart,
              ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = this.chart.config.options.defaultFontColor;
            this.data.datasets.forEach((dataset, i) =>{
              const meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach( (bar, index)=> {
                const data = dataset.data[index];
                ctx.fillText(data, bar._model.x, bar._model.y +2);
              });
            });
          }
        },
        responsive: true,
        maintainAspectRatio: true,
        legend: {display: true},
        scales: {
          xAxes: [{
           /* barPercentage: 0.5,*/
            offset: true,
            scaleLabel: {
              display: true,
              labelString: 'Months'
            },
            ticks: {fontSize: 10, beginAtZero: true,}
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Deviation',
            },
            ticks: {
              fontSize: 10,
              beginAtZero: true
            }
          }]
        },
      }
    });
  }


}
