import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fg-planning-horizon',
  templateUrl: './fg-planning-horizon.component.html',
  styleUrls: ['./fg-planning-horizon.component.css']
})
export class FgPlanningHorizonComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('deltaTab').click();
  }

}
