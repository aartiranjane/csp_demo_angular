import {Component, OnInit} from '@angular/core';
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {FgModuleService} from "../../fg-module.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-fg-skid-modal',
  templateUrl: './fg-skid-modal.component.html',
  styleUrls: ['./fg-skid-modal.component.css']
})
export class FgSkidModalComponent implements OnInit {
  rscDtSkidsList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService, private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit() {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.fgModuleService.getRscDtSkidsList().subscribe((rscDtSkidsResponse: any) => {
          if (rscDtSkidsResponse !== undefined && rscDtSkidsResponse !== null) {
            this.rscDtSkidsList = rscDtSkidsResponse;
            this.filterDataList = this.rscDtSkidsList;
            this.totalLengthOfCollection = this.filterDataList.length;
          }
          console.log(this.rscDtSkidsList);
          this.ngxSpinnerService.hide();
        },
        (error) => {
          console.log(error);
        });
    })
  }

//search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    console.log("csearchTerm1");
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.rscDtSkidsList);
    return this.rscDtSkidsList.filter(x => String(x.name)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.shifts)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.batchesPerShift)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.hrsPerShift)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}
