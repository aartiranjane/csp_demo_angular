import {Component, OnInit} from '@angular/core';
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {FgModuleService} from "../../fg-module.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-fg-lines-modal',
  templateUrl: './fg-lines-modal.component.html',
  styleUrls: ['./fg-lines-modal.component.css']
})
export class FgLinesModalComponent implements OnInit {
  rscDtLinesList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService, private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit() {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.fgModuleService.getRscDtLinesList().subscribe((rscDtLinesResponse: any) => {
          this.rscDtLinesList = rscDtLinesResponse;
          if (rscDtLinesResponse !== undefined && rscDtLinesResponse !== null) {
            this.rscDtLinesList = rscDtLinesResponse;
            this.filterDataList = this.rscDtLinesList;
            this.totalLengthOfCollection = this.filterDataList.length;
          }
          console.log(this.rscDtLinesList);
          this.ngxSpinnerService.hide();
        },
        (error) => {
          console.log(error);
        });
    })
  }

//search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    console.log("csearchTerm1");
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.rscDtLinesList);
    return this.rscDtLinesList.filter(x => String(x.name)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.capacityPerShift)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.shifts)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.speed)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.gupTime)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.modPerLinePerShift)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.hrsPerShift)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}

