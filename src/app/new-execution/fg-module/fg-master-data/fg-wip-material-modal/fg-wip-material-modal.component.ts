import {Component, OnInit} from '@angular/core';
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {FgModuleService} from "../../fg-module.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-fg-wip-material-modal',
  templateUrl: './fg-wip-material-modal.component.html',
  styleUrls: ['./fg-wip-material-modal.component.css']
})
export class FgWipMaterialModalComponent implements OnInit {
  fgWipMaterialListList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(
    private excelExportConstantsService: ExcelExportConstantsService,
    private executionFlowPageService: ExecutionFlowPageServiceService,
    private fgModuleService: FgModuleService, private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit() {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.fgModuleService.getFgWipMaterialList().subscribe((fgMaterialListResponse: any) => {
          if (fgMaterialListResponse !== undefined && fgMaterialListResponse !== null) {
            this.fgWipMaterialListList = fgMaterialListResponse;
            this.filterDataList = this.fgWipMaterialListList;
            this.totalLengthOfCollection = this.filterDataList.length;
          }
          this.ngxSpinnerService.hide();
        },
        (error) => {
          console.log(error);
        });
    });
  }

  //search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    return this.fgWipMaterialListList.filter(x => String(x.itemCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.moqValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.seriesValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stockValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.mapPrice)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stdPrice)?.toLowerCase().indexOf(v.toLowerCase()) !== -1
    );
  }
}
