import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {saveAs} from "file-saver";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {FgModuleService} from "../fg-module.service";


@Component({
  selector: 'app-fg-master-data',
  templateUrl: './fg-master-data.component.html',
  styleUrls: ['./fg-master-data.component.css']
})
export class FgMasterDataComponent implements OnInit  {
  materialList = [
    ['FG Materials', 'fgMaterialPopup'],
    ['SF/WIP Materials', 'fgWipMaterialPopup'],
    ['LINES', 'fgLinesPopup'],
    ['SKIDS', 'fgSkidPopup'],
  ];
  slobList = [
    ['SLOB Reason', 'fgSlobReasonPopup'],
    ['SLOB Remarks', 'fgSlobRemarkPopup'],
/*    ['Map', 'fgMapPopup'],
    ['STD Price', 'fgStdPricePopup']*/
  ];

  successMessage = '';
  errorMessage = '';
  isExcelDisable = false;
  currentMasterName!: string;
  constructor(private router: Router,private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService) { }

  ngOnInit(): void {
  }

  loadFgMasterData(fgMasterName :any, fgMasterUrl :any ) {
    this.currentMasterName = fgMasterName;
    this.router.navigate(['fgModule/fgMasterData/' + fgMasterUrl]);
  }
  exportAsXLSX() {
    switch (this.currentMasterName) {
      case 'FG Materials':
        this.fgModuleService.getfgMaterialMastersExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.fgMaterialMasters + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'SF/WIP Materials':
        this.fgModuleService.getFgWipMaterialExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.fgSFWIPMaterialsMasters + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'LINES':
        this.fgModuleService.getMasterDataLineExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.fgMasterDataLine + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'SKIDS':
        this.fgModuleService.getMasterDataSkidExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.fgMasterDataSkid + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      default:
        console.log("No such Name exists!");
        break;
    }

  }
}
