import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fg-backup-page',
  templateUrl: './fg-backup-page.component.html',
  styleUrls: ['./fg-backup-page.component.css']
})
export class FgBackupPageComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('nav-org-tabb').click();
  }

}
