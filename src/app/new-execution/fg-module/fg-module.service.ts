import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {UriService} from "../../uri.service";
import {ExecutionFlowPageServiceService} from "../execution-flow-page-service.service";
import {catchError, map} from "rxjs/operators";
import {throwError} from "rxjs";
import {ExcelExportConstantsService} from "../../excel-export-constants.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class FgModuleService {
  headers = [];
  endpoint = this.uriService.getResourceServerUri();
  ppHeaderId = this.executionFlowPageService.selectedHeaderId;

  constructor(private http: HttpClient,
              private uriService: UriService, private executionFlowPageService: ExecutionFlowPageServiceService, private excelExportConstantsService: ExcelExportConstantsService) {
  }

  getFgMpsExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMpsPlan}`, {responseType: 'blob'});
  }

  getFgLineSaturationData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/fg-line-saturation/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSkidSaturationData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/fg-skid-saturation/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/slob/fg/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobTotalValuesData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/slob/total-values/fg/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgLineExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.fgLineSaturation}`, {responseType: 'blob'});
  }

  getFgSkidExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.fgSkidSaturation}`, {responseType: 'blob'});
  }

  getSlobExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.fgSlobSaturation}`, {responseType: 'blob'});
  }

  getSlobSummaryExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.fgSlobSummary}`, {responseType: 'blob'});
  }

  getMasterDataLineExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.fgMasterDataLine}`, {responseType: 'blob'});
  }

  getFgWipMaterialExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.fgSFWIPMaterialsMasters}`, {responseType: 'blob'});
  }

  getfgMaterialMastersExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.fgMaterialMasters}`, {responseType: 'blob'});
  }

  getMasterDataSkidExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fg/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.fgMasterDataSkid}`, {responseType: 'blob'});
  }

  getFgLinesTabListData() {
    return this.http.get(this.endpoint + '/api/fg-line-saturation/all-lines/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllRscDtLinesList() {
    return this.http.get(this.endpoint + '/api/all-rsc-dt-lines/present-in-saturation', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllRscDtSkidsList() {
    return this.http.get(this.endpoint + '/api/all-rsc-dt-skid/present-in-saturation', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgLineWiseTableData(linesId: any) {
    return this.http.get(this.endpoint + '/api/fg-line-saturation/lines-details/' + this.executionFlowPageService.selectedHeaderId + '/' + linesId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSkidTabListData() {
    return this.http.get(this.endpoint + '/api/fg-skid-saturation/all-skids/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSkidWiseTableData(skidId: any) {
    return this.http.get(this.endpoint + '/api/fg-skid-saturation/skids-details/' + this.executionFlowPageService.selectedHeaderId + '/' + skidId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgLinesRangeWiseData() {
    return this.http.get(this.endpoint + '/api/fg-line-saturation-summary/range-wise-lines-details/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSkidRangeWiseData() {
    return this.http.get(this.endpoint + '/api/fg-skid-saturation-summary/range-wise-skids-details/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllDivision() {
    return this.http.get(this.endpoint + '/api/rsc-dt-division', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllBrand() {
    return this.http.get(this.endpoint + '/api/rsc-dt-brand', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllSignature() {
    return this.http.get(this.endpoint + '/api/rsc-dt-signature', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllFgMpsDashboardDataByBrand(divisionId: string, signatureId: any, brandId: any) {
    const params: Record<string, string> = {};
    if (divisionId) {
      params[`divisionId`] = divisionId;
    }
    if (signatureId) {
      params[`signatureId`] = signatureId;
    }
    if (brandId) {
      params[`brandId`] = brandId;
    }

    return this.http.get(this.endpoint + '/api/mps-plan-summary/by-brand/' + this.executionFlowPageService.selectedHeaderId, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: params
    }).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllMaterialCategory() {
    return this.http.get(this.endpoint + '/api/rsc-dt-material-category', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllMaterialSubCategory() {
    return this.http.get(this.endpoint + '/api/rsc-dt-material-sub-category', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllFgMpsDashboardDataByCategory(categoryId: string, subCategoryId: any) {
    const params: Record<string, string> = {};
    if (categoryId) {
      params[`categoryId`] = categoryId;
    }
    if (subCategoryId) {
      params[`subCategoryId`] = subCategoryId;
    }
    return this.http.get(this.endpoint + '/api/mps-plan-summary/by-category/' + this.executionFlowPageService.selectedHeaderId, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: params
    }).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllLines() {
    return this.http.get(this.endpoint + '/api/rsc-dt-lines', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllSkids() {
    return this.http.get(this.endpoint + '/api/rsc-dt-skids', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllFgMpsDashboardDataByLinesAndSkids(lineId: string, skidId: any) {
    const params: Record<string, string> = {};
    if (lineId) {
      params[`lineId`] = lineId;
    }
    if (skidId) {
      params[`skidId`] = skidId;
    }
    return this.http.get(this.endpoint + '/api/mps-plan-summary/by-lines-and-skids/' + this.executionFlowPageService.selectedHeaderId, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
      params: params
    }).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobSummaryTableData() {
    return this.http.get(this.endpoint + '/api/slob-summary/fg/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  fgMasterDataSkid

  getFgSlobDeviationData() {
    return this.http.get(this.endpoint + '/api/slob-deviation/fg/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobDivisionDeviationData() {
    return this.http.get(this.endpoint + '/api/slob-deviation/division-wise/fg/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobDashboardDropdownData() {
    return this.http.get(this.endpoint + '/api/rsc-dt-fg-type', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobDashboardSlobDetailsData(slobDetailsId: any) {
    return this.http.get(this.endpoint + '/api/slob-summary/division-wise/dashboard/fg/' + this.executionFlowPageService.selectedHeaderId + '/' + slobDetailsId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobDashboardInventoryData(iqId: any) {
    return this.http.get(this.endpoint + '/api/slob-summary/division-wise/iq-dashboard/fg/' + this.executionFlowPageService.selectedHeaderId + '/' + iqId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobDashboardIqTotalValuesData() {
    return this.http.get(this.endpoint + '/api/slob-summary/dashboard/fg/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobMovementDashboardData() {
    return this.http.get(this.endpoint + '/api/slob-movement/fg', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgInventoryQualityDashboardData() {
    return this.http.get(this.endpoint + '/api/slob-inventory-quality-index/fg', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSlobSummaryDashboardTreeStructureData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/slob-summary/fg-types-and-division/fg/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getSlobSummaryData(ppheaderId: any, fgtypeId: any, divisionId: any) {
    let params = new HttpParams();
    if (divisionId) {
      params = params.append('divisionId', divisionId);
    }
    return this.http.get(this.endpoint + '/api/fg-slob-summary/division-wise/dashboard/' + ppheaderId + "/" + fgtypeId, {params}).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgLineDeltaAnalyticsData(lineDeltaId: any) {
    return this.http.get(this.endpoint + '/api/line-saturation-delta-analysis/' + lineDeltaId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSkidDeltaAnalyticsData(skidDeltaId: any) {
    return this.http.get(this.endpoint + '/api/skid-saturation-delta-analysis/' + skidDeltaId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgMaterialList() {
    return this.http.get(this.endpoint + '/api/material-masters/fg-material-list', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgWipMaterialList() {
    return this.http.get(this.endpoint + '/api/material-masters/wip-material-list', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRscDtLinesList() {
    return this.http.get(this.endpoint + '/api/rsc-dt-lines', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRscDtSkidsList() {
    return this.http.get(this.endpoint + '/api/rsc-dt-skids', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }


  getFgLinesList() {
    return this.http.get(this.endpoint + '/api/rsc-dt-lines', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgSkidsList() {
    return this.http.get(this.endpoint + '/api/rsc-dt-skids', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getfgMPSTriangleAnalysisExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fgAnnualExport/export/' + this.excelExportConstantsService.fgMpsTriangleAnalysis}`, {responseType: 'blob'});
  }

  getfgSlobMovementExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fgAnnualExport/export/' + this.excelExportConstantsService.fgSlobMovement}`, {responseType: 'blob'});
  }

  getfgInventoryQualityIndexExcelFile() {
    return this.http.get(`${this.endpoint + '/api/fgAnnualExport/export/' + this.excelExportConstantsService.fgInventoryQualityIndex}`, {responseType: 'blob'});
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
