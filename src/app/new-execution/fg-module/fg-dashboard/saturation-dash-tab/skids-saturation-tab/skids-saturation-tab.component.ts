import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../../execution-flow-page-service.service";
import {FgModuleService} from "../../../fg-module.service";
import {switchMap} from "rxjs/operators";
import * as $ from "jquery";
import {Chart} from "chart.js";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-skids-saturation-tab',
  templateUrl: './skids-saturation-tab.component.html',
  styleUrls: ['./skids-saturation-tab.component.css']
})
export class SkidsSaturationTabComponent implements OnInit , AfterViewChecked,OnDestroy{
  $subs:Subscription;
  firstPanel = true;
  secondPanel = false;
  thirdPanel =false;
  rangeWiseSkidSaturationChartData: any = [];
  monthNamesList?: any = [];
  skidList: any=[];
  skidId:any;
  skidWiseTableList: any =[];
  rangeWiseSkidSaturation: any =[];
  labels= [];
  plan= [];
  capacity=[];
  saturation=[];
  highSaturationRangeSkidsDTOList:any=[];
  mediumSaturationRangeSkidsDTOList:any=[];
  lowSaturationRangeSkidsDTOList:any=[];
  skidListId:any;


  constructor(private spinner: NgxSpinnerService,
              public executionFlowPageService: ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService,
              private changeRef: ChangeDetectorRef) { }
  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }


  ngOnInit(): void {
    this.spinner.show();
    this.$subs= this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      console.log(value1);
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.fgModuleService.getFgSkidRangeWiseData();
        })).pipe(
        switchMap((rangeWiseSkidSaturationResponse: any) => {
          if (rangeWiseSkidSaturationResponse !== null && rangeWiseSkidSaturationResponse !== undefined) {
            this.rangeWiseSkidSaturation = rangeWiseSkidSaturationResponse;
            console.log(this.rangeWiseSkidSaturation);
            this.highSaturationRangeSkidsDTOList =rangeWiseSkidSaturationResponse?.highSaturationRangeSkidsDTOList;
            this.mediumSaturationRangeSkidsDTOList = rangeWiseSkidSaturationResponse?.mediumSaturationRangeSkidsDTOList;
            this.lowSaturationRangeSkidsDTOList = rangeWiseSkidSaturationResponse?.lowSaturationRangeSkidsDTOList;
            this.rangeWiseSkidSaturationChart();
          }
          return this.fgModuleService.getFgSkidTabListData();
        })).subscribe((skidListResponse: any) => {
        console.log(skidListResponse);
        if (skidListResponse !== null && skidListResponse !== undefined) {
          this.skidList = skidListResponse?.skidsDTOList;
          if(this.skidList.length >0) {
            this.getskidWiseData(this.skidList[0]);
          }
          console.log( this.skidList);


        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })

    $('.panel-heading ').on('click', function (e) {
      if ($(this).parents('.panel').children('.panel-collapse').hasClass('show')) {
        e.stopPropagation();
      }
    });

  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  isCheck:any=1;
  getskidWiseData(row:any) {
    console.log(this.skidListId+"="+row.id);
    if (row.id == this.skidListId) {
      this.isCheck = 1;
      this.skidListId = row.id;
    } else {
      if (this.isCheck == 1) {
        this.skidListId = row.id;
      }
      this.skidWiseTableList = [];
      this.skidId = row.id;
      console.log(row.id);
      this.fgModuleService.getFgSkidWiseTableData(this.skidId).subscribe((skidWiseTableResponse: any) => {

        if (skidWiseTableResponse !== null && skidWiseTableResponse !== undefined) {
          this.skidWiseTableList = skidWiseTableResponse?.fgSkidSaturationDTO;

          this.mixedChart();
        }
      }, (error) => {
        console.log(error);
      });
    }
  }


  rangeWiseSkidSaturationChart(){
    this.rangeWiseSkidSaturationChartData = new Chart('SKID_SATURATION_RANGEWISE', {
      type: 'doughnut',
      data: {
        labels: ['GreaterThan','Between','LessThan'],
        datasets: [
          {
            data: [this.rangeWiseSkidSaturation?.highSaturationRangeListCount,
                   this.rangeWiseSkidSaturation?.mediumSaturationRangeListCount,
                   this.rangeWiseSkidSaturation?.lowSaturationRangeListCount],
            backgroundColor: ['#db0000',
              '#7b97ff',
              'green'],
            fill: false
          },
        ]
      },
      options: {

        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }

  window1: any;

  mixedChart() {
    const canvas : any = document.getElementById("Mixed_chart");
    const ctx = canvas.getContext("2d");
    if(this.window1 !== undefined)
      this.window1.destroy();
    this.window1 = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [this.monthNamesList?.month1?.monthNameAlias,
          this.monthNamesList?.month2?.monthNameAlias,
          this.monthNamesList?.month3?.monthNameAlias,
          this.monthNamesList?.month4?.monthNameAlias,
          this.monthNamesList?.month5?.monthNameAlias,
          this.monthNamesList?.month6?.monthNameAlias,
          this.monthNamesList?.month7?.monthNameAlias,
          this.monthNamesList?.month8?.monthNameAlias,
          this.monthNamesList?.month9?.monthNameAlias,
          this.monthNamesList?.month10?.monthNameAlias,
          this.monthNamesList?.month11?.monthNameAlias,
          this.monthNamesList?.month12?.monthNameAlias,],
        datasets: [{
          label: "Plan",
          type: "line",
          borderColor: "darkslategrey",
          data:[this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m1TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m2TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m3TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m4TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m5TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m6TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m7TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m8TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m9TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m10TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m11TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m12TotalRatioOfPlanQtyWithTs],
          fill: false
        }, {
          label: "Capacity",
          type: "line",
          borderColor: "#3e95cd",
          data: [this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month1Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month2Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month3Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month4Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month5Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month6Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month7Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month8Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month9Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month10Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month11Capacity,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.month12Capacity],
          fill: false
        } /*{
         /!* label: "Saturation",*!/
       /!*   type: "bar",*!/
          /!*backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 99, 132, 0.2)',

          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
            'rgba(255, 99, 132, 1)',
          ],*!/
         /!* borderWidth: 1,*!/
         /!* data: [this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m1SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m2SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m3SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m4SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m5SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m6SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m7SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m8SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m9SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m10SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m11SkidSaturation,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m12SkidSaturation],*!/
       /!*   data: [this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m1TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m2TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m3TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m4TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m5TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m6TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m7TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m8TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m9TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m10TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m11TotalRatioOfPlanQtyWithTs,
            this.skidWiseTableList?.rscIotFgSkidSaturationDTO?.m12TotalRatioOfPlanQtyWithTs],*!/
        }*/
        ]
      },
      options: {
         responsive: true,
         maintainAspectRatio: true,
        legend: {display: true},
        scales: {
          xAxes: [{
            offset: true,
            scaleLabel: {
              display: true,
              labelString: 'Months'
            },
            ticks: {fontSize: 10, beginAtZero: false,}
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Quantity',
            },
            ticks: {
              fontSize: 10,
              beginAtZero: false
            }
          }]
        },
      }
    });
  }
}
