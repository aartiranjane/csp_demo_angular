import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import * as $ from "jquery";
import {Chart} from "chart.js";
import {switchMap} from "rxjs/operators";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../../execution-flow-page-service.service";
import {FgModuleService} from "../../../fg-module.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-line-saturation-tab',
  templateUrl: './line-saturation-tab.component.html',
  styleUrls: ['./line-saturation-tab.component.css']
})
export class LineSaturationTabComponent implements OnInit, AfterViewChecked,OnDestroy {
  $subs:Subscription;
  firstPanel = true;
  secondPanel = false;
  thirdPanel =false;
  rangeWiseLinesSaturationChartData: any = [];
  monthNamesList?: any = [];
  lineList: any=[];
  linesId:any;
  lineWiseTableList: any =[];
  rangeWiseLinesSaturation:any =[];
    labels= [];
    plan= [];
    capacity=[];
    saturation=[];
  highSaturationRangeLinesDTOList: any =[];
  mediumSaturationRangeLinesDTOList: any =[];
  lowSaturationRangeLinesDTOList:any =[];
  lineListId: any;


  constructor(private spinner: NgxSpinnerService,
              public executionFlowPageService: ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService,
              private changeRef: ChangeDetectorRef) { }
  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }


  ngOnInit(): void {
    this.spinner.show();
    this.$subs = this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      console.log(value1);
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.fgModuleService.getFgLinesRangeWiseData();
        })).pipe(
        switchMap((rangeWiseLinesSaturationResponse: any) => {
          if (rangeWiseLinesSaturationResponse !== null && rangeWiseLinesSaturationResponse !== undefined) {
            this.rangeWiseLinesSaturation = rangeWiseLinesSaturationResponse;
            console.log(this.rangeWiseLinesSaturation);
            this.highSaturationRangeLinesDTOList = rangeWiseLinesSaturationResponse?.highSaturationRangeLinesDTOList;
            this.mediumSaturationRangeLinesDTOList = rangeWiseLinesSaturationResponse?.mediumSaturationRangeLinesDTOList;
            this.lowSaturationRangeLinesDTOList = rangeWiseLinesSaturationResponse?.lowSaturationRangeLinesDTOList;
            this.rangeWiseLinesSaturationChart();
          }
          return this.fgModuleService.getFgLinesTabListData();
        })).subscribe((lineListResponse: any) => {
        console.log(lineListResponse);
        if (lineListResponse !== null && lineListResponse !== undefined) {
          this.lineList = lineListResponse?.linesDTOList;
          if(this.lineList.length > 0) {
            this.getlineWiseData(this.lineList[0]);
          }
          console.log( this.lineList);


        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });

    });

    $('.panel-heading').on('click', function (e) {
      if ($(this).parents('.panel').children('.panel-collapse').hasClass('show')) {
        e.stopPropagation();
      }
    });
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }


  isCheck:any =1;
  getlineWiseData(row:any){
    if(row.id == this.lineListId)
    {
      this.lineListId=row.id;
    }else {
      if (this.isCheck == 1) {
        this.lineListId = row.id;
      } this.lineWiseTableList =[];
      this.linesId = row.id;
        this.fgModuleService.getFgLineWiseTableData(this.linesId).subscribe((linesWiseTableResponse: any) => {
          if (linesWiseTableResponse !== null && linesWiseTableResponse !== undefined) {
            this.lineWiseTableList = linesWiseTableResponse?.fgLineSaturationDTO;
            this.mixedChart();
          }
        }, (error) => {
          console.log(error);
        });
      }
  }

  rangeWiseLinesSaturationChart(){
    this.rangeWiseLinesSaturationChartData = new Chart('LINES_SATURATION_RANGEWISE', {
      type: 'doughnut',
      data: {
        labels: ['GreaterThan','Between','LessThan'],
        datasets: [
          {
            data: [this.rangeWiseLinesSaturation?.highSaturationRangeListCount,
              this.rangeWiseLinesSaturation?.mediumSaturationRangeListCount,
              this.rangeWiseLinesSaturation?.lowSaturationRangeListCount],
            backgroundColor: ['#db0000',
              '#7b97ff',
              'green'],
            fill: false
          },
        ]
      },
      options: {
        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }

  window1: any;

  mixedChart() {
    const canvas : any = document.getElementById("Mixed_chart");
    const ctx = canvas.getContext("2d");
    if(this.window1 !== undefined)
      this.window1.destroy();
    this.window1 = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [this.monthNamesList?.month1?.monthNameAlias,
          this.monthNamesList?.month2?.monthNameAlias,
          this.monthNamesList?.month3?.monthNameAlias,
          this.monthNamesList?.month4?.monthNameAlias,
          this.monthNamesList?.month5?.monthNameAlias,
          this.monthNamesList?.month6?.monthNameAlias,
          this.monthNamesList?.month7?.monthNameAlias,
          this.monthNamesList?.month8?.monthNameAlias,
          this.monthNamesList?.month9?.monthNameAlias,
          this.monthNamesList?.month10?.monthNameAlias,
          this.monthNamesList?.month11?.monthNameAlias,
          this.monthNamesList?.month12?.monthNameAlias,],
        datasets: [{
          label: "Plan",
          type: "line",
          borderColor: "darkslategrey",
          data:[this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m1TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m2TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m3TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m4TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m5TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m6TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m7TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m8TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m9TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m10TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m11TotalMpsPlanQty/1000,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.m12TotalMpsPlanQty/1000],
          fill: false
        }, {
          label: "Capacity",
          type: "line",
          borderColor: "#3e95cd",
          data: [this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month1Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month2Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month3Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month4Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month5Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month6Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month7Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month8Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month9Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month10Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month11Capacity,
            this.lineWiseTableList?.rscIotFgLineSaturationDTOS?.month12Capacity],
          fill: false
        }
        ]
      },
      options: {
         responsive: true,
         maintainAspectRatio: true,
        legend: {display: true},
        scales: {
          xAxes: [{
            offset: true,
            scaleLabel: {
              display: true,
              labelString: 'Months'
            },
            ticks: {fontSize: 10, beginAtZero: true,}
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Quantity',
            },
            ticks: {
              fontSize: 10,
              beginAtZero: true
            }
          }]
        },
      }
    });
  }
}
