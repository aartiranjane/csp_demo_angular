import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-saturation-dash-tab',
  templateUrl: './saturation-dash-tab.component.html',
  styleUrls: ['./saturation-dash-tab.component.css']
})
export class SaturationDashTabComponent implements OnInit, AfterViewChecked {

  constructor(private changeRef: ChangeDetectorRef) { }
  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }
  ngOnInit(): void {
    document.getElementById('linesTab').click();


  }

}
