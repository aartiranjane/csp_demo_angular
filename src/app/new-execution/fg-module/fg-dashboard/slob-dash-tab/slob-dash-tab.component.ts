import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Chart} from "chart.js";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {FgModuleService} from "../../fg-module.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-slob-dash-tab',
  templateUrl: './slob-dash-tab.component.html',
  styleUrls: ['./slob-dash-tab.component.css']
})
export class SlobDashTabComponent implements OnInit, AfterViewChecked,OnDestroy {
  $subs:Subscription;
  nodes: any = [];
  divisionId: any;
  fgtypeId: any;
  slobSummaryList: any = [];
  valueBasis: any;
  percentageBasis: any;
  slobDetails: any;
  fgType: string;
  materialName: string;
  uniqueNameList: string;
  materialUniqueName: string;
  typeId: any;

  /*totalIqValuesList: any = [];
  materialSelectList: any = [];
  singleselectedMaterial: any = [];
  singledropdownSettingsForMaterial = {};

  material2SelectList: any = [];
  singleselectedMaterial2: any = [];
  singledropdownSettingsForMaterial2 = {};

  doughnutLeft: any = [];
  doughnutMiddle: any = [];
  doughnutRight: any = [];
  /!* selectMaterialList :any =[];*!/
  slobDetailsId: any;
  slobDetailsMaterialWiseData: any = [];
  inventoryDetailsList: any = [];
  slobValuesId: any;
  iqId: any;
  slobValue: any;
  obValue: any;
  stValue = 34351588;
  sbValue = 2804222;
  inventoryChild: any = [];*/

  constructor(
    private spinner: NgxSpinnerService,
    public executionFlowPageService: ExecutionFlowPageServiceService,
    private fgModuleService: FgModuleService,
    private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
      this.spinner.show();
      this.fgModuleService.getFgSlobSummaryDashboardTreeStructureData(this.executionFlowPageService.selectedHeaderId).subscribe((treeStructureResponse: any) => {
        this.nodes = treeStructureResponse;
        this.nodes = [];
        if (treeStructureResponse !== null && treeStructureResponse !== undefined) {
          if (this.slobSummaryList.length === 0) {
            this.fgtypeId = treeStructureResponse.id;
            this.fgModuleService.getSlobSummaryData(this.executionFlowPageService.selectedHeaderId, this.fgtypeId, this.divisionId).subscribe((summaryResponse: any) => {
              this.materialName = summaryResponse?.fgType;
              this.slobSummaryList = summaryResponse?.fgSlobSummaryDashboardDTO;
              this.valueBasisChart();
              this.percentageBasisChart();
              this.slobDetailsChart();
            });
          }
          this.nodes.push({
            id: treeStructureResponse?.id,
            name: treeStructureResponse?.name,
            childs: treeStructureResponse?.childs,
            parentId: treeStructureResponse?.parentId,
            uniqueName: treeStructureResponse?.uniqueName,
            cssClass: 'nodeBgColor',
          });
        }

        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    });
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  isCheck: any = 1;

  onNodeSelection(parentId: any, id: any, item: any) {
    if (item.uniqueName === this.uniqueNameList) {
      this.isCheck = 1;
      this.uniqueNameList = item.uniqueName;
    } else {
      if (this.isCheck === 1) {
        this.uniqueNameList = item.uniqueName;
      }

      if (parentId == null) {
        this.fgtypeId = id;
        this.divisionId = null;
      } else {
        this.fgtypeId = parentId;
        this.divisionId = id;
      }
      this.fgModuleService.getSlobSummaryData(this.executionFlowPageService.selectedHeaderId, this.fgtypeId, this.divisionId).subscribe((summaryResponse: any) => {
        if (parentId == null) {
          this.materialName = summaryResponse?.fgType;
          this.materialUniqueName = item.uniqueName;
          this.typeId = parentId;
        } else {
          this.materialName = summaryResponse?.divisionName;
          this.materialUniqueName = item.uniqueName;
          this.typeId = parentId;
        }
        this.slobSummaryList = summaryResponse?.fgSlobSummaryDashboardDTO;
        this.valueBasisChart();
        this.percentageBasisChart();
        this.slobDetailsChart();
        this.colorChart();
      });
    }

  }

  colorChart() {
    for (var i = 0; i < this.nodes.length; i++) {
      for (var j = 0; j < this.nodes[i].childs.length; j++) {
        if (this.nodes[i].childs[j].uniqueName === this.materialUniqueName) {
          this.nodes[i].childs[j].cssClass = 'nodeBgColor';
        } else {
          this.nodes[i].childs[j].cssClass = 'nodeColor';
        }
        for (var k = 0; k < this.nodes[i].childs[j].childs.length; k++) {
          if (this.nodes[i].uniqueName === this.materialUniqueName) {
            this.nodes[i].cssClass = 'nodeBgColor';
            this.nodes[i].childs[j].cssClass = 'nodeColor';
            this.nodes[i].childs[j].childs[k].cssClass = 'nodeColor';
          } else {
            if (this.materialName !== this.nodes[i].childs[j].name && this.materialName !== this.nodes[i].childs[j].name) {
              if (this.nodes[i].childs[j].childs[k].parentId === this.typeId) {
                this.nodes[i].childs[j].cssClass = 'nodeBlackColor';
                this.nodes[i].cssClass = 'nodeBlackColor';
                if (this.nodes[i].childs[j].childs[k].uniqueName === this.materialUniqueName) {
                  this.nodes[i].childs[j].childs[k].cssClass = 'nodeBgColor';
                } else {
                  this.nodes[i].childs[j].childs[k].cssClass = 'nodeColor';
                }
              } else {
                this.nodes[i].childs[j].cssClass = 'nodeColor';
              }
              if (this.nodes[i].childs[j].childs[k].uniqueName === this.materialUniqueName) {
                this.nodes[i].childs[j].childs[k].cssClass = 'nodeBgColor';
              } else {
                this.nodes[i].childs[j].childs[k].cssClass = 'nodeColor';
              }
            } else {
              this.nodes[i].cssClass = 'nodeBlackColor';
              if (this.nodes[i].childs[j].uniqueName === this.materialUniqueName) {
                this.nodes[i].childs[j].cssClass = 'nodeBgColor';
              } else {
                this.nodes[i].childs[j].cssClass = 'nodeColor';
              }
              if (this.nodes[i].childs[j].childs[k].uniqueName === this.materialUniqueName) {
                this.nodes[i].childs[j].childs[k].cssClass = 'nodeBgColor';
              } else {
                this.nodes[i].childs[j].childs[k].cssClass = 'nodeColor';
              }
            }
          }
        }
      }
    }
  }

  valueBasisDougnut: any;

  valueBasisChart() {
    const canvas: any = document.getElementById("doughnut_slob_stock");
    if (this.valueBasisDougnut !== undefined)
      this.valueBasisDougnut.destroy();
    if (canvas.getContext) {
      const ctx = canvas.getContext("2d");
      this.valueBasisDougnut = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: ['SLOB', 'Stock'],
          datasets: [
            {
              data: [this.slobSummaryList?.totalSlobValue,
                this.slobSummaryList?.totalStockValue],
              backgroundColor: ['#db0000',
                'green'],
              fill: false
            },
          ]
        },
        options: {

          cutoutPercentage: 40,
          legend: {
            display: false
          },
          tooltips: {
            enabled: true,
            titleFontSize: 12,
            bodyFontSize: 12
          },
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            labels: {
              render: 'percentage',
              fontColor: ['green', 'white', 'red'],
              precision: 2,
              arc: true,
            }
          },
        }
      });
    }
  }

  percentageBasisDoughnut: any;

  percentageBasisChart() {
    const canvas: any = document.getElementById("doughnut_Ob_Sm_Iq");
    if (this.percentageBasisDoughnut !== undefined)
      this.percentageBasisDoughnut.destroy();
    if (canvas.getContext) {
      const ctx = canvas.getContext("2d");
      this.percentageBasisDoughnut = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: ['OB', 'SM', 'IQ'],
          datasets: [
            {
              data: [this.slobSummaryList?.obPercentage,
                this.slobSummaryList?.slPercentage,
                this.slobSummaryList?.iqPercentage],
              backgroundColor: ['#db0000',
                '#ff8c00',
                'green'],
              fill: false
            },
          ]
        },
        options: {

          cutoutPercentage: 40,
          legend: {
            display: false
          },
          tooltips: {
            enabled: true,
            titleFontSize: 12,
            bodyFontSize: 12
          },
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            labels: {
              render: 'percentage',
              fontColor: ['green', 'white', 'red'],
              precision: 2,
              arc: true,
            }
          },
        }
      });
    }
  }

  slobDetailsDoughnut: any;

  slobDetailsChart() {
    const canvas: any = document.getElementById("doughnut_slob_details");
    if (this.slobDetailsDoughnut !== undefined)
      this.slobDetailsDoughnut.destroy();
    if (canvas.getContext) {
      const ctx = canvas.getContext("2d");
      this.slobDetailsDoughnut = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: ['OB', 'SM'],
          datasets: [
            {
              data: [this.slobSummaryList?.totalOBValue,
                this.slobSummaryList?.totalSLValue],
              backgroundColor: ['#db0000',
                '#ff8c00'],
              fill: false
            },
          ]
        },
        options: {

          cutoutPercentage: 40,
          legend: {
            display: false
          },
          tooltips: {
            enabled: true,
            titleFontSize: 12,
            bodyFontSize: 12
          },
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            labels: {
              render: 'percentage',
              fontColor: ['green', 'white', 'red'],
              precision: 2,
              arc: true,
            }
          },
        }
      });
    }
  }
}
