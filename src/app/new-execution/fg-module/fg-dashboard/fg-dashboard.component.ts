import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fg-dashboard',
  templateUrl: './fg-dashboard.component.html',
  styleUrls: ['./fg-dashboard.component.css']
})
export class FgDashboardComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  thirdTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('mpsTab').click();
  }

}
