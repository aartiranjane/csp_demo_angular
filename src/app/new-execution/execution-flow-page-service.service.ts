import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UriService} from "../uri.service";
import {catchError, map} from "rxjs/operators";
import {BehaviorSubject, Observable, Subject, throwError} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ExecutionFlowPageServiceService {
  statusIdp: boolean;
  endpoint = this.uriService.getResourceServerUri();
  selectedHeaderId!: number;
  selectedPlanDate!: any;
  loginUsers: any;
  adminLogin: any;
  selectedPlanName:any;
  moduleName:any;
  moduleWisePage:any;
  moduleNameDisabled = true ;
  mpsPlanHeader:any=[];
  backupMonthList:any=[];
  roleModuleList:any=[];
  userName:any;
  selectedModuleName:any;
  msgHideShow:any;
  public subject = new Subject<any>();

  constructor(private http: HttpClient,
              private uriService: UriService) {
  }

  setselectedHeaderId(selectedHeaderId: number) {
    this.updatedPpHeaderId.next(selectedHeaderId);
  }

  private updatedPpHeaderId = new BehaviorSubject(this.selectedHeaderId);
  getPpHeaderId = this.updatedPpHeaderId.asObservable();

  getMpsHeaderData() {
    return this.http.get(this.endpoint + '/api/mps-plan-header', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getCurrentMonthsMpsHeaderData() {
    return this.http.get(this.endpoint + '/api/current-month-mps-plan-header', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPlanManagementData() {
    return this.http.get(this.endpoint + '/api/all-mps-plan-header', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPlanMpsDataWithoutBackup() {
    return this.http.get(this.endpoint + '/api/get-all-mps-plan/without-backup', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getMonthNamesData(ppheader: any) {
    return this.http.get(this.endpoint + '/api/month-names/' + ppheader, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getMpsData(ppheader: any) {
    return this.http.get(this.endpoint + '/api/mps-plan/' + ppheader, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }
  getLatestMpsData() {
    return this.http.get(this.endpoint + '/api/mps-plan/latest', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }
  getFgCodeListData() {
    return this.http.get(this.endpoint + '/api/mps/fg-list/' + this.selectedHeaderId,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getMpsByFg(selectedFgCode: any) {
    return this.http.get(this.endpoint + '/api/mps-plan/by-fg/' + this.selectedHeaderId + '/' + selectedFgCode,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updatePPheaderDetails(rscMtPPheaderDTOS: any) {
    return this.http.put(this.endpoint + '/api/mps-plan-header', JSON.stringify(rscMtPPheaderDTOS), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getDeltaAnalyticsMonthNames() {
    return this.http.get(this.endpoint + '/api/month-names/delta-analysis', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }
  rmCoverDaysAndSlobExcelExport(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-cover-days/excel/export/' + this.selectedHeaderId}`, {responseType: 'text'});
  }

  pmCoverDaysAndSlobExcelExport(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-cover-days/excel/export/' + this.selectedHeaderId}`, {responseType: 'text'});
  }
  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }

}
