import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {saveAs} from "file-saver";
import {PmServiceService} from "../../pm-service.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {PmExportService} from "../../pm-export.service";
import {switchMap} from "rxjs/operators";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-gross-consumption-new',
  templateUrl: './gross-consumption-new.component.html',
  styleUrls: ['./gross-consumption-new.component.css']
})
export class GrossConsumptionNewComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  grossConsumptionSearchTerm: any;
  grossConsumptionDataList: any = [];
  monthNamesList?: any = [];
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;
  constructor(private router: Router, private pmService: PmServiceService,
              private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getGrossConsumptionData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((grossConsumptionResponse: any) => {
        if (grossConsumptionResponse !== null && grossConsumptionResponse !== undefined) {
          this.grossConsumptionDataList = grossConsumptionResponse?.itemCodes;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })

  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  exportAsXLSX() {
    this.pmExportService.getPmGrossExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
        saveAs(blob, this.excelExportConstantsService.pmGrossConsumption + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
