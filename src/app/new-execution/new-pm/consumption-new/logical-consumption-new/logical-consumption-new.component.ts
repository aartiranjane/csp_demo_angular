import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {PmServiceService} from "../../pm-service.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {saveAs} from 'file-saver';
import {PmExportService} from "../../pm-export.service";
import {switchMap} from "rxjs/operators";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-logical-consumption-new',
  templateUrl: './logical-consumption-new.component.html',
  styleUrls: ['./logical-consumption-new.component.css']
})
export class LogicalConsumptionNewComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  nodes: any = [];
  logicalConsumptionSearchTerm: any;
  selectedParentCode: any;
  fgCodeSearchTerm: any;
  logicalConsumptionDataList: any = [];
  fgCodeList: any = [];
  monthNamesList?: any = [];
  selectedFgCode: any;
  mpsByFgDataList: any = [];
  successMessage = '';
  errorMessage = '';
  logicalConsumptionList = [];
  isExcelDisable=false;
  constructor(private router: Router, private pmService: PmServiceService,
              private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) {
  }


  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getPmLogicalConsumptionAllData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((logicalResponse: any) => {
       if (logicalResponse !== null && logicalResponse !== undefined) {
          this.logicalConsumptionList = logicalResponse;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })

  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  getFgDetails(row: any) {
    this.selectedFgCode = row.parentId;
    this.executionFlowPageService.getMpsByFg(this.selectedFgCode).subscribe((mpsByFgResponse: any) => {
      if (mpsByFgResponse !== null && mpsByFgResponse !== undefined) {
        this.mpsByFgDataList = mpsByFgResponse;
        console.log(this.mpsByFgDataList);
      }
      this.spinner.hide();
    }, (error) => {
      console.log(error);
    });

  }

  exportAsXLSX() {
    this.pmExportService.getPmLogicalExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmLogicalConsumption + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }


  displayFgRelationGraph() {
    this.pmService.getFgRelationData(this.selectedFgCode).subscribe((fgRelationResponse: any) => {
      this.nodes = [];
      if (fgRelationResponse !== null && fgRelationResponse !== undefined) {
        this.nodes.push({name: fgRelationResponse.name, childs: fgRelationResponse.childs});
      }
    }, (error) => {
      console.log(error);
    });
  }

  test(ck: any) {
  }

}
