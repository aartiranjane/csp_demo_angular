import {Component, OnDestroy, OnInit} from '@angular/core';
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {saveAs} from "file-saver";
import {PmExportService} from "../../pm-export.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-supplier-wise-new',
  templateUrl: './supplier-wise-new.component.html',
  styleUrls: ['./supplier-wise-new.component.css']
})
export class SupplierWiseNewComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  currentIndex: number = 0;
  monthNamesList?: any = [];
  selectedId: any;
  len:any=0;
  supplierWiseDataList: any = [];
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;
  constructor(private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private pmExportService: PmExportService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      console.log(resValue);
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getPmSupplierWiseData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((supplierWiseResponse: any) => {
        console.log(supplierWiseResponse );
        if (supplierWiseResponse !== null && supplierWiseResponse !== undefined) {
          this.supplierWiseDataList = supplierWiseResponse;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  exportAsXLSX() {
    this.pmExportService.getPmSupplierWiseExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmSupplierWiseMouldSaturation + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
