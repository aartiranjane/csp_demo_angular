import {Component, OnDestroy, OnInit} from '@angular/core';
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {saveAs} from "file-saver";
import {PmExportService} from "../../pm-export.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-mould-wise-new',
  templateUrl: './mould-wise-new.component.html',
  styleUrls: ['./mould-wise-new.component.css']
})
export class MouldWiseNewComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  monthNamesList?: any = [];
  mouldWiseList: any = [];
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;
  constructor(private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private pmExportService: PmExportService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      console.log(resValue);
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getPmMouldWiseData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((mouldWiseResponse: any) => {
        if (mouldWiseResponse !== null && mouldWiseResponse !== undefined) {
          console.log(mouldWiseResponse);
          this.mouldWiseList = mouldWiseResponse;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    });

  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  exportAsXLSX() {
    this.pmExportService.getPmMouldWiseExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmMoudWiseMouldSaturation + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }


}
