import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pm-planning-horizon',
  templateUrl: './pm-planning-horizon.component.html',
  styleUrls: ['./pm-planning-horizon.component.css']
})
export class PmPlanningHorizonComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('pmOtifTab').click();
  }

}
