import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {PmServiceService} from "../../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import * as $ from "jquery";
import {switchMap} from "rxjs/operators";
import {Chart} from "chart.js";
import {ExecutionFlowPageServiceService} from "../../../../execution-flow-page-service.service";
import {saveAs} from "file-saver";
import {PmExportService} from "../../../pm-export.service";
import {ExcelExportConstantsService} from "../../../../../excel-export-constants.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-pm-otif-dash',
  templateUrl: './pm-otif-dash.component.html',
  styleUrls: ['./pm-otif-dash.component.css']
})
export class PmOtifDashComponent implements OnInit, AfterViewChecked, OnDestroy {
  firstPanel = true;
  secondPanel = false;
  thirdPanel = false;
  fourthPanel = false;
  fifthPanel = false;
  successMessage = '';
  errorMessage = '';
  trendingSupplier: any = [];
  supplierSelectList: any = [];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  lineChartSupplierWise: any = [];
  isExcelDisable = false;
  rangeWiseSupplier: any = [];
  highReceiptRangeSuppliersList: any = [];
  mediumReceiptRangeSuppliersList: any = [];
  averageReceiptRangeSuppliersList: any = [];
  lowReceiptRangeSuppliersList: any = [];
  lowestReceiptRangeSuppliersList: any = [];
  supplierId: any;
  avgReceiptCount?: number;
  avgReceiptPercentage?: number;
  lineGraphData = {
    labels: [],
    data: []
  };
  itemBasedLineGraphData = {
    labels: [],
    data: []
  };
  otifDataList: any = [];
  otifDate: any;
  currentMonth: any;
  previousMonth: any;
  itembasedOtifList: any = [];
  finalItemList: any = [];
  checkedItemList: any = [];
  isSelected = true;
  selectedAll: any;
  $subs: Subscription;

  constructor(private pmService: PmServiceService, public executionFlowPageService: ExecutionFlowPageServiceService,
              private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private ngxSpinnerService: NgxSpinnerService,
              private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.$subs = this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
      this.ngxSpinnerService.show();
      $('.panel-heading ').on('click', function (e) {
        if ($(this).parents('.panel').children('.panel-collapse').hasClass('show')) {
          e.stopPropagation();
        }
      });


      this.singledropdownSettings = {
        text: 'Select',
        showCheckbox: false,
        singleSelection: true,
        enableFilterSelectAll: false,
        enableSearchFilter: true,
        classes: 'myclass custom-class',
        position: 'bottom',
        autoPosition: false,
        lazyLoading: false,
        maxHeight: 200,
        width: 50,
      };
      this.pmService.getPmOtifRangeWiseSuppliersData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((rangeWiseResponse: any) => {
          this.finalItemList = [];
          if (rangeWiseResponse !== null && rangeWiseResponse !== undefined) {
            this.rangeWiseSupplier = rangeWiseResponse;
            /*  let temp;
              for(var i=0; i< rangeWiseResponse?.score4ReceiptRangeSuppliersDTOList.length; i++){
                this.itembasedOtifList[temp++]= rangeWiseResponse?.score4ReceiptRangeSuppliersDTOList[i]
              }
           console.log(this.itembasedOtifList);*/

            console.log(this.rangeWiseSupplier);
            if (rangeWiseResponse?.score4ReceiptRangeSuppliersDTOList != null && rangeWiseResponse?.score4ReceiptRangeSuppliersDTOList !== undefined) {
              this.highReceiptRangeSuppliersList = rangeWiseResponse?.score4ReceiptRangeSuppliersDTOList;
            }
            if (rangeWiseResponse?.score3ReceiptRangeSuppliersDTOList != null && rangeWiseResponse?.score3ReceiptRangeSuppliersDTOList !== undefined) {
              this.mediumReceiptRangeSuppliersList = rangeWiseResponse?.score3ReceiptRangeSuppliersDTOList;
            }
            if (rangeWiseResponse?.score2ReceiptRangeSuppliersDTOList != null && rangeWiseResponse?.score2ReceiptRangeSuppliersDTOList !== undefined) {
              this.averageReceiptRangeSuppliersList = rangeWiseResponse?.score2ReceiptRangeSuppliersDTOList;
            }
            if (rangeWiseResponse?.score1ReceiptRangeSuppliersDTOList != null && rangeWiseResponse?.score1ReceiptRangeSuppliersDTOList !== undefined) {
              this.lowReceiptRangeSuppliersList = rangeWiseResponse?.score1ReceiptRangeSuppliersDTOList;
            }
            if (rangeWiseResponse?.score0ReceiptRangeSuppliersDTOList != null && rangeWiseResponse?.score0ReceiptRangeSuppliersDTOList !== undefined) {
              this.lowestReceiptRangeSuppliersList = rangeWiseResponse?.score0ReceiptRangeSuppliersDTOList;
            }
            this.itembasedOtifList = this.highReceiptRangeSuppliersList.concat(this.mediumReceiptRangeSuppliersList)
              .concat(this.averageReceiptRangeSuppliersList).concat(this.lowReceiptRangeSuppliersList).concat(this.lowestReceiptRangeSuppliersList);

            console.log(this.itembasedOtifList);
            for (let i = 0; i < this.itembasedOtifList.length; i++) {
              this.finalItemList.push({
                supplierName: this.itembasedOtifList[i].supplierName,
                receiptPercentage: this.itembasedOtifList[i].receiptPercentage,
                selected: i < 10 ? true : false
              });
              this.getCheckedItemList(this.finalItemList[0]);
            }
            this.trendingSupplierChart();
          }
          return this.pmService.getPmOtifSupplierList(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((supplierListResponse: any) => {
          if (supplierListResponse !== null && supplierListResponse !== undefined) {
            supplierListResponse.forEach((res: any) => {
              this.supplierSelectList.push({id: res.rscDtSupplierId, itemName: res.supplierName});
              this.singleselectedItems[0] = this.supplierSelectList[0];
            });
            this.onItemSelect(this.supplierSelectList[0]);
          }
          return this.pmService.getPmOtifData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((dataResponse: any) => {
        if (dataResponse !== null && dataResponse !== undefined) {
          console.log(dataResponse);
          this.otifDate=dataResponse?.otifDate;
          this.otifDataList = dataResponse?.otifCalculationDetailsDTO;
          this.currentMonth = dataResponse?.currentMonth;
          this.previousMonth = dataResponse?.previousMonth;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    });
  }

  selectAll() {
    for (var i = 0; i < this.finalItemList.length; i++) {
      this.finalItemList[i].selected = this.selectedAll;
    }
    this.getCheckedItemList();
  }

  checkIfAllSelected() {
    this.selectedAll = this.finalItemList.every((items: any) => {
      return items.selected === true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList(row?: any) {
    this.checkedItemList = [];

    for (let i = 0; i < this.finalItemList.length; i++) {
      if (this.finalItemList[i].selected) {
        this.checkedItemList.push(this.finalItemList[i]);
      }
    }

    this.itemBasedLineGraphData = {
      labels: [],
      data: []
    };
    for (let i = 0; i < this.checkedItemList.length; i++) {
      this.itemBasedLineGraphData.data[i] = this.checkedItemList[i].receiptPercentage;
      this.itemBasedLineGraphData.labels[i] = this.checkedItemList[i].supplierName;
    }
    this.itemWiseLineChart();


  }

  onItemSelect(item: any) {
    this.lineGraphData = {
      labels: [],
      data: []
    };
    this.ngxSpinnerService.show();
    this.supplierId = item.id;
    this.pmService.getPmOtifSupplierWiseDetails(this.executionFlowPageService.selectedHeaderId, this.supplierId).subscribe((supplierWiseDetailsResponse: any) => {
      if (supplierWiseDetailsResponse !== null && supplierWiseDetailsResponse !== undefined) {
        this.avgReceiptCount = supplierWiseDetailsResponse.avgScore;
        this.avgReceiptPercentage = supplierWiseDetailsResponse.avgReceiptPercentage;
        for (let i = 0; i < supplierWiseDetailsResponse.rscIotPmOtifCalculationDTOList.length; i++) {
          this.lineGraphData.data[i] = supplierWiseDetailsResponse.rscIotPmOtifCalculationDTOList[i].receiptPercentage;
          this.lineGraphData.labels[i] = supplierWiseDetailsResponse.rscIotPmOtifCalculationDTOList[i].itemCode;
        }
        this.supplierWiseLineChart();
        this.ngxSpinnerService.hide();

      }

    }, (error) => {
      this.ngxSpinnerService.hide();
      console.log(error);
    });


  }

  onItemDeSelect() {
  }

  supplierDoughnut: any;

  trendingSupplierChart() {
    const canvas: any = document.getElementById("TRENDING_SUPPLIER");
    if (this.supplierDoughnut !== undefined)
      this.supplierDoughnut.destroy();
    if (canvas.getContext) {
      const ctx = canvas.getContext("2d");
      this.supplierDoughnut = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: ['GreaterThan', 'Between 95 to 98', 'Between 90 to 95', 'Between 80 to 90', 'LessThan'],
          datasets: [
            {
              data: [this.rangeWiseSupplier?.score4ReceiptRangeListCount,
                this.rangeWiseSupplier?.score3ReceiptRangeListCount,
                this.rangeWiseSupplier?.score2ReceiptRangeListCount,
                this.rangeWiseSupplier?.score1ReceiptRangeListCount,
                this.rangeWiseSupplier?.score0ReceiptRangeListCount],
              backgroundColor: [' green',
                '#7b97ff',
                '#ffd11a',
                '#ff8c00',
                '#db0000',
              ],
              fill: false
            },
          ]
        },
        options: {

          cutoutPercentage: 40,
          legend: {
            display: false
          },
          tooltips: {
            enabled: true,
            titleFontSize: 12,
            bodyFontSize: 12
          },
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            labels: {
              render: 'percentage',
              fontColor: ['green', 'white', 'red'],
              precision: 2,
              arc: true,
            }
          },

        }
      });
    }
  }

  window: any;

  supplierWiseLineChart() {
    const canvas: any = document.getElementById("SUPPLIER-WISE");
    if (this.window !== undefined)
      this.window.destroy();
    if (canvas.getContext) {
      const ctx = canvas.getContext("2d");
      this.window = new Chart(ctx, {
        type: 'line',
        data: {
          labels: this.lineGraphData.labels,
          datasets: [{
            data: this.lineGraphData.data,
            type: 'line',
            borderColor: '#B22222',
            fill: false,
            backgroundColor: [],
          }]
        },
        options: {
          /* responsive: true,
           maintainAspectRatio: true,*/
          legend: {display: false},
          scales: {
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Items Codes'
              },
              ticks: {fontSize: 10, beginAtZero: true,}
            }],
            yAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Receipt%',
              },
              ticks: {
                fontSize: 10,
                beginAtZero: true
              }
            }]
          },
        }
      });
    }
  }


  itemBasedLine: any;

  itemWiseLineChart() {
    const canvas: any = document.getElementById("Item_Based");
    if (this.itemBasedLine !== undefined)
      this.itemBasedLine.destroy();
    if (canvas.getContext) {
      const ctx = canvas.getContext("2d");
      this.itemBasedLine = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: this.itemBasedLineGraphData.labels,
          datasets: [{
            data: this.itemBasedLineGraphData.data,
            type: 'bar',
            borderColor: 'rgba(210,105,30,1)',
            backgroundColor: 'rgba(210,105,30,0.6)',
            borderWidth: 1
            /* borderColor: 'transparent',
             fill: false,
             pointBackgroundColor: 'rgba(210,105,30,0.6)',
             pointBorderColor: 'rgba(210,105,30,1)',
             pointRadius: 12,
             radius: 12,
             pointHoverRadius: 12,
             borderWidth: 0,*/
          }]
        },
        options: {
          /* responsive: true,
           maintainAspectRatio: true,*/
          legend: {display: false},
          scales: {
            xAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Supplier'
              },
              ticks: {fontSize: 10, beginAtZero: true,}
            }],
            yAxes: [{
              scaleLabel: {
                display: true,
                labelString: 'Receipt%',
              },
              ticks: {
                fontSize: 10,
                beginAtZero: true
              }
            }]
          },
        }
      });
    }
  }

  exportAsXLSX() {
    this.pmExportService.getPmOtifDashComponentExcelFile(this.otifDate).subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmOtifSummary + " " + this.otifDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

  ngOnDestroy(): void {
    this.$subs.unsubscribe();
  }
}
