import {Component, OnInit} from '@angular/core';
import {PmServiceService} from "../../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../../execution-flow-page-service.service";

@Component({
  selector: 'app-pm-data-dash',
  templateUrl: './pm-data-dash.component.html',
  styleUrls: ['./pm-data-dash.component.css']
})
export class PmDataDashComponent implements OnInit {
  otifDataList: any = [];
  currentMonth: any;
  previousMonth: any;

  constructor(private pmService: PmServiceService, private executionFlowPageService: ExecutionFlowPageServiceService, private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmService.getPmOtifData(this.executionFlowPageService.selectedHeaderId).subscribe((dataResponse: any) => {
        if (dataResponse !== null && dataResponse !== undefined) {
          this.otifDataList = dataResponse.otifCalculationDetailsDTO;
          this.currentMonth = dataResponse?.currentMonth;
          this.previousMonth = dataResponse?.previousMonth;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    });

  }

}
