import { Component, OnInit } from '@angular/core';
import {switchMap} from "rxjs/operators";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {Chart} from "chart.js";

@Component({
  selector: 'app-pm-otif-cummulative-score',
  templateUrl: './pm-otif-cummulative-score.component.html',
  styleUrls: ['./pm-otif-cummulative-score.component.css']
})
export class PmOtifCummulativeScoreComponent implements OnInit {
  monthNamesList?: any = [];
  supplierId:any;
  supplierListId:any;
  cummulativeScoreSupplierList:any =[];
  cummulativeScoreWiseSupplierDetailsList?:any;
  score0SuppliersList:any=[];
  score1SuppliersList:any=[];
  score2SuppliersList:any=[];
  score3SuppliersList:any=[];
  score4SuppliersList:any=[];
  constructor(public executionFlowPageService: ExecutionFlowPageServiceService,
              private pmService: PmServiceService,
              private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.spinner.show();
    this.pmService.getOtifCummulativeScoreWiseSupplierListData().subscribe((scoreListResponse:any)=>{
      if(scoreListResponse!== undefined && scoreListResponse!==null){
        console.log(scoreListResponse);
        if(scoreListResponse?.score0SuppliersList!== undefined && scoreListResponse?.score0SuppliersList!==null){
        this.score0SuppliersList = scoreListResponse?.score0SuppliersList;}
        if(scoreListResponse?.score1SuppliersList!== undefined && scoreListResponse?.score1SuppliersList!==null) {
          this.score1SuppliersList = scoreListResponse?.score1SuppliersList;
        }
        if(scoreListResponse?.score2SuppliersList!== undefined && scoreListResponse?.score2SuppliersList!==null) {
          this.score2SuppliersList = scoreListResponse?.score2SuppliersList;
        }
        if(scoreListResponse?.score3SuppliersList!== undefined && scoreListResponse?.score3SuppliersList!==null) {
          this.score3SuppliersList = scoreListResponse?.score3SuppliersList;
        }
        if(scoreListResponse?.score4SuppliersList!== undefined && scoreListResponse?.score4SuppliersList!==null) {
          this.score4SuppliersList = scoreListResponse?.score4SuppliersList;
        }
        if(this.score4SuppliersList.length !==0){
          this.getSupplierDetails(this.score4SuppliersList[0]);
        }
        else if(this.score3SuppliersList.length !==0){
          this.getSupplierDetails(this.score3SuppliersList[0]);
        }
        else if(this.score2SuppliersList.length !==0){
          this.getSupplierDetails(this.score2SuppliersList[0]);
        }
        else if(this.score1SuppliersList.length !==0){
          this.getSupplierDetails(this.score1SuppliersList[0]);
        }
        else if(this.score0SuppliersList.length !==0){
          this.getSupplierDetails(this.score0SuppliersList[0]);
        }
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });

  }
  isCheck:any=1;
  getSupplierDetails(row:any) {
    if (row.id == this.supplierListId) {
      this.isCheck = 1;
      this.supplierListId = row.id;
    } else {
      if (this.isCheck == 1) {
        this.supplierListId = row.id;
      }
      this.supplierId = row.supplierId;
      this.pmService.getOtifCummulativeScoreWiseSupplierDetailsData(this.supplierId).subscribe((supplierDetailsResponse: any) => {
        if(supplierDetailsResponse!== undefined && supplierDetailsResponse!==null){
          console.log(supplierDetailsResponse);
          this.cummulativeScoreWiseSupplierDetailsList = supplierDetailsResponse;
        }
        this.scoreAnalysisChart();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });this.spinner.hide();
    }
  }
  scoreAnalysis: any;

  scoreAnalysisChart() {
    const canvas : any = document.getElementById("ScoreAnalysis_Graph");
    const ctx = canvas.getContext("2d");
    if(this.scoreAnalysis !== undefined)
      this.scoreAnalysis.destroy();
    this.scoreAnalysis = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[0]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[1]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[2]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[3]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[4]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[5]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[6]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[7]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[8]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[9]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[10]?.monthValue,
          this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[11]?.monthValue],
        datasets: [{
          label: "Score by Actual Receipts",
          type: "line",
          borderColor: "#ff8c00",
          data:[
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[0]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[1]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[2]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[3]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[4]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[5]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[6]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[7]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[8]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[9]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[10]?.scoreByActualReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.otifCumulativeScoreByMonthDTOS[11]?.scoreByActualReceipt
          ],
          fill: false
        }, {
          label: "Score by Average Receipts",
          type: "line",
          borderColor: "#3e95cd",
          data: [
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
            this.cummulativeScoreWiseSupplierDetailsList?.scoreByAverageReceipt,
          ],
          fill: false
        }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: true,
        legend: {display: true},
        scales: {
          xAxes: [{
            offset: true,
            scaleLabel: {
              display: true,
              labelString: 'Months'
            },
            ticks: {fontSize: 10, beginAtZero: true,}
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Score',
            },
            ticks: {
              fontSize: 10,
              beginAtZero: true
            }
          }]
        },
      }
    });
  }

}
