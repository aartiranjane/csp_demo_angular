import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UriService} from "../../uri.service";
import {ExecutionFlowPageServiceService} from "../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../excel-export-constants.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})


export class PmExportService {
  headers = [];
  endpoint = this.uriService.getResourceServerUri();
  ppHeaderId = this.executionFlowPageService.selectedHeaderId;

  constructor(private http: HttpClient,
              private excelExportConstantsService: ExcelExportConstantsService, private uriService: UriService, private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  getPmMpsExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMpsPlan}`, {responseType: 'blob'});
  }

  getPmLogicalExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmLogicalConsumption}`, {responseType: 'blob'});

  }

  getPmGrossExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmGrossConsumption}`, {responseType: 'blob'});
  }

  getPmPurchaseExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmPurchasePlan}`, {responseType: 'blob'});
  }

  getPmMouldWiseExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMoudWiseMouldSaturation}`, {responseType: 'blob'});
  }

  getPmSupplierWiseExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmSupplierWiseMouldSaturation}`, {responseType: 'blob'});
  }

  getPmSlobExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmSlob}`, {responseType: 'blob'});
  }

  getPmSummaryExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmSlobSummary}`, {responseType: 'blob'});
  }

  getPmCoverDaysExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmCoverDays}`, {responseType: 'blob'});
  }

  getMesRejectionStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMesRejectionStock}`, {responseType: 'blob'});
  }

  getMesStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMesStock}`, {responseType: 'blob'});
  }

  getPmMaterialExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMaterialMasterData}`, {responseType: 'blob'});
  }

  getPmSupplierExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmSupplierMasterData}`, {responseType: 'blob'});
  }

  getReceiptStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmReceiptStock}`, {responseType: 'blob'});
  }

  getPmPurchaseCoverdaysExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmPurchaseCoverdays}`, {responseType: 'blob'});
  }

  getPmPurchaseStockEquation() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmPurchaseStockEquation}`, {responseType: 'blob'});
  }

  getPmCoverDaySummary() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmCoverDaysSummary}`, {responseType: 'blob'});
  }

  getpmMasterDataMouldExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMasterDataMould}`, {responseType: 'blob'});
  }

  getpmMaterialCategoryModalExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMaterialCategoryModal}`, {responseType: 'blob'});
  }

  getPmOtifDashComponentExcelFile(otifDate: any) {
    return this.http.get(`${this.endpoint + '/api/pmOtif/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmOtifSummary + otifDate + ".xlsx"}`, {responseType: 'blob'});
  }

  getpmMPSTriangleAnalysisExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pmAnnualExport/export/' + this.excelExportConstantsService.pmMpsTriangleAnalysis}`, {responseType: 'blob'});
  }

  getpmSlobMovementExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pmAnnualExport/export/' + this.excelExportConstantsService.pmSlobMovement}`, {responseType: 'blob'});
  }

  getpmInventoryQualityIndexExcelFile() {
    return this.http.get(`${this.endpoint + '/api/pmAnnualExport/export/' + this.excelExportConstantsService.pmInventoryQualityIndex}`, {responseType: 'blob'});
  }
}
