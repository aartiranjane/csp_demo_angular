import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pm-reports-page',
  templateUrl: './pm-reports-page.component.html',
  styleUrls: ['./pm-reports-page.component.css']
})
export class PmReportsPageComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('nav-org-tabb').click();
  }

}
