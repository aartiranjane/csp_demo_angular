import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pm-backup-page',
  templateUrl: './pm-backup-page.component.html',
  styleUrls: ['./pm-backup-page.component.css']
})
export class PmBackupPageComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  thirdTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('nav-org-tabb').click();
  }

}
