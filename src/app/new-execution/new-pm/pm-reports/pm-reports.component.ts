import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {AnalyzerService} from "../../analyzer/analyzer/analyzer.service";
import {NgxSpinnerService} from "ngx-spinner";
import {saveAs} from "file-saver";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-pm-reports',
  templateUrl: './pm-reports.component.html',
  styleUrls: ['./pm-reports.component.css']
})
export class PmReportsComponent implements OnInit, AfterViewChecked,OnDestroy {
  $subs:Subscription;
  isDisabled = true;
  selectedAll: any;
  mpsSelectList: any = [];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  analyzerMpsId: any;
  pmFileDataList = [];
  selected: boolean = false;
  checkedList = {
    pmCheckedList: [],
    fgCheckedList: [],
    rmCheckedList: [],
    pmOtifCheckedList:[]
  };
  selectedExports: any[] = [];
  selectedPlanName:any;

  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private spinner: NgxSpinnerService, private analyzerService: AnalyzerService,
              private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
   /* this.spinner.show();
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };*/
    //getCurrentMonthsMpsHeaderData
    //getMpsHeaderData
   /* this.executionFlowPageService.getMpsHeaderData().subscribe((mpsHeaderResponse: any) => {
      if (mpsHeaderResponse !== null && mpsHeaderResponse !== undefined && mpsHeaderResponse.length !== 0) {
        mpsHeaderResponse.reverse().forEach((res: any) => {
          if (res.moduleName === "ALL" || res.moduleName === "PM") {
            this.mpsSelectList.push({id: res.id, itemName: res.mpsName});
            this.singleselectedItems[0] = this.mpsSelectList[0];
          }
        });
        this.onItemSelect(this.mpsSelectList[0]);
        this.spinner.hide();
      }
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });*/


    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmFileDataList = [];
      this.analyzerService.getAllExcelExportFileNamesData(this.executionFlowPageService.selectedHeaderId).subscribe((allExcelExportFileNamesRes: any) => {
        if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
          for (let i = 0; i < allExcelExportFileNamesRes.pmFileList.length; i++) {
            this.pmFileDataList.push({name: allExcelExportFileNamesRes.pmFileList[i], selected: false});
          }
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    });


  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
 /* onItemSelect(item: any) {
    this.selectedPlanName = item.itemName;
    this.analyzerMpsId = item.id;
    this.pmFileDataList = [];
    this.analyzerService.getAllExcelExportFileNamesData(this.analyzerMpsId).subscribe((allExcelExportFileNamesRes: any) => {
      if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
        for (let i = 0; i < allExcelExportFileNamesRes.pmFileList.length; i++) {
          this.pmFileDataList.push({name: allExcelExportFileNamesRes.pmFileList[i], selected: false});
        }
      }
    }, (error) => {
      console.log(error);
    });
  }

  onItemDeSelect() {
    this.pmFileDataList = [];
  }*/

  selectAll() {
    for (var i = 0; i < this.pmFileDataList.length; i++) {
      this.pmFileDataList[i].selected = this.selectedAll;
    }
    this.getCheckedItemList();
  }

  checkIfAllSelected() {
    this.selectedAll = this.pmFileDataList.every((items: any) => {
      return items.selected === true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedList.pmCheckedList = [];
    this.isDisabled = true;
    for (var i = 0; i < this.pmFileDataList.length; i++) {
      if (this.pmFileDataList[i].selected) {
        this.checkedList.pmCheckedList.push(this.pmFileDataList[i].name);
        this.isDisabled = false;
      }
    }
    console.log(this.checkedList);
  }

  download() {
    let fileName;
    if (this.selectedExports.length === 1) {
      fileName = this.selectedExports.toString();
    }
    if (this.checkedList.pmCheckedList.length === 1) {
      if (this.checkedList.pmCheckedList.length === 1) {
        fileName = this.checkedList.pmCheckedList[0];
      }
    }
    if (this.checkedList.fgCheckedList.length <= 0) {
      this.checkedList.fgCheckedList.push("null");
    }
    if (this.checkedList.pmCheckedList.length <= 0) {
      this.checkedList.pmCheckedList.push("null");
    }
    if (this.checkedList.pmOtifCheckedList.length <= 0) {
      this.checkedList.pmOtifCheckedList.push("null");
    }
    if (this.checkedList.rmCheckedList.length <= 0) {
      this.checkedList.rmCheckedList.push("null");
    }
    this.analyzerService.getAllSelectedExcelExport(this.executionFlowPageService.selectedHeaderId, this.checkedList.fgCheckedList, this.checkedList.pmCheckedList,
      this.checkedList.pmOtifCheckedList, this.checkedList.rmCheckedList).subscribe((updatedPmSlobListResponse: any) => {
      this.spinner.hide();
      console.log(updatedPmSlobListResponse);
      if (updatedPmSlobListResponse.type === "application/vnd.xlsx") {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/xlsx;'});
        saveAs(blob, fileName);
      } else {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
        saveAs(blob, "PM - " + this.executionFlowPageService.selectedPlanName);
      }
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
    this.selectedExports = [];
  }

}
