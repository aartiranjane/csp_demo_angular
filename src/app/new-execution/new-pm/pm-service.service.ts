import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {UriService} from "../../uri.service";
import {ExecutionFlowPageServiceService} from "../execution-flow-page-service.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class PmServiceService {
  headers = [];
  endpoint = this.uriService.getResourceServerUri();
  ppHeaderId = this.executionFlowPageService.selectedHeaderId;
  monthNameList: any = [];
  fgCodeList: any = [];

  constructor(private http: HttpClient,
              private uriService: UriService, private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  getLogicalConsumptionData(selectedFgCode: any) {
    return this.http.get(this.endpoint + '/api/logical-consumption/pm/' + this.executionFlowPageService.selectedHeaderId + '/' + selectedFgCode,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getGrossConsumptionData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/gross-consumption/pm/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgRelationData(selectedFgCode: any) {
    return this.http.get(this.endpoint + '/api/bom/fg-relation/pm/' + selectedFgCode, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getMonthNamesData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/month-names/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getMpsData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/mps-plan/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgCodeListData() {
    return this.http.get(this.endpoint + '/api/mps/fg-list/' + this.executionFlowPageService.selectedHeaderId,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getMpsByFg(selectedFgCode: any) {
    return this.http.get(this.endpoint + '/api/mps-plan/by-fg/' + this.executionFlowPageService.selectedHeaderId + '/' + selectedFgCode,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSupplyData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-latest-purchase-plan/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmStockData() {
    return this.http.get(this.endpoint + '/api/stocks/pm/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmStockDetailsData(selectedStockId: any) {
    return this.http.get(this.endpoint + '/api/stocks/details/pm/' + this.executionFlowPageService.selectedHeaderId + '/' + selectedStockId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updatePMLatestPurchasePlan(purchasePlanDTOs: any) {
    return this.http.put(this.endpoint + '/api/pm-latest-purchase-plans', JSON.stringify(purchasePlanDTOs), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmPurchasePlanCalculationData(selectedId: any) {
    return this.http.get(this.endpoint + '/api/pm-supply-calculations/' + selectedId + '/' + this.executionFlowPageService.selectedHeaderId,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmMouldWiseData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/total-mould-saturation/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-slob/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobTotalStockData(selectedId: any, ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-slob/total-consumption/' + ppHeaderId + '/' + selectedId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSupplierWiseData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/latest-mould-saturation/supplier-wise/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSummaryData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-slob-summary-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getCoverDaysAllMonthsData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-cover-days-summary-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getCoverDaysMonthWiseData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-cover-days-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobReasonsData() {
    return this.http.get(this.endpoint + '/api/pm-slob/reasons/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobNullMapPriceData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-slob-nullMapPrice-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updatePMSlobList(PmSlobDTOs: any, sitValue: any) {
    /* let params = new HttpParams();
     params = params.append('sitValue', sitValue);*/
    return this.http.put(this.endpoint + '/api/pm-slob-save-reasons/' + sitValue + "/" + this.executionFlowPageService.selectedHeaderId, JSON.stringify(PmSlobDTOs), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updatePMSlobsListBasedOnMap(PmSlobDTOs: any, sitValue: any) {
    return this.http.put(this.endpoint + '/api/pm-slob-saveAllDetails/' + sitValue, JSON.stringify(PmSlobDTOs), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSummaryCoverDays(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-cover-days-summary-all-details/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmCoverDaysNullStdPriceData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-coverDays-nullStdPrice-details/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmMesStockData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/stocks/pm/mes/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmMateriallistData() {
    return this.http.get(this.endpoint + '/api/material-masters/packing-material-list', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRscDtMouldList() {
    return this.http.get(this.endpoint + '/api/rsc-dt-mould', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmMaterialCategory() {
    return this.http.get(this.endpoint + '/api/material-Category-masters/packing-material-list/', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSupplierListData() {
    return this.http.get(this.endpoint + '/api/suppliers-masters/packing-materials-suppliers-list', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmReceiptStockData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/stocks/pm/rtd/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmLogicalConsumptionAllData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/logical-consumption/pm/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updatePMCoverDaysListBasedOnStdPrice(pmCoverDaysDTOList: any) {
    return this.http.put(this.endpoint + '/api/pm-coverDays-saveAllDetails', JSON.stringify(pmCoverDaysDTOList), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmRemarks(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/remark/pm', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobReason(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/rsc-dt-reason', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmOtifData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-otif-calculation/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmOtifRangeWiseSuppliersData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-otif-calculation/range-wise-suppliers-details/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmOtifSupplierList(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-otif-calculation/suppliers-list/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmOtifSupplierWiseDetails(ppheaderId: any, supplierId: any) {
    return this.http.get(this.endpoint + '/api/pm-otif/supplier-details/' + ppheaderId + '/' + supplierId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobSummaryOverAllDeviationData() {
    return this.http.get(this.endpoint + '/api/slob-summary/pm/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobSummaryDashboardData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/slob-summary/dashboard/pm/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobMovementDashboardData() {
    return this.http.get(this.endpoint + '/api/slob-movement/pm', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmSlobInventoryQualityDashboardData() {
    return this.http.get(this.endpoint + '/api/slob-inventory-quality-index/pm', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmPurchaseCoverDaysData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-purchase-plan-cover-days/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmPurchaseStockEquationData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/pm-purchase-plan-cover-days/stock-equation/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmMouldSaturationDashboardRangeWiseData(ppheaderId: any, monthValue: any) {
    let params = new HttpParams();
    if (monthValue) {
      params = params.append('monthValue', monthValue);
    }
    return this.http.get(this.endpoint + '/api/mould-saturation/dashboard/range-wise-mould-details/' + ppheaderId, {params}).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmMouldSaturationDashboardSupplierListData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/latest-mould-saturation/dashboard/suppliers-list/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmMouldSaturationDashboardTableData(supplierId: any) {
    return this.http.get(this.endpoint + '/api/latest-mould-saturation-list/dashboard/' + this.executionFlowPageService.selectedHeaderId + '/' + supplierId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getOtifCummulativeScoreWiseSupplierListData() {
    return this.http.get(this.endpoint + '/api/otif-cumulative/score-wise/suppliersList', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getOtifCummulativeScoreWiseSupplierDetailsData(supplierId: any) {
    return this.http.get(this.endpoint + '/api/otif-cumulative-score/suppliers-details/' + supplierId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmDeltaAnalyticsData(rscMtPPDetailsCriteria: any) {
    return this.http.post(this.endpoint + '/api/mps-delta-analysis' + '/?criteria=', JSON.stringify(rscMtPPDetailsCriteria), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }

  pmPurchasePlanAndMouldExcelExport(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-latest-purchase-plans/excel/export/' + this.executionFlowPageService.selectedHeaderId}`, {responseType: 'text'});
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
