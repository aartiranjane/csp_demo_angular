import {Component, OnDestroy, OnInit} from '@angular/core';
import {switchMap} from "rxjs/operators";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {saveAs} from "file-saver";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {PmExportService} from "../../pm-export.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-pm-purchase-stock-equation',
  templateUrl: './pm-purchase-stock-equation.component.html',
  styleUrls: ['./pm-purchase-stock-equation.component.css']
})
export class PmPurchaseStockEquationComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  monthNamesList?: any = [];
  stockEquationDataList?: any = [];
  successMessage = "";
  errorMessage = '';
  isExcelDisable=false;
  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private pmExportService: PmExportService, private pmService: PmServiceService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getPmPurchaseStockEquationData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((stockEquationDataResponse: any) => {
        this.stockEquationDataList = stockEquationDataResponse;
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      })
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  exportAsXLSX() {
    this.pmExportService.getPmPurchaseStockEquation().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmPurchaseStockEquation + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
