import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-analytics-dashboard',
  templateUrl: './pm-analytics-dashboard.component.html',
  styleUrls: ['./pm-analytics-dashboard.component.css']
})
export class PmAnalyticsDashboardComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  thirdTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('pmMpsTab').click();
  }

}
