import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {FgModuleService} from "../../../fg-module/fg-module.service";
import {switchMap} from "rxjs/operators";
import * as $ from "jquery";
import {Chart} from "chart.js";
import {PmServiceService} from "../../pm-service.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-pm-saturation-dash-tab',
  templateUrl: './pm-saturation-dash-tab.component.html',
  styleUrls: ['./pm-saturation-dash-tab.component.css']
})
export class PmSaturationDashTabComponent implements OnInit , AfterViewChecked,OnDestroy {
  $subs:Subscription;
  firstPanel = true;
  secondPanel = false;
  thirdPanel =false;
  rangeWiseLinesSaturationChartData: any = [];
  monthNamesList?: any = [];
  supplierList: any=[];
  supplierId:any;
  supplierTableList: any =[];
  rangeWiseMouldSaturation:any =[];
  labels= [];
  plan= [];
  capacity=[];
  saturation=[];
  highSaturationRangeMouldDTOList: any =[];
  mediumSaturationRangeMouldDTOList: any =[];
  lowSaturationRangeMouldDTOList:any =[];
  lineGraphData = [];
  monthSelectList: any = [];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  selectedAll: any;
  supplierCheckedList:any=[];
  selectedCheckbox:boolean;
  monthValue:any;
  supplierListId:any;
  constructor(private spinner: NgxSpinnerService,
              public executionFlowPageService: ExecutionFlowPageServiceService,
              private fgModuleService: FgModuleService,
              private pmService: PmServiceService,
              private changeRef: ChangeDetectorRef) { }
  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };

    this.monthSelectList=[ { id: 0, itemName: 'Current Month' },
      { id: 3, itemName: '3 Months' },
      { id: 6, itemName: '6 Months' },
      { id: 12, itemName: '12 Months' }];

    this.onItemSelect(this.monthSelectList[0]);
    this.singleselectedItems[0] = this.monthSelectList[0];


    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      console.log(value1);
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getPmMouldSaturationDashboardSupplierListData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((supplierListResponse: any) => {
        console.log(supplierListResponse);
        if (supplierListResponse !== null && supplierListResponse !== undefined) {
          this.supplierList = supplierListResponse;
          this.getSupplierWiseData(this.supplierList[0]);
          console.log( this.supplierList);
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });

    });
    $('.panel-heading ').on('click', function (e) {
      if ($(this).parents('.panel').children('.panel-collapse').hasClass('show')) {
        e.stopPropagation();
      }
    });
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  onItemSelect(item:any){
   this.monthValue= item.id;
    this.pmService.getPmMouldSaturationDashboardRangeWiseData(this.executionFlowPageService.selectedHeaderId, this.monthValue).subscribe((rangeWiseMouldSaturationResponse:any)=>{
      if (rangeWiseMouldSaturationResponse !== null && rangeWiseMouldSaturationResponse !== undefined) {
        this.rangeWiseMouldSaturation = rangeWiseMouldSaturationResponse;
        console.log(this.rangeWiseMouldSaturation);
        this.highSaturationRangeMouldDTOList = rangeWiseMouldSaturationResponse?.highSaturationRangeMouldsDTOList;
        this.mediumSaturationRangeMouldDTOList = rangeWiseMouldSaturationResponse?.mediumSaturationRangeMouldsDTOList;
        this.lowSaturationRangeMouldDTOList = rangeWiseMouldSaturationResponse?.lowSaturationRangeMouldsDTOList;
        this.rangeWiseLinesSaturationChart();
      }
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  }
  onItemDeSelect(){}
  isCheck:any =1;
  getSupplierWiseData(row:any) {
    if (row.id == this.supplierListId) {
      this.isCheck = 1;
      this.supplierListId = row.id;
    } else {
      if (this.isCheck == 1) {
        this.supplierListId = row.id;
      }
      this.supplierTableList = [];
      this.spinner.show();
      this.supplierId = row.id;
      this.pmService.getPmMouldSaturationDashboardTableData(this.supplierId).subscribe((mouldSaturationTableResponse: any) => {
        if (mouldSaturationTableResponse !== null && mouldSaturationTableResponse !== undefined) {
          for (let i = 0; i < mouldSaturationTableResponse.length; i++) {
            this.supplierTableList.push({
              mouldName: mouldSaturationTableResponse[i].mouldName,
              m1Value: mouldSaturationTableResponse[i].m1Value,
              m2Value: mouldSaturationTableResponse[i].m2Value,
              m3Value: mouldSaturationTableResponse[i].m3Value,
              m4Value: mouldSaturationTableResponse[i].m4Value,
              m5Value: mouldSaturationTableResponse[i].m5Value,
              m6Value: mouldSaturationTableResponse[i].m6Value,
              m7Value: mouldSaturationTableResponse[i].m7Value,
              m8Value: mouldSaturationTableResponse[i].m8Value,
              m9Value: mouldSaturationTableResponse[i].m9Value,
              m10Value: mouldSaturationTableResponse[i].m10Value,
              m11Value: mouldSaturationTableResponse[i].m11Value,
              m12Value: mouldSaturationTableResponse[i].m12Value,
              monthAverage: mouldSaturationTableResponse[i].monthAverage,
              selected: i === 0 ? true : false
            });
          }
          this.getCheckedItemList(false, this.supplierTableList[0]);
        }
        this.spinner.hide();
      }, (error) => {
        console.log(error);
      });
    }
  }
  /*checkIfAllSelected() {
   /!* this.selectedAll = this.supplierTableList.every((items: any) => {
      return items.selected === true;
    });*!/
    this.getCheckedItemList();
  }*/

  getCheckedItemList(status:boolean, row?:any) {
    this.supplierCheckedList = [];
    var i=0;
    if(status === false && i===0){
      this.supplierCheckedList.push(row);
      /*if (this.supplierTableList[i].selected) {
        this.supplierCheckedList.push(row);
      }*/
    }
    else{
      for ( i = 0; i < this.supplierTableList.length; i++) {
        if (this.supplierTableList[i].selected) {
          this.supplierCheckedList.push(this.supplierTableList[i]);
        }
      }
    }
    console.log(this.supplierCheckedList);
    this.lineGraphData = [];
    this.supplierCheckedList.forEach((res: any) => {
      /* this.lineGraphData.push({label: res.mouldName, data: res.m1Value});*/
      const object ={
        label:'',
        data:[],
        color: this.getRandomColor(),
        fill: false,
        borderWidth:2,
      };
      for (let i = 1; i <= 12; i++) {
        const mouldName = res.mouldName;
        const monthKey = 'm' + i+'Value';
        object.label=mouldName;
        object.data.push(res[monthKey]);
        object.fill=false;
        object.borderWidth =1;
      }
      this.lineGraphData.push({label: object.label, data:
        object.data, borderColor: object.color, fill:object.fill, borderWidth: object.borderWidth});
    });
    this.mixedChart();
  }

   getRandomColor() {
    const letters = '0123456789ABCDEF'.split('');
    let color = '#';
    for (let i = 0; i < 6; i++ ) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  mouldDoughnut:any;
  rangeWiseLinesSaturationChart(){
    const canvas: any = document.getElementById("Mould_SATURATION_RANGEWISE");
    if (this.mouldDoughnut !== undefined)
      this.mouldDoughnut.destroy();
    if (canvas.getContext) {
      const ctx = canvas.getContext("2d");
      this.mouldDoughnut = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: ['GreaterThan', 'Between', 'LessThan'],
          datasets: [
            {
              data: [this.rangeWiseMouldSaturation?.highSaturationRangeListCount,
                this.rangeWiseMouldSaturation?.mediumSaturationRangeListCount,
                this.rangeWiseMouldSaturation?.lowSaturationRangeListCount],
              backgroundColor: ['#db0000',
                '#7b97ff',
                'green'],
              fill: false
            },
          ]
        },
        options: {
          cutoutPercentage: 40,
          legend: {
            display: false
          },
          tooltips: {
            enabled: true,
            titleFontSize: 12,
            bodyFontSize: 12
          },
          responsive: true,
          maintainAspectRatio: true,
          plugins: {
            labels: {
              render: 'percentage',
              fontColor: ['green', 'white', 'red'],
              precision: 2,
              arc: true,
            }
          },
        }
      });
    }
  }
  window1: any;
  mixedChart() {
    const canvas : any = document.getElementById("Mixed_chart");
    const ctx = canvas.getContext("2d");
    if(this.window1 !== undefined)
      this.window1.destroy();
    this.window1 = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [this.monthNamesList?.month1?.monthNameAlias,
          this.monthNamesList?.month2?.monthNameAlias,
          this.monthNamesList?.month3?.monthNameAlias,
          this.monthNamesList?.month4?.monthNameAlias,
          this.monthNamesList?.month5?.monthNameAlias,
          this.monthNamesList?.month6?.monthNameAlias,
          this.monthNamesList?.month7?.monthNameAlias,
          this.monthNamesList?.month8?.monthNameAlias,
          this.monthNamesList?.month9?.monthNameAlias,
          this.monthNamesList?.month10?.monthNameAlias,
          this.monthNamesList?.month11?.monthNameAlias,
          this.monthNamesList?.month12?.monthNameAlias,],
        datasets: this.lineGraphData,
      },
      options: {
         responsive: true,
         maintainAspectRatio: false,
        legend: {display: true,
        labels:{
          boxWidth: 14,
          fontSize:12,
        }
        },
        scales: {
          xAxes: [{
            offset: true,
            scaleLabel: {
              display: true,
              labelString: 'Months'
            },
            ticks: {fontSize: 10, beginAtZero: true,}
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: '%',
            },
            ticks: {
              fontSize: 10,
              beginAtZero: true
            }
          }]
        },
      }
    });
  }
}
