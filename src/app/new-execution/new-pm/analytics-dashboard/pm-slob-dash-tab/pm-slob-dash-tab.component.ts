import {Component, OnDestroy, OnInit} from '@angular/core';
import {Chart} from "chart.js";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-pm-slob-dash-tab',
  templateUrl: './pm-slob-dash-tab.component.html',
  styleUrls: ['./pm-slob-dash-tab.component.css']
})
export class PmSlobDashTabComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  slobSummaryDashboardList:any = [];
  valueBasis:any;
  percentageBasis:any;
  slobDetails:any;
  constructor(private pmService: PmServiceService, private ngxSpinnerService: NgxSpinnerService,
             public executionFlowPageService: ExecutionFlowPageServiceService) { }

  ngOnInit(): void {
    this.$subs= this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      console.log(value1);
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
    this.ngxSpinnerService.show();
    this.pmService.getPmSlobSummaryDashboardData(this.executionFlowPageService.selectedHeaderId).subscribe((slobSummaryDashboardResponse: any)=>{
     if(slobSummaryDashboardResponse!==null && slobSummaryDashboardResponse!==undefined){
       this.slobSummaryDashboardList = slobSummaryDashboardResponse;
       this.valueBasisChart();
       this.percentageBasisChart();
       this.slobDetailsChart();
     }
      this.ngxSpinnerService.hide();
    }, (error) => {
      this.ngxSpinnerService.hide();
      console.log(error);
    });
    });

  }

  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  valueBasisChart(){
    this.valueBasis = new Chart('doughnut_slob_stock', {
      type: 'doughnut',
      data: {
        labels: ['SLOB','Stock'],
        datasets: [
          {
            data: [this.slobSummaryDashboardList?.totalSlobValue,
                   this.slobSummaryDashboardList?.totalStockValue ],
            backgroundColor: ['#db0000',
              'green'],
            fill: false
          },
        ]
      },
      options: {

        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }

  percentageBasisChart(){
    this.percentageBasis = new Chart('doughnut_Ob_Sm_Iq', {
      type: 'doughnut',
      data: {
        labels: ['OB','SM','IQ'],
        datasets: [
          {
            data: [this.slobSummaryDashboardList?.obPercentage,
                   this.slobSummaryDashboardList?.slPercentage,
                   this.slobSummaryDashboardList?.iqPercentage],
            backgroundColor: ['#db0000',
              '#ff8c00',
              'green'],
            fill: false
          },
        ]
      },
      options: {

        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }

  slobDetailsChart(){
    this.slobDetails = new Chart('doughnut_slob_details', {
      type: 'doughnut',
      data: {
        labels: ['OB','SM'],
        datasets: [
          {
            data: [this.slobSummaryDashboardList?.obValue,
                   this.slobSummaryDashboardList?.slValue],
            backgroundColor: ['#db0000',
              '#ff8c00'],
            fill: false
          },
        ]
      },
      options: {

        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }
}
