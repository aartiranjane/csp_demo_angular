import {Component, OnInit} from '@angular/core';
import {saveAs} from "file-saver";
import {PmExportService} from "../../pm-export.service";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";

@Component({
  selector: 'app-pm-stock-receipt',
  templateUrl: './pm-stock-receipt.component.html',
  styleUrls: ['./pm-stock-receipt.component.css']
})
export class PmStockReceiptComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  receiptDataList = [];

  constructor(private pmExportService: PmExportService, private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmService.getPmReceiptStockData(this.executionFlowPageService.selectedHeaderId).subscribe((receiptResponse: any) => {
        if (receiptResponse !== null && receiptResponse !== undefined) {
          this.receiptDataList = receiptResponse.itemCodes;
          console.log(this.receiptDataList);
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  exportAsXLSX() {
    this.successMessage = 'Downloading...';
    this.pmExportService.getReceiptStockExcelFile().subscribe((excelFileResponse: any) => {
      console.log(excelFileResponse);
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmReceiptStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = this.excelExportConstantsService.pmReceiptStock + ' '+ 'Downloaded successfully';
      setTimeout( () => {
        this.successMessage = '';
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
    });
  }

}
