import {Component, OnInit} from '@angular/core';
import {saveAs} from "file-saver";
import {PmExportService} from "../../pm-export.service";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";

@Component({
  selector: 'app-pm-stock-mes',
  templateUrl: './pm-stock-mes.component.html',
  styleUrls: ['./pm-stock-mes.component.css']
})
export class PmStockMesComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  mesDataList = [];

  constructor(private pmExportService: PmExportService, private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmService.getPmMesStockData(this.executionFlowPageService.selectedHeaderId).subscribe((mesResponse: any) => {
        if (mesResponse !== null && mesResponse !== undefined) {
          this.mesDataList = mesResponse.itemCodes;
          console.log(this.mesDataList);
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })

  }


  exportAsXLSX() {
    this.successMessage = 'Downloading...';
    this.pmExportService.getMesStockExcelFile().subscribe((excelFileResponse: any) => {
      console.log(excelFileResponse);
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmMesStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = this.excelExportConstantsService.pmMesStock + ' '+ 'Downloaded successfully';
      setTimeout( () => {
        this.successMessage = '';
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
    });
  }

}
