import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {saveAs} from "file-saver";
import {PmExportService} from "../../pm-export.service";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-slob-tab-new',
  templateUrl: './slob-tab-new.component.html',
  styleUrls: ['./slob-tab-new.component.css']
})
export class SlobTabNewComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  isEdit = false;
  monthNamesList?: any = [];
  slobDataList: any = [];
  nullSlobDataList: any = [];
  monthValuesList: any = [];
  supplyTableSearchTerm: any;
  slobTotalData: any = [];
  slobReasonsDataList: any = [];
  selectedId: any;
  sitValue;
  sit?: number;
  successMessage = '';
  errorMessage = '';
  successModalMessage = '';
  errorModalMessage = '';
  saveDisabled = false;
  selectedReason: string = '';
  isExcelDisable = false;
  pmNullMapSlobListToSave: Record<string, any>[] = [];
  pmSlobListToSave: Record<string, any>[] = [];

  changedRowCodeList: string[] = [];

  highlightEditValue = {
    editSelection: false
  };

  highlightMapEditValue = {
    editSelection: false
  };
  overAllDeviationList: any = [];
  @ViewChild('table', {read: ElementRef}) public table: ElementRef<any>;
  dataChangedRowIndexList = [];
  slobDataForm = new FormGroup({
    slobDataFormArray: new FormArray([])
  });
  mapDataChangedRowIndexList = [];
  nullMapSlobDataForm = new FormGroup({
    nullMapSlobDataFormArray: new FormArray([])
  });

  constructor(private router: Router,
              private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private pmService: PmServiceService, private spinner: NgxSpinnerService) {

  }

  ngOnInit(): void {
    this.getData();
  }


  getData() {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }

      this.spinner.show();

      this.pmService.getPmSlobData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((slobResponse: any) => {
          if (slobResponse !== null && slobResponse !== undefined) {
            this.slobTotalData = slobResponse;
            this.sit = slobResponse?.sitValue;
            this.sitValue = new FormControl(slobResponse?.sitValue);
            this.slobDataList = slobResponse.pmSlobs;
            this.setSlobFormData();
            this.totalStockMonths(this.slobDataList[0]);
            if (this.successMessage === 'Slob updating...') {
              this.successMessage = 'Slob updated Successfully';
              setTimeout(() => {
                this.successMessage = '';
              }, 2000);
            }
          }
          return this.pmService.getPmSlobNullMapPriceData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((nullMapSlobResponse: any) => {
          if (nullMapSlobResponse !== null && nullMapSlobResponse !== undefined) {
            this.nullSlobDataList = nullMapSlobResponse.pmSlobs;
            if (this.nullSlobDataList.length > 0) {
              this.setNullMapSlobFormData();
              this.highlightMapEditValue.editSelection = true;
              this.openMapModal();
            }
          }
          return this.pmService.getPmSlobSummaryOverAllDeviationData();
        })).subscribe((overAllDeviationResponse: any) => {
        if (overAllDeviationResponse !== null && overAllDeviationResponse !== undefined) {
          this.overAllDeviationList = overAllDeviationResponse;
          console.log(this.overAllDeviationList)
        }

        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    });
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  openMapModal() {
    document.getElementById("openMapModal").click();
  }

  setSlobFormData() {
    this.slobDataList.forEach((pmSlobs: any) => {
      const rowView = new FormGroup({
        code: new FormControl(pmSlobs?.code),
        description: new FormControl(pmSlobs?.description),
        supplier: new FormControl(pmSlobs?.supplier),
        category: new FormControl(pmSlobs?.category),
        stock: new FormControl(pmSlobs?.stock),
        totalStock: new FormControl(pmSlobs?.totalStock),
        map: new FormControl(pmSlobs?.map),
        stdPrice: new FormControl(pmSlobs?.stdPrice),
        slobQuantity: new FormControl(pmSlobs?.slobQuantity),
        price: new FormControl(pmSlobs?.price),
        slobValue: new FormControl(pmSlobs?.slobValue),
        slobType: new FormControl(pmSlobs?.slobType),
        reasonId: new FormControl(pmSlobs?.reasonId),
        originalReasonId: new FormControl(pmSlobs?.reasonId),
        reason: new FormControl(pmSlobs?.reason),
        originalReason: new FormControl(pmSlobs?.reason),
        remark: new FormControl(pmSlobs?.remark),
        originalRemark: new FormControl(pmSlobs?.remark),
      });
      (<FormArray>this.slobDataForm.get('slobDataFormArray')).push(rowView);
    });
  }

  setNullMapSlobFormData() {
    if (((this.nullMapSlobDataForm.get('nullMapSlobDataFormArray').value).length) === 0) {
      this.nullSlobDataList.forEach((pmSlobs: any) => {
        const rowView = new FormGroup({
          code: new FormControl(pmSlobs?.code),
          description: new FormControl(pmSlobs?.description),
          supplier: new FormControl(pmSlobs?.supplier),
          stock: new FormControl(pmSlobs?.stock),
          totalStock: new FormControl(pmSlobs?.totalStock),
          slobQuantity: new FormControl(pmSlobs?.slobQuantity),
          slobValue: new FormControl(pmSlobs?.slobValue),
          slobType: new FormControl(pmSlobs?.slobType),
          map: new FormControl(pmSlobs?.map, [Validators.required]),
          stdPrice: new FormControl(pmSlobs?.stdPrice),
          price: new FormControl(pmSlobs?.price),
          reasonId: new FormControl(pmSlobs?.reasonId),
          reason: new FormControl(pmSlobs?.reason),
          remark: new FormControl(pmSlobs?.remark),
        });
        (<FormArray>this.nullMapSlobDataForm.get('nullMapSlobDataFormArray')).push(rowView);
      });
    }
  }

  UpdateMap(row: any, columnName: string, mapValue: string, rowIndex?: number) {
    console.log(row)
    this.spinner.show();
    const currentRowIndex: number = this.nullSlobDataList.findIndex(value => value.code === row.code.value);
    if ((this.mapDataChangedRowIndexList.findIndex(value => value === rowIndex)) === -1) {
      this.mapDataChangedRowIndexList.push(rowIndex);
    }
    this.nullSlobDataList[currentRowIndex].map = mapValue;
    this.setChangedRowCodeMapList(row?.code?.value, currentRowIndex);
    this.spinner.hide();
  }

  totalStockMonths(row: any) {
    this.selectedId = row.rscMtItemId;
    this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
      switchMap((monthNamesDataResponse: any) => {
        if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
          this.monthNamesList = monthNamesDataResponse;
        }
        return this.pmService.getPmSlobTotalStockData(this.selectedId, this.executionFlowPageService.selectedHeaderId);
      })).subscribe((pmSlobTotalStockResponse: any) => {
      if (pmSlobTotalStockResponse !== null && pmSlobTotalStockResponse !== undefined) {
        this.monthValuesList = pmSlobTotalStockResponse;
      }

      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  }

  updateReason(row: any, name: string, changedValue: string, rowIndex?: number) {
    const newReasonId = changedValue.split(":");
    let selectedReason;
    const newReasonId1 = newReasonId[1].trim();
    this.slobReasonsDataList.forEach(slobReasonList => {
      if ((slobReasonList.id).toString() === newReasonId1.toString()) {
        selectedReason = slobReasonList.reason;
      }
    });
    return selectedReason;
  }

  updateSitValueData(changedValue: any) {
    this.sitValue.value = changedValue;
  }

  updateReasonData(row: any, name: string, changedValue: string, rowIndex?: number) {
    this.spinner.show();
    const currentRowIndex: number = this.slobDataList.findIndex(value => value.code === row.code.value);
    if ((this.dataChangedRowIndexList.findIndex(value => value === rowIndex)) === -1) {
      this.dataChangedRowIndexList.push(rowIndex);
    }
    if (name === 'reason') {
      this.slobDataList[currentRowIndex].reason = this.updateReason(row, name, changedValue, rowIndex);
      this.slobDataList[currentRowIndex].reasonId = row.reasonId.value;
    }
    if (name === 'remark') {
      this.slobDataList[currentRowIndex].remark = changedValue;
    }
    this.setChangedRowCodeList(row?.code?.value, currentRowIndex);
    this.spinner.hide();
  }

  setChangedRowCodeMapList(materialCode: string, currentRowIndex: number) {
    const isPresent = this.changedRowCodeList.findIndex((value: any) => value === materialCode);
    if (isPresent === -1) {
      this.changedRowCodeList.push(materialCode);
      this.pmNullMapSlobListToSave.push(this.nullSlobDataList[currentRowIndex]);
    }
  }

  setChangedRowCodeList(materialCode: string, currentRowIndex: number) {
    const isPresent = this.changedRowCodeList.findIndex((value: any) => value === materialCode);
    if (isPresent === -1) {
      this.changedRowCodeList.push(materialCode);
      this.pmSlobListToSave.push(this.slobDataList[currentRowIndex]);
    }
  }

  editBtn() {
    this.table.nativeElement.scrollTo({
      left: (this.table.nativeElement.scrollLeft + 80.36377473363775),
      behavior: 'smooth'
    });
    this.isEdit = true;
    this.highlightEditValue.editSelection = true;
    this.pmService.getPmSlobReasonsData().subscribe((slobResponse: any) => {
      if (slobResponse !== null && slobResponse !== undefined) {
        console.log(slobResponse);
        this.slobReasonsDataList = slobResponse;
      }
      console.log(this.slobReasonsDataList);
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  }

  saveMap() {
    if (this.nullMapSlobDataForm.invalid) {
      return;
    }
    document.getElementById("openMapModal").click();
    this.pmService.updatePMSlobsListBasedOnMap(this.pmNullMapSlobListToSave, this.sit).pipe(
      switchMap((updatedPmSlobListResponse: any) => {
        return this.executionFlowPageService.pmCoverDaysAndSlobExcelExport();
      })).subscribe((res: any) => {
      this.successMessage = 'Slob updating...';
      this.isEdit = false;
      this.highlightEditValue.editSelection = false;
      this.pmNullMapSlobListToSave = [];
      this.nullSlobDataList = [];
      this.slobDataList = [];
      this.getData();
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
      this.spinner.hide();
    });
  }

  closeBtn() {
    this.table.nativeElement.scrollTo({
      left: (this.table.nativeElement.scrollLeft - 74.36377473363775),
      behavior: 'smooth'
    });
    this.isEdit = false;
    this.slobReasonsDataList = [];
    this.pmSlobListToSave = [];
    this.highlightEditValue.editSelection = false;
    this.sitValue.setValue(this.sit);
    this.dataChangedRowIndexList.forEach((index: any) => {
      this.setSlobDataFormValues(index);
      this.setSlobDataListValues(index);
    });
  }

  setSlobDataFormValues(index: number) {
    this.slobDataForm.get('slobDataFormArray')['controls'][index].controls.reason.value = this.slobDataForm.value.slobDataFormArray[index].originalReason;
    this.slobDataForm.get('slobDataFormArray')['controls'][index].controls.reasonId.value = this.slobDataForm.value.slobDataFormArray[index].originalReasonId;
    this.slobDataForm.get('slobDataFormArray')['controls'][index].controls.remark.value = this.slobDataForm.value.slobDataFormArray[index].originalRemark;
  }

  setSlobDataListValues(index) {
    this.slobDataList[index].reason = this.slobDataForm.value.slobDataFormArray[index].reason;
    this.slobDataList[index].reasonId = this.slobDataForm.value.slobDataFormArray[index].reasonId;
    this.slobDataList[index].remark = this.slobDataForm.value.slobDataFormArray[index].remark;
  }

  saveChanges() {
    this.spinner.show();
    this.successMessage = 'Slob updating...';
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);
    this.pmService.updatePMSlobList(this.pmSlobListToSave, this.sitValue.value).pipe(
      switchMap((updatedPmSlobListResponse: any) => {
        return this.executionFlowPageService.pmCoverDaysAndSlobExcelExport();
      })).subscribe((res: any) => {
      console.log(res);
      this.isEdit = false;
      this.slobReasonsDataList = [];
      this.highlightEditValue.editSelection = false;
      if (this.sitValue.value !== this.sit) {
        this.slobTotalData = [];
        this.slobDataList = [];
        this.sit = this.sitValue.value;
      } else {
     /*   this.spinner.hide();*/
      }
      this.getData();
      this.pmSlobListToSave = [];
      this.pmNullMapSlobListToSave = [];
      this.slobTotalData = [];
      this.slobDataList = [];
      this.successMessage = 'Slob updated Successfully';
      setTimeout(() => {
        this.successMessage = '';
      }, 5000);
    }, (error) => {
      this.spinner.hide();
      this.errorMessage = 'Something Went Wrong';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
      console.log(error);
    })
  }

  exportAsXLSX() {
    this.pmExportService.getPmSlobExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmSlob + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

  exportAsXLSXForSummary() {
    this.pmExportService.getPmSummaryExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmSlobSummary + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successModalMessage = 'Download Success!';
      setTimeout(() => {
        this.successModalMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorModalMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorModalMessage = '';
      }, 2000);
    });
  }
}
