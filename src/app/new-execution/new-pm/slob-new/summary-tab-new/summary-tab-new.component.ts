import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {PmServiceService} from "../../pm-service.service";
import {Chart} from "chart.js";
import {saveAs} from "file-saver";
import {switchMap} from "rxjs/operators";
import {PmExportService} from "../../pm-export.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";

@Component({
  selector: 'app-summary-tab-new',
  templateUrl: './summary-tab-new.component.html',
  styleUrls: ['./summary-tab-new.component.css']
})
export class SummaryTabNewComponent implements OnInit {
  summaryDataList: any = [];
  slobSummary: any = [];
  coverDaysGraph: any = [];
  slowItemPercentage: any;
  obsoleteItemPercentage: any;
  stockQualityPercentage: any;
  totalCoverDays: any;
  totalDaysInMonth: any;
  monthNamesList?: any = [];
  coverDaysList: any = [];
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;

  constructor(private spinner: NgxSpinnerService,
              private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private pmService: PmServiceService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getPmSummaryData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((summaryResponse: any) => {
          if (summaryResponse !== null && summaryResponse !== undefined) {
            this.summaryDataList = summaryResponse?.slobSummaryFormulaDetailsDTO;
            this.slowItemPercentage = summaryResponse?.slobSummaryFormulaDetailsDTO?.totalSlowItemValueDTO?.slowItemPercentage;
            this.obsoleteItemPercentage = summaryResponse?.slobSummaryFormulaDetailsDTO?.totalObsoleteItemValueDTO?.obsoleteItemPercentage;
            this.stockQualityPercentage = summaryResponse?.slobSummaryFormulaDetailsDTO?.totalStockQualityValueDTO?.stockQualityPercentage;
            this.slobSummaryChart();
          }
          return this.pmService.getPmSummaryCoverDays(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((responseOfCoverDays: any) => {
        if (responseOfCoverDays !== null && responseOfCoverDays !== undefined) {
          this.coverDaysList = responseOfCoverDays;
          this.totalCoverDays = responseOfCoverDays.totalCoverDays;
          this.totalDaysInMonth = responseOfCoverDays.totalDaysOfMonths;
          this.coverDaysChart();
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })

  }

  slobSummaryChart() {
    /*slobSummary*/
    this.slobSummary = new Chart('SLOB_SUMMARY', {
      type: 'doughnut',
      data: {
        labels: ['SlowItemPercentage', 'ObsoleteItemPercentage', 'StockQualityPercentage'],
        datasets: [
          {
            data: [this.slowItemPercentage, this.obsoleteItemPercentage, this.stockQualityPercentage],
            backgroundColor: ['#ff8c00',
              '#db0000',
              '#a6d608'],
            fill: false
          },
        ]
      },
      options: {

        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }

  coverDaysChart() {
    /*coverDaysGraph*/
    this.coverDaysGraph = new Chart('COVER_DAYS', {
      type: 'doughnut',
      data: {
        labels: ['TotalCoverDays', 'OutOf100'],
        datasets: [
          {
            data: [this.totalCoverDays, this.totalDaysInMonth - this.totalCoverDays],
            backgroundColor: ['rgba(255,165,0,0.4)'],
            fill: false
          },
        ]
      },
      options: {
        rotation: 0.8 * Math.PI,
        circumference: 1.4 * Math.PI,
        cutoutPercentage: 60,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },

      }
    });
  }

  exportAsXLSX() {
    this.pmExportService.getPmSummaryExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmSlobSummary + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

}
