import {Component, OnDestroy, OnInit} from '@angular/core';
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {saveAs} from "file-saver";
import {PmExportService} from "../../pm-export.service";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-coverdays-tab-new',
  templateUrl: './coverdays-tab-new.component.html',
  styleUrls: ['./coverdays-tab-new.component.css']
})
export class CoverdaysTabNewComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  monthNamesList?: any = [];
  totalMonthData?: any = [];
  monthWiseData?: any = [];
  summaryDataList:any=[];
  totalCoverDays: any;
  totalDaysInMonth: any;
  coverDaysList: any = [];
  nullStdPriceDataList: any = [];
  successMessage = '';
  errorMessage = '';
  successmodalMessage='';
  errormodalMessage = '';
  isExcelDisable=false;
  totalStockWithSit:any;
  highlightEditValue = {
    editSelection: false
  };
  isEdit = false;
  nullStdPriceCoverDaysDataForm = new FormGroup({
    nullStdPriceCoverDaysFormArray: new FormArray([])
  });
  stdPriceDataChangedRowIndexList = [];
  changedRowCodeList: string[] = [];
  pmCoverDaysListToSave: Record<string, any>[] = [];

  constructor(private pmService: PmServiceService, private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private spinner: NgxSpinnerService) {
  }


  ngOnInit() {
    this.getData();
  }

  getData() {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: object) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.pmService.getCoverDaysAllMonthsData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((totalMonthDataResponse: any) => {
          if (totalMonthDataResponse !== null && totalMonthDataResponse !== undefined) {
            this.totalMonthData = totalMonthDataResponse;
          }
          return this.pmService.getCoverDaysMonthWiseData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((monthWiseDataResponse: any) => {
          if (monthWiseDataResponse !== null && monthWiseDataResponse !== undefined) {
            this.monthWiseData = monthWiseDataResponse?.pmCoverDaysDTOList;
            if (this.successMessage === 'Slob updating...') {
              this.successMessage = 'Slob updated Successfully';
            }
          }
          return this.pmService.getPmCoverDaysNullStdPriceData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((nullStdPriceDataResponse: any) => {
          if (nullStdPriceDataResponse !== null && nullStdPriceDataResponse !== undefined) {
            this.nullStdPriceDataList = nullStdPriceDataResponse.pmCoverDaysDTOList;
            if (this.nullStdPriceDataList.length > 0) {
              this.setNullStdPriceCoverDaysFormData();
              this.highlightEditValue.editSelection = true;
              this.openStdPriceModal();
            }
          }
          return this.pmService.getPmSummaryData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((summaryResponse: any) => {
          if (summaryResponse !== null && summaryResponse !== undefined) {
            this.summaryDataList = summaryResponse?.slobSummaryFormulaDetailsDTO;

          }
          return this.pmService.getPmSummaryCoverDays(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((responseOfCoverDays: any) => {
        if (responseOfCoverDays !== null && responseOfCoverDays !== undefined) {
          this.coverDaysList = responseOfCoverDays;
          this.totalStockWithSit=responseOfCoverDays.totalStockSitValue;
          this.totalCoverDays = responseOfCoverDays.totalCoverDays;
          this.totalDaysInMonth = responseOfCoverDays.totalDaysOfMonths;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })

  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  setNullStdPriceCoverDaysFormData() {
    this.nullStdPriceDataList.forEach((pmCoverDaysDTOList: any) => {
      const rowView = new FormGroup({
        code: new FormControl(pmCoverDaysDTOList?.code),
        description: new FormControl(pmCoverDaysDTOList?.description),
        supplier: new FormControl(pmCoverDaysDTOList?.supplier),
        category: new FormControl(pmCoverDaysDTOList?.category),
        stockQuantity: new FormControl(pmCoverDaysDTOList?.stockQuantity),
        stockValue: new FormControl(pmCoverDaysDTOList?.stockValue),
        map: new FormControl(pmCoverDaysDTOList?.map),
        stdPrice: new FormControl(pmCoverDaysDTOList?.stdPrice, [Validators.required]),
        rate: new FormControl(pmCoverDaysDTOList?.rate),
        month1ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month1ConsumptionQuantity),
        month2ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month2ConsumptionQuantity),
        month3ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month3ConsumptionQuantity),
        month4ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month4ConsumptionQuantity),
        month5ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month5ConsumptionQuantity),
        month6ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month6ConsumptionQuantity),
        month7ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month7ConsumptionQuantity),
        month8ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month8ConsumptionQuantity),
        month9ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month9ConsumptionQuantity),
        month10ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month10ConsumptionQuantity),
        month11ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month11ConsumptionQuantity),
        month12ConsumptionQuantity: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month12ConsumptionQuantity),
        month1Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month1Value),
        month2Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month2Value),
        month3Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month3Value),
        month4Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month4Value),
        month5Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month5Value),
        month6Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month6Value),
        month7Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month7Value),
        month8Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month8Value),
        month9Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month9Value),
        month10Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month10Value),
        month11Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month11Value),
        month12Value: new FormControl(pmCoverDaysDTOList?.coverDaysMonthDetailsDTO?.month12Value),
      });
      (<FormArray>this.nullStdPriceCoverDaysDataForm.get('nullStdPriceCoverDaysFormArray')).push(rowView);
    });
  }

  openStdPriceModal() {
    document.getElementById("openStdPriceModal").click();
  }

  UpdateStdPrice(row: any, columnName: string, stdPriceValue: string, rowIndex?: number) {
    this.spinner.show();
    const currentRowIndex: number = this.monthWiseData.findIndex(value => value.code === row.code.value);
    if ((this.stdPriceDataChangedRowIndexList.findIndex(value => value === rowIndex)) === -1) {
      this.stdPriceDataChangedRowIndexList.push(rowIndex);
    }
    this.monthWiseData[currentRowIndex].stdPrice = stdPriceValue;
    this.setChangedRowCodeStdPriceList(row?.code?.value, currentRowIndex);
    this.spinner.hide();
  }

  setChangedRowCodeStdPriceList(materialCode: string, currentRowIndex: number) {
    const isPresent = this.changedRowCodeList.findIndex((value: any) => value === materialCode);
    if (isPresent === -1) {
      this.changedRowCodeList.push(materialCode);
      this.pmCoverDaysListToSave.push(this.monthWiseData[currentRowIndex]);
    }
  }

  saveStdPrice() {
    if (this.nullStdPriceCoverDaysDataForm.invalid) {
      return;
    }
    document.getElementById("openStdPriceModal").click();
    this.pmService.updatePMCoverDaysListBasedOnStdPrice(this.pmCoverDaysListToSave).subscribe((updatedPmSlobListResponse: any) => {
      this.successMessage = 'Slob updating...';
      this.isEdit = false;
      this.highlightEditValue.editSelection = false;
      this.pmCoverDaysListToSave = [];
      this.nullStdPriceDataList = [];
      this.monthWiseData = [];
      this.getData();
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
      this.spinner.hide();
    });
  }

  exportAsXLSX() {
    this.pmExportService.getPmCoverDaysExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmCoverDays + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

  exportAsXLSXForCoverDays(){
    this.pmExportService.getPmCoverDaySummary().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.pmCoverDaysSummary + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successmodalMessage = 'Download Success!';
      setTimeout(() => {
        this.successmodalMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errormodalMessage = 'Download Failure!';
      setTimeout(() => {
        this.errormodalMessage = '';
      }, 2000);
    });
  }
}
