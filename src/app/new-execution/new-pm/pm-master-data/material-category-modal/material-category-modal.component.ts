import {Component, OnInit} from '@angular/core';
import {PmExportService} from "../../pm-export.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-material-category-modal',
  templateUrl: './material-category-modal.component.html',
  styleUrls: ['./material-category-modal.component.css']
})
export class MaterialCategoryModalComponent implements OnInit {
  materialCategoryList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private pmExportService: PmExportService, private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmService.getPmMaterialCategory().subscribe((receiptResponse: any) => {
        if (receiptResponse !== null && receiptResponse !== undefined) {
          this.materialCategoryList = receiptResponse;
          this.filterDataList = this.materialCategoryList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  //search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.materialCategoryList);
    return this.materialCategoryList.filter(x =>  String(x.id)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemTypeDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}
