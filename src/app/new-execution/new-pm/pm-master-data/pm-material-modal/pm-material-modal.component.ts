import {Component, OnInit} from '@angular/core';
import {PmExportService} from "../../pm-export.service";
import {PmServiceService} from "../../pm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-pm-material-modal',
  templateUrl: './pm-material-modal.component.html',
  styleUrls: ['./pm-material-modal.component.css']
})
export class PmMaterialModalComponent implements OnInit {
  pmMaterialDataList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private pmExportService: PmExportService, private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmService.getPmMateriallistData().subscribe((mesResponse: any) => {
        if (mesResponse !== null && mesResponse !== undefined) {
          this.pmMaterialDataList = mesResponse;
          this.filterDataList = this.pmMaterialDataList;
          this.totalLengthOfCollection = this.filterDataList.length;

        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

//search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    return this.pmMaterialDataList.filter(x => String(x.itemCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.moqValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.seriesValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stockValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.mapPrice)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stdPrice)?.toLowerCase().indexOf(v.toLowerCase()) !== -1
    );
  }
}
