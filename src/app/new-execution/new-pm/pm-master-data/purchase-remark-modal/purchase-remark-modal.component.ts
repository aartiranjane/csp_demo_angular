import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {PmExportService} from "../../pm-export.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {PmMasterService} from "../pm-master.service";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-purchase-remark-modal',
  templateUrl: './purchase-remark-modal.component.html',
  styleUrls: ['./purchase-remark-modal.component.css']
})
export class PurchaseRemarkModalComponent implements OnInit {
  selectedId: any;
  isEditMode = false;

  isEdit = false;
  supplyDataForm = new FormGroup({
    supplyDataFormArray: new FormArray([])
  });
  latestPurchasePlanListToSave: Record<string, any>[] = [];
  changedRowCodeList: string[] = [];
  successMessage = '';
  errorMessage = '';
  highlightEditValue = {
    editSelection: false
  };
  dataChangedRowIndexList = [];
  coverDaysValue: any;
  priorityList = [];
  dataSource: MatTableDataSource<any>;
  pmRemarkList = [];
  remarkList: any = [];
  remarkForm: FormGroup;
  mastersList =[];
  constructor(private router: Router, private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private pmMasterService:PmMasterService,
              private pmService: PmServiceService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.pmService.getPmRemarks(this.executionFlowPageService.selectedHeaderId).subscribe((rscDtPmRemarksResponse: any) => {
      this.pmRemarkList = rscDtPmRemarksResponse;
      if (rscDtPmRemarksResponse != null) {
        this.pmRemarkList = [];
        rscDtPmRemarksResponse.forEach((response) => {
            {
              this.pmRemarkList.push({
                name: response.name,
                aliasName: response.aliasName,
                isClicked: false
              });
            }
          }
        );
      }
        this.pmMasterService.remarkChanged.subscribe((res: any) => {
           this.remarkList = res;
           this.pmRemarkList = [];
           res.forEach((response) => {
             this.pmRemarkList.push({
               name: response.name,
               aliasName: response.aliasName,
               isClicked: false
             });
           });

         }, (error) => {
           console.log(error);
         });
      /*  if (rscDtPmRemarksResponse !== undefined && rscDtPmRemarksResponse !== null) {
          this.pmRemarkList = rscDtPmRemarksResponse;
        }*/
      console.log(this.pmRemarkList);
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    }) ;
    this.initForms();
  }

  initForms() {
    this.remarkForm = new FormGroup({
        aliasName: new FormControl('', Validators.required),
        name: new FormControl('', Validators.required)
      }
    );
  }
  editRemark(remark: any, index: any) {

    this.isEditMode = true;
    if (this.remarkForm !== undefined) {
      this.remarkForm.patchValue({
        name: remark.name,
        aliasName: remark.aliasName,
      });
    } else {
      this.remarkForm = new FormGroup({
        name: new FormControl(this.remarkForm.value.name),
        aliasName: new FormControl(this.remarkForm.value.aliasName),
      });
    }
  }

  save() {
    const remark = {
      name: this.remarkForm.value.name,
      aliasName: this.remarkForm.value.aliasName,
    };
    if (this.isEditMode) {
      this.pmMasterService.updateRemark(remark).subscribe((res: any) => {
        const index = this.remarkList.findIndex(function (remarkFormList) {
          return remarkFormList.name === remark.name;
        });
        if (index !== -1) {
          this.remarkList[index] = remark;
          this.pmMasterService.remarkChanged.next(this.remarkList);
        }
        this.successMessage = 'remark Updated successfully';
      }, (error) => {
        console.log(error);
      });
    } else {
      this.pmMasterService.addRemark(remark).subscribe((res: any) => {
        this.remarkList.push(res);
        this.pmMasterService.remarkChanged.next(this.remarkList);
        this.successMessage = 'remark added successfully';
      }, (error) => {
        console.log(error);
      });
    }
    this.remarkForm.reset();
    this.isEditMode = false;
  }
}
