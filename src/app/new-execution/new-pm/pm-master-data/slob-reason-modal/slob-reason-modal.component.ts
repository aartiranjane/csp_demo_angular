import {Component, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {PmExportService} from "../../pm-export.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {PmMasterService} from "../pm-master.service";
import {PmServiceService} from "../../pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-slob-reason-modal',
  templateUrl: './slob-reason-modal.component.html',
  styleUrls: ['./slob-reason-modal.component.css']
})
export class SlobReasonModalComponent implements OnInit {
  selectedId: any;
  pmSlobReasonList = [];
  remarkForm: FormGroup;

  constructor(private router: Router, private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private pmMasterService: PmMasterService,
              private pmService: PmServiceService, private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmService.getPmSlobReason(this.executionFlowPageService.selectedHeaderId).subscribe((rscDtPMSlobReasonResponse: any) => {
        if (rscDtPMSlobReasonResponse !== null && rscDtPMSlobReasonResponse !== undefined) {
          this.pmSlobReasonList = rscDtPMSlobReasonResponse;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  save() {
  }
}
