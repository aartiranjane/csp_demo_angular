import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {PmExportService} from "../pm-export.service";
import {PmServiceService} from "../pm-service.service";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {saveAs} from "file-saver";

@Component({
  selector: 'app-pm-master-data',
  templateUrl: './pm-master-data.component.html',
  styleUrls: ['./pm-master-data.component.css']
})
export class PmMasterDataComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  isExcelDisable = false;
  materialList = [
    ['PM Materials', 'pmMaterialPopup'],
    ['Material Category', 'materialCategoryPopup'],
    ['Moulds', 'mouldsPopup'],
  ];
  pmSupplierList = [
    ['PM Supplier', 'pmSupplierPopup']
  ];
  purchaseList = [
    ['Purchase Remark', 'purchaseRemarkPopup']
  ];
  slobList = [
    ['SLOB Reason', 'slobReasonPopup'],
    ['SLOB Remarks', 'slobRemarkPopup'],
    /* ['Map', 'mapPopup'],
     ['STD Price', 'stdPricePopup']*/
  ];
  stockList = [
    ['Midnight A+Q', 'midNightPopup'],
    ['Midnight Rejection', 'pmMesWithRejectionPopup'],
    ['Receipts', 'receiptPopup']
  ];

  currentMasterName!: string;

  constructor(private router: Router, private pmExportService: PmExportService, private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  ngOnInit(): void {
  }

  loadPmMasterData(pmMasterName: any, pmMasterUrl: any) {
    this.currentMasterName = pmMasterName;
    this.router.navigate(['newPm/pmMasterData/' + pmMasterUrl]);
  }

  exportAsXLSX() {
    switch (this.currentMasterName) {
      case 'PM Materials':
        this.pmExportService.getPmMaterialExcelFile().subscribe((excelFileResponse: any) => {
          this.successMessage = 'Download Success!';
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService. pmMaterialMasterData + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Material Category':
        this.pmExportService.getpmMaterialCategoryModalExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.pmMaterialCategoryModal + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Moulds':
        this.pmExportService.getpmMasterDataMouldExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.pmMasterDataMould + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'PM Supplier':
        this.pmExportService.getPmSupplierExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.pmSupplierMasterData + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Midnight A+Q':
        this.pmExportService.getMesStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.pmMesStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Receipts':
        this.pmExportService.getReceiptStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.pmReceiptStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Midnight Rejection':
        this.pmExportService.getMesRejectionStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.pmMesRejectionStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      default:
        console.log("No such Name exists!");
        break;
    }

  }
}
