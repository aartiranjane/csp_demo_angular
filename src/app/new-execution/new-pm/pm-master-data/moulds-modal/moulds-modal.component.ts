import {Component, OnInit} from '@angular/core';
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {PmServiceService} from "../../pm-service.service";
import {PmExportService} from "../../pm-export.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-moulds-modal',
  templateUrl: './moulds-modal.component.html',
  styleUrls: ['./moulds-modal.component.css']
})
export class MouldsModalComponent implements OnInit {
  rscDtMouldList: any = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private pmService: PmServiceService, private pmExportService: PmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit() {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmService.getRscDtMouldList().subscribe((rscDtMouldResponse: any) => {
          this.rscDtMouldList = rscDtMouldResponse;
          if (rscDtMouldResponse !== undefined && rscDtMouldResponse !== null) {
            this.rscDtMouldList = rscDtMouldResponse;
            this.filterDataList = this.rscDtMouldList;
            this.totalLengthOfCollection = this.filterDataList.length;
          }
          this.ngxSpinnerService.hide();
        },
        (error) => {
          this.ngxSpinnerService.hide();
          console.log(error);
        });
    })
  }

  //search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    console.log("csearchTerm1");
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.rscDtMouldList);
    return this.rscDtMouldList.filter(x => String(x.mould)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.childMould)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.noOfCavities)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.perCavity)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.totalCapacity)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.active)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }

}
