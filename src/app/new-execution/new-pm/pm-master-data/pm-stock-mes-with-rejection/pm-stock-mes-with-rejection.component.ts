import {Component, OnInit} from '@angular/core';
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {PmMasterService} from "../pm-master.service";

@Component({
  selector: 'app-pm-stock-mes-with-rejection',
  templateUrl: './pm-stock-mes-with-rejection.component.html',
  styleUrls: ['./pm-stock-mes-with-rejection.component.css']
})
export class PmStockMesWithRejectionComponent implements OnInit {
  mesWithRejectionList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private pmMaster: PmMasterService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmMaster.getPmMesWithRejectionStockData(this.executionFlowPageService.selectedHeaderId).subscribe((mesWithRejection: any) => {
        console.log(mesWithRejection);
        if (mesWithRejection !== null && mesWithRejection !== undefined) {
          this.mesWithRejectionList = mesWithRejection;
          this.filterDataList = this.mesWithRejectionList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  //complete example................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.mesWithRejectionList);
    return this.mesWithRejectionList.filter(x => String(x.itemCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.quantity)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.lotWiseStock)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.lotNumber)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stockStatus)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}

