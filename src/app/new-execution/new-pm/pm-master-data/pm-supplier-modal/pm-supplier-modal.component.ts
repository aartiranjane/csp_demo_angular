import {Component, OnInit} from '@angular/core';
import {PmExportService} from "../../pm-export.service";
import {PmServiceService} from "../../pm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-pm-supplier-modal',
  templateUrl: './pm-supplier-modal.component.html',
  styleUrls: ['./pm-supplier-modal.component.css']
})
export class PmSupplierModalComponent implements OnInit {
  pmSupplierDataList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private pmExportService: PmExportService, private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmService.getPmSupplierListData().subscribe((supplierResponse: any) => {
        if (supplierResponse !== null && supplierResponse !== undefined) {
          this.pmSupplierDataList = supplierResponse;
          this.filterDataList = this.pmSupplierDataList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

//search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.pmSupplierDataList);
    return this.pmSupplierDataList.filter(x => String(x.supplierCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.supplierName)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}
