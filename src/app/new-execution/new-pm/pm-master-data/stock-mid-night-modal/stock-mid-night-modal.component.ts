import {Component, OnInit} from '@angular/core';
import {PmExportService} from "../../pm-export.service";
import {PmServiceService} from "../../pm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {PmMasterService} from "../pm-master.service";

@Component({
  selector: 'app-stock-mid-night-modal',
  templateUrl: './stock-mid-night-modal.component.html',
  styleUrls: ['./stock-mid-night-modal.component.css']
})
export class StockMidNightModalComponent implements OnInit {
  midNightStockList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private pmExportService: PmExportService, private pmService: PmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private pmMaster: PmMasterService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.pmMaster.getPmMesStockData(this.executionFlowPageService.selectedHeaderId).subscribe((mesResponse: any) => {
        if (mesResponse !== null && mesResponse !== undefined) {
          this.midNightStockList = mesResponse;
          this.filterDataList = this.midNightStockList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  //complete example................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.midNightStockList);
    return this.midNightStockList.filter(x => x.itemCode?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.itemDescription?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.quantity.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.lotWiseStock.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.lotNumber.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.stockStatus.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}

