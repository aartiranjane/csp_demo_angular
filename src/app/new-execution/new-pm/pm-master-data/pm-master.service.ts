import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UriService} from "../../../uri.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {catchError, map} from "rxjs/operators";
import {Subject, throwError} from 'rxjs';
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {PmServiceService} from "../pm-service.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PmMasterService {
  headers = [];
  endpoint = this.uriService.getResourceServerUri();
  ppHeaderId = this.executionFlowPageService.selectedHeaderId;
  monthNameList: any = [];
  fgCodeList: any = [];

  remarkChanged = new Subject();

  constructor(private http: HttpClient, private excelExportConstantsService: ExcelExportConstantsService,
              private pmService: PmServiceService,
              private uriService: UriService, private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  addRemark(remark: any) {

    return this.http.post(this.endpoint + '/api/remark/pm', JSON.stringify(remark), httpOptions).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }

  updateRemark(remark: any) {
    return this.http.put(this.endpoint + '/api/remark/pm', JSON.stringify(remark), httpOptions).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }

  getPmMesStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stocks/pm/mes/with-named-query/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmReceiptStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stocks/pm/rtd/with-named-query/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getPmMesWithRejectionStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stocks/pm/mes/with-rejection/with-named-query/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
