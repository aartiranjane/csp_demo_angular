import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('nav-org-tabb').click();
  }

}
