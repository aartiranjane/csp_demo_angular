import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";

@Component({
  selector: 'app-plan-management',
  templateUrl: './plan-management.component.html',
  styleUrls: ['./plan-management.component.css']
})
export class PlanManagementComponent implements OnInit {
  planNameList: any = [];
  planDataForm = new FormGroup({
    planDataFormArray: new FormArray([])
  });
  planNameListToSave: Record<string, any>[] = [];
  isEdit = false;
  isPlanNameValid = true;
  successMessage = '';
  errorMessage = '';
  invalidPlanId;

  constructor(private spinner: NgxSpinnerService, private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPlanMpsDataWithoutBackup().subscribe((planNameList: any) => {
      this.planNameList = planNameList;
      this.setPlanFormData();
    });
  }


  setPlanFormData() {
    this.planNameList.forEach((itemCodes: any) => {
      const rowView = new FormGroup({
        id: new FormControl(itemCodes?.id),
        date: new FormControl(itemCodes?.mpsDate),
        planName: new FormControl(itemCodes?.mpsName, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]),
        originalPlanName: new FormControl(itemCodes?.mpsName),
        tagName: new FormControl(itemCodes?.tagName),
        isMpsValidated: new FormControl(itemCodes?.isMpsValidated),
        isMpsBackup: new FormControl(itemCodes?.isMpsBackup)
      });
      (<FormArray>this.planDataForm.get('planDataFormArray')).push(rowView);
    });
  }

  updatePlanData(row: any, changedValue: string, rowIndex: number) {
    this.spinner.show();
    const currentRowIndex: number = this.planNameList.findIndex(value => value.id === row.id.value);
    this.planNameList[currentRowIndex].mpsName = changedValue;
    this.setChangedRowCodeList(row?.id?.value, currentRowIndex);
    this.checkIsValidPlan(row, changedValue, rowIndex);
    this.spinner.hide();
  }

  checkIsValidPlan(row: any, changedValue: string, rowIndex: number) {
    const convertedDate = new Date(row.date.value);
    const listOfSameMonth = this.planNameList.filter((value, index) => (new Date(value.mpsDate).getMonth() === convertedDate.getMonth()) && value.mpsName === changedValue.trim() && index !== rowIndex);
    this.isPlanNameValid = listOfSameMonth.length === 0;
    if (!this.isPlanNameValid) {
      this.errorMessage = 'Found one months two same plan name';
      this.invalidPlanId = row.id.value;
    } else {
      this.invalidPlanId = '';
    }
  }

  setChangedRowCodeList(id: string, currentRowIndex: number) {
    const isPresent = this.planNameListToSave.findIndex((value: any) => value.id === id);
    if (isPresent === -1) {
      this.planNameListToSave.push(this.planNameList[currentRowIndex]);
    }
  }

  setPlanDataFormValues(index: number) {
    this.planDataForm.get('planDataFormArray')['controls'][index].controls.planName.value =
      this.planDataForm.value.planDataFormArray[index].originalPlanName;
  }

  setPlanDataListValues(index) {
    this.planNameList[index].mpsName = this.planDataForm.value.planDataFormArray[index].originalPlanName;
  }

  savePlanDetails() {
    this.isEdit = false;
    this.spinner.show();
    this.successMessage = 'Details Saving...';
    this.executionFlowPageService.updatePPheaderDetails(this.planNameList).subscribe(value => {
      console.log(value);
      this.spinner.hide();
      this.successMessage = 'Details Saved Successfully';
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });

  }


  cancelPlanDetails() {
    this.isEdit = false;
    this.planNameList.forEach((value, index: number) => {
      this.setPlanDataFormValues(index);
      this.setPlanDataListValues(index);
    });
  }

  isValidate(row: any, value: any, rowIndex: number) {
    let planDataFormArray = [];
    planDataFormArray = this.planDataForm.get('planDataFormArray')['controls'];
    planDataFormArray.forEach((plan, index: number) => {
      if (index !== rowIndex) {
        plan.controls.isMpsValidated.value = false;
        plan.controls.isMpsBackup.value = false;
      } else {
        plan.controls.isMpsValidated.value = true;
        plan.controls.isMpsBackup.value = true;
      }
    });

    this.planNameList.forEach((plan, index: number) => {
      if (index !== rowIndex) {
        plan.isMpsValidated = false;
        plan.isMpsBackup = false;
      } else {
        plan.isMpsValidated = true;
        plan.isMpsBackup = true;
      }
    });
  }

  isFormValid() {
    let formArray = [];
    formArray = this.planDataForm.get('planDataFormArray')['controls'];
    const temp = formArray.filter(value => value.valid === false);
    return temp.length !== 0 || !this.isPlanNameValid;
  }
}
