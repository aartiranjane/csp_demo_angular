import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-organisation',
  templateUrl: './organisation.component.html',
  styleUrls: ['./organisation.component.css']
})
export class OrganisationComponent implements OnInit, AfterViewChecked {
  serviceTypeList: any = [];
  selectedServiceType: any = [];
  singleSelectDropdownSettings = {};
  model: NgbDateStruct;
  constructor(private changeRef: ChangeDetectorRef) { }

  ngAfterViewChecked(): void { this.changeRef.detectChanges(); }
  ngOnInit() {
    this.singleSelectDropdownSettings = {
      text: 'Select',
      enableSearchFilter: true,
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class'
    };
    this.serviceTypeList = [
      { id: 1, itemName: 'item1' },
      { id: 2, itemName: 'item2' },
      { id: 3, itemName: 'item3' },
      { id: 4, itemName: 'item4' }
    ];

  }
  onItemSelect(item: any) {
    console.log(item);
    console.log(this.selectedServiceType);
  }

}
