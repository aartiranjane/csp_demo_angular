import {Component, OnInit} from '@angular/core';
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-rm-material-modal',
  templateUrl: './rm-material-modal.component.html',
  styleUrls: ['./rm-material-modal.component.css']
})
export class RmMaterialModalComponent implements OnInit {
  rmMaterialDataList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmMaterialCategoryMastersData().subscribe((rmMateriallisResponse: any) => {
        if (rmMateriallisResponse !== null && rmMateriallisResponse !== undefined) {
          this.rmMaterialDataList = rmMateriallisResponse;
          this.filterDataList = this.rmMaterialDataList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  //search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    return this.rmMaterialDataList.filter(x => String(x.itemCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.moqValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.seriesValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stockValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.mapPrice)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stdPrice)?.toLowerCase().indexOf(v.toLowerCase()) !== -1
    );
  }
}
