import {Component, OnInit} from '@angular/core';
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-rm-transfer-stock-modal',
  templateUrl: './rm-transfer-stock-modal.component.html',
  styleUrls: ['./rm-transfer-stock-modal.component.css']
})
export class RmTransferStockModalComponent implements OnInit {
  transferStockList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmTransferStockData(this.executionFlowPageService.selectedHeaderId).subscribe((transferStockResponse: any) => {
        console.log(transferStockResponse);
        if (transferStockResponse !== null && transferStockResponse !== undefined) {
          this.transferStockList = transferStockResponse;
          this.filterDataList = this.transferStockList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

//search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    console.log("csearchTerm1");
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.transferStockList);
    return this.transferStockList.filter(x => String(x.itemCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.quantity)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }

}
