import {Component, OnInit} from '@angular/core';
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-rm-stock-receipt-modal',
  templateUrl: './rm-stock-receipt-modal.component.html',
  styleUrls: ['./rm-stock-receipt-modal.component.css']
})
export class RmStockReceiptModalComponent implements OnInit {
  receiptList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmReceiptStockData(this.executionFlowPageService.selectedHeaderId).subscribe((receiptResponse: any) => {
        console.log(receiptResponse);
        if (receiptResponse !== null && receiptResponse !== undefined) {
          this.receiptList = receiptResponse;
          this.filterDataList = this.receiptList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

//complete example................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.receiptList);
    return this.receiptList.filter(x => String(x.itemCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.totalReceiptStock)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.lotWiseReceiptStock)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.lotNumber)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.supplierName)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.poNumber)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}
