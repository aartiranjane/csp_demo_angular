import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RmExportService} from "../rm-export.service";
import {RmServiceService} from "../rm-service.service";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {saveAs} from "file-saver";

@Component({
  selector: 'app-rm-master-data',
  templateUrl: './rm-master-data.component.html',
  styleUrls: ['./rm-master-data.component.css']
})
export class RmMasterDataComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  isExcelDisable = false;

  rmMaterialList = [
    ['RM Materials', 'rmMaterialPopup'],
    ['Bulk Materials', 'rmBulkMaterialPopup'],
    ['Base/SubBase Materials', 'rmSubBasesMaterialPopup'],
  ];
  rmSupplierList = [
    ['RM Supplier', 'rmSupplierPopup']
  ];
  slobList = [
    ['SLOB Reason', 'rmSlobReasonPopup'],
    ['SLOB Remarks', 'rmSlobRemarkPopup'],
    /* ['Map', 'rmMapPopup'],
     ['STD Price', 'rmStdPricePopup']*/
  ];

  stockList = [
    ['Midnight A+Q', 'rmMidNightPopup'],
    ['Midnight Rejection', 'mesWithRejectionPopup'],
    ['Receipts', 'rmReceiptPopup'],
    ['Bulk OP RM', 'bulkOprmPopup'],
    ['Transfer Stocks', 'transferStockPopup'],
    ['Rejection', 'rejectionPopup'],

  ];

  currentMasterName: string;

  constructor(private router: Router, private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  ngOnInit(): void {
  }

  loadRmMasterData(rmMasterName: any, rmMasterUrl: any) {
    this.currentMasterName = rmMasterName;
    this.router.navigate(['newRm/rmMasterData/' + rmMasterUrl]);
  }

  exportAsXLSX() {
    switch (this.currentMasterName) {
      case 'RM Materials':
        this.rmExportService.getRmMaterialMasterDataExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmMaterialMasterData + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Bulk Materials':
        this.rmExportService.getRmBulkMaterialExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmBulkMaterialMasters + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'RM Supplier':
        this.rmExportService.getRmSupplierExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          console.log(excelFileResponse);
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmSupplierMasterData + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Base/SubBase Materials':
        this.rmExportService.getRmBaseSubBaseMaterialExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmBaseSubBaseMaterialMasters + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Midnight A+Q':
        this.rmExportService.getMesStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmMesStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Receipts':
        this.rmExportService.getReceiptStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmReceiptStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Bulk OP RM':
        this.rmExportService.getBulkOpRmStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmBulkOpRmStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Transfer Stocks':
        this.rmExportService.getTransferStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable =true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmTransferStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable =false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Rejection':
        this.rmExportService.getRejectionStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmRejectionStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      case 'Midnight Rejection':
        this.rmExportService.getMesRejectionStockExcelFile().subscribe((excelFileResponse: any) => {
          this.isExcelDisable = true;
          const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
          saveAs(blob, this.excelExportConstantsService.rmMesRejectionStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
          this.successMessage = 'Download Success!';
          setTimeout(() => {
            this.successMessage = '';
            this.isExcelDisable = false;
          }, 2000);
        }, (error) => {
          console.log(error);
          this.errorMessage = 'Download Failure!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 2000);
        });
        break;
      default:
        console.log("No such Name exists!");
        break;
    }

  }
}
