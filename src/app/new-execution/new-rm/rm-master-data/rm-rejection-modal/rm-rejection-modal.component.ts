import {Component, OnInit} from '@angular/core';
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-rm-rejection-modal',
  templateUrl: './rm-rejection-modal.component.html',
  styleUrls: ['./rm-rejection-modal.component.css']
})
export class RmRejectionModalComponent implements OnInit {
  rejectionDataList = [];
  cfilterClient = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;
  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmRejectionStockData(this.executionFlowPageService.selectedHeaderId).subscribe((rejectionStockResponse: any) => {
        console.log(rejectionStockResponse);
        if (rejectionStockResponse !== null && rejectionStockResponse !== undefined) {
          this.rejectionDataList = rejectionStockResponse;
          this.cfilterClient = this.rejectionDataList;
          this.totalLengthOfCollection = this.cfilterClient.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }
  cpage = 1;
  cpageSize = 15;
  csearchTerm1: any = '';

  get csearchTerm(): any {
    return this.csearchTerm1;
  }

  set csearchTerm(val: any) {
    this.csearchTerm1 = val;
    this.cfilterClient = this.cfilter(val);
    this.totalLengthOfCollection = this.cfilterClient.length;
  }

  cfilter(v: any) {
    console.log(this.rejectionDataList);
    return this.rejectionDataList.filter(x => x.itemCode?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.itemDescription?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.totalReceiptStock.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.lotWiseReceiptStock.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.lotNumber.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.supplierName.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      x.poNumber.toString()?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}
