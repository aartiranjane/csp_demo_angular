import {Component, OnInit} from '@angular/core';
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-rm-bulkoprm-modal',
  templateUrl: './rm-bulkoprm-modal.component.html',
  styleUrls: ['./rm-bulkoprm-modal.component.css']
})
export class RmBulkoprmModalComponent implements OnInit {
  bulkOpRmDataList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getBulkOpRmStockData(this.executionFlowPageService.selectedHeaderId).subscribe((bulkOpRmResponse: any) => {
        if (bulkOpRmResponse !== null && bulkOpRmResponse !== undefined) {
          this.bulkOpRmDataList = bulkOpRmResponse;
          this.filterDataList = this.bulkOpRmDataList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  //search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.bulkOpRmDataList);
    return this.bulkOpRmDataList.filter(x => String(x.bulkCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.bulkDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.rmCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.rmDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.bulkTotalQuantity)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.bulkWiseRmQuantity)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}
