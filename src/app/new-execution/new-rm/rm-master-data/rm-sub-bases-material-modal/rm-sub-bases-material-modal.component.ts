import {Component, OnInit} from '@angular/core';
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-rm-sub-bases-material-modal',
  templateUrl: './rm-sub-bases-material-modal.component.html',
  styleUrls: ['./rm-sub-bases-material-modal.component.css']
})
export class RmSubBasesMaterialModalComponent implements OnInit {
  rmSubBasesMaterialList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;


  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmSubBasesMaterialData().subscribe((subBasesMaterialResponse: any) => {
        if (subBasesMaterialResponse !== null && subBasesMaterialResponse !== undefined) {
          this.rmSubBasesMaterialList = subBasesMaterialResponse;
          this.filterDataList = this.rmSubBasesMaterialList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  //search Term................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    console.log("csearchTerm1");
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    return this.rmSubBasesMaterialList.filter(x => String(x.itemCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.moqValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.seriesValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stockValue)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.mapPrice)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stdPrice)?.toLowerCase().indexOf(v.toLowerCase()) !== -1
    );
  }
}
