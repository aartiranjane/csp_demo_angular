import {Component, OnInit} from '@angular/core';
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";

@Component({
  selector: 'app-rm-stock-mid-night-modal',
  templateUrl: './rm-stock-mid-night-modal.component.html',
  styleUrls: ['./rm-stock-mid-night-modal.component.css']
})
export class RmStockMidNightModalComponent implements OnInit {
  midNightStockList = [];
  filterDataList = null;
  totalLengthOfCollection: number = 0;
  isExcelDisable = false;

  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmMesStockData(this.executionFlowPageService.selectedHeaderId).subscribe((mesResponse: any) => {
        if (mesResponse !== null && mesResponse !== undefined) {
          console.log(mesResponse);
          this.midNightStockList = mesResponse;
          this.filterDataList = this.midNightStockList;
          this.totalLengthOfCollection = this.filterDataList.length;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

//complete example................
  cpage = 1;
  cpageSize = 15;
  searchValue: any = '';

  get searchTerm(): any {
    return this.searchValue;
  }

  set searchTerm(val: any) {
    this.searchValue = val;
    this.filterDataList = this.filterTable(val);
    this.totalLengthOfCollection = this.filterDataList.length;
  }

  filterTable(v: any) {
    console.log(this.midNightStockList);
    return this.midNightStockList.filter(x => String(x.itemCode)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.itemDescription)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.quantity)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.lotWiseStock)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.lotNumber)?.toLowerCase().indexOf(v.toLowerCase()) !== -1 ||
      String(x.stockStatus)?.toLowerCase().indexOf(v.toLowerCase()) !== -1);
  }
}
