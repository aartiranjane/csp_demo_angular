import {Component, OnInit} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";

@Component({
  selector: 'app-rm-slob-reason-modal',
  templateUrl: './rm-slob-reason-modal.component.html',
  styleUrls: ['./rm-slob-reason-modal.component.css']
})
export class RmSlobReasonModalComponent implements OnInit {
  selectedId: any;
  rmSlobReasonList = [];
  remarkForm: FormGroup;

  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmSlobReason(this.executionFlowPageService.selectedHeaderId).subscribe((rscDtRmSlobReasonResponse: any) => {
        if (rscDtRmSlobReasonResponse !== null && rscDtRmSlobReasonResponse !== undefined) {
          this.rmSlobReasonList = rscDtRmSlobReasonResponse;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
  }

  save() {
  }
}
