import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {RmServiceService} from "../../rm-service.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {switchMap} from "rxjs/operators";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-new-bulk-gross',
  templateUrl: './new-bulk-gross.component.html',
  styleUrls: ['./new-bulk-gross.component.css']
})
export class NewBulkGrossComponent implements OnInit, OnDestroy {
  $subs:Subscription;
  bulkDataList: any = [];
  monthNamesList?: any = [];
  bulkSearchTerm: any;
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;
  constructor(private router: Router, private rmService: RmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getGrossConsumptionBulkData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((grossConsumptionResponse: any) => {
        if (grossConsumptionResponse !== null && grossConsumptionResponse !== undefined) {
          this.bulkDataList = grossConsumptionResponse?.itemCodes;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  exportAsXLSX() {
    this.rmExportService.getBulkGrossExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmBulkGrossConsumption + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }


}
