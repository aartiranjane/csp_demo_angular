import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RmServiceService} from "../../rm-service.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {switchMap} from "rxjs/operators";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-new-bulk-logical',
  templateUrl: './new-bulk-logical.component.html',
  styleUrls: ['./new-bulk-logical.component.css']
})
export class NewBulkLogicalComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  nodes: any = [];
  logicalConsumptionSearchTerm: any;
  fgCodeSearchTerm: any;
  logicalConsumptionDataList: any = [];
  fgCodeList: any = [];
  monthNamesList?: any = [];
  selectedFgCode: any;
  mpsByFgDataList: any = [];
  successMessage = '';
  errorMessage = '';
  bulkLogicalList = [];
  isExcelDisable=false;
  constructor(private router: Router, private rmService: RmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getRmBulkLogicalData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((bulkLogicalResponse: any) => {
        if (bulkLogicalResponse !== null && bulkLogicalResponse !== undefined) {
          this.bulkLogicalList = bulkLogicalResponse;
          console.log(this.bulkLogicalList);
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  getFgDetails(row: any) {
    this.selectedFgCode = row.parentId;
    this.executionFlowPageService.getMpsByFg(this.selectedFgCode).subscribe((mpsByFgResponse: any) => {
      if (mpsByFgResponse !== null && mpsByFgResponse !== undefined) {
        this.mpsByFgDataList = mpsByFgResponse;
        console.log(this.mpsByFgDataList);

      }
      this.spinner.hide();
    }, (error) => {
      console.log(error);
    });
  }

  displayFgRelationGraph() {
    this.rmService.getFgRelationByBulkData(this.selectedFgCode).subscribe((fgRelationResponse: any) => {
      this.nodes = [];
      if (fgRelationResponse !== null && fgRelationResponse.length !== 0) {
        this.nodes.push({name: fgRelationResponse.name, childs: fgRelationResponse.childs});
      }
    }, (error) => {
      console.log(error);
    });
  }

  test(ck: any) {

  }

  exportAsXLSX() {
    this.rmExportService.getBulkLogicalExcelFile().subscribe((excelFileResponse:any)=>{
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse],  {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmBulkLogicalConsumption + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

}
