import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rm-analytics-dashboard',
  templateUrl: './rm-analytics-dashboard.component.html',
  styleUrls: ['./rm-analytics-dashboard.component.css']
})
export class RmAnalyticsDashboardComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  thirdTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('rmMpsTab').click();
  }

}
