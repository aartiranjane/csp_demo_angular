import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {Chart} from "chart.js";
import {RmServiceService} from "../../rm-service.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-rm-slob-dash-tab',
  templateUrl: './rm-slob-dash-tab.component.html',
  styleUrls: ['./rm-slob-dash-tab.component.css']
})
export class RmSlobDashTabComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  rmSlobSummaryDashboardList:any = [];
  valueBasis:any;
  percentageBasis:any;
  slobDetails:any;
  constructor(private rmService: RmServiceService, private ngxSpinnerService: NgxSpinnerService,
              public executionFlowPageService: ExecutionFlowPageServiceService) { }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      console.log(value1);
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmSlobSummaryDashboardData(this.executionFlowPageService.selectedHeaderId).subscribe((slobSummaryDashboardResponse: any)=>{
        if(slobSummaryDashboardResponse!==null && slobSummaryDashboardResponse!==undefined){
          this.rmSlobSummaryDashboardList = slobSummaryDashboardResponse;
          this.valueBasisChart();
          this.percentageBasisChart();
          this.slobDetailsChart();
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    });

  }

  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  valueBasisChart(){
    this.valueBasis = new Chart('doughnut_slob_stock', {
      type: 'doughnut',
      data: {
        labels: ['SLOB','Stock'],
        datasets: [
          {
            data: [this.rmSlobSummaryDashboardList?.totalSlobValue,
              this.rmSlobSummaryDashboardList?.totalStockValue ],
            backgroundColor: ['#db0000',
              'green'],
            fill: false
          },
        ]
      },
      options: {

        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }

  percentageBasisChart(){
    this.percentageBasis = new Chart('doughnut_Ob_Sm_Iq', {
      type: 'doughnut',
      data: {
        labels: ['OB','SM','IQ'],
        datasets: [
          {
            data: [this.rmSlobSummaryDashboardList?.obPercentage,
              this.rmSlobSummaryDashboardList?.slPercentage,
              this.rmSlobSummaryDashboardList?.iqPercentage],
            backgroundColor: ['#db0000',
              '#ff8c00',
              'green'],
            fill: false
          },
        ]
      },
      options: {

        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }

  slobDetailsChart(){
    this.slobDetails = new Chart('doughnut_slob_details', {
      type: 'doughnut',
      data: {
        labels: ['OB','SM'],
        datasets: [
          {
            data: [this.rmSlobSummaryDashboardList?.obValue,
              this.rmSlobSummaryDashboardList?.slValue],
            backgroundColor: ['#db0000',
              '#ff8c00'],
            fill: false
          },
        ]
      },
      options: {

        cutoutPercentage: 40,
        legend: {
          display: false
        },
        tooltips: {
          enabled: true,
          titleFontSize: 12,
          bodyFontSize: 12
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['green', 'white', 'red'],
            precision: 2,
            arc: true,
          }
        },

      }
    });
  }
}
