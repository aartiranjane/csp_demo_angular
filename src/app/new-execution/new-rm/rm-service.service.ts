import {Injectable} from '@angular/core';
import {catchError, map} from "rxjs/operators";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UriService} from "../../uri.service";
import {ExecutionFlowPageServiceService} from "../execution-flow-page-service.service";
import {Observable, throwError} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RmServiceService {

  headers = [];
  endpoint = this.uriService.getResourceServerUri();
  ppHeaderId = this.executionFlowPageService.selectedHeaderId;
  monthNameList: any = [];
  fgCodeList: any = [];

  constructor(private http: HttpClient,
              private uriService: UriService, private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  getGrossConsumptionBulkData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/gross-consumption/bulk/' + ppHeaderId,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getGrossConsumptionRmData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/gross-consumption/rm/' + ppHeaderId,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgRelationByBulkData(selectedFgCode: any) {
    return this.http.get(this.endpoint + '/api/bom/fg-relation/bulk/' + selectedFgCode, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getFgRelationByRmData(selectedFgCode: any) {
    return this.http.get(this.endpoint + '/api/bom/fg-relation/rm/' + selectedFgCode, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getLogicalConsumptionBulkData(selectedFgCode: any) {
    return this.http.get(this.endpoint + '/api/logical-consumption/bulk/' + this.executionFlowPageService.selectedHeaderId + '/' + selectedFgCode,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getLogicalConsumptionRmData(selectedFgCode: any) {
    return this.http.get(this.endpoint + '/api/logical-consumption/rm/' + this.executionFlowPageService.selectedHeaderId + '/' + selectedFgCode,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getBulkCodeListData() {
    return this.http.get(this.endpoint + '/api/bulk-list/' + this.executionFlowPageService.selectedHeaderId,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getGCByBulk(bulkItemId: any) {
    return this.http.get(this.endpoint + '/api/gross-consumption/by-bulk/' + this.executionFlowPageService.selectedHeaderId + '/' + bulkItemId,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmStockData() {
    return this.http.get(this.endpoint + '/api/stocks/rm/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSupplyData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-purchase-plan/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updateRMLatestPurchasePlan(rmPurchasePlanDTOs: any) {
    return this.http.put(this.endpoint + '/api/rm-latest-purchase-plans', JSON.stringify(rmPurchasePlanDTOs), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmPurchasePlanCalculationData(selectedId: any, supplierDetailsId: any) {
    return this.http.get(this.endpoint + '/api/rm-purchase-plan-calculations/' + selectedId + '/' + this.executionFlowPageService.selectedHeaderId + '/' + supplierDetailsId,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmStockDetailsData(selectedRmStockId: any) {
    return this.http.get(this.endpoint + '/api/stocks/rm/details/' + this.executionFlowPageService.selectedHeaderId + '/' + selectedRmStockId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSlobReasonsData() {
    return this.http.get(this.endpoint + '/api/rm-slob/reasons/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSlob(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-slob/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSlobTotalStockData(selectedId: any, ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-slob/total-consumption/' + ppHeaderId + '/' + selectedId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmPivotConsumptionDescription(ppHeaderId: any, itemId: any) {
    return this.http.get(this.endpoint + '/api/consumption/pivot/rm/' + ppHeaderId + '/' + itemId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSummaryData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-slob-summary-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSummaryCoverDays(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-cover-days-summary-all-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSummaryCoverDaysClassWise(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-cover-days-class-wise-summary-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmCoverDaysAllMonthsData(ppHeaderId) {
    return this.http.get(this.endpoint + '/api/rm-cover-days-summary-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updateRMSlobList(RmSlobDTOs: any, sitValue: any) {
    return this.http.put(this.endpoint + '/api/rm-slob-save-reasons/' + sitValue + "/" + this.executionFlowPageService.selectedHeaderId, JSON.stringify(RmSlobDTOs), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmCoverDaysMonthWiseData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-cover-days-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmMesStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stock/rm/mes/with-named-query/' + ppHeaderId , httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmReceiptStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stock/rm/rtd/with-named-query/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getBulkOpRmStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stock/rm/bulkOpRm/with-named-query/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmTransferStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stock/rm/transferStock/with-named-query/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmRejectionStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stock/rm/rejection/with-named-query/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmMesWithRejectionStockData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/stock/rm/mes/with-rejection/with-named-query/' + ppHeaderId , httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }


  getRmSlobNullMapPriceData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-slob-nullMapPrice-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updateRMSlobsListBasedOnMap(PmSlobDTOs: any, sitValue: any) {
    return this.http.put(this.endpoint + '/api/rm-slob-saveAllDetails/' + sitValue, JSON.stringify(PmSlobDTOs), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmBulkLogicalData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/logical-consumption/bulk/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmLogicalData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/logical-consumption/rm/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmCoverDaysNullStdPriceData(ppHeaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-coverDays-nullStdPrice-details/' + ppHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updateRMCoverDaysListBasedOnStdPrice(rmCoverDaysDTO: any) {
    return this.http.put(this.endpoint + '/api/rm-coverDays-saveAllDetails', JSON.stringify(rmCoverDaysDTO), httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSlobSummaryOverAllDeviationData() {
    return this.http.get(this.endpoint + '/api/slob-summary/rm/' + this.executionFlowPageService.selectedHeaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSlobSummaryDashboardData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/slob-summary/dashboard/rm/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSlobMovementDashboardData() {
    return this.http.get(this.endpoint + '/api/slob-movement/rm', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmInventoryQualityDashboardData() {
    return this.http.get(this.endpoint + '/api/slob-inventory-quality-index/rm', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmPurchaseCoverDaysData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-purchase-plan-cover-days/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmPurchaseStockEquationData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/rm-purchase-plan-cover-days/stock-equation/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmPivotConsumptionData(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/consumption/pivot/rm/' + ppheaderId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmMateriallistData() {
    return this.http.get(this.endpoint + '/api/material-masters/raw-material-list', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmMaterialCategoryMastersData() {
    return this.http.get(this.endpoint + '/api/material-masters/raw-material-list', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSubBasesMaterialData() {
    return this.http.get(this.endpoint + '/api/material-masters/subBases-material-list', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSupplierMaterialData() {
    return this.http.get(this.endpoint + '/api/suppliers-masters/raw-materials-suppliers-list' , httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmBulkMaterialData() {
    return this.http.get(this.endpoint + '/api/material-masters/bulk-material-list', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getRmSlobReason(ppheaderId: any) {
    return this.http.get(this.endpoint + '/api/rsc-dt-reason', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  rmPurchasePlanExcelExport(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-latest-purchase-plans/excel/export/' + this.executionFlowPageService.selectedHeaderId}`, {responseType: 'text'});
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
