import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UriService} from "../../uri.service";
import {ExecutionFlowPageServiceService} from "../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../excel-export-constants.service";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RmExportService {
  headers = [];
  endpoint = this.uriService.getResourceServerUri();
  ppHeaderId = this.executionFlowPageService.selectedHeaderId;

  constructor(private http: HttpClient,
              private excelExportConstantsService: ExcelExportConstantsService, private uriService: UriService, private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  getRmMpsExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.pmMpsPlan}`, {responseType: 'blob'});
  }

  getRmLogicalExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmLogicalConsumption}`, {responseType: 'blob'});
  }

  getRmGrossExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmGrossConsumption}`, {responseType: 'blob'});
  }

  getRmPurchaseExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmPurchasePlan}`, {responseType: 'blob'});
  }

  getBulkLogicalExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmBulkLogicalConsumption}`, {responseType: 'blob'});
  }

  getBulkGrossExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmBulkGrossConsumption}`, {responseType: 'blob'});
  }

  getRmSlobExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmSlobExcel}`, {responseType: 'blob'});
  }

  getRmSummaryExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmSlobCoverDaysSummary}`, {responseType: 'blob'});
  }

  getRmCoverDaysExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmCoverDaysExcel}`, {responseType: 'blob'});
  }

  getMesRejectionStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmMesRejectionStock}`, {responseType: 'blob'});
  }

  getMesStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmMesStock}`, {responseType: 'blob'});
  }

  getReceiptStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmReceiptStock}`, {responseType: 'blob'});
  }

  getBulkOpRmStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmBulkOpRmStock}`, {responseType: 'blob'});
  }

  getTransferStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmTransferStock}`, {responseType: 'blob'});
  }

  getRmSupplierExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmSupplierMasterData}`, {responseType: 'blob'});
  }

  getRejectionStockExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmRejectionStock}`, {responseType: 'blob'});
  }

  getRmBulkMaterialExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmBulkMaterialMasters}`, {responseType: 'blob'});
  }

  getRmMaterialMasterDataExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmMaterialMasterData}`, {responseType: 'blob'});
  }

  getRmBaseSubBaseMaterialExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmBaseSubBaseMaterialMasters}`, {responseType: 'blob'});
  }

  getRmPurchaseCoverdaysExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmPurchaseCoverdays}`, {responseType: 'blob'});
  }

  getRmPurchaseStockEquation() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmPurchaseStockEquation}`, {responseType: 'blob'});
  }

  getRmSLOBSummary() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmSLOBSummary}`, {responseType: 'blob'});
  }

  getRmCoverDaySummary() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmCoverDaysSummary}`, {responseType: 'blob'});
  }

  getRmPivotComponentExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rm/export/' + this.executionFlowPageService.selectedHeaderId + "/" + this.excelExportConstantsService.rmPivotConsumption}`, {responseType: 'blob'});
  }

  getMPSRmTriangleAnalysisExportExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rmAnnualExport/export/' + this.excelExportConstantsService.rmMpsTriangleAnalysis}`, {responseType: 'blob'});
  }

  getRmSlobMovementExportExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rmAnnualExport/export/' + this.excelExportConstantsService.rmSlobMovement}`, {responseType: 'blob'});
  }

  getRmInventoryQualityIndexExportExcelFile() {
    return this.http.get(`${this.endpoint + '/api/rmAnnualExport/export/' + this.excelExportConstantsService.rmInventoryQualityIndex}`, {responseType: 'blob'});
  }
}
