import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rm-reports-page',
  templateUrl: './rm-reports-page.component.html',
  styleUrls: ['./rm-reports-page.component.css']
})
export class RmReportsPageComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('nav-org-tabb').click();
  }

}
