import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RmServiceService} from "../rm-service.service";
import {RmExportService} from "../rm-export.service";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-rm-pivot',
  templateUrl: './rm-pivot.component.html',
  styleUrls: ['./rm-pivot.component.css']
})
export class RmPivotComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  pivotDataList: any = [];
  monthNamesList?: any = [];
  pivotDescriptionList: any = [];
  successMessage = '';
  errorMessage = '';
  bulkCode:any;
  selectedId: any;
  isExcelDisable=false;
  constructor(private router: Router, private rmService: RmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getRmPivotConsumptionData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((pivotDataResponse: any) => {
          console.log(pivotDataResponse);
          this.pivotDataList = pivotDataResponse.consumptionPivotDTOs;
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      })
    })
  }

  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  pivotConsumption(row: any) {
      this.selectedId = row.parentItemId;
      this.bulkCode=row.parentItemCode;
        this.spinner.show();
        this.rmService.getRmPivotConsumptionDescription( this.executionFlowPageService.selectedHeaderId,this.selectedId).subscribe((rmPivotDescriptionResponse: any) => {
            if (rmPivotDescriptionResponse !== null && rmPivotDescriptionResponse !== undefined) {
              this.pivotDescriptionList = rmPivotDescriptionResponse;
            }
            console.log(this.pivotDescriptionList);
            this.spinner.hide();
          },

          (error) => {
            console.log(error);
          });

    }


  exportAsXLSX() {
    this.rmExportService.getRmPivotComponentExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmPivotConsumption + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
