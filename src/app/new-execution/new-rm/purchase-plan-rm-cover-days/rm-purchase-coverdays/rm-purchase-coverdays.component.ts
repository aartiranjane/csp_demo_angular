import {Component, OnDestroy, OnInit} from '@angular/core';
import {switchMap} from "rxjs/operators";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {RmServiceService} from "../../rm-service.service";
import {PmServiceService} from "../../../new-pm/pm-service.service";
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-rm-purchase-coverdays',
  templateUrl: './rm-purchase-coverdays.component.html',
  styleUrls: ['./rm-purchase-coverdays.component.css']
})
export class RmPurchaseCoverdaysComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  monthNamesList?: any = [];
  coverDaysDataList?: any = [];
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;
  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private rmService: RmServiceService, private pmService: PmServiceService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getRmPurchaseCoverDaysData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((coverDaysDataResponse: any) => {
        this.coverDaysDataList = coverDaysDataResponse;
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      })
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  exportAsXLSX() {
    this.rmExportService.getRmPurchaseCoverdaysExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmPurchaseCoverdays + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
