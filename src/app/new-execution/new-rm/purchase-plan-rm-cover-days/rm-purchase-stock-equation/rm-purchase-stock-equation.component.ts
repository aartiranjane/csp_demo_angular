import {Component, OnDestroy, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {PmServiceService} from "../../../new-pm/pm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {RmServiceService} from "../../rm-service.service";
import {saveAs} from "file-saver";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {RmExportService} from "../../rm-export.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-rm-purchase-stock-equation',
  templateUrl: './rm-purchase-stock-equation.component.html',
  styleUrls: ['./rm-purchase-stock-equation.component.css']
})
export class RmPurchaseStockEquationComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  monthNamesList?: any = [];
  stockEquationDataList?: any = [];
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;
  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private rmService: RmServiceService, private pmService: PmServiceService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getRmPurchaseStockEquationData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((stockEquationDataResponse: any) => {
        this.stockEquationDataList = stockEquationDataResponse;
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      })
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  exportAsXLSX() {
    this.rmExportService.getRmPurchaseStockEquation().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmPurchaseStockEquation + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
