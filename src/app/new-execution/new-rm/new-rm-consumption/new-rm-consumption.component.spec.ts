import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRmConsumptionComponent } from './new-rm-consumption.component';

describe('NewRmConsumptionComponent', () => {
  let component: NewRmConsumptionComponent;
  let fixture: ComponentFixture<NewRmConsumptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewRmConsumptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRmConsumptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
