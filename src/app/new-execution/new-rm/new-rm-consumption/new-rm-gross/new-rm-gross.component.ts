import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {RmServiceService} from "../../rm-service.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import * as XLSX from "xlsx";
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {switchMap} from "rxjs/operators";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-new-rm-gross',
  templateUrl: './new-rm-gross.component.html',
  styleUrls: ['./new-rm-gross.component.css']
})
export class NewRmGrossComponent implements OnInit, OnDestroy {
  $subs:Subscription;
  rmDataList: any = [];
  monthNamesList?:any= [];
  rmSearchTerm:any;
  successMessage = '';
  errorMessage = '';
  isExcelDisable=false;
  constructor(private router: Router, private rmService: RmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) { }

  ngOnInit(): void {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getGrossConsumptionRmData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((grossConsumptionRmResponse: any) => {
        if (grossConsumptionRmResponse !== null &&  grossConsumptionRmResponse !== undefined) {
          this.rmDataList = grossConsumptionRmResponse?.itemCodes;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  exportAsXLSX(){
    this.rmExportService.getRmGrossExcelFile().subscribe((excelFileResponse:any)=>{
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse],  {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmGrossConsumption + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
