import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {RmServiceService} from "../../rm-service.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-new-rm-logical',
  templateUrl: './new-rm-logical.component.html',
  styleUrls: ['./new-rm-logical.component.css']
})
export class NewRmLogicalComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  nodes: any =[];
  logicalConsumptionSearchTerm:any;
  bulkCodeSearchTerm:any;
  logicalConsumptionDataList: any = [];
  fgCodeList: any = [];
  monthNamesList?:any = [];
  selectedBulkCode: any;
  mpsByBulkDataList:any = [];
  mpsByFgCode:any;
  mpsByFgIdealValue:any;
  mpsByFgTotal:any;
  bulkCodeList: any = [];
  isExcelDisable=false;
  successMessage = '';
  errorMessage = '';
  rmLogicalList =[];
  constructor(private router: Router, private rmService: RmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) {
  }
  ngOnInit(): void {
    this.$subs= this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getRmLogicalData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((rmLogicalResponse: any) => {
        if (rmLogicalResponse !== null && rmLogicalResponse !== undefined) {
          this.rmLogicalList = rmLogicalResponse;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })
  }

  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  getFgDetails(row:any){
    this.selectedBulkCode= row.parentId;
    this.rmService.getGCByBulk(this.selectedBulkCode).subscribe((mpsByBulkResponse: any) => {
      if (mpsByBulkResponse !== null && mpsByBulkResponse !== undefined) {
        this.mpsByBulkDataList= mpsByBulkResponse;
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });


  }

  displayFgRelationGraph()
  {
    this.rmService.getFgRelationByRmData(this.selectedBulkCode).subscribe((fgRelationResponse: any) => {
      this.nodes = [];
      if(fgRelationResponse!==null && fgRelationResponse!==undefined) {
        this.nodes.push({name: fgRelationResponse.name, childs: fgRelationResponse.childs});
      }
    }, (error) => {
      console.log(error);
    });
  }
  test(ck:any){
  }

  exportAsXLSX(){
    this.rmExportService.getRmLogicalExcelFile().subscribe((excelFileResponse:any)=>{
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse],  {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmLogicalConsumption + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
