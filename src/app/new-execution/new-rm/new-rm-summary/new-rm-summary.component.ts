import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {RmExportService} from "../rm-export.service";
import {PmServiceService} from "../../new-pm/pm-service.service";
import {RmServiceService} from "../rm-service.service";
import {switchMap} from "rxjs/operators";
import {saveAs} from "file-saver";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";

@Component({
  selector: 'app-new-rm-summary',
  templateUrl: './new-rm-summary.component.html',
  styleUrls: ['./new-rm-summary.component.css']
})
export class NewRmSummaryComponent implements OnInit {
  summaryDataList: any = [];
  slobSummary: any = [];
  coverDaysGraph: any = [];
  slowItemPercentage: any;
  obsoleteItemPercentage: any;
  stockQualityPercentage: any;
  totalCoverDays: any;
  totalDaysInMonth = 365;
  monthNamesList?: any = [];
  coverDaysList: any = [];
  successMessage = '';
  errorMessage = '';


  constructor(private spinner: NgxSpinnerService,
              private rmExportService: RmExportService,
              private pmService: PmServiceService, private rmService: RmServiceService,
              private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getRmSummaryData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((summaryResponse: any) => {
          if (summaryResponse !== null && summaryResponse !== undefined) {
            this.summaryDataList = summaryResponse?.slobSummaryFormulaDetailsDTO;
            this.slowItemPercentage = summaryResponse?.slobSummaryFormulaDetailsDTO?.totalSlowItemValueDTO?.slowItemPercentage;
            this.obsoleteItemPercentage = summaryResponse?.slobSummaryFormulaDetailsDTO?.totalObsoleteItemValueDTO?.obsoleteItemPercentage;
            this.stockQualityPercentage = summaryResponse?.slobSummaryFormulaDetailsDTO?.totalStockQualityValueDTO?.stockQualityPercentage;

          }
          return this.rmService.getRmSummaryCoverDays(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((responseOfCoverDays: any) => {
        if (responseOfCoverDays !== null && responseOfCoverDays !== undefined) {
          this.coverDaysList = responseOfCoverDays;
          this.totalCoverDays = responseOfCoverDays.totalCoverDays;
          this.totalDaysInMonth = responseOfCoverDays.totalDaysOfMonths;
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
      this.rmService.getRmSummaryData(this.executionFlowPageService.selectedHeaderId).subscribe((res: any) => {
        console.log(res);
      })
    })
  }


  exportAsXLSX() {
    this.successMessage = 'Downloading...';
    this.rmExportService.getRmSummaryExcelFile().subscribe((excelFileResponse: any) => {
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, 'RM Summary.xlsx');
      this.successMessage = 'RM Summary Downloaded successfully';
      setTimeout( () => {
        this.successMessage = '';
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
    });
  }

}
