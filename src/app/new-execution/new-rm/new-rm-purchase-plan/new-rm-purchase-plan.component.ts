import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormArray, FormControl, FormGroup} from "@angular/forms";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {RmServiceService} from "../rm-service.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {saveAs} from "file-saver";
import {RmExportService} from "../rm-export.service";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-new-rm-purchase-plan',
  templateUrl: './new-rm-purchase-plan.component.html',
  styleUrls: ['./new-rm-purchase-plan.component.css']
})
export class NewRmPurchasePlanComponent implements OnInit, OnDestroy {
  $subs:Subscription;
  selectedId: any;
  supplierDetailsId: any;
  purchasePlanCalculationDataList: any = [];
  rmSupplyTableSearchTerm: any;
  monthNamesList?: any = [];
  supplyDataList: Record<string, any>[] = [];
  supplyDataForm: AbstractControl = new FormGroup({
    supplyDataFormArray: new FormArray([])
  });
  latestPurchasePlanListToSave: Record<string, any>[] = [];
  changedRowCodeList: string[] = [];
  successMessage = '';
  errorMessage = '';
  highlightEditValue = {
    editSelection: false
  };
  isEdit = false;
  dataChangedRowIndexList = [];
  isExcelDisable = false;

  constructor(private rmService: RmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private spinner: NgxSpinnerService, private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
      this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getRmSupplyData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((rmSupplyResponse: any) => {
        if (rmSupplyResponse !== null && rmSupplyResponse !== undefined) {
          this.supplyDataList = rmSupplyResponse.itemCodes;
          this.setSupplyFormData();
          if (this.supplyDataList.length > 0) {
            this.purchasePlanCalculation(this.supplyDataList[0]);
          }
          if (this.successMessage === 'Plan updating...') {
            this.successMessage = 'Plan updated Successfully';
            setTimeout(() => {
              this.successMessage = '';
            }, 2000);
          }
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })

  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  setSupplyFormData() {
    this.supplyDataList.forEach((itemCodes: any) => {
      const rowView = new FormGroup({
        code: new FormControl(itemCodes?.materialCode),
        supplierCode: new FormControl(itemCodes?.supplierCode),
        supplier: new FormControl(itemCodes?.supplierName),
        description: new FormControl(itemCodes?.materialDescription),
        category: new FormControl(itemCodes?.category),
        orderType: new FormControl(itemCodes?.orderType),
        packSize: new FormControl(itemCodes?.packSize),
        moqValue: new FormControl(itemCodes?.moqValue),
        m1: new FormControl(itemCodes?.supplyMonthValues?.month1?.value),
        originalM1: new FormControl(itemCodes?.supplyMonthValues?.month1?.value),
        m2: new FormControl(itemCodes?.supplyMonthValues?.month2?.value),
        originalM2: new FormControl(itemCodes?.supplyMonthValues?.month2?.value),
        m3: new FormControl(itemCodes?.supplyMonthValues?.month3?.value),
        originalM3: new FormControl(itemCodes?.supplyMonthValues?.month3?.value),
        m4: new FormControl(itemCodes?.supplyMonthValues?.month4?.value),
        originalM4: new FormControl(itemCodes?.supplyMonthValues?.month4?.value),
        m5: new FormControl(itemCodes?.supplyMonthValues?.month5?.value),
        originalM5: new FormControl(itemCodes?.supplyMonthValues?.month5?.value),
        m6: new FormControl(itemCodes?.supplyMonthValues?.month6?.value),
        originalM6: new FormControl(itemCodes?.supplyMonthValues?.month6?.value),
        m7: new FormControl(itemCodes?.supplyMonthValues?.month7?.value),
        originalM7: new FormControl(itemCodes?.supplyMonthValues?.month7?.value),
        m8: new FormControl(itemCodes?.supplyMonthValues?.month8?.value),
        originalM8: new FormControl(itemCodes?.supplyMonthValues?.month8?.value),
        m9: new FormControl(itemCodes?.supplyMonthValues?.month9?.value),
        originalM9: new FormControl(itemCodes?.supplyMonthValues?.month9?.value),
        m10: new FormControl(itemCodes?.supplyMonthValues?.month10?.value),
        originalM10: new FormControl(itemCodes?.supplyMonthValues?.month10?.value),
        m11: new FormControl(itemCodes?.supplyMonthValues?.month11?.value),
        originalM11: new FormControl(itemCodes?.supplyMonthValues?.month11?.value),
        m12: new FormControl(itemCodes?.supplyMonthValues?.month12?.value),
        originalM12: new FormControl(itemCodes?.supplyMonthValues?.month12?.value),
        halalComments: new FormControl(itemCodes?.halalComments),
        OtherComments: new FormControl(itemCodes?.OtherComments),
        poRemark: new FormControl(itemCodes?.poRemark),
        originalPoRemark: new FormControl(itemCodes?.poRemark),
        totalValue: new FormControl(itemCodes?.totalValue),
        originalTotalValue: new FormControl(itemCodes?.totalValue)
      });
      (<FormArray>this.supplyDataForm.get('supplyDataFormArray')).push(rowView);
    });
  }

  public purchasePlanCalculation(row: any) {
    this.selectedId = row?.materialId;
    this.supplierDetailsId = row?.rscIitRMItemWiseSupplierDetailsId;
    this.rmService.getRmPurchasePlanCalculationData(this.selectedId, this.supplierDetailsId).subscribe((calculationResponse: any) => {
      if (calculationResponse !== null && calculationResponse !== undefined) {
        this.purchasePlanCalculationDataList = calculationResponse;
      }
    }, (error) => {
      console.log(error);
    })
  }

  exportAsXLSX() {
    this.rmExportService.getRmPurchaseExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmPurchasePlan + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

  calculateTotalValue(row: any, columnName: string, columnValue: string, rowIndex?: number) {
    this.spinner.show();
    const currentRowIndex: number = this.supplyDataList.findIndex(value => value.materialCode === row.code.value);
    if ((this.dataChangedRowIndexList.findIndex(value => value === rowIndex)) === -1) {
      this.dataChangedRowIndexList.push(rowIndex);
    }
    if (columnName.slice(0, 5) == 'month') {
      this.supplyDataList[currentRowIndex].supplyMonthValues[columnName.toString()].value = columnValue;
      this.supplyDataList[currentRowIndex].totalValue = this.calculateTotalOfMonths(this.supplyDataList[currentRowIndex].supplyMonthValues);
      row.totalValue.value = this.supplyDataList[currentRowIndex].totalValue;
    } else if (columnName == 'remark') {
      this.supplyDataList[currentRowIndex].poRemark = columnValue;
    }

    this.setChangedRowCodeList(row?.code?.value, currentRowIndex);
    this.spinner.hide();
  }

  editBtn() {
    this.isEdit = true;
    this.highlightEditValue.editSelection = true;
  }

  closeBtn() {
   this.isEdit = false;
    this.highlightEditValue.editSelection = false;
    this.latestPurchasePlanListToSave = [];
    this.changedRowCodeList = [];
    this.setSupplyFormData();
    this.dataChangedRowIndexList.forEach((index: any) => {
      this.setSupplyDataFormValues(index);
      this.setSupplyDataListValues(index);
    });
  }

  setSupplyDataFormValues(index: number) {
    for (let i = 1; i <= 12; i++) {
      const month = "m" + i;
      const originalMonth = "originalM" + i;
      this.supplyDataForm.get('supplyDataFormArray')['controls'][index].controls[month].value = this.supplyDataForm.value.supplyDataFormArray[index][originalMonth];
      this.supplyDataForm.get('supplyDataFormArray')['controls'][index].controls.totalValue.value = this.supplyDataForm.value.supplyDataFormArray[index].originalTotalValue;
    }
    this.supplyDataForm.get('supplyDataFormArray')['controls'][index].controls.poRemark.value = this.supplyDataForm.value.supplyDataFormArray[index].originalPoRemark;
  }

  setSupplyDataListValues(index) {
    for (let i = 1; i <= 12; i++) {
      const month = "month" + i;
      const originalMonth = "originalM" + i;
      this.supplyDataList[index].supplyMonthValues[month.toString()].value = this.supplyDataForm.value.supplyDataFormArray[index][originalMonth];
      this.supplyDataList[index].totalValue = this.supplyDataForm.value.supplyDataFormArray[index].originalTotalValue;
    }
    this.supplyDataList[index].poRemark = this.supplyDataForm.value.supplyDataFormArray[index].originalPoRemark;
  }

  saveChanges() {
    this.spinner.show();
    this.successMessage = 'Plan updating...';
   /* this.rmService.updateRMLatestPurchasePlan(this.latestPurchasePlanListToSave).subscribe((updatedSupplierListResponse: any) => {
      this.successMessage = 'Plan updated Successfully';
      this.isEdit = false;
      this.highlightEditValue.editSelection = false;
      setTimeout(() => {
        this.successMessage = '';
      }, 2000);
      this.spinner.hide();
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
      this.spinner.hide();
    })*/
    this.rmService.updateRMLatestPurchasePlan(this.latestPurchasePlanListToSave).pipe(
      switchMap((updatedSupplierListResponse: any) => {
        return this.rmService.rmPurchasePlanExcelExport();
      })).subscribe((res: any) => {
      this.isEdit = false;
      this.latestPurchasePlanListToSave = [];
      this.changedRowCodeList = [];
      this.supplyDataList = [];
      this.dataChangedRowIndexList=[];
      this.supplyDataForm.value.supplyDataFormArray=[];
      this.highlightEditValue.editSelection = false;
      this.getData();
    }, (error) => {
      this.errorMessage = 'Something Went Wrong';
      this.spinner.hide();
    });
  }

  calculateTotalOfMonths(monthValues: any) {
    let total;
    total = (+monthValues?.month1?.value) + (+monthValues?.month2?.value) + (+monthValues?.month3?.value) + (+monthValues?.month4?.value) + (+monthValues?.month5?.value) + (+monthValues?.month6?.value) +
      (+monthValues?.month7?.value) + (+monthValues?.month8?.value) + (+monthValues?.month9?.value) + (+monthValues?.month10?.value) + (+monthValues?.month11?.value) + (+monthValues?.month12?.value);
    return total;
  }

  setChangedRowCodeList(materialCode: string, currentRowIndex: number) {
    const isPresent = this.changedRowCodeList.findIndex((value: any) => value === materialCode);
    if (isPresent === -1) {
      this.changedRowCodeList.push(materialCode);
      this.latestPurchasePlanListToSave.push(this.supplyDataList[currentRowIndex]);
    }
  }
}
