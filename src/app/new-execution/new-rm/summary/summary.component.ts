import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from "ngx-spinner";
import {PmServiceService} from "../../new-pm/pm-service.service";
import {switchMap} from "rxjs/operators";
import {Chart} from "chart.js";
import {saveAs} from "file-saver";
import {RmServiceService} from "../rm-service.service";
import {RmExportService} from "../rm-export.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {Router} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  rmDataList: any = [];
  monthNamesList?:any= [];
  rmSearchTerm:any;
  successMessage = '';
  errorMessage = '';
  filterValues = {};
  dataSource!: MatTableDataSource<any>;
  displayedColumns: string[] = ['materialCode','materialDescription','month1','month2',
    'month3','month4','month5','month6','month7','month8','month9','month10','month11','month12','total'];
  filterSelectObj = [];
  constructor(private router: Router, private rmService: RmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) {

    this.filterSelectObj = [
      {
        name: 'Code',
        columnProp: 'materialCode',
        options: []
      }, {
        name: 'Description',
        columnProp: 'materialDescription',
        options: []
      }, {
        name: 'month1',
        columnProp: 'month1',
        options: []
      }, {
        name: 'month2',
        columnProp: 'month2',
        options: []
      }, {
        name: 'month3',
        columnProp: 'month3',
        options: []
      }
    ]
  }

  ngOnInit(): void {
    this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }
      this.spinner.show();
      this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getGrossConsumptionRmData(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((grossConsumptionRmResponse: any) => {
        /*if (grossConsumptionRmResponse !== null &&  grossConsumptionRmResponse !== undefined) {
          this.rmDataList = grossConsumptionRmResponse?.itemCodes;
        }*/
        this.rmDataList = grossConsumptionRmResponse;
        if (grossConsumptionRmResponse !== null &&  grossConsumptionRmResponse !== undefined) {
          const rscDtLinesView :any = [];
          grossConsumptionRmResponse?.itemCodes.forEach((response :any) => {
              {
                rscDtLinesView.push({
                  materialCode: response?.materialCode,
                  materialDescription: response?.materialDescription,
                  month1: response?.grossConsumptionMonthValues?.month1?.value,
                  month2: response?.grossConsumptionMonthValues?.month2?.value,
                  month3: response?.grossConsumptionMonthValues?.month3?.value,
                  month4: response?.grossConsumptionMonthValues?.month4?.value,
                  month5: response?.grossConsumptionMonthValues?.month5?.value,
                  month6: response?.grossConsumptionMonthValues?.month6?.value,
                  month7: response?.grossConsumptionMonthValues?.month7?.value,
                  month8: response?.grossConsumptionMonthValues?.month8?.value,
                  month9: response?.grossConsumptionMonthValues?.month9?.value,
                  month10: response?.grossConsumptionMonthValues?.month10?.value,
                  month11: response?.grossConsumptionMonthValues?.month11?.value,
                  month12: response?.grossConsumptionMonthValues?.month12?.value,
                  total: response?.totalValue,

                });
                this.dataSource = new MatTableDataSource(rscDtLinesView);

                this.filterSelectObj.filter((o) => {
                  o.options = this.getFilterObject(rscDtLinesView, o.columnProp);
                });
                this.dataSource.filterPredicate = this.createFilter();
              }

            }
          );
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })


  }
  // Called on Filter change
  filterChange(filter, event) {
    //let filterValues = {}
    this.filterValues[filter.columnProp] = event.target.value.trim().toLowerCase()
    this.dataSource.filter = JSON.stringify(this.filterValues)
  }

  // Custom filter method fot Angular Material Datatable
  createFilter() {
    let filterFunction = function (data: any, filter: string): boolean {
      let searchTerms = JSON.parse(filter);
      let isFilterSet = false;
      for (const col in searchTerms) {
        if (searchTerms[col].toString() !== '') {
          isFilterSet = true;
        } else {
          delete searchTerms[col];
        }
      }

      console.log(searchTerms);

      let nameSearch = () => {
        let found = false;
        if (isFilterSet) {
          for (const col in searchTerms) {
            searchTerms[col].trim().toLowerCase().split(' ').forEach(word => {
              if (data[col].toString().toLowerCase().indexOf(word) != -1 && isFilterSet) {
                found = true
              }
            });
          }
          return found
        } else {
          return true;
        }
      }
      return nameSearch()
    }
    return filterFunction
  }
  getFilterObject(fullObj, key) {
    const uniqChk = [];
    fullObj.filter((obj) => {
      if (!uniqChk.includes(obj[key])) {
        uniqChk.push(obj[key]);
      }
      return obj;
    });
    return uniqChk;
  }
  exportAsXLSX(){
    this.successMessage = 'Downloading...';
    this.rmExportService.getRmGrossExcelFile().subscribe((excelFileResponse:any)=>{
      const blob = new Blob([excelFileResponse],  {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmGrossConsumption + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = this.excelExportConstantsService.rmGrossConsumption + ' '+ 'Downloaded successfully';
      setTimeout( () => {
        this.successMessage = '';
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
    });
  }
}
