import { Component, OnInit } from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {PmServiceService} from "../../../new-pm/pm-service.service";
import {Chart} from "chart.js";
import {RmServiceService} from "../../rm-service.service";
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";

@Component({
  selector: 'app-rm-inventory-quality-index-dash',
  templateUrl: './rm-inventory-quality-index-dash.component.html',
  styleUrls: ['./rm-inventory-quality-index-dash.component.css']
})
export class RmInventoryQualityIndexDashComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  isExcelDisable = false;
  lineGraphData = {
    labels: [],
    slobData: [],
    iqData: []
  };
  slobInventoryQualityIndexList:any=[];
  constructor(public executionFlowPageService: ExecutionFlowPageServiceService, private rmExportService: RmExportService, private excelExportConstantsService: ExcelExportConstantsService,
              private ngxSpinnerService: NgxSpinnerService, private rmService: RmServiceService) { }

  ngOnInit(): void {

    /*this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
      if (value1 !== undefined && value1 !== null) {
        this.executionFlowPageService.selectedHeaderId = value1;
      }*/
      this.ngxSpinnerService.show();
      this.lineGraphData = {
        labels: [],
        slobData: [],
        iqData: []
      };
      this.rmService.getRmInventoryQualityDashboardData()
        .subscribe((inventoryQualityResponse: any) => {
          if (inventoryQualityResponse !== null && inventoryQualityResponse !== undefined) {
            console.log(inventoryQualityResponse);
            this.slobInventoryQualityIndexList = inventoryQualityResponse?.slobInventoryQualityIndexDTOList;
            for (let i = 0; i < inventoryQualityResponse?.slobInventoryQualityIndexDTOList.length; i++) {
              this.lineGraphData.slobData[i] = inventoryQualityResponse?.slobInventoryQualityIndexDTOList[i].slobPercentage;
              this.lineGraphData.iqData[i] = inventoryQualityResponse?.slobInventoryQualityIndexDTOList[i].inventoryQualityPercentage;
              this.lineGraphData.labels[i] = inventoryQualityResponse?.slobInventoryQualityIndexDTOList[i].monthValue;
            }
            this.inventoryQualityGraph();
          }

          this.ngxSpinnerService.hide();
        }, (error) => {
          this.ngxSpinnerService.hide();
          console.log(error);
        });

   /* });*/


  }
  window2:any;
  inventoryQualityGraph() {
    const canvas: any = document.getElementById("Inventory_Quality");
    const ctx = canvas.getContext("2d");
    if (this.window2 !== undefined)
      this.window2.destroy();
    this.window2 = new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.lineGraphData.labels,
        datasets: [{
        /*  pointStyle:'rect',
          pointRadius: 7,
          pointHoverRadius: 7,*/
          borderWidth:2,
          data: this.lineGraphData.slobData,
          type: 'line',
          borderColor: 'rgb(123,151,255)',
          label: "SLOB[%]",
          fill: false,
          backgroundColor: 'rgba(123,151,255,1)',


        },
          {
            borderWidth:2,
            data: this.lineGraphData.iqData,
            type: "line",
            borderColor: "#ff8c00",
            backgroundColor: '#ff8c00',
            label: "IQ[%]",
            fill: false,


          },]
      },
      options: {
        animation: {

          onComplete() {
            const chartInstance = this.chart,
              ctx = chartInstance.ctx;
            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
            ctx.textAlign = 'center';
            ctx.textBaseline = 'bottom';
            ctx.fillStyle = this.chart.config.options.defaultFontColor;
            this.data.datasets.forEach((dataset, i) =>{
              const meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach( (line, index)=> {
                const data = dataset.data[index];
                ctx.fillText(data, line._model.x, line._model.y - 7);
              });
            });
          }
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          datalabels: {
            display: false
          }
        },
        scales: {
          xAxes: [{
            offset: true,
            scaleLabel: {
              display: true,
              labelString: 'Month'
            },
            ticks: {
              beginAtZero: true,
              fontSize: 10,
            }
          }],
          yAxes: [{

            scaleLabel: {
              display: true,
              labelString: 'Percentage %'
            },
            ticks: {
              beginAtZero: true,
              fontSize: 10,
              min: 0,
              max: 100,
              stepSize: 20
            }
          },
          ]
        },
        title: {
          display: false,
        },
        legend: {
          display: true,
          labels: {
            fontSize: 12,

          }

        },
      }
    });
  }
  exportAsXLSX() {
    this.rmExportService.getRmInventoryQualityIndexExportExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmInventoryQualityIndex + " "  + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
