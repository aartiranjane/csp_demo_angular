import {Component, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {switchMap} from "rxjs/operators";
import {Chart} from "chart.js";
import {RmServiceService} from "../../rm-service.service";
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";

@Component({
  selector: 'app-rm-slob-movement-dash',
  templateUrl: './rm-slob-movement-dash.component.html',
  styleUrls: ['./rm-slob-movement-dash.component.css']
})
export class RmSlobMovementDashComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  isExcelDisable = false;
  monthNamesList: any = [];
  areaGraphData = {
    labels: [],
    obData: [],
    smData: []
  };
  slobMovementList: any = [];

  constructor(public executionFlowPageService: ExecutionFlowPageServiceService, private rmExportService: RmExportService, private excelExportConstantsService: ExcelExportConstantsService,
              private ngxSpinnerService: NgxSpinnerService, private rmService: RmServiceService) {
  }

  ngOnInit(): void {


    /* this.executionFlowPageService.getPpHeaderId.subscribe(value1 => {
       if (value1 !== undefined && value1 !== null) {
         this.executionFlowPageService.selectedHeaderId = value1;
       }*/
    this.ngxSpinnerService.show();
    this.areaGraphData = {
      labels: [],
      obData: [],
      smData: []
    };
    /*this.executionFlowPageService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
      switchMap((monthNamesDataResponse: any) => {
        if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
          this.monthNamesList = monthNamesDataResponse;
        } return*/
    this.rmService.getRmSlobMovementDashboardData().subscribe((slobMovementResponse: any) => {
      if (slobMovementResponse !== null && slobMovementResponse !== undefined) {
        console.log(slobMovementResponse);
        this.slobMovementList = slobMovementResponse?.slobMovementDTOList;
        for (let i = 0; i < slobMovementResponse?.slobMovementDTOList.length; i++) {
          this.areaGraphData.obData[i] = slobMovementResponse?.slobMovementDTOList[i].obValue;
          this.areaGraphData.smData[i] = slobMovementResponse?.slobMovementDTOList[i].slValue;
          /*  this.areaGraphSlobData.data[i] = slobMovementResponse.slobMovementDTOList[i].slobValue;*/
          this.areaGraphData.labels[i] = slobMovementResponse?.slobMovementDTOList[i].monthValue;
        }
        this.slobMovementGraph();
      }

      this.ngxSpinnerService.hide();
    }, (error) => {
      this.ngxSpinnerService.hide();
      console.log(error);
    });

    /*  });*/
  }

  window1: any;

  slobMovementGraph() {
    const canvas: any = document.getElementById("slob_Movement");
    const ctx = canvas.getContext("2d");
    if (this.window1 !== undefined)
      this.window1.destroy();
    this.window1 = new Chart(ctx, {
      type: 'line',
      data: {
        labels: this.areaGraphData.labels,
        datasets: [
          /* {
             data: this.areaGraphSlobData.data,
             type: 'line',
             label: "SLOB",
             borderColor: 'rgb(123,151,255)',
             backgroundColor: 'rgba(123,151,255,0.5)',
             fill: '2',

           },*/
          {
            data: this.areaGraphData.obData,
            type: 'line',
            label: "OB",
            borderColor: 'rgb(123,151,255)',
            backgroundColor: 'rgba(123,151,255,0.5)',
            fill: true,

          },
          {
            data: this.areaGraphData.smData,
            type: "line",
            label: "SM",
            borderColor: 'rgb(255.0, 140.0, 0.0)',
            backgroundColor: 'rgba(255.0, 140.0, 0.0, 0.5)',
            fill: 0,

          },]
      },
      options: {
        elements: {
          line: {
            tension: 0
          }
        },
        responsive: true,
        maintainAspectRatio: true,
        plugins: {
          datalabels: {
            display: false
          }
        },
        scales: {
          xAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Month'
            },
            ticks: {
              beginAtZero: true,
              fontSize: 10
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Values'
            },
            ticks: {
              beginAtZero: true,
              fontSize: 10
            }
          },
          ]
        },
        title: {
          display: false,
        },
        legend: {
          display: true,
          labels: {
            fontSize: 12,

          }

        },
      }
    });
  }
  exportAsXLSX() {
    this.rmExportService.getRmSlobMovementExportExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmSlobMovement + " " + '.xlsx');
      this.successMessage = 'Download Success!';
     setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
}
