import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rm-planning-horizon',
  templateUrl: './rm-planning-horizon.component.html',
  styleUrls: ['./rm-planning-horizon.component.css']
})
export class RmPlanningHorizonComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('rmOtifTab').click();
  }


}
