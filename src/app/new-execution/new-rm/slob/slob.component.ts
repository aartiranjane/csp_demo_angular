import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {switchMap} from "rxjs/operators";
import {saveAs} from "file-saver";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {RmServiceService} from "../rm-service.service";
import {RmExportService} from "../rm-export.service";
import {PmServiceService} from "../../new-pm/pm-service.service";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {AuthService} from "../../../auth.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-slob',
  templateUrl: './slob.component.html',
  styleUrls: ['./slob.component.css']
})
export class SlobComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  isEdit = false;
  isExcelDisable = false;
  monthNamesList?: any = [];
  slobDataList: any = [];
  nullSlobDataList: any = [];
  monthValuesList: any = [];
  slobTotalData: any = [];
  supplyTableSearchTerm: any;
  selectedId: any;
  successMessage = '';
  successMessageUpdate = '';
  errorMessage = '';
  successSLOBSummaryMessage = '';
  errorSLOBSummaryMessage = '';
  changedRowCodeList: string[] = [];
  slobReasonsDataList: any = [];
  dataChangedRowIndexList = [];
  highlightEditValue = {
    editSelection: false
  };
  highlightMapEditValue = {
    editSelection: false
  };
  sitValue;
  sit?: number;
  rmNullMapSlobListToSave: Record<string, any>[] = [];
  rmSlobListToSave: Record<string, any>[] = [];
  slobDataForm = new FormGroup({
    slobDataFormArray: new FormArray([])
  });
  mapDataChangedRowIndexList = [];
  nullMapSlobDataForm = new FormGroup({
    nullMapSlobDataFormArray: new FormArray([])
  });
  overAllDeviationList: any = [];
  @ViewChild('table', {read: ElementRef}) public table: ElementRef<any>;

  constructor(private router: Router, private authService: AuthService,
              private pmService: PmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private rmService: RmServiceService,
              private executionFlowPageService: ExecutionFlowPageServiceService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.getData();

  }

  getData() {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {

      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }

       this.spinner.show();
      this.rmService.getRmSlob(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((slobResponse: any) => {
          if (slobResponse !== null && slobResponse !== undefined) {
            this.slobTotalData = slobResponse;
            this.slobDataList = slobResponse.rmSlobs;
            console.log(slobResponse);
            this.sit = slobResponse?.sitValue;
            this.sitValue = new FormControl(slobResponse?.sitValue);
            this.setSlobFormData();
            this.totalStockMonths(this.slobDataList[0]);

          }
          return this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((monthNamesDataResponse: any) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
              this.monthNamesList = monthNamesDataResponse;
              console.log(this.monthNamesList);
            }
          }
          return this.rmService.getRmSlobNullMapPriceData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((nullMapSlobResponse: any) => {
          if (nullMapSlobResponse !== null && nullMapSlobResponse !== undefined) {
            this.nullSlobDataList = nullMapSlobResponse.rmSlobs;
            if (this.nullSlobDataList.length > 0) {
              this.setNullMapSlobFormData();
              this.highlightMapEditValue.editSelection = true;
              this.openMapModal();
            }
          }
          return this.rmService.getRmSlobSummaryOverAllDeviationData();
        })).subscribe((overAllDeviationResponse: any) => {
        if (overAllDeviationResponse !== null && overAllDeviationResponse !== undefined) {
          this.overAllDeviationList = overAllDeviationResponse;
          console.log(this.overAllDeviationList)
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });

    })
  }

  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  openMapModal() {
    document.getElementById("openMapModal").click();
    this.authService.showWarning.subscribe((res: any) => {
    if (res === 1) {
      document.getElementById('closemodelbuttonmap').click();
      }
    });

  }

  setSlobFormData() {
    this.slobDataList.forEach((rmSlobs: any) => {
      const rowView = new FormGroup({
        code: new FormControl(rmSlobs?.code),
        description: new FormControl(rmSlobs?.description),
        midNightStock: new FormControl(rmSlobs?.midNightStock),
        expiredStock: new FormControl(rmSlobs?.expiredStock),
        finalStock: new FormControl(rmSlobs?.finalStock),
        month1Value: new FormControl(rmSlobs?.monthValuesDTO?.month1Value),
        month2Value: new FormControl(rmSlobs?.monthValuesDTO?.month2Value),
        month3Value: new FormControl(rmSlobs?.monthValuesDTO?.month3Value),
        month4Value: new FormControl(rmSlobs?.monthValuesDTO?.month4Value),
        month5Value: new FormControl(rmSlobs?.monthValuesDTO?.month5Value),
        month6Value: new FormControl(rmSlobs?.monthValuesDTO?.month6Value),
        month7Value: new FormControl(rmSlobs?.monthValuesDTO?.month7Value),
        month8Value: new FormControl(rmSlobs?.monthValuesDTO?.month8Value),
        month9Value: new FormControl(rmSlobs?.monthValuesDTO?.month9Value),
        month10Value: new FormControl(rmSlobs?.monthValuesDTO?.month10Value),
        month11Value: new FormControl(rmSlobs?.monthValuesDTO?.month11Value),
        month12Value: new FormControl(rmSlobs?.monthValuesDTO?.month12Value),
        totalConsumptionQuantity: new FormControl(rmSlobs?.totalConsumptionQuantity),
        balanceStock: new FormControl(rmSlobs?.balanceStock),
        map: new FormControl(rmSlobs?.map),
        stdPrice: new FormControl(rmSlobs?.stdPrice),
        slobQuantity: new FormControl(rmSlobs?.slobQuantity),
        rate: new FormControl(rmSlobs?.price),
        totalStockValue: new FormControl(rmSlobs?.totalStockValue),
        slobType: new FormControl(rmSlobs?.slobType),
        slobValue: new FormControl(rmSlobs?.slobValue),
        previousMonthsSlobValue: new FormControl(rmSlobs?.previousMonthsSlobValue),
        slobIncrease: new FormControl(rmSlobs?.slobValue - rmSlobs?.previousMonthsSlobValue),
        obsoleteExpiryValue: new FormControl(rmSlobs?.obsoleteExpiryValue),
        previousMonthsObsoleteExpiryValue: new FormControl(rmSlobs?.previousMonthsObsoleteExpiryValue),
        obsoleteIncrease: new FormControl(rmSlobs?.obsoleteExpiryValue - rmSlobs?.previousMonthsObsoleteExpiryValue),
        obsoleteNonExpiryValue: new FormControl(rmSlobs?.obsoleteNonExpiryValue),
        previousMonthsObsoleteNonExpiryValue: new FormControl(rmSlobs?.previousMonthsObsoleteNonExpiryValue),
        obsoleteNonExpiryIncrease: new FormControl(rmSlobs?.obsoleteNonExpiryValue - rmSlobs?.previousMonthsObsoleteNonExpiryValue),
        slowMovingValue: new FormControl(rmSlobs?.slowMovingValue),
        previousMonthsSlowMovingValue: new FormControl(rmSlobs?.previousMonthsSlowMovingValue),
        slowMovingIncrease: new FormControl(rmSlobs?.slowMovingValue - rmSlobs?.previousMonthsSlowMovingValue),
        totalSlobValue: new FormControl(rmSlobs?.totalSlobValue),
        reasonId: new FormControl(rmSlobs?.reasonId),
        originalReasonId: new FormControl(rmSlobs?.reasonId),
        reason: new FormControl(rmSlobs?.reason),
        originalReason: new FormControl(rmSlobs?.reason),
        remark: new FormControl(rmSlobs?.remark),
        originalRemark: new FormControl(rmSlobs?.remark),
      });
      (<FormArray>this.slobDataForm.get('slobDataFormArray')).push(rowView);
    });
  }

  setNullMapSlobFormData() {
    if (((this.nullMapSlobDataForm.get('nullMapSlobDataFormArray').value).length) === 0) {
      this.nullSlobDataList.forEach((rmSlobs: any) => {
        const rowView = new FormGroup({
          code: new FormControl(rmSlobs?.code),
          description: new FormControl(rmSlobs?.description),
          midNightStock: new FormControl(rmSlobs?.midNightStock),
          expiredStock: new FormControl(rmSlobs?.expiredStock),
          finalStock: new FormControl(rmSlobs?.finalStock),
          month1Value: new FormControl(rmSlobs?.monthValuesDTO?.month1Value),
          month2Value: new FormControl(rmSlobs?.monthValuesDTO?.month2Value),
          month3Value: new FormControl(rmSlobs?.monthValuesDTO?.month3Value),
          month4Value: new FormControl(rmSlobs?.monthValuesDTO?.month4Value),
          month5Value: new FormControl(rmSlobs?.monthValuesDTO?.month5Value),
          month6Value: new FormControl(rmSlobs?.monthValuesDTO?.month6Value),
          month7Value: new FormControl(rmSlobs?.monthValuesDTO?.month7Value),
          month8Value: new FormControl(rmSlobs?.monthValuesDTO?.month8Value),
          month9Value: new FormControl(rmSlobs?.monthValuesDTO?.month9Value),
          month10Value: new FormControl(rmSlobs?.monthValuesDTO?.month10Value),
          month11Value: new FormControl(rmSlobs?.monthValuesDTO?.month11Value),
          month12Value: new FormControl(rmSlobs?.monthValuesDTO?.month12Value),
          totalConsumptionQuantity: new FormControl(rmSlobs?.totalConsumptionQuantity),
          balanceStock: new FormControl(rmSlobs?.balanceStock),
          map: new FormControl(rmSlobs?.map, [Validators.required]),
          stdPrice: new FormControl(rmSlobs?.stdPrice),
          slobQuantity: new FormControl(rmSlobs?.slobQuantity),
          rate: new FormControl(rmSlobs?.price),
          totalStockValue: new FormControl(rmSlobs?.totalStockValue),
          slobType: new FormControl(rmSlobs?.slobType),
          slobValue: new FormControl(rmSlobs?.slobValue),
          previousMonthsSlobValue: new FormControl(rmSlobs?.previousMonthsSlobValue),
          slobIncrease: new FormControl(rmSlobs?.slobValue - rmSlobs?.previousMonthsSlobValue),
          obsoleteExpiryValue: new FormControl(rmSlobs?.obsoleteExpiryValue),
          previousMonthsObsoleteExpiryValue: new FormControl(rmSlobs?.previousMonthsObsoleteExpiryValue),
          obsoleteIncrease: new FormControl(rmSlobs?.obsoleteExpiryValue - rmSlobs?.previousMonthsObsoleteExpiryValue),
          obsoleteNonExpiryValue: new FormControl(rmSlobs?.obsoleteNonExpiryValue),
          previousMonthsObsoleteNonExpiryValue: new FormControl(rmSlobs?.previousMonthsObsoleteNonExpiryValue),
          obsoleteNonExpiryIncrease: new FormControl(rmSlobs?.obsoleteNonExpiryValue - rmSlobs?.previousMonthsObsoleteNonExpiryValue),
          slowMovingValue: new FormControl(rmSlobs?.slowMovingValue),
          previousMonthsSlowMovingValue: new FormControl(rmSlobs?.previousMonthsSlowMovingValue),
          slowMovingIncrease: new FormControl(rmSlobs?.slowMovingValue - rmSlobs?.previousMonthsSlowMovingValue),
          totalSlobValue: new FormControl(rmSlobs?.totalSlobValue),
          reasonId: new FormControl(rmSlobs?.reasonId),
          reason: new FormControl(rmSlobs?.reason),
          remark: new FormControl(rmSlobs?.remark),
        });
        (<FormArray>this.nullMapSlobDataForm.get('nullMapSlobDataFormArray')).push(rowView);
      });
    }

  }

  totalStockMonths(row: any) {
    this.selectedId = row.rscMtItemId;
    this.spinner.show();
    this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
      switchMap((monthNamesDataResponse: any) => {
        if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
          this.monthNamesList = monthNamesDataResponse;
        }
        return this.rmService.getRmSlobTotalStockData(this.selectedId, this.executionFlowPageService.selectedHeaderId);
      })).subscribe((rmSlobTotalStockResponse: any) => {
      console.log(rmSlobTotalStockResponse);
      if (rmSlobTotalStockResponse !== null && rmSlobTotalStockResponse !== undefined) {
        this.monthValuesList = rmSlobTotalStockResponse;
      }
      /*this.showLoder = false;
      this.authService.idle.watch();
      if( this.showLoder ===  false && this.loderMessage=== true)
      {console.log("totalStockMonths"+this.percent);
        this.no = 0;
        this.excelNo=0;
        this.successMessage = 'Slob updated Successfully';
        setTimeout(() => {
          this.successMessage = '';
        }, 3000);
        this.loderMessage = false}*/
      this.authService.idle.watch();
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  }

  updateReason(row: any, name: string, changedValue: string, rowIndex?: number) {
    const newReasonId = changedValue.split(":");
    let returnobj;
    const newReasonId1 = newReasonId[1].trim();
    this.slobReasonsDataList.forEach(obj => {
      if ((obj.id).toString() === newReasonId1.toString()) {
        returnobj = obj.reason;
      }
    });
    return returnobj;
  }

  updateSitValueData(changedValue: any) {
    this.sitValue.value = changedValue;
  }

  updateReasonData(row: any, name: string, changedValue: string, rowIndex?: number) {
    this.spinner.show();
    const currentRowIndex: number = this.slobDataList.findIndex(value => value.code === row.code.value);
    if ((this.dataChangedRowIndexList.findIndex(value => value === rowIndex)) === -1) {
      this.dataChangedRowIndexList.push(rowIndex);
    }
    if (name === 'reason') {
      this.slobDataList[currentRowIndex].reason = this.updateReason(row, name, changedValue, rowIndex);

      this.slobDataList[currentRowIndex].reasonId = row.reasonId.value;
    }
    if (name === 'remark') {
      this.slobDataList[currentRowIndex].remark = changedValue;
    }
    this.setChangedRowCodeList(row?.code?.value, currentRowIndex);
    this.spinner.hide();
  }

  UpdateMap(row: any, columnName: string, mapValue: string, rowIndex?: number) {
    console.log(row)
    this.spinner.show();
    const currentRowIndex: number = this.nullSlobDataList.findIndex(value => value.code === row.code.value);
    if ((this.mapDataChangedRowIndexList.findIndex(value => value === rowIndex)) === -1) {
      this.mapDataChangedRowIndexList.push(rowIndex);
    }
    this.nullSlobDataList[currentRowIndex].map = mapValue;
    this.setChangedRowCodeMapList(row?.code?.value, currentRowIndex);
    this.spinner.hide();
  }

  setChangedRowCodeMapList(materialCode: string, currentRowIndex: number) {
    const isPresent = this.changedRowCodeList.findIndex((value: any) => value === materialCode);
    if (isPresent === -1) {
      this.changedRowCodeList.push(materialCode);
      this.rmNullMapSlobListToSave.push(this.nullSlobDataList[currentRowIndex]);
    }
  }

  setChangedRowCodeList(materialCode: string, currentRowIndex: number) {
    const isPresent = this.changedRowCodeList.findIndex((value: any) => value === materialCode);
    if (isPresent === -1) {
      this.changedRowCodeList.push(materialCode);
      this.rmSlobListToSave.push(this.slobDataList[currentRowIndex]);
    }
  }

  setSlobDataFormValues(index: number) {
    this.slobDataForm.get('slobDataFormArray')['controls'][index].controls.reason.value = this.slobDataForm.value.slobDataFormArray[index].originalReason;
    this.slobDataForm.get('slobDataFormArray')['controls'][index].controls.reasonId.value = this.slobDataForm.value.slobDataFormArray[index].originalReasonId;
    this.slobDataForm.get('slobDataFormArray')['controls'][index].controls.remark.value = this.slobDataForm.value.slobDataFormArray[index].originalRemark;
  }

  setSlobDataListValues(index) {
    this.slobDataList[index].reason = this.slobDataForm.value.slobDataFormArray[index].reason;
    this.slobDataList[index].reasonId = this.slobDataForm.value.slobDataFormArray[index].reasonId;
    this.slobDataList[index].remark = this.slobDataForm.value.slobDataFormArray[index].remark;
  }
  // rmCoverDaysExcelExport
  saveChanges() {
    this.spinner.show();
    //this.showLoder = true;
    //this.loderMessage=true;
    this.authService.idle.stop();
   this.successMessageUpdate = 'Plan updating...';
    setTimeout(() => {
      this.successMessageUpdate = '';
    }, 2000);
    this.rmService.updateRMSlobList(this.rmSlobListToSave, this.sitValue.value).pipe(
      switchMap((updatedPmSlobListResponse: any) => {
      return this.executionFlowPageService.rmCoverDaysAndSlobExcelExport();
    })).subscribe((res: any) => {
      this.isEdit = false;
      this.slobReasonsDataList = [];
      this.highlightEditValue.editSelection = false;
      if (this.sitValue.value !== this.sit) {
        this.slobTotalData = [];
        this.slobDataList = [];
        this.sit = this.sitValue.value;
      } else {
       this.spinner.hide();
       //this.showLoder = false;
       this.authService.idle.watch();
   /*     this.spinner.hide();*/
      }
      this.getData();
      this.rmSlobListToSave = [];
      this.rmNullMapSlobListToSave = [];
      this.slobTotalData = [];
      this.slobDataList = [];
      this.successMessage = 'Slob updated Successfully';
      setTimeout(() => {
        this.successMessage = '';
      }, 5000);
    }, (error) => {
      this.spinner.hide();
      this.authService.idle.watch();
     /* this.spinner.hide();*/
      this.errorMessage = 'Something Went Wrong';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
      console.log(error);
    })
  }

  saveMap() {
    if (this.nullMapSlobDataForm.invalid) {
      return;
    }
   this.spinner.show();
    document.getElementById("openMapModal").click();
    this.authService.showWarning.subscribe((res: any) => {
      if (res === 1) {
        document.getElementById('closemodelbuttonmap').click();
      }
    });
    this.rmService.updateRMSlobsListBasedOnMap(this.rmNullMapSlobListToSave,this.sit).pipe(
      switchMap((updatedPmSlobListResponse: any) => {
        return this.executionFlowPageService.rmCoverDaysAndSlobExcelExport();
      })).subscribe((res: any) => {
      this.successMessageUpdate = 'Slob updating...';
      this.isEdit = false;
      this.highlightEditValue.editSelection = false;
      this.rmNullMapSlobListToSave = [];
      this.nullSlobDataList = [];
      this.slobDataList = [];
      this.getData();

    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
      this.spinner.hide();

    });
  }

  editBtn() {
    this.isEdit = true;
    this.highlightEditValue.editSelection = true;
    this.rmService.getRmSlobReasonsData().subscribe((slobResponse: any) => {
      if (slobResponse !== null && slobResponse !== undefined) {
        this.slobReasonsDataList = slobResponse;
      }
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  }

  closeBtn() {
    this.table.nativeElement.scrollTo({left: (this.table.nativeElement.scrollLeft - 1000), behavior: 'smooth'});
    this.isEdit = false;
    this.highlightEditValue.editSelection = false;
    this.sitValue.setValue(this.sit);
    this.slobReasonsDataList = [];
    this.rmSlobListToSave = [];
    this.dataChangedRowIndexList.forEach((index: any) => {
      this.setSlobDataFormValues(index);
      this.setSlobDataListValues(index);
    });
  }

  exportAsXLSX() {
    this.rmExportService.getRmSlobExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmSlobExcel + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }

  exportAsXLSXForSummary() {
    this.rmExportService.getRmSLOBSummary().subscribe((excelFileResponse: any) => {
      this.isExcelDisable = true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmSLOBSummary + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successSLOBSummaryMessage = 'Download Success!';
      setTimeout(() => {
        this.successSLOBSummaryMessage = '';
        this.isExcelDisable = false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorSLOBSummaryMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorSLOBSummaryMessage = '';
      }, 2000);
    });
  }
}
