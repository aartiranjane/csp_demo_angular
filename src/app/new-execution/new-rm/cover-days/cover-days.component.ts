import {Component, OnDestroy, OnInit} from '@angular/core';
import {switchMap} from "rxjs/operators";
import {NgxSpinnerService} from "ngx-spinner";
import {RmServiceService} from "../rm-service.service";
import {PmServiceService} from "../../new-pm/pm-service.service";
import {saveAs} from "file-saver";
import {RmExportService} from "../rm-export.service";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../../excel-export-constants.service";
import {Subscription} from "rxjs";
@Component({
  selector: 'app-cover-days',
  templateUrl: './cover-days.component.html',
  styleUrls: ['./cover-days.component.css']
})
export class CoverDaysComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  monthNamesList?: any = [];
  totalMonthData?: any = [];
  monthWiseData?: any = [];
  isExcelDisable=false;
  successMessage = '';
  errorMessage = '';
  successmodalMessage='';
  errormodalMessage = '';
  isEdit = false;
  nullStdPriceDataList: any = [];
  highlightEditValue = {
    editSelection: false
  };
  nullStdPriceCoverDaysDataForm = new FormGroup({
    nullStdPriceCoverDaysFormArray: new FormArray([])
  });
  rmCoverDaysListToSave: Record<string, any>[] = [];
  stdPriceDataChangedRowIndexList = [];
  changedRowCodeList: string[] = [];
  totalCoverDays: any;
  totalCoverDaysWithSit:any;
  totalStockValue:any;
  sitValue:any;
  totalStockSitValue:any;
  totalDaysInMonth = 365;
  summaryDataList: any = [];
  coverDaysList: any = [];
  classACoverDaysConsumption:any=[];
  classBCoverDaysConsumption:any=[];
  classCCoverDaysConsumption:any=[];
  totalCoverDaysConsumption:any=[];

  constructor(private pmService: PmServiceService,
              private rmExportService: RmExportService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private spinner: NgxSpinnerService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private rmService: RmServiceService) {
  }


  ngOnInit() {
    this.getData();

  }

  getData() {
    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.pmService.getMonthNamesData(this.executionFlowPageService.selectedHeaderId).pipe(
        switchMap((monthNamesDataResponse: object) => {
          if (monthNamesDataResponse !== null && monthNamesDataResponse !== undefined) {
            this.monthNamesList = monthNamesDataResponse;
          }
          return this.rmService.getRmCoverDaysAllMonthsData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((totalMonthDataResponse: any) => {
          if (totalMonthDataResponse !== null && totalMonthDataResponse !== undefined) {
            this.totalMonthData = totalMonthDataResponse;
          }
          return this.rmService.getRmCoverDaysMonthWiseData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((monthWiseDataResponse: any) => {
          if (monthWiseDataResponse !== null && monthWiseDataResponse !== undefined) {
            this.monthWiseData = monthWiseDataResponse?.rmCoverDaysDTO;
            if (this.successMessage === 'Slob updating...') {
              this.successMessage = 'Slob updated Successfully';
            }
          }
          return this.rmService.getRmCoverDaysNullStdPriceData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((nullStdPriceDataResponse: any) => {
          if (nullStdPriceDataResponse !== null && nullStdPriceDataResponse !== undefined) {
            this.nullStdPriceDataList = nullStdPriceDataResponse.rmCoverDaysDTO;
            if (this.nullStdPriceDataList.length > 0) {
              this.setNullStdPriceCoverDaysFormData();
              this.highlightEditValue.editSelection = true;
              this.openStdPriceModal();
            }
          }
          return this.rmService.getRmSummaryData(this.executionFlowPageService.selectedHeaderId);
        })).pipe(
        switchMap((summaryResponse: any) => {
          if (summaryResponse !== null && summaryResponse !== undefined) {
            this.summaryDataList = summaryResponse?.slobSummaryFormulaDetailsDTO;
          }
          return this.rmService.getRmSummaryCoverDays(this.executionFlowPageService.selectedHeaderId);
        }))
        .pipe(
        switchMap((responseOfCoverDays: any) => {
          if (responseOfCoverDays !== null && responseOfCoverDays !== undefined) {
            console.log(responseOfCoverDays);
            this.coverDaysList = responseOfCoverDays;
            this.totalCoverDays = responseOfCoverDays.totalCoverDays;
            this.totalDaysInMonth = responseOfCoverDays.totalDaysOfMonths;
            this.totalCoverDaysWithSit=responseOfCoverDays.totalCoverDaysWithSit;
            this.totalStockValue=responseOfCoverDays.totalStockValue;
            this.sitValue=responseOfCoverDays.sitValue;
            this.totalStockSitValue=responseOfCoverDays.totalStockSitValue;
          }
          return this.rmService.getRmSummaryCoverDaysClassWise(this.executionFlowPageService.selectedHeaderId);
        })).subscribe((classWiseRes: any) => {
          if (classWiseRes !== null && classWiseRes !== undefined) {
            console.log(classWiseRes);
            this.classACoverDaysConsumption =classWiseRes.classACoverDaysConsumptionDTO;
            this.classBCoverDaysConsumption =classWiseRes.classBCoverDaysConsumptionDTO;
            this.classCCoverDaysConsumption =classWiseRes.classCCoverDaysConsumptionDTO;
            this.totalCoverDaysConsumption = classWiseRes.totalCoverDaysConsumptionDTO;
          }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    })

  }

  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  openStdPriceModal() {
    document.getElementById("openStdPriceModal").click();
  }

  setNullStdPriceCoverDaysFormData() {
    this.nullStdPriceDataList.forEach((rmCoverDaysDTO: any) => {
      const rowView = new FormGroup({
        code: new FormControl(rmCoverDaysDTO?.code),
        description: new FormControl(rmCoverDaysDTO?.description),
        category: new FormControl(rmCoverDaysDTO?.category),
        stockQuantity: new FormControl(rmCoverDaysDTO?.stockQuantity),
        stockValue: new FormControl(rmCoverDaysDTO?.stockValue),
        map: new FormControl(rmCoverDaysDTO?.map),
        stdPrice: new FormControl(rmCoverDaysDTO?.stdPrice, [Validators.required]),
        rate: new FormControl(rmCoverDaysDTO?.rate),
        month1ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month1ConsumptionQuantity),
        month2ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month2ConsumptionQuantity),
        month3ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month3ConsumptionQuantity),
        month4ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month4ConsumptionQuantity),
        month5ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month5ConsumptionQuantity),
        month6ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month6ConsumptionQuantity),
        month7ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month7ConsumptionQuantity),
        month8ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month8ConsumptionQuantity),
        month9ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month9ConsumptionQuantity),
        month10ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month10ConsumptionQuantity),
        month11ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month11ConsumptionQuantity),
        month12ConsumptionQuantity: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month12ConsumptionQuantity),
        month1Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month1Value),
        month2Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month2Value),
        month3Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month3Value),
        month4Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month4Value),
        month5Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month5Value),
        month6Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month6Value),
        month7Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month7Value),
        month8Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month8Value),
        month9Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month9Value),
        month10Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month10Value),
        month11Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month11Value),
        month12Value: new FormControl(rmCoverDaysDTO?.coverDaysMonthDetailsDTO?.month12Value),
      });
      (<FormArray>this.nullStdPriceCoverDaysDataForm.get('nullStdPriceCoverDaysFormArray')).push(rowView);
    });
  }

  UpdateStdPrice(row: any, columnName: string, stdPriceValue: string, rowIndex?: number) {
    this.spinner.show();
    const currentRowIndex: number = this.monthWiseData.findIndex(value => value.code === row.code.value);
    if ((this.stdPriceDataChangedRowIndexList.findIndex(value => value === rowIndex)) === -1) {
      this.stdPriceDataChangedRowIndexList.push(rowIndex);
    }
    this.monthWiseData[currentRowIndex].stdPrice = stdPriceValue;
    this.setChangedRowCodeStdPriceList(row?.code?.value, currentRowIndex);
    this.spinner.hide();
  }

  setChangedRowCodeStdPriceList(materialCode: string, currentRowIndex: number) {
    const isPresent = this.changedRowCodeList.findIndex((value: any) => value === materialCode);
    if (isPresent === -1) {
      this.changedRowCodeList.push(materialCode);
      this.rmCoverDaysListToSave.push(this.monthWiseData[currentRowIndex]);
    }
  }

  saveStdPrice() {
    if (this.nullStdPriceCoverDaysDataForm.invalid) {
      return;
    }
    document.getElementById("openStdPriceModal").click();
    this.rmService.updateRMCoverDaysListBasedOnStdPrice(this.rmCoverDaysListToSave).subscribe((updatedPmSlobListResponse: any) => {
      this.successMessage = 'Slob updating...';
      this.isEdit = false;
      this.highlightEditValue.editSelection = false;
      this.rmCoverDaysListToSave = [];
      this.nullStdPriceDataList = [];
      this.monthWiseData = [];
      this.getData();
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
      this.spinner.hide();
    });
  }

  exportAsXLSX() {
    this.rmExportService.getRmCoverDaysExcelFile().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmCoverDaysExcel + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = 'Download Success!';
      setTimeout(() => {
        this.successMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Download Failure!';
      setTimeout(() => {
        this.errorMessage = '';
      }, 2000);
    });
  }
  exportAsXLSXForCoverDays(){
    this.rmExportService.getRmCoverDaySummary().subscribe((excelFileResponse: any) => {
      this.isExcelDisable =true;
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmCoverDaysSummary + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successmodalMessage = 'Download Success!';
      setTimeout(() => {
        this.successmodalMessage = '';
        this.isExcelDisable =false;
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errormodalMessage = 'Download Failure!';
      setTimeout(() => {
        this.errormodalMessage = '';
      }, 2000);
    });
  }
}
