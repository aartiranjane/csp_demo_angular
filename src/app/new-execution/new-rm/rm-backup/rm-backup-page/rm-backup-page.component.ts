import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rm-backup-page',
  templateUrl: './rm-backup-page.component.html',
  styleUrls: ['./rm-backup-page.component.css']
})
export class RmBackupPageComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('nav-org-tabb').click();
  }

}
