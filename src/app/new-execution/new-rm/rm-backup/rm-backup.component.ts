import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {AnalyzerService} from "../../analyzer/analyzer/analyzer.service";
import {PmServiceService} from "../../new-pm/pm-service.service";

@Component({
  selector: 'app-rm-backup',
  templateUrl: './rm-backup.component.html',
  styleUrls: ['./rm-backup.component.css']
})
export class RmBackupComponent implements OnInit, AfterViewChecked , AfterViewChecked {
  isDisabled = true;
  folders = [
    "First Cut Plan",
    "Validated Plan",
  ];
  selectedAll: any;
  names: any;
  monthSelectList: any = [];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  analyzerMpsId: any;
  preValidatedFileDataList = [{name: 'Row Cut Plan 1 A (1.11.20)', selected:false}];
  currentValidatedFileDataList = [{name:'Main Plan (05.11.20)', selected:false}];
  postValidatedFileDataList = [{name : 'Final Addition Plan (14.05.20)', selected: false}
  ];
  selected: boolean = false;
  checkedAllList = {
    preCheckedList: [],
    currentCheckedList: [],
    postCheckedList: []
  };
  selectedExports: any[] = [];

  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private spinner: NgxSpinnerService, private analyzerService: AnalyzerService, private pmServiceService: PmServiceService,
              private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    /*   this.spinner.show();*/
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };
    this.analyzerService.getBackMonthNamesList().subscribe((bkMonthRes: any) => {
      console.log(bkMonthRes);
      if (bkMonthRes !== null && bkMonthRes !== undefined && bkMonthRes.length !== 0) {
        bkMonthRes.forEach((res: any) => {
          this.monthSelectList.push({itemName: res});
          this.singleselectedItems[0] = this.monthSelectList[0];
        });
        this.onItemSelect(this.monthSelectList[0]);
        this.spinner.hide();
      }
    }, (error) => {
      this.spinner.hide();
    });
  }

  onItemSelect(item: any) {
    /*  this.analyzerMpsId = item.id;*/
    /*    this.fgFileDataList = [];
        this.pmFileDataList = [];
        this.rmFileDataList = [];
        this.analyzerService.getAllExcelExportFileNamesData(this.analyzerMpsId).subscribe((allExcelExportFileNamesRes: any) => {
          if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
            /!*this.fgList = allExcelExportFileNamesRes?.fgFileList;*!/
            for (let i = 0; i < allExcelExportFileNamesRes.fgFileList.length; i++) {
              this.fgFileDataList.push({name: allExcelExportFileNamesRes.fgFileList[i], selected: false});
            }
            /!*  this.pmList = allExcelExportFileNamesRes?.pmFileList;*!/
            for (let i = 0; i < allExcelExportFileNamesRes.pmFileList.length; i++) {
              this.pmFileDataList.push({name: allExcelExportFileNamesRes.pmFileList[i], selected: false});
            }
            /!* this.rmList = allExcelExportFileNamesRes?.rmFileList;*!/
            for (let i = 0; i < allExcelExportFileNamesRes.rmFileList.length; i++) {
              this.rmFileDataList.push({name: allExcelExportFileNamesRes.rmFileList[i], selected: false});
            }

          }
        }, (error) => {
          console.log(error);
        });*/
  }

  onItemDeSelect() {
    /*  this.fgFileDataList = [];
      this.pmFileDataList = [];
      this.rmFileDataList = [];*/
  }

  selectAll() {
    this.preValidatedFileDataList[0].selected = this.selectedAll;
    this.currentValidatedFileDataList[0].selected =this.selectedAll;
    this.postValidatedFileDataList[0].selected = this.selectedAll;
    /* for (var i = 0; i < this.pmFileDataList.length; i++) {
       this.pmFileDataList[i].selected = this.selectedAll;
     }
     for (var i = 0; i < this.rmFileDataList.length; i++) {
       this.rmFileDataList[i].selected = this.selectedAll;
     }
     */
    this.getCheckedItemList();
  }

  checkIfAllSelected() {
    this.selectedAll = this.preValidatedFileDataList.every((items: any) => {
      return items.selected === true;
    });
    this.selectedAll = this.currentValidatedFileDataList.every((items: any) => {
      return items.selected === true;
    });
    this.selectedAll = this.postValidatedFileDataList.every((items: any) => {
      return items.selected === true;
    });
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedAllList.preCheckedList = [];
    this.checkedAllList.currentCheckedList = [];
    this.checkedAllList.postCheckedList = [];
    this.isDisabled = true;
    for (var i = 0; i < this.preValidatedFileDataList.length; i++) {
      if (this.preValidatedFileDataList[i].selected) {
        this.checkedAllList.preCheckedList.push(this.preValidatedFileDataList[i].name);
        this.isDisabled = false;
      }
    }
    for (var i = 0; i < this.currentValidatedFileDataList.length; i++) {
      if (this.currentValidatedFileDataList[i].selected) {
        this.checkedAllList.currentCheckedList.push(this.currentValidatedFileDataList[i].name);
        this.isDisabled = false;
      }
    }

    for (var i = 0; i < this.postValidatedFileDataList.length; i++) {
      if (this.postValidatedFileDataList[i].selected) {
        this.checkedAllList.postCheckedList.push(this.postValidatedFileDataList[i].name);
        this.isDisabled = false;
      }
    }
    console.log(this.checkedAllList);
  }

  /*  download() {
      let fileName;
      if (this.selectedExports.length === 1) {
        fileName = this.selectedExports.toString();
      }

      if (this.checkedAllList.fgCheckedList.length + this.checkedAllList.pmCheckedList.length + this.checkedAllList.rmCheckedList.length == 1) {
        if (this.checkedAllList.fgCheckedList.length === 1) {
          fileName = this.checkedAllList.fgCheckedList[0];
        }
        if (this.checkedAllList.pmCheckedList.length === 1) {
          fileName = this.checkedAllList.pmCheckedList[0];
        }
        if (this.checkedAllList.rmCheckedList.length === 1) {
          fileName = this.checkedAllList.rmCheckedList[0];
        }
      }


      if (this.checkedAllList.fgCheckedList.length <= 0) {
        this.checkedAllList.fgCheckedList.push("null");
      }
      if (this.checkedAllList.pmCheckedList.length <= 0) {
        this.checkedAllList.pmCheckedList.push("null");
      }
      if (this.checkedAllList.rmCheckedList.length <= 0) {
        this.checkedAllList.rmCheckedList.push("null");
      }
      this.analyzerService.getAllSelectedExcelExport(this.analyzerMpsId, this.checkedAllList.fgCheckedList, this.checkedAllList.pmCheckedList, this.checkedAllList.rmCheckedList).subscribe((updatedPmSlobListResponse: any) => {
        this.spinner.hide();
        console.log(updatedPmSlobListResponse);
        if (updatedPmSlobListResponse.type === "application/vnd.xlsx") {
          const blob = new Blob([updatedPmSlobListResponse], {type: 'application/xlsx;'});
          saveAs(blob, fileName);
        } else {
          const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
          saveAs(blob, 'SelectedExports.zip');
        }
      }, (error) => {
        console.log(error);
        this.spinner.hide();
      });
      this.selectedExports = [];
    }

    fillList(list: any) {
      list.forEach((value) => {
        if (value.selected) {
          const temp = value.name;
          this.selectedExports.push(temp)
        }
      });
    }*/
}
