import {Component, OnDestroy, OnInit} from '@angular/core';
import {AnalyzerService} from "../../../analyzer/analyzer/analyzer.service";
import {NgxSpinnerService} from "ngx-spinner";
import {saveAs} from "file-saver";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-rm-backup-annual-analysis',
  templateUrl: './rm-backup-annual-analysis.component.html',
  styleUrls: ['./rm-backup-annual-analysis.component.css']
})
export class RmBackupAnnualAnalysisComponent implements OnInit,OnDestroy {
  $subs:Subscription;
  isDownloadDis = true;
  annualSelectedAll: any;
  bkAnnual:any=[];
  allModuleZipList:any=[];
  annualSelectedAll2: any;
  annualCheckedAllList = {
    firstCutCheckedList: [],
    validatedCheckedList: [],
    pmOtifCheckedList:[],
    allModuleCheckedList:[]
  };
  annualSelectedExports: any[] = [];


  constructor(private analyzerService: AnalyzerService, private spinner:NgxSpinnerService) { }

  ngOnInit(): void {

    this.$subs=this.analyzerService.getMonth.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.analyzerService.selectedMonth = resValue;
      }
      this.spinner.show();
      this.allModuleZipList = [];
      this.analyzerService.getAllBackUpFileNamesData(this.analyzerService.selectedMonth).subscribe((allExcelExportFileNamesRes: any) => {
        if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
          console.log(allExcelExportFileNamesRes);
          for (let i = 0; i < allExcelExportFileNamesRes.allModuleZipList.length; i++) {
            this.allModuleZipList.push({name: allExcelExportFileNamesRes.allModuleZipList[i], selected: false});
          }
          console.log(this.allModuleZipList);
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    });

  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  annualSelectAll() {
    for (var i = 0; i < this.allModuleZipList.length; i++) {
      this.allModuleZipList[i].selected = this.annualSelectedAll;
    }
    console.log("selectedAll 1 =" + this.annualSelectedAll);

    this.getCheckedItemListAnnual();
  }

  checkIfAllSelectedAnnual() {

    this.annualSelectedAll2 = this.allModuleZipList.every((items: any) => {
      return items.selected === true;
    });
    if (this.annualSelectedAll2 === true) {
      this.annualSelectedAll = true;
    } else {
      this.annualSelectedAll = false;
    }


    this.getCheckedItemListAnnual();
  }

  getCheckedItemListAnnual() {
    this.annualCheckedAllList.allModuleCheckedList = [];
    this.isDownloadDis = true;
    for (var i = 0; i < this.allModuleZipList.length; i++) {
      if (this.allModuleZipList[i].selected) {
        this.annualCheckedAllList.allModuleCheckedList.push(this.allModuleZipList[i].name);
        this.isDownloadDis = false;
      }
    }
  }


  annualDownload() {
    let fileName;
    if (this.annualSelectedExports.length === 1) {
      fileName = this.annualSelectedExports.toString();
    }
    if (this.annualCheckedAllList.allModuleCheckedList.length === 1) {
      if (this.annualCheckedAllList.allModuleCheckedList.length === 1) {
        fileName = this.annualCheckedAllList.allModuleCheckedList[0];
      }
    }
    else{
      fileName = this.analyzerService.monthNameOnZip;
    }
    if (this.annualCheckedAllList.firstCutCheckedList.length <= 0) {
      this.annualCheckedAllList.firstCutCheckedList.push("null");
    }
    if (this.annualCheckedAllList.pmOtifCheckedList.length <= 0) {
      this.annualCheckedAllList.pmOtifCheckedList.push("null");
    }
    if (this.annualCheckedAllList.validatedCheckedList.length <= 0) {
      this.annualCheckedAllList.validatedCheckedList.push("null");
    }

    this.analyzerService.getAllBackUpExcelExportDownload(this.analyzerService.selectedMonth, this.annualCheckedAllList.firstCutCheckedList, this.annualCheckedAllList.validatedCheckedList,
      this.annualCheckedAllList.pmOtifCheckedList, this.annualCheckedAllList.allModuleCheckedList).subscribe((updatedPmSlobListResponse: any) => {
      this.spinner.hide();
      console.log(updatedPmSlobListResponse);
      if (updatedPmSlobListResponse.type === "application/vnd.zip") {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
        saveAs(blob, fileName);
      } else {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
        saveAs(blob,fileName );
      }
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
    this.annualSelectedExports = [];
  }


}
