import {Component, OnInit} from '@angular/core';
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";

@Component({
  selector: 'app-stock-rejection',
  templateUrl: './stock-rejection.component.html',
  styleUrls: ['./stock-rejection.component.css']
})
export class StockRejectionComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  rejectionDataList = [];

  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
/*
    this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmRejectionStockData(this.executionFlowPageService.selectedHeaderId).subscribe((rejectionStockResponse: any) => {
        if (rejectionStockResponse !== null && rejectionStockResponse !== undefined) {
          this.rejectionDataList = rejectionStockResponse.itemCodes;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })
*/

  }

  /*exportAsXLSX() {
    this.successMessage = 'Downloading...';
    this.rmExportService.getRejectionStockExcelFile().subscribe((excelFileResponse: any) => {
      console.log(excelFileResponse);
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmRejectionStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = this.excelExportConstantsService.rmRejectionStock+ ' '+ 'Downloaded successfully';
      setTimeout( () => {
        this.successMessage = '';
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
    });
  }*/
}
