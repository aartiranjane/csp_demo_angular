import { Component, OnInit } from '@angular/core';
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";

@Component({
  selector: 'app-rm-transfer-stock',
  templateUrl: './rm-transfer-stock.component.html',
  styleUrls: ['./rm-transfer-stock.component.css']
})
export class RmTransferStockComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  transferStockList=[];
  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
   /* this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmTransferStockData(this.executionFlowPageService.selectedHeaderId).subscribe((transferStockResponse:any)=>{
        if(transferStockResponse!==null && transferStockResponse!== undefined){
          this.transferStockList = transferStockResponse.itemCodes;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })*/

  }
  /*exportAsXLSX() {
    this.successMessage = 'Downloading...';
    this.rmExportService.getTransferStockExcelFile().subscribe((excelFileResponse: any) => {
      console.log(excelFileResponse);
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmTransferStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = this.excelExportConstantsService.rmTransferStock + ' '+ 'Downloaded successfully';
      setTimeout( () => {
        this.successMessage = '';
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
    });
  }*/
}
