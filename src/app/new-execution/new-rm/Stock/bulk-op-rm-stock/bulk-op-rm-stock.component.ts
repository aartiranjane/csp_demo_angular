import { Component, OnInit } from '@angular/core';
import {saveAs} from "file-saver";
import {RmExportService} from "../../rm-export.service";
import {RmServiceService} from "../../rm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";

@Component({
  selector: 'app-bulk-op-rm-stock',
  templateUrl: './bulk-op-rm-stock.component.html',
  styleUrls: ['./bulk-op-rm-stock.component.css']
})
export class BulkOpRmStockComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  bulkOpRmDataList=[];
  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
  /*  this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
        if (resValue !== undefined && resValue !== null) {
          this.executionFlowPageService.selectedHeaderId = resValue;
        }
      this.ngxSpinnerService.show();
      this.rmService.getBulkOpRmStockData(this.executionFlowPageService.selectedHeaderId).subscribe((bulkOpRmResponse:any)=>{
        if(bulkOpRmResponse!==null && bulkOpRmResponse!== undefined){
          this.bulkOpRmDataList = bulkOpRmResponse.itemCodes;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })*/

  }
 /* exportAsXLSX() {
    this.successMessage = 'Downloading...';
    this.rmExportService.getBulkOpRmStockExcelFile().subscribe((excelFileResponse: any) => {
      console.log(excelFileResponse);
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmBulkOpRmStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = this.excelExportConstantsService.rmBulkOpRmStock + ' '+ 'Downloaded successfully';
      setTimeout( () => {
        this.successMessage = '';
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
    });
  }*/
}
