import {Component, OnInit} from '@angular/core';
import {RmExportService} from "../../rm-export.service";
import {saveAs} from "file-saver";
import {RmServiceService} from "../../rm-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {ExcelExportConstantsService} from "../../../../excel-export-constants.service";

@Component({
  selector: 'app-rm-stock-mes',
  templateUrl: './rm-stock-mes.component.html',
  styleUrls: ['./rm-stock-mes.component.css']
})
export class RmStockMesComponent implements OnInit {
  successMessage = '';
  errorMessage = '';
  mesDataList = [];

  constructor(private rmExportService: RmExportService, private rmService: RmServiceService,
              private excelExportConstantsService: ExcelExportConstantsService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private ngxSpinnerService: NgxSpinnerService) {
  }

  ngOnInit(): void {
    /*this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.ngxSpinnerService.show();
      this.rmService.getRmMesStockData(this.executionFlowPageService.selectedHeaderId).subscribe((mesResponse: any) => {
        if (mesResponse !== null && mesResponse !== undefined) {
          this.mesDataList = mesResponse.itemCodes;
        }
        this.ngxSpinnerService.hide();
      }, (error) => {
        this.ngxSpinnerService.hide();
        console.log(error);
      });
    })*/
  }

  /*exportAsXLSX() {
    this.successMessage = 'Downloading...';
    this.rmExportService.getMesStockExcelFile().subscribe((excelFileResponse: any) => {
      console.log(excelFileResponse);
      const blob = new Blob([excelFileResponse], {type: 'application/vnd.ms-excel;'});
      saveAs(blob, this.excelExportConstantsService.rmMesStock + " " + this.executionFlowPageService.selectedPlanDate + '.xlsx');
      this.successMessage = this.excelExportConstantsService.rmMesStock +' ' + 'Downloaded successfully';
      setTimeout( () => {
        this.successMessage = '';
      }, 2000);
    }, (error) => {
      console.log(error);
      this.errorMessage = 'Something Went Wrong';
    });
  }*/
}
