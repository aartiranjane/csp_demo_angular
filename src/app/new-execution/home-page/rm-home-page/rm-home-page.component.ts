import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../auth.service";
import {PlanModuleService} from "../../plan-a-module/plan-module.service";
import {AnalyzerService} from "../../analyzer/analyzer/analyzer.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-rm-home-page',
  templateUrl: './rm-home-page.component.html',
  styleUrls: ['./rm-home-page.component.css']
})
export class RmHomePageComponent implements OnInit {

  constructor(private authService: AuthService,
              private planModuleService: PlanModuleService,
              private analyzerService:AnalyzerService,
              private ngxSpinnerService: NgxSpinnerService,
              public executionFlowPageService: ExecutionFlowPageServiceService, private router: Router) { }

  ngOnInit(): void {
  }


  planModule() {
    if (this.executionFlowPageService.statusIdp == true) {
      this.router.navigateByUrl('/planModule/validationPage');
    }
    if (this.executionFlowPageService.statusIdp == false) {
      this.router.navigateByUrl('/planModule/planPage');
    }

  }

  rmViewPlannedModule(){
    if(this.executionFlowPageService.mpsPlanHeader.length ===0 ){
      document.getElementById("openDvmModal").click();
      console.log('No Plan is executed for the current Month. Kindly run a Plan to see DVM.');
    }
    else {
      this.executionFlowPageService.moduleWisePage = 'RMView';
      this.router.navigateByUrl('/newRm/rmAnalyticsDashboard/rmMpsTab');
    }
  }

  rmBackUp(){
    this.executionFlowPageService.moduleWisePage = 'RMBkup';
    this.router.navigateByUrl('/rmBackupPage/rmBackup/rmPlan');
  }

  rmYearlyAnalysis(){
    this.executionFlowPageService.moduleWisePage = 'RMYearly';
    this.router.navigateByUrl('/rmYearlyModule/rmDeltaTabs');
  }
}
