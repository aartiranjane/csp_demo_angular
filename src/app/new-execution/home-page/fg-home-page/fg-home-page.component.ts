import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../auth.service";
import {PlanModuleService} from "../../plan-a-module/plan-module.service";
import {AnalyzerService} from "../../analyzer/analyzer/analyzer.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-fg-home-page',
  templateUrl: './fg-home-page.component.html',
  styleUrls: ['./fg-home-page.component.css']
})
export class FgHomePageComponent implements OnInit {

  constructor(private authService: AuthService,
              private planModuleService: PlanModuleService,
              private analyzerService:AnalyzerService,
              private ngxSpinnerService: NgxSpinnerService,
              public executionFlowPageService: ExecutionFlowPageServiceService, private router: Router) { }

  ngOnInit(): void {
  }


  planModule() {

    if (this.executionFlowPageService.statusIdp == true) {
      this.router.navigateByUrl('/planModule/validationPage');
    }
    if (this.executionFlowPageService.statusIdp == false) {
      this.router.navigateByUrl('/planModule/planPage');
    }

  }

  fgViewPlannedModule(){
    if(this.executionFlowPageService.mpsPlanHeader.length ===0 ){
      document.getElementById("openDvmModal").click();
      console.log('No Plan is executed for the current Month. Kindly run a Plan to see DVM.');
    }
    else {
      this.executionFlowPageService.moduleWisePage = 'FGView';
      this.router.navigateByUrl('/fgModule/fgDashboard/mpsTab');
    }
  }

  fgBackUp(){
    this.executionFlowPageService.moduleWisePage = 'FGBkup';
    this.router.navigateByUrl('/fgBackupPage/fgBackup/fgPlan');
  }

  fgYearlyAnalysis(){
    this.executionFlowPageService.moduleWisePage = 'FGYearly';
    this.router.navigateByUrl('/fgYearlyModule/deltaTab');
  }

}
