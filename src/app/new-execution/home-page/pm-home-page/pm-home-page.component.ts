import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../../auth.service";
import {PlanModuleService} from "../../plan-a-module/plan-module.service";
import {AnalyzerService} from "../../analyzer/analyzer/analyzer.service";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-pm-home-page',
  templateUrl: './pm-home-page.component.html',
  styleUrls: ['./pm-home-page.component.css']
})
export class PmHomePageComponent implements OnInit {
  constructor(private authService: AuthService,
              private planModuleService: PlanModuleService,
              private analyzerService:AnalyzerService,
              private ngxSpinnerService: NgxSpinnerService,
              public executionFlowPageService: ExecutionFlowPageServiceService, private router: Router) { }

  ngOnInit(): void {

  }


  planModule() {

    if (this.executionFlowPageService.statusIdp == true) {
      this.router.navigateByUrl('/planModule/validationPage');
    }
    if (this.executionFlowPageService.statusIdp == false) {
      this.router.navigateByUrl('/planModule/planPage');
    }

  }

  pmViewPlannedModule(){
    if(this.executionFlowPageService.mpsPlanHeader.length ===0 ){
      document.getElementById("openDvmModal").click();
      console.log('No Plan is executed for the current Month. Kindly run a Plan to see DVM.');
    }
    else{
      this.executionFlowPageService.moduleWisePage = 'PMView';
      this.router.navigateByUrl('/newPm/analyticsDashboard/pmMpsTab');
    }

  }

  pmBackUp(){
    this.executionFlowPageService.moduleWisePage = 'PMBkup';
    this.router.navigateByUrl('/pmBackupPage/pmBackup/pmPlan');
  }

  pmYearlyAnalysis(){
    this.executionFlowPageService.moduleWisePage = 'PMYearly';
    this.router.navigateByUrl('/pmYearlyModule/pmDeltaAnalytics');
  }

}
