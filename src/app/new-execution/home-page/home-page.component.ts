import {Component, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../execution-flow-page-service.service";
import {Router} from "@angular/router";
import {PlanModuleService} from "../plan-a-module/plan-module.service";
import {NgxSpinnerService} from "ngx-spinner";
import {AnalyzerService} from "../analyzer/analyzer/analyzer.service";
import {AuthService} from "../../auth.service";
import {switchMap} from "rxjs/operators";

@Component({
  selector: 'app-fg-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  loginModuleName: any;
  status: boolean;
  mpsHeaderList: any = [];
  monthYearList: any = [];
  moduleNamesArray = [];
  moduleNameDisabled = true;
  userName: any;
  moduleName: any;
  adminName: any;
  roleModule:any=[];
  roleModuleSelectList:any=[];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  selectedRole: string;
  selectModuleMsg = '';
  selectedDevice:any;
  roleWiseSpinner:any;
  constructor(private authService: AuthService,
              private planModuleService: PlanModuleService,
              private analyzerService: AnalyzerService,
              private ngxSpinnerService: NgxSpinnerService,
              public executionFlowPageService: ExecutionFlowPageServiceService, private router: Router) {
  }

  ngOnInit(): void {
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      enableSearchFilter: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };

    this.userName = this.executionFlowPageService.userName;
 /*   this.moduleName = this.executionFlowPageService.moduleName;*/
   /* const role = this.executionFlowPageService.roleModuleList;
    this.roleModule = role.reverse();*/
   this.roleModule = this.executionFlowPageService.roleModuleList;
    console.log(this.roleModule);
   /* this.roleModule.forEach((res: any) => {
      this.roleModuleSelectList.push({itemName: res});
      this.singleselectedItems[0] = this.roleModuleSelectList[0];
      this.onItemSelect(this.roleModuleSelectList[0]);

    });*/

    console.log('role' + this.selectedRole);
    /*this.selectModuleMsg = this.selectedRole + 'Module Selected';*/
    console.log("nullll"+" "+this.roleModule[0].slice(5));

      if(this.executionFlowPageService.selectedModuleName != undefined){
        this.selectedDevice = this.executionFlowPageService.selectedModuleName;
        this.onChange(this.selectedDevice);
      }
     if(this.executionFlowPageService.selectedModuleName == undefined ){
       this.selectedDevice = this.roleModule[0];
       this.onChange(this.selectedDevice);
      }

    this.planModuleService.getPlanModuleStatus().subscribe((res: any) => {
      this.executionFlowPageService.statusIdp = res;
      this.planModuleService.pageStatus = res;
      console.log(this.executionFlowPageService.statusIdp)
    });
   this.moduleNameDisabled = this.executionFlowPageService.moduleNameDisabled;
   /* this.getHeaderAndMonthData();*/
  }

  /*selectedModule(row:any){
   /!* this.ngxSpinnerService.show();*!/
    console.log(row);
    this.selectedRole = row;
   /!* this.selectModuleMsg = this.selectedRole +' '+ 'Module Selected';*!/
    this.executionFlowPageService.selectedModuleName = this.selectedRole;

    if (this.selectedRole == 'FG') {
      this.router.navigateByUrl('/homePage/fgRole');
    }
    else if (this.selectedRole == 'PM') {
      this.router.navigateByUrl('/homePage/pmRole');
    }
    else if (this.selectedRole == 'RM') {
      this.router.navigateByUrl('/homePage/rmRole');
    }
   /!* setTimeout(() => {
    this.ngxSpinnerService.hide();
    /!*this.selectModuleMsg ='';*!/
    }, 2000);
    setTimeout(() => {
      this.selectModuleMsg ='';
    }, 4000);
*!/

  }*/

  onChange(newValue) {
    console.log(newValue);
    this.selectedDevice = newValue;
    this.roleWiseSpinner = newValue;
    this.executionFlowPageService.selectedModuleName = this.selectedDevice;
  if( this.executionFlowPageService.msgHideShow !== this.selectedDevice ){
    this.ngxSpinnerService.show();
    this.selectModuleMsg = this.selectedDevice.slice(5) + ' ' + 'Module Selected';
   }
    if (this.selectedDevice == 'role_FG') {
      this.router.navigateByUrl('/homePage/fgRole');
    }
    else if (this.selectedDevice == 'role_PM'){
      this.router.navigateByUrl('/homePage/pmRole');
    }
    else if (this.selectedDevice == 'role_RM'){
      this.router.navigateByUrl('/homePage/rmRole');
    }
    else if (this.selectedDevice == 'role_Analyst'){
      this.router.navigateByUrl('/homePage/analystRole');
    }
    setTimeout(() => {
      this.ngxSpinnerService.hide();
      /*this.selectModuleMsg ='';*/
    }, 400);
    setTimeout(() => {
      this.selectModuleMsg ='';
    }, 1200);

    // ... do other stuff here ...
  }

  cspNavigate() {
    this.router.navigateByUrl('/newExecutionflow');
   /* if (this.executionFlowPageService.loginUsers === 'rucha' || this.executionFlowPageService.loginUsers === 'nayan') {
      this.router.navigateByUrl('/newExecutionflow');
    }
    else {
      this.router.navigateByUrl('/homePage');
    }*/

  }


  logOut() {
    if (this.mpsHeaderList.length > 0) {
      this.executionFlowPageService.selectedHeaderId = this.mpsHeaderList[this.mpsHeaderList.length - 1].id;
      this.executionFlowPageService.selectedPlanDate = this.mpsHeaderList[this.mpsHeaderList.length - 1].mpsDate;
    }
    /*this.router.navigate(['/login']);*/
 /*
   */

    this.router.navigate(['/login']).then(() => {
      this.authService.isAuthenticatedEvent.next(false);
      this.authService.currentUserDetailsView = undefined;
      this.authService.relogSub.unsubscribe();
      this.authService.idle.stop();
     /* window.location.reload();*/
      });
    this.executionFlowPageService.selectedModuleName = undefined;


    /*window.location.reload();*/
   /* console.log(this.authService.currentUserDetailsView);*/
  }
}
