import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "./execution-flow-page-service.service";
import {AuthService} from "../auth.service";


@Component({
  selector: 'app-new-execution',
  templateUrl: './new-execution.component.html',
  styleUrls: ['./new-execution.component.css']
})
export class NewExecutionComponent implements OnInit {
  mpsHeaderList: any = [];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  mpsPlanSelected = false;
  roleModule:any=[];
  mpsPlanSelectedColor = {
    mpsPlanDisabledColor: false
  };
  usersName:any;
  constructor( private authService: AuthService,
               private router: Router,
               private executionFlowPageService: ExecutionFlowPageServiceService,
               private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.usersName = this.executionFlowPageService.userName;
    console.log(this.usersName);
    this.roleModule = this.executionFlowPageService.roleModuleList;
    console.log(this.roleModule);

  }



  navigationToPmPage() {
    this.executionFlowPageService.moduleName ='PM';
    this.router.navigateByUrl('/homePage');
   /* this.router.navigateByUrl('/newPm/analyticsDashboard/pmMpsTab');*/
  }

  navigationToRmPage() {
    this.executionFlowPageService.moduleName ='RM';
    this.router.navigateByUrl('/homePage');
  /*  this.router.navigateByUrl('/newRm/rmAnalyticsDashboard/rmMpsTab');*/
  }
  navigationToFgPage() {
  this.executionFlowPageService.moduleName ='FG';
    this.router.navigateByUrl('/homePage');
  }
  navigationToAdministratorPage() {
    this.router.navigateByUrl('/administrator/userManagement/organisation');
  }

  AnalystPage(){
    this.executionFlowPageService.moduleName ='Analyst';
    this.router.navigateByUrl('/homePage');
   /* this.router.navigateByUrl('/analyzer/commonAnalytics/mpsTab');*/
  }

  logOut(){
    this.router.navigateByUrl('/login');
    this.authService.idle.stop();
  }
}
