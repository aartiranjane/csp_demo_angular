import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UriService} from "../../uri.service";
import {catchError, map} from "rxjs/operators";
import {Observable, Subject, throwError} from "rxjs";


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class PlanModuleService {
  endpoint = this.uriService.getResourceServerUri();
  allFilesAvailable = {};
  allFilesSelected = {};
  folders = [];
  selectedFileCount = 0;
  drapDropFilesList = [];
  wrongFilesList = [];
  currentPlanName = '';
  pageStatus: boolean = false;
  mpsState = {
    mpsName: '',
    mpsDateStr: '',
    mpsDate: '',
    planDate: new Date(),
    tagName: '',
    rscDtTagId: 0,
    tagDescription: '',
    rscIitIdpStatusDTOs: [],
    planType: 'requiredFiles',
    allFilesCount: 0,
    edit: false,
    firstMps: false,
    otifDate: '',
    editable: false,
    modifiedDate: '',
    executionEnable: false,
    isExecutionDone: false
  };
  originalList: any = [];
  moduleWiseTab = false;
  goToValidationPage: boolean = true;
  /* mpsPlanViewDto = {
     mpsName: '',
     mpsDateStr: '',
     tagName: '',
     rscDtTagId: 0,
     rscIitIdpStatusDTOS: []
   };*/
  wrongExtension = [];
  cachedSelection = {};

  addPmOtifBtn = true;
  pmOtifDisabled = true;
  allExecuteFilesDisabled = false;
  deleteHidden = false;
  validationErrorBtn = true;
  otifDisabledBasedOnFile = true;
  stateOfOtifPm = false;
  planChanged = new Subject();
  executionError = true;

  constructor(private http: HttpClient,
              private uriService: UriService) {
  }

  getPlanModuleTagData() {
    return this.http.get(this.endpoint + '/api/rsc-dt-tag', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllFilesData(tagName: any) {
    return this.http.get(this.endpoint + '/api/all-files/' + tagName, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getMpsName(mpsName: string) {
    return this.http.get(this.endpoint + '/api/mps-plan/' + mpsName, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  isUniquePlanName(name: string): Observable<any> {
    return this.http.get(this.endpoint + '/api/mps-plan-name/exist/' + name,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  isUniqueMpsPlanManagementName(name: string, id: any): Observable<any> {
    return this.http.get(this.endpoint + '/api/mps-plan-name/exist/except-active-mps-plan/' + name + "/" + id,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  isUniquePlanNameForEdit(name: string): Observable<any> {
    return this.http.get(this.endpoint + '/api/mps-plan-name/exist/except-active-plan/' + name,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }


  updateLicenceFile(file, idpStatusId): Observable<any> {
    const url = this.uriService.getResourceServerUri() + '/api/upload-file/' + idpStatusId;
    const formdata: FormData = new FormData();
    // formdata.append('headers', httpOptions);
    formdata.append('file', file);
    return this.http.post(url, formdata);
  }

  getCreatePlanData(mpsPlanViewDto: any) {
    return this.http.post(this.endpoint + '/api/rsc-iit-idp-status' + '/?mpsPlanViewDto=', JSON.stringify(mpsPlanViewDto), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }

  updatePlanData(mpsPlanViewDto: any) {
    return this.http.post(this.endpoint + '/api/rsc-iit-idp-status/update' + '/?mpsPlanViewDto=', JSON.stringify(mpsPlanViewDto), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }


  getCurrentMpsAvailable() {
    return this.http.get(this.endpoint + '/api/current-mps-plan/available', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getCurrentMpsPlanDetails() {
    return this.http.get(this.endpoint + '/api/current-mps-plan/details', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getValidationError() {
    return this.http.get(this.endpoint + '/api/error-log', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }


  validationAndDataExecutionData(mpsName: any) {
    return this.http.get(this.endpoint + '/api/current-uploaded-mps-plan/' + mpsName,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }


  fileValidation(mpsName: any) {
    return this.http.get(this.endpoint + '/api/file-validation/' + mpsName,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  deleteSelectedFiles(filesToBeDeleted: any): Observable<any> {
    return this.http.put(
      this.uriService.getResourceServerUri() + `/api/current-mps-plan/delete/selected-files`,
      filesToBeDeleted, httpOptions
    );
  }

  getPpHeader(mpsName, mpsDate): Observable<any> {
    return this.http.get(
      this.endpoint + `/api/mps-plan-header/update/mps-name/${mpsName}/mps-date/${mpsDate}`,
      httpOptions
    );
  }

  savePpHeader(ppHeader: any): Observable<any> {
    return this.http.put(
      this.uriService.getResourceServerUri() + `/api/mps-plan-header/update`,
      ppHeader, httpOptions
    );
  }

  getPlanModuleStatus() {
    return this.http.get(this.endpoint + '/api/current-mps-plan/file-upload-page/status', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  setOtifDateToPlan(mpsPlanViewDto: any) {
    return this.http.post(this.endpoint + '/api/rsc-iit-idp-status/saveOtifDate' + '/?mpsPlanViewDto=', JSON.stringify(mpsPlanViewDto), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }

  setExecutionStatusInRscIitIdpStatus(mpsPlanViewDto: any) {
    return this.http.post(this.endpoint + '/api/rsc-iit-idp-status/save/execution-status' + '/?mpsPlanViewDto=', JSON.stringify(mpsPlanViewDto), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }

  updateMultipleValidationTagToPseudo(mpsName, mpsDate, tagName): Observable<any> {
    return this.http.get(this.endpoint + '/api/update-validated-plans/to-Pseudo/' + mpsName + '/mps-date/' + mpsDate + '/tag-name/' + tagName,
      httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getCurrentMonthsUniqueOtifDateDetails(mpsName, mpsDate) {
    return this.http.get(this.endpoint + '/api/pm-otif-date-list/current-months/' + mpsName + '/mpsDate/' + mpsDate, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  updatePlanManagement(tagChangeStatus: any, rscMtPPheaderDTO: any) {
    return this.http.put(this.endpoint + '/api/update-mps-details/plan-management/' + tagChangeStatus + '?rscMtPPheaderDTOs=', JSON.stringify(rscMtPPheaderDTO), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }

  getLatestPlanMpsDataWithoutBackup() {
    return this.http.get(this.endpoint + '/api/mps-plan-header/with-latest-backup-date', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  apiExecutionErrorExcelCreation(executionResponseMainDTO: any) {
    return this.http.post(this.endpoint + '/api/create-error-excel' + '/?executionResponseMainDTO=', JSON.stringify(executionResponseMainDTO), {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).pipe(
      map((response: Response) => response),
      catchError(this.errorHandler));
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
