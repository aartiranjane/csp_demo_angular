import {Component, OnInit} from '@angular/core';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {PlanModuleService} from "../plan-module.service";
import {switchMap} from "rxjs/operators";
import {AuthService} from "../../../auth.service";

@Component({
  selector: 'app-plan-page',
  templateUrl: './plan-page.component.html',
  styleUrls: ['./plan-page.component.css']
})

export class PlanPageComponent implements OnInit {
  /* isEdit = false;*/
  createForm: FormGroup;
  editForm: FormGroup;
  // @ViewChild('planCreateModal') modalClose;
  model: NgbDateStruct;
  otifModel: NgbDateStruct;
  editModel: NgbDateStruct;
  duplicateName = false;
  duplicateEditName = false;
  planErrorMessage = '';
  folders: any = [];
  allFilesSelected: any = {};
  allFilesAvailable: any = {};
  singleselectedItems: any = [];
  singledropdownSettings = {};
  tagList: any = [];
  fileTemp: any = [];
  dragDropConfig = {
    showList: true,
    showProgress: false,
  };
  finalList: any = [];
  tagName: any;
  selectedItemName: any = '';
  planNameList: any = [];
  planDataForm = new FormGroup({
    planDataFormArray: new FormArray([])
  });
  planNameListToSave: Record<string, any>[] = [];
  isEdit = false;
  isPlanNameValid = true;
  successMessage = '';
  errorMessage = '';
  invalidPlanId;
  selectedTagName: string;
  selectedTagDescription: string;
  saveButtonDisabledForEdit = true;
  selectedMpsName: string;
  isErrorIcon = false;

  mpsAvailable: boolean;

  highlightClass = {
    dangercs: false
  };

  newMpsPlan = {
    mpsName: '',
    mpsDateStr: '',
    planDate: new Date(),
    tagName: '',
    rscDtTagId: 0,
    tagDescription: '',
    rscIitIdpStatusDTOs: [],
    allFilesCount: 0,
    otifDateStr: '',
    otifDate: '',
    executionEnable: false,
  };
  uploadedFilesList: any = [];
  nextBtnEnable: any;
  nextDisabled = true;
  uploadDisabled = true;

  addPmOtif = true;

  deleteBtn = true;
  originalMpsDate = '';
  /*otifDate = new Date();*/
  otifDateStr = "";
  validatedDate: string;
  objDate;
  objDate1;
  preObjDate;
  otifDateValid = false;
  otifDateMsg = '';
  dragDropFiles: any;
  selectedItems = [];
  otifDateDisabled = true;
  errorMsg: any;
  pmOtifDeleteDisabled = true;
  planExist = false;
  currentMonthsAllOtifDateList = [];
  lastBackupDate: string;
  minDate = undefined;

  constructor(private router: Router, private authService: AuthService,
              private spinner: NgxSpinnerService, public planModuleService: PlanModuleService,
              private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  ngOnInit(): void {
    this.createForm = new FormGroup({
      mpsName: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z][a-zA-Z0-9\s_]*$/),
        Validators.maxLength(25), Validators.minLength(5)]),
      date: new FormControl('', [Validators.required]),
      tag: new FormControl('', [Validators.required]),
    });

    this.planModuleService.getCurrentMpsAvailable().subscribe((currentMps: any) => {
      this.mpsAvailable = currentMps;
      console.log(this.mpsAvailable);//false
      this.planModuleService.getPlanModuleTagData().subscribe((tag: any) => {
        console.log(tag);
        if (tag !== null && tag !== undefined) {
          tag.forEach((res: any) => {
            this.tagList.push({id: res.id, name: res.name, itemName: res.description});
            /* if(this.planModuleService.mpsState.tagDescription === res.description){
               this.selectedItems.push({id: res.id, itemName: res.description});
             }*/
          });

          this.selectedItems = [];
          console.log(this.selectedItemName);
        }
      });
      if (this.mpsAvailable === false) {
        document.getElementById("openCreateModal").click();
      }
      else {
        this.getCurrentMpsPlanDetails();
        if (this.planModuleService.stateOfOtifPm === true) {
          this.pmOtifDeleteDisabled = false;
        }
        this.planModuleService.getCurrentMpsPlanDetails().subscribe((currentMpsDetails: any) => {
          this.originalMpsDate = currentMpsDetails.mpsDate;
          console.log(currentMpsDetails);
          if (currentMpsDetails.executionEnable === true && currentMpsDetails.isExecutionDone === null) {
            this.planModuleService.goToValidationPage = false;
          }
          if (currentMpsDetails.isExecutionDone === true && this.planModuleService.stateOfOtifPm === false) {
            this.planModuleService.otifDisabledBasedOnFile = false;
          }
          this.planModuleService.mpsState.rscIitIdpStatusDTOs.forEach(value => {
            if (value.fileName === 'OTIF_PM' && value.extension === 'csv' &&
              (value.uploadStatus === true && value.resourceStatus === true && value.validationStatus === true)) {
              this.pmOtifDeleteDisabled = true;
              this.planModuleService.otifDisabledBasedOnFile = false;
            }
            else if (value.fileName === 'OTIF_PM' && value.extension === 'csv' &&
              (value.uploadStatus === true && value.resourceStatus === false && value.validationStatus === false)) {
              this.pmOtifDeleteDisabled = false;
            }
          });
        });
      }

    });


    this.singledropdownSettings = {
      text: 'Select a Tag',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };

    this.model = {
      year: this.newMpsPlan.planDate.getFullYear(),
      month: this.newMpsPlan.planDate.getMonth() + 1,
      day: this.newMpsPlan.planDate.getDate()
    };
    this.getDate(this.model);
    /* this.getOtifDate(this.model);*/

    /* if(this.planModuleService.mpsState.tagDescription === 'Validated'){
       this.planModuleService.addPmOtifBtn = false;
     }*/


    this.planModuleService.getCurrentMpsPlanDetails().subscribe((currentMpsDetails: any) => {
      if (currentMpsDetails != null && currentMpsDetails !== undefined) {
        console.log(currentMpsDetails);
        this.validatedDate = currentMpsDetails.mpsDate;

      }

      /*  if (currentMpsDetails.isExecutionDone === true) {
         this.planModuleService.otifDisabledBasedOnFile = false;
        }*/
    });

    this.planModuleService.getLatestPlanMpsDataWithoutBackup().subscribe((currentMpsDetails: any) => {
      if (currentMpsDetails != null && currentMpsDetails !== undefined) {
        console.log(currentMpsDetails);
        this.lastBackupDate = currentMpsDetails.mpsDate;
        const backUpDate = new Date(this.lastBackupDate);
        // const backUpDate = new Date(lastDayOfBackupMonth.getFullYear(), lastDayOfBackupMonth.getMonth()+1, 0)
        this.minDate = {
          year: backUpDate.getFullYear(),
          month: backUpDate.getMonth() + 1,
          day: backUpDate.getDate()
        };
      }
    });
  }

  newPlan() {
    this.planModuleService.getCurrentMpsPlanDetails().subscribe((currentMpsRes: any) => {
      console.log(currentMpsRes);
      this.createForm = new FormGroup({
        mpsName: new FormControl('', [
          Validators.required,
          Validators.pattern(/^[a-zA-Z][a-zA-Z0-9\s_]*$/),
          Validators.maxLength(25), Validators.minLength(5)]),
        date: new FormControl('', [Validators.required]),
        tag: new FormControl('', [Validators.required]),
      });

      this.newMpsPlan.planDate = new Date();
      this.model = {
        year: this.newMpsPlan.planDate.getFullYear(),
        month: this.newMpsPlan.planDate.getMonth() + 1,
        day: this.newMpsPlan.planDate.getDate()
      };
      this.getDate(this.model);
      if (currentMpsRes.editable === true) {

        document.getElementById("openConfirmModal").click();
      }
      else {
        document.getElementById("openCreateModal").click();
      }

    });

    this.planModuleService.executionError = true;
  }

  no() {

  }

  getCurrentOtifDetails() {
    this.planModuleService.getCurrentMonthsUniqueOtifDateDetails(this.planModuleService.mpsState.mpsName, this.planModuleService.mpsState.mpsDate).subscribe((otifDateListResponse: any) => {
      if (otifDateListResponse != null && otifDateListResponse !== undefined) {
        console.log(otifDateListResponse);
        this.currentMonthsAllOtifDateList = otifDateListResponse;
      }
    });
  }

  yes() {
    document.getElementById("openCreateModal").click();
  }


  editPlanDate(date: any) {
    const year = date.year;
    const month = date.month <= 9 ? '0' + date.month : date.month;
    const day = date.day <= 9 ? '0' + date.day : date.day;
    const finalDate = year + '-' + month + '-' + day;
    console.log(finalDate);
  }

  editPlanOtifDate(date: any) {

  }

  getOtifDate(date: any) {
    const year = date.year;
    const month = date.month <= 9 ? '0' + date.month : date.month;
    const day = date.day <= 9 ? '0' + date.day : date.day;
    // const month = date.month;
    // const day = date.day;

    const finalDate = year + '-' + month + '-' + day;
    console.log(finalDate);
    if (this.planModuleService.mpsState.mpsDate !== this.originalMpsDate) {
      this.originalMpsDate = this.planModuleService.mpsState.mpsDate;
    }
    console.log(this.originalMpsDate);
    this.otifDateStr = finalDate;
    const newStartDate = new Date(this.originalMpsDate);
    this.objDate = {
      month: newStartDate.getMonth() + 1,
    };
    // this.objDate.month = this.objDate.month <= 9 ? '0' + this.objDate.month : this.objDate.month;

    if (this.objDate.month.toString().length === 1) {
      this.objDate.month = "0" + this.objDate.month.toString();
    }
    console.log(this.objDate.month);
    if (month === this.objDate.month) {
      this.otifDateValid = true;
      this.otifDateMsg = '';
      this.otifDateStr = finalDate;
      console.log(this.otifDateStr);
      if (this.checkValidOtifDateOrNot(this.otifDateStr)) {
        this.otifDateValid = false;
        this.saveButtonDisabledForEdit = true;
        this.otifDateMsg = 'Otif is already calculated for this date.';

      } else {
        if (this.checkMonthOfOtifDateAndMpsDate(this.originalMpsDate, this.otifDateStr)) {
          this.saveButtonDisabledForEdit = false;
          this.otifDateMsg = '';
        }
      }
    }
    else {
      this.otifDateValid = false;
      this.saveButtonDisabledForEdit = true;
      this.otifDateMsg = 'please select valid month';
      console.log('dis' + this.otifDateValid);
    }
  }

  checkValidOtifDateOrNot(otifDate: string) {
    let otifStatus = false;
    this.currentMonthsAllOtifDateList.forEach(value => {
      console.log(value);
      if (value === otifDate) {
        otifStatus = true;
      }
    });
    return otifStatus;
  }

  getCurrentMpsPlanDetails() {
    this.planModuleService.getCurrentMpsPlanDetails().subscribe((currentMpsDetails: any) => {
      if (currentMpsDetails != null && currentMpsDetails !== undefined) {
        this.planModuleService.mpsState = currentMpsDetails;
        this.originalMpsDate = currentMpsDetails.mpsDate;
        if (currentMpsDetails.otifDate !== null) {
          this.otifDateStr = currentMpsDetails.otifDate;
          this.otifDateDisabled = false;
        } else {
          this.otifDateStr = null;
        }

        if (currentMpsDetails.enable === true) {
          /*this.nextDisabled = false;*/
          this.planModuleService.goToValidationPage = false;
        }
        else {
          this.planModuleService.goToValidationPage = true;
        }
        if (currentMpsDetails.executionEnable === true && currentMpsDetails.isExecutionDone === null) {
          this.planModuleService.goToValidationPage = false;
        }
        if (currentMpsDetails.pmOTIFEnable === true) {
          this.addPmOtif = false;
        }
        if (this.uploadDisabled === false) {
          if (currentMpsDetails.isExecutionDone === true) {
            this.planModuleService.otifDisabledBasedOnFile = false;
          }
          this.planModuleService.mpsState.rscIitIdpStatusDTOs.forEach(value => {
            if (value.fileName === 'OTIF_PM' && value.extension === 'csv' &&
              (value.uploadStatus === true && value.resourceStatus === false && value.validationStatus === false)) {
              this.pmOtifDeleteDisabled = false;
            }
          });
        }
        /* else{
          this.planModuleService.otifDisabledBasedOnFile = false;
         }*/


        console.log(currentMpsDetails);
        console.log(this.planModuleService.mpsState);
        console.log("moduleWiseTab : " + this.planModuleService.moduleWiseTab);
        this.setAllFilesAvailable();
      }
    });
  }

  otifSet() {
    this.planModuleService.mpsState.otifDate = this.otifDateStr;
    this.planModuleService.setOtifDateToPlan(this.planModuleService.mpsState).subscribe((currentMpsDetails: any) => {
      if (currentMpsDetails != null && currentMpsDetails !== undefined) {
        this.planModuleService.mpsState = currentMpsDetails;
        if (currentMpsDetails.enable === true) {
          /*this.nextDisabled = false;*/
          this.planModuleService.goToValidationPage = false;
        } else {
          this.planModuleService.goToValidationPage = true;
        }
        if (currentMpsDetails.otifDate !== null) {
          this.otifDateStr = currentMpsDetails.otifDate;
          this.otifDateDisabled = false;
        } else {
          this.otifDateStr = null;
        }
        if (currentMpsDetails.pmOTIFEnable === true) {
          this.addPmOtif = false;

        }
        console.log(currentMpsDetails);
        console.log(this.planModuleService.mpsState);
        // this.setAllFilesAvailable();
      }
    });
  }

  pmOtif() {
    this.pmOtifDeleteDisabled = false;
    const otifToBeDeleted = [];
    this.planModuleService.mpsState.rscIitIdpStatusDTOs.forEach((a) => {
      if (a.fileName === 'OTIF_PM' && a.extension === 'csv') {
        console.log("fileName is: " + a.fileName);
        otifToBeDeleted.push(a);
      }
    });
    this.deleteSelectedFiles(otifToBeDeleted);
    /*for (let i = 0; i < this.allFilesAvailable[this.folders[2]].length; i++) {
      if (this.allFilesAvailable[this.folders[2]][i].fileName === 'OTIF_PM') {
        this.allFilesAvailable[this.folders[2]][i].resourceStatus = false;
        this.allFilesAvailable[this.folders[2]][i].uploadStatus = false;
        this.allFilesAvailable[this.folders[2]][i].validationStatus = false;
      }
      console.log(this.allFilesAvailable[this.folders[2]]);
    }*/
    this.planModuleService.stateOfOtifPm = true;
    this.planModuleService.otifDisabledBasedOnFile = true;
  }

  resetOtif() {
    this.removeSelectedFile(2, 'OTIF_PM', 'csv');
    this.planModuleService.otifDisabledBasedOnFile = false;
    this.uploadDisabled = true;
    for (var i = 0; i < this.fileTemp.length; i++) {
      if (this.fileTemp[i].name === 'OTIF_PM.csv') {
        this.fileTemp.splice(i, 1);
        i--;
      }
    }
    this.getCurrentMpsPlanDetails();
    this.pmOtifDeleteDisabled = true;
  }

  setAllFilesAvailable(): void {
    this.allFilesSelected = [];
    this.allFilesAvailable = [];
    this.folders = [];
    /*this.fileTemp=[];*/
    /* console.log(res);
     console.log(JSON.parse(JSON.stringify(res)));*/
    this.planModuleService.mpsState.allFilesCount = this.planModuleService.mpsState.rscIitIdpStatusDTOs.length;
    this.planModuleService.mpsState.rscIitIdpStatusDTOs.forEach((a) => {
      /*   console.log(a);*/
      this.allFilesAvailable[a.folderName]
        ? this.allFilesAvailable[a.folderName].push(a)
        : (this.allFilesAvailable[a.folderName] = [a]);
      // console.log(this.allFilesAvailable[a.folderName]);

      this.allFilesAvailable[a.folderName].sort((x, y) => {
        return (x.isRequired === y.isRequired) ? 0 : x.isRequired ? -1 : 1;
      });
    });


    Object.keys(this.allFilesAvailable).forEach((a) => {
      this.allFilesSelected[a] = this.allFilesAvailable[a].filter(
        (b) => b.required
      );
      this.planModuleService.selectedFileCount += this.allFilesSelected[a].length;
      this.folders.push(a);
      console.log(this.folders);
      /*  console.log(this.allFilesAvailable[this.folders[2]]);*/

    });

    /*this.checkPendingMps();
    this.setCheckedStatus(true);*/
    this.getCurrentOtifDetails();

  }

  editTagData(edit) {
  }

  createPlan() {
    this.spinner.show();
    console.log(this.newMpsPlan);
    this.planModuleService.getCreatePlanData(this.newMpsPlan).pipe(
      switchMap((createResponse: any) => {
        console.log(createResponse);
        this.planModuleService.mpsState = createResponse;
        return this.planModuleService.getCurrentMpsPlanDetails();
      })).subscribe((currentMpsDetails: any) => {
      if (currentMpsDetails !== null && currentMpsDetails !== undefined) {
        this.planModuleService.mpsState = currentMpsDetails;
        this.originalMpsDate = currentMpsDetails.mpsDate;
        console.log(currentMpsDetails);
        this.setAllFilesAvailable();
        // this.getCurrentOtifDetails();
      }
      this.spinner.hide();
      this.successMessage = 'Plan Created successfully';
      this.planModuleService.goToValidationPage = true;
      this.addPmOtif = true;
      this.planModuleService.otifDisabledBasedOnFile = true;
      this.uploadDisabled = true;
      setTimeout(() => {
        this.successMessage = '';
      }, 3000);
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
    /*   this.tagList = [];*/
    this.mpsAvailable = true;
    /*this.cancelModal();*/
    /* this.createForm.reset();*/
    this.planModuleService.allExecuteFilesDisabled = false;
    this.fileTemp = [];
    this.planModuleService.wrongFilesList = [];
    this.isErrorIcon = false;
  }

  cancelModal() {
    /*  this.createForm.reset();*/
  }

  hideResetBtn() {
    this.resetOtif();
  }

  removeSelectedFile(index: any, fileName: any, extension: any) {
    this.spinner.show();
    const filesToBeDeleted = [];
    this.planModuleService.mpsState.rscIitIdpStatusDTOs.forEach((a) => {
      if (a.fileName === fileName && a.extension === extension) {
        console.log("fileName is: " + a.fileName);
        filesToBeDeleted.push(a);
      }
    });
    console.log(filesToBeDeleted);

    this.deleteSelectedFiles(filesToBeDeleted);
    for (let i = 0; i < this.fileTemp.length; i++) {
      if (this.fileTemp[i].name.toUpperCase() === fileName.toUpperCase() + "." + extension.toUpperCase()) {
        this.fileTemp.splice(i, 1);
        i--;
      }
    }
    this.spinner.hide();
    if (this.fileTemp.length <= 0) {
      this.uploadDisabled = true;
    }
    if (fileName === 'OTIF_PM' && extension === 'csv') {
      this.otifDateStr = null;
    }
    console.log(this.otifDateStr)
  }

  otifUnset() {
    const otifToBeDeleted = [];
    this.planModuleService.mpsState.rscIitIdpStatusDTOs.forEach((a) => {
      if (a.fileName === 'OTIF_PM' && a.extension === 'csv') {
        console.log("fileName is: " + a.fileName);
        otifToBeDeleted.push(a);
      }
    });
    console.log(otifToBeDeleted);
    // this.deleteSelectedFiles(otifToBeDeleted);
    this.removeSelectedFile(2, 'OTIF_PM', 'csv');
    for (let i = 0; i < this.fileTemp.length; i++) {
      if (this.fileTemp[i].name === 'OTIF_PM' + "." + 'csv') {
        this.fileTemp.splice(i, 1);
        i--;
        this.otifDateStr = null;
        const newStartDate = new Date('');
        this.otifModel = {
          year: newStartDate.getFullYear(),
          month: newStartDate.getMonth() + 1,
          day: newStartDate.getDate()
        };
        this.otifDateDisabled = true;
        this.getOtifDate(this.otifModel);
        this.otifDateMsg = '';
      }
    }
  }

  deleteSelectedFiles(filesToBeDeleted: any) {
    /*  console.log(filesToBeDeleted);
      this.planModuleService.deleteSelectedFiles(filesToBeDeleted).subscribe((fileDeletedResponse: any) => {
          console.log("currentMpsDetails" + fileDeletedResponse);
          this.planModuleService.mpsState = fileDeletedResponse;
          if (filesToBeDeleted[0].fileName === 'OTIF_PM' && filesToBeDeleted[0].extension === 'csv') {
            this.otifDateStr = null;
            this.otifSet();
            const newStartDate = new Date('');
            this.otifModel = {
              year: newStartDate.getFullYear(),
              month: newStartDate.getMonth() + 1,
              day: newStartDate.getDate()
            };
            this.otifDateDisabled = true;
            this.getOtifDate(this.otifModel);
            this.otifDateMsg = '';
          } else {
            this.setAllFilesAvailable();
          }
        }*/
    this.spinner.show();
    this.planModuleService.deleteSelectedFiles(filesToBeDeleted).pipe(
      switchMap((fileDeletedResponse: any) => {
        console.log("currentMpsDetails" + fileDeletedResponse);
        this.planModuleService.mpsState = fileDeletedResponse;
        if (filesToBeDeleted[0].fileName === 'OTIF_PM' && filesToBeDeleted[0].extension === 'csv') {
          this.otifDateStr = null;
          this.otifSet();
          const newStartDate = new Date('');
          this.otifModel = {
            year: newStartDate.getFullYear(),
            month: newStartDate.getMonth() + 1,
            day: newStartDate.getDate()
          };
          this.otifDateDisabled = true;
          this.getOtifDate(this.otifModel);
          this.otifDateMsg = '';
        } else {
          this.setAllFilesAvailable();
        }
        return this.planModuleService.getCurrentMpsPlanDetails();
      })).subscribe((currentMpsDetails: any) => {
      console.log(currentMpsDetails);
      if (currentMpsDetails != null && currentMpsDetails !== undefined) {
        this.planModuleService.mpsState = currentMpsDetails;
        this.originalMpsDate = currentMpsDetails.mpsDate;
        if (currentMpsDetails.otifDate !== null) {
          this.otifDateDisabled = false;
          this.otifDateStr = currentMpsDetails.otifDate;
          /*  this.pmOtifDeleteDisabled = true; color change*/
        } else {
          this.otifDateStr = null;
        }
        if (currentMpsDetails.enable === true) {
          this.planModuleService.goToValidationPage = false;
        } else {
          this.planModuleService.goToValidationPage = true;
        }
        if (currentMpsDetails.pmOTIFEnable === true) {
          this.addPmOtif = false;
          //this.planModuleService.otifDisabledBasedOnFile=false;
        }


        this.setAllFilesAvailable();

      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  }

  getUploadedFiles(event: FileList, folder): void {
    console.log(event);
    if (event.length > 0) {
      this.uploadDisabled = false;
    }
    console.log(event);
    this.isErrorIcon = false;
    this.planModuleService.wrongFilesList = [];
    const idp = this.planModuleService.mpsState;
    console.log(folder);
    // console.log(this.allFilesAvailable[folder]);
    const filesInCurrentFolder = this.planModuleService.mpsState.rscIitIdpStatusDTOs.filter(
      (f) => f.folderName === folder
    );
    console.log(filesInCurrentFolder);
    for (let i = 0; i < event.length; i++) {
      console.log(event.item(i).name);
      const validFile = this.pickValidFile(event[i], filesInCurrentFolder);
      this.dragDropFiles = validFile;
      console.log(validFile);
      if (validFile !== null) {
        if (validFile.name === 'OTIF_PM.csv') {
          this.pmOtifDeleteDisabled = false;
          this.planModuleService.goToValidationPage = false;
          const newStartDate = new Date(this.originalMpsDate);
          this.otifModel = {
            year: newStartDate.getFullYear(),
            month: newStartDate.getMonth() + 1,
            day: newStartDate.getDate()
          };
          this.otifDateDisabled = true;
          this.getOtifDate(this.otifModel);
          document.getElementById("openOtifModal").click();
        }
      }
      if (validFile) {
        const ind = this.fileTemp.findIndex(
          (fin) => fin.name === validFile.name
        );
        if (ind === -1) {
          // this.fileTemp.push(validFile);
          if (this.planModuleService.mpsState.isExecutionDone === false || this.planModuleService.mpsState.isExecutionDone === null) {
            this.fileTemp.push(validFile);
          }
          else if (this.planModuleService.mpsState.isExecutionDone === true && validFile.name === 'OTIF_PM.csv') {
            this.fileTemp.push(validFile);
          }
        }
      } else {
        this.planModuleService.wrongFilesList.push(event.item(i).name);
        this.isErrorIcon = true;
        this.highlightClass.dangercs = true;
        setTimeout(() => {
          this.highlightClass.dangercs = false;
        }, 2000);
      }
    }


    /*  for (let i=0; i< this.dragDropFiles.length ; i++){*/

    /* }
     this.dragDropFiles=[];
 */


    /*  if (this.fileTemp.length === 1) {
        this.fileTemp.find(a => a.fileName === 'OTIF_PM' && a.extension === 'csv');
        this.planModuleService.goToValidationPage = false;
        document.getElementById("openOtifModal").click();
      }
      }*/
    console.log("wrongFilesList: " + this.planModuleService.wrongFilesList);
    if (this.fileTemp.length <= 0) {
      this.uploadDisabled = true;
    }
  }

  pickValidFile(file, validFiles: any[]): any {
    if (file.size < 5) {
      return null;
    }
    for (let i = 0; i < validFiles.length; i++) {
      const f = validFiles[i];
      console.log(f);
      if (file.name.toUpperCase().startsWith(f.fileName.toUpperCase()) &&
        file.name.toString().length === f.fileName.toString().concat(".").concat(f.extension.toString()).length) {
        if (!file.name.toUpperCase().endsWith(f.extension.toUpperCase())) {
          // this.planModuleService.wrongExtension.push(file.name.toUpperCase());
          return null;
        }
        if (f.uploadStatus === 'UPLOADED') {
          f.uploadStatus = 'PENDING';
        }
        return file;
      }
    }
    return null;
  }

  pickColor(file): string {
    /* this.planModuleService.deleteHidden = false;*/
    const f = this.planModuleService.mpsState.rscIitIdpStatusDTOs.find((a) =>
      file.fileName.toUpperCase().startsWith(a.fileName.toUpperCase()) &&
      file.extension.toUpperCase().startsWith(a.extension.toUpperCase()) &&
      file.fileName.toUpperCase().endsWith(a.fileName.toUpperCase())
    );
    /*console.log(f);*/
    if (!f) return '';
    if (f.uploadStatus === true) {
      this.uploadedFilesList.push(file.fileName.toUpperCase() + '.' + file.extension.toUpperCase());
      return 'bg-uploaded';
    }
    /*if (
      this.planModuleService.wrongExtension.findIndex((a: string) =>
        a.startsWith(file.fileName.toUpperCase())
      ) !== -1
    ) {
      // return 'bg-wrong';
      return '';
    }*/
    if (this.fileTemp.findIndex((a) => a.name.toUpperCase() ===
      file.fileName.toUpperCase() + '.' + file.extension.toUpperCase()) !== -1) {

      return 'bg-correct';
    }
    return '';
  }

  nextBtn() {
    this.planModuleService.validationErrorBtn = true;
    console.log(this.planModuleService.validationErrorBtn);
    this.spinner.show();
    this.authService.idle.stop();
    /*  this.planModuleService.fileValidation(this.planModuleService.mpsState.mpsName).subscribe((validation: any) => {
        console.log(validation);
        this.spinner.hide();
      });*/
  }


  onItemDeSelect() {
  }

  setEditDate(event: any) {
    const year = event.year;
    const month = event.month <= 9 ? '0' + event.month : event.month;
    const day = event.day <= 9 ? '0' + event.day : event.day;
    const finalDate = year + '-' + month + '-' + day;
    const newStartDate = new Date(this.originalMpsDate);
    /*  this.planModuleService.mpsState.mpsDate = finalDate;*/
    console.log("newStartDate : " + newStartDate)
    this.objDate = {
      month: newStartDate.getMonth() + 1,
      day: newStartDate.getDay().toString(),
    };
    this.objDate.month = this.objDate.month <= 9 ? '0' + this.objDate.month : this.objDate.month;

    const previousDate = new Date(finalDate);
    console.log("previousDate : " + previousDate)

    this.preObjDate = {
      month: previousDate.getMonth() + 1,
      day: previousDate.getDay().toString(),
    };
    this.preObjDate.month = this.preObjDate.month <= 9 ? '0' + this.preObjDate.month : this.preObjDate.month;

    if (this.preObjDate !== this.objDate) {
      this.planModuleService.mpsState.mpsDate = finalDate;
      this.errorMsg = '';
      console.log(this.planModuleService.mpsState.mpsDate);
      console.log(this.otifDateStr);

      if (this.checkMonthOfOtifDateAndMpsDate(this.planModuleService.mpsState.mpsDate, this.otifDateStr)) {
        this.saveButtonDisabledForEdit = false;
        this.otifDateMsg = '';
      } else {
        this.saveButtonDisabledForEdit = true;
        this.errorMsg = 'otif date month is not same.'
      }
    }
    else {
      this.saveButtonDisabledForEdit = true;
      /* this.planModuleService.mpsState.mpsDate = this.originalMpsDate;*/
      this.planModuleService.getCurrentMpsPlanDetails().subscribe((res: any) => {
        this.planModuleService.mpsState.mpsDate = res.mpsDate;
        this.planModuleService.mpsState.mpsDateStr = res.mpsDateStr;
        this.originalMpsDate = res.mpsDate;
      });
      this.errorMsg = 'selected date is already a mps date';
    }
  }

  setEditTag(item: any) {
    this.planModuleService.mpsState.rscDtTagId = item.id;
    this.planModuleService.mpsState.tagName = item.name;
    this.planModuleService.mpsState.tagDescription = item.itemName;
    this.saveButtonDisabledForEdit = false;
    this.checkName();
  }


  getDate(event: any) {
    const year = event.year;
    const month = event.month <= 9 ? '0' + event.month : event.month;
    const day = event.day <= 9 ? '0' + event.day : event.day;
    const finalDate = year + '-' + month + '-' + day;
    this.newMpsPlan.mpsDateStr = finalDate;
    // this.planModuleService.mpsPlanViewDto.mpsDateStr = finalDate;
    // this.planModuleService.mpsPlanViewDto.mpsName =  this.planModuleService.mpsState.mpsName;
    console.log(this.newMpsPlan.mpsDateStr);

  }

  getTagData(item: any) {
    console.log(item);
    /*   this.selectedItemName.description = item.itemName;*/
    /*this.planModuleService.mpsState.rscDtTagId = 0;
    this.planModuleService.mpsState.tagDescription = item.itemName;
    this.planModuleService.mpsState.rscDtTagId = item.id;
    this.planModuleService.mpsState.tagName = item.name;*/
    this.newMpsPlan.rscDtTagId = item.id;
    this.newMpsPlan.tagName = item.name;
    this.newMpsPlan.tagDescription = item.itemName;

    /*this.selectedTagName = item.itemName;*/
    /* this.allFilesAvailable = {};
     this.planModuleService.drapDropFilesList = [];*/
    this.tagName = item.name;

    /*this.planModuleService.mpsPlanViewDto.rscDtTagId = item.id;
    this.planModuleService.mpsPlanViewDto.tagName = item.name;
*/
  }


  uploadAllFiles(index: number, folder?): void {
    this.uploadedFilesList = [];
    console.log(index);
    console.log(this.uploadedFilesList);
    // this.planModuleService.spinnerOn = true;
    console.log(this.planModuleService.mpsState.rscIitIdpStatusDTOs);
    console.log(this.fileTemp);

    if (index < this.fileTemp.length) {
      this.planModuleService.pageStatus = false;
      this.spinner.show();
      const i = this.planModuleService.mpsState.rscIitIdpStatusDTOs.findIndex((a) => {
          return (this.fileTemp[index].name
            .toUpperCase()
            .startsWith(a.fileName.toUpperCase()) && this.fileTemp[index].name
            .toUpperCase()
            .endsWith(a.fileName.toUpperCase().concat("." + a.extension.toUpperCase())));
        }
      );
      this.planModuleService
        .updateLicenceFile(
          this.fileTemp[index++],
          this.planModuleService.mpsState.rscIitIdpStatusDTOs[i].id
        )
        .subscribe((res: any) => {
          console.log(res);
          if (res.status === true) {
            this.planModuleService.mpsState.rscIitIdpStatusDTOs[i].uploadStatus =
              'UPLOADED';
          }
          this.uploadAllFiles(index);
        });
    }
    else {
      this.fileTemp = [];
      this.spinner.hide();
      // this.idpService.spinnerOn = false;
    }
    this.allFilesAvailable = [];
    this.getCurrentMpsPlanDetails();
    this.uploadDisabled = true;
  }


  editDuplicateName() {
    this.planModuleService
      .isUniquePlanNameForEdit(this.planModuleService.mpsState.mpsName)
      .subscribe(
        (res) => {
          console.log('VALID' + '' + this.editForm.valid);
          console.log('savebtn' + '' + this.saveButtonDisabledForEdit);
          this.duplicateEditName = res;
          this.checkName();
        },
        (e) => console.log(e)
      );
    /* if(this.editForm.valid===true){
       this.saveButtonDisabledForEdit = false;
     }else{
       this.saveButtonDisabledForEdit = true;
     }*/
  }

  checkMonthOfOtifDateAndMpsDate(mpsDate: any, OtifDate: any) {
    if (OtifDate === null) {
      this.editForm.controls.planOtifDate.setValue(undefined);
      return true;
    } else {
      const newMpsDate = new Date(mpsDate);
      const newOtifDate = new Date(OtifDate);
      this.objDate = {
        month: newMpsDate.getMonth() + 1,
      };
      this.objDate.month = this.objDate.month <= 9 ? '0' + this.objDate.month : this.objDate.month;
      this.objDate1 = {
        month: newOtifDate.getMonth() + 1,
      };
      this.objDate1.month = this.objDate1.month <= 9 ? '0' + this.objDate1.month : this.objDate1.month;
      if (this.objDate1.month === this.objDate.month) {
        this.errorMsg = "";
        return true;
      }
      else {
        this.errorMsg = "Otif date month is not same";
        return false;
      }
    }
  }

  editPlanValidate(): boolean {
    return this.planModuleService.mpsState.mpsName.length >= 5;
  }

  checkName() {
    let otifDate;
    if (this.planModuleService.mpsState.otifDate !== this.otifDateStr) {
      otifDate = this.otifDateStr;
    } else {
      otifDate = this.planModuleService.mpsState.otifDate;
    }
    if (this.planModuleService.mpsState.mpsName.length >= 5 &&
      this.checkMonthOfOtifDateAndMpsDate(this.planModuleService.mpsState.mpsDate, otifDate)) {
      if (this.duplicateEditName === true) {
        this.saveButtonDisabledForEdit = true;
      } else {
        this.saveButtonDisabledForEdit = false;
      }
    } else {
      this.saveButtonDisabledForEdit = true;
    }
  }

  checkDuplicateName(): void {
    if (this.newMpsPlan.mpsName === this.planModuleService.currentPlanName)
      return;
    this.planModuleService
      .isUniquePlanName(this.newMpsPlan.mpsName)
      .subscribe(
        (res) => {
          console.log('VALID' + '' + this.createForm.valid);
          console.log('IN' + '' + this.createForm.invalid);
          console.log('form' + '' + this.createForm);
          this.duplicateName = res;
        },
        (e) => console.log(e)
      );
  }

  planValidate(): boolean {
    return this.newMpsPlan.mpsName.length >= 5;
  }

  editBtn() {
    this.isEdit = true;
    this.editForm = new FormGroup({
      planName: new FormControl(this.planModuleService.mpsState.mpsName, [
        Validators.required,
        Validators.pattern(/^[a-zA-Z][a-zA-Z0-9\s_]*$/),
        Validators.maxLength(25), Validators.minLength(5)]),
      planDate2: new FormControl(this.planModuleService.mpsState.mpsDateStr),
      planTag: new FormControl(this.planModuleService.mpsState.tagDescription),
      planOtifDate: new FormControl(this.planModuleService.mpsState.otifDate)
    });
  }

  saveChanges() {
    console.log(this.editForm);
    this.isEdit = false;
    this.otifDateMsg = '';
    this.errorMsg = '';
    this.planModuleService.mpsState.otifDate = this.otifDateStr;
    this.planModuleService.updatePlanData(this.planModuleService.mpsState).pipe(
      switchMap((res: any) => {
        console.log(res);
        return this.planModuleService.getCurrentMpsPlanDetails();
      })).subscribe((currentMpsDetails: any) => {
      if (currentMpsDetails != null && currentMpsDetails !== undefined) {
        this.planModuleService.mpsState = currentMpsDetails;
        this.originalMpsDate = currentMpsDetails.mpsDate;
        if (currentMpsDetails.otifDate !== null) {
          this.otifDateDisabled = false;
          this.otifDateStr = currentMpsDetails.otifDate;
        } else {
          this.otifDateStr = null;
        }
        if (currentMpsDetails.enable === true) {
          /*this.nextDisabled = false;*/
          this.planModuleService.goToValidationPage = false;
        } else {
          this.planModuleService.goToValidationPage = true;
        }
        if (currentMpsDetails.pmOTIFEnable === true) {
          this.addPmOtif = false;

        }
        console.log(currentMpsDetails);
        console.log(this.planModuleService.mpsState);
        this.setAllFilesAvailable();
      }
    });
    this.saveButtonDisabledForEdit = true;
  }

  closeBtn() {
    this.isEdit = false;
    const newStartDate = new Date(this.originalMpsDate);
    this.editModel = {
      year: newStartDate.getFullYear(),
      month: newStartDate.getMonth() + 1,
      day: newStartDate.getDate()
    };
    this.setEditDate(this.editModel);
    this.saveButtonDisabledForEdit = true;
    this.getCurrentMpsPlanDetails();
  }
}

