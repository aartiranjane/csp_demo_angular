import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {PlanModuleService} from "../plan-module.service";
import * as XLSX from "xlsx";
import {CalculationService} from "../calculation.service";
import {switchMap} from "rxjs/operators";
import {ExecutionFlowPageServiceService} from "../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {Router} from "@angular/router";
import {AuthService} from "../../../auth.service";
import {forkJoin} from "rxjs/observable/forkJoin";
import {AnalyzerService} from "../../analyzer/analyzer/analyzer.service";

@Component({
  selector: 'app-go-to-validation',
  templateUrl: './go-to-validation.component.html',
  styleUrls: ['./go-to-validation.component.css']
})
export class GoToValidationComponent implements OnInit {
  @ViewChild('TABLE') table!: ElementRef;
  model: NgbDateStruct;
  currentdate = new Date();
  isError: boolean = false;
  errorMsg = '';
  singleselectedItems: any = [];
  singledropdownSettings = {};
  tagList: any = [];
  folders: any = [];
  allFilesSelected: any = {};
  validationErrorList: any = [];
  validationError: boolean = true;
  response = [];
  executionErrorList = [];

  constructor(public planModuleService: PlanModuleService, private authService: AuthService,
              private executionFlowPageService: ExecutionFlowPageServiceService,
              private spinner: NgxSpinnerService,
              private router: Router,
              private calculationService: CalculationService,
              private analyzerService: AnalyzerService) {
  }

  mpsState2 = {
    mpsName: '',
    mpsDateStr: '',
    planDate: new Date(),
    tagName: '',
    tagDescription: '',
    rscDtTagId: 0,
    rscIitIdpStatusDTOs: [],
    planType: 'requiredFiles',
    allFilesCount: 0,
    edit: false,
    firstMps: false,
    otifDateStr: '',
    otifDate: '',
    executionEnable: false,
  };

  allFilesAvailable: any = {};
  selectedFileCount = 0;

  executeBtn: boolean = true;

  subs = 0;
  moduleNamesArray = [];
  mpsHeaderList = [];
  monthYearList = [];
  tagNameIsValidated: any;

  ngOnInit(): void {
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };
    $(document).click(function (event) {
      $('#myDIV:visible').hide();
    });
    /* if (this.planModuleService.pageStatus === false) {
       this.planModuleService.fileValidation(this.planModuleService.mpsState.mpsName).subscribe((validation: any) => {
         console.log(validation);
         this.spinner.hide();
       });
       this.planModuleService.pageStatus = true;
     }*/

    if (this.planModuleService.pageStatus === false) {
      this.planModuleService.getCurrentMpsPlanDetails().pipe(
        switchMap((currentMpsDetails: any) => {
          if (currentMpsDetails != null && currentMpsDetails !== undefined) {
            this.planModuleService.mpsState = currentMpsDetails;
            console.log(currentMpsDetails);
          }
          return this.planModuleService.fileValidation(this.planModuleService.mpsState.mpsName);
        })).pipe(
        switchMap((validation: any) => {
          console.log(validation);
          return this.planModuleService.getValidationError();
        })).pipe(
        switchMap((validationErrorResponse: any) => {
          console.log(validationErrorResponse);
          if (validationErrorResponse !== null && validationErrorResponse !== undefined) {
            this.validationErrorList = validationErrorResponse;
          }
          return this.planModuleService.validationAndDataExecutionData(this.planModuleService.mpsState.mpsName);
        })).subscribe((nextResponse: any) => {
        if (nextResponse !== null && nextResponse !== undefined) {
          this.mpsState2 = nextResponse;
          console.log(nextResponse);
          /*   let count = 0;
             for (let i = 0; i < this.mpsState2.rscIitIdpStatusDTOs.length; i++) {
               /!* *!/
               if (this.mpsState2.rscIitIdpStatusDTOs[i].validationStatus === true) {
                 count++;
                 if (count === this.mpsState2.rscIitIdpStatusDTOs.length) {
                   this.executeBtn = false;
                   this.planModuleService.moduleWiseTab = true;
                   /!* this.planModuleService.deleteHidden = true;*!/
                 }
               }
               else {
                 this.planModuleService.validationErrorBtn = false;
                 this.planModuleService.moduleWiseTab = false;
               }
             }
             console.log(this.mpsState2.rscIitIdpStatusDTOs.length);*/
          if (this.mpsState2.executionEnable === true) {
            this.executeBtn = false;
            this.planModuleService.moduleWiseTab = true;
          }
          else {
            this.planModuleService.validationErrorBtn = false;
            this.planModuleService.moduleWiseTab = false;
          }
          this.getAllFiles();
        }
        this.spinner.hide();
        this.authService.idle.watch();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    }

    if (this.planModuleService.pageStatus === true) {
      this.planModuleService.getCurrentMpsPlanDetails().pipe(
        switchMap((currentMpsDetails: any) => {
          if (currentMpsDetails != null && currentMpsDetails !== undefined) {
            this.planModuleService.mpsState = currentMpsDetails;
            console.log(currentMpsDetails);
          }
          return this.planModuleService.getValidationError();
        })).pipe(
        switchMap((validationErrorResponse: any) => {
          console.log(validationErrorResponse);
          if (validationErrorResponse !== null && validationErrorResponse !== undefined) {
            this.validationErrorList = validationErrorResponse;
          }
          return this.planModuleService.validationAndDataExecutionData(this.planModuleService.mpsState.mpsName);
        })).subscribe((nextResponse: any) => {
        if (nextResponse !== null && nextResponse !== undefined) {
          this.mpsState2 = nextResponse;
          console.log(nextResponse);
          /*   let count = 0;
             for (let i = 0; i < this.mpsState2.rscIitIdpStatusDTOs.length; i++) {
               if (this.mpsState2.rscIitIdpStatusDTOs[i].validationStatus === true) {
                 count++;
                 if (count === this.mpsState2.rscIitIdpStatusDTOs.length) {
                   this.executeBtn = false;
                   this.planModuleService.moduleWiseTab = true;
                   /!* this.planModuleService.deleteHidden = true;*!/
                 }
               }
               else {
                 this.planModuleService.validationErrorBtn = false;
                 this.planModuleService.moduleWiseTab = false;
               }
             }
             console.log(this.mpsState2.rscIitIdpStatusDTOs.length);*/
          if (this.mpsState2.executionEnable === true) {
            this.executeBtn = false;
            this.planModuleService.moduleWiseTab = true;
          }
          else {
            this.planModuleService.validationErrorBtn = false;
            this.planModuleService.moduleWiseTab = false;
          }
          this.getAllFiles();
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    }

    /*   if (this.planModuleService.pageStatus === false) {
      this.planModuleService.fileValidation(this.planModuleService.mpsState.mpsName).pipe(
        switchMap((validation: any) => {
          console.log(validation);
          return this.planModuleService.getValidationError();
        })).pipe(
        switchMap((validationErrorResponse: any) => {
          console.log(validationErrorResponse);
          if (validationErrorResponse !== null && validationErrorResponse !== undefined) {
            this.validationErrorList = validationErrorResponse;
          }
          return this.planModuleService.validationAndDataExecutionData(this.planModuleService.mpsState.mpsName);
        })).subscribe((nextResponse: any) => {
        if (nextResponse !== null && nextResponse !== undefined) {
          this.mpsState2 = nextResponse;
          console.log(nextResponse);
          let count = 0;
          for (let i = 0; i < this.mpsState2.rscIitIdpStatusDTOs.length; i++) {
            if (this.mpsState2.rscIitIdpStatusDTOs[i].validationStatus == true) {
              count++;
              if (count == this.mpsState2.rscIitIdpStatusDTOs.length) {
                this.executeBtn = false;
              }
            }
            else {
              this.validationError = false;
            }
          }
          console.log(this.mpsState2.rscIitIdpStatusDTOs.length);
          this.getAllFiles();
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    }*/
    /*if (this.planModuleService.pageStatus === false) {
      this.planModuleService.fileValidation(this.planModuleService.mpsState.mpsName).pipe(
        switchMap((validation: any) => {
          console.log(validation);
          return this.planModuleService.getValidationError();
        })).pipe(
        switchMap((validationErrorResponse: any) => {
          console.log(validationErrorResponse);
          if (validationErrorResponse !== null && validationErrorResponse !== undefined) {
            this.validationErrorList = validationErrorResponse;
          }
          return this.planModuleService.validationAndDataExecutionData(this.planModuleService.mpsState.mpsName);
        })).subscribe((nextResponse: any) => {
        if (nextResponse !== null && nextResponse !== undefined) {
          this.mpsState2 = nextResponse;
          console.log(nextResponse);
          let count = 0;
          for (let i = 0; i < this.mpsState2.rscIitIdpStatusDTOs.length; i++) {
            /!* *!/
            if (this.mpsState2.rscIitIdpStatusDTOs[i].validationStatus == true) {
              count++;
              if (count == this.mpsState2.rscIitIdpStatusDTOs.length) {
                this.executeBtn = false;
              }
            }
            else {
              this.validationError = false;
            }
          }
          console.log(this.mpsState2.rscIitIdpStatusDTOs.length);
          this.getAllFiles();
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    }*/

  }

  getAllFiles() {
    this.mpsState2.rscIitIdpStatusDTOs.forEach((a) => {
      console.log(a);
      this.allFilesAvailable[a.folderName]
        ? this.allFilesAvailable[a.folderName].push(a)
        : (this.allFilesAvailable[a.folderName] = [a]);
      console.log(this.allFilesAvailable[a.folderName]);

      this.allFilesAvailable[a.folderName].sort((x, y) => {
        return (x.isRequired === y.isRequired) ? 0 : x.isRequired ? -1 : 1;
      });
    });


    Object.keys(this.allFilesAvailable).forEach((a) => {
      this.allFilesSelected[a] = this.allFilesAvailable[a].filter(
        (b) => b.required
      );
      this.selectedFileCount += this.allFilesSelected[a].length;
      this.folders.push(a);
      console.log(this.folders);
    });
  }

  goToExecution() {
    this.spinner.show();
    this.authService.idle.stop();
    this.updateValidationTag();
    this.planModuleService.getPpHeader(this.planModuleService.mpsState.mpsName, this.planModuleService.mpsState.mpsDateStr).subscribe((res: any) => {
      this.calculationService.selectedPpHeaderId = res.id;
      this.calculationService.ppHeader = res;
      this.calculationService.otifDate = res.otifDate;
      console.log(res);
      this.executionErrorList = [];
      this.consumptionApis();
      // this.spinner.hide();
      /*  this.calculationService.selectedPpHeaderId = res.id;
      this.calculationService.ppHeader = res;
      this.callApi();*/
    }, (e) => console.log(e));
  }

  updateValidationTag() {
    this.planModuleService.updateMultipleValidationTagToPseudo
    (this.planModuleService.mpsState.mpsName,
      this.planModuleService.mpsState.mpsDateStr, this.planModuleService.mpsState.tagName)
      .subscribe((res: any) => {
        if (res !== null && res !== undefined) {
          console.log(res);

        }
      }, (e) => console.log(e));
  }

  viewModule() {
    this.router.navigateByUrl('/homePage');
    this.planModuleService.pmOtifDisabled = false;
    this.planModuleService.allExecuteFilesDisabled = true;
    this.getMonthHeaderData();
    this.getHeaderData();
  }

  getMonthHeaderData() {
    this.analyzerService.getBackMonthNamesList().subscribe((bkMonthRes: any) => {
      console.log(bkMonthRes);
      if (bkMonthRes !== null && bkMonthRes !== undefined && bkMonthRes.length !== 0) {
        this.monthYearList = bkMonthRes;
        this.executionFlowPageService.backupMonthList = bkMonthRes;
        if (this.monthYearList.length > 0) {

          this.analyzerService.selectedMonth = this.monthYearList[0].monthNameAlias;
          this.analyzerService.monthNameOnZip = this.monthYearList[0].monthName;
          this.analyzerService.setselectedMonth(this.analyzerService.selectedMonth);

        }

      }
    }, (error) => {
      console.log(error);
    });
  }

  getHeaderData() {
    this.executionFlowPageService.getPlanMpsDataWithoutBackup().subscribe((headerResponse: any) => {
      console.log(headerResponse);
      if (headerResponse !== null && headerResponse !== undefined) {
        const mpsHeaderListOfData = headerResponse;
        this.mpsHeaderList = mpsHeaderListOfData.reverse();
        this.executionFlowPageService.mpsPlanHeader = this.mpsHeaderList;
        this.mpsHeaderList = mpsHeaderListOfData;
        if (this.mpsHeaderList.length > 0) {
          this.executionFlowPageService.selectedHeaderId = this.mpsHeaderList[0].id;
          this.executionFlowPageService.selectedPlanDate = this.mpsHeaderList[0].mpsDate;
          this.executionFlowPageService.selectedPlanName = this.mpsHeaderList[0].mpsName;
          this.executionFlowPageService.setselectedHeaderId(this.executionFlowPageService.selectedHeaderId);
        }
        this.getModuleName();

      }
    }, (error) => {
      console.log(error);
    });
  }

  getModuleName() {
    this.mpsHeaderList.forEach((res: any) => {
      this.moduleNamesArray = [...new Set(this.mpsHeaderList.map(item => item.moduleName))];
      /*if (res.moduleName === "ALL") {
        this.moduleNamesArray.push("ALL");
      } else {
        this.moduleNamesArray = [...new Set(this.mpsHeaderList.map(item => item.moduleName))];
      }*/
    });
    this.executionFlowPageService.moduleNameDisabled = false;

  }

  exportActive: boolean = false;

  exportAsXLSX() {
    this.exportActive = true;
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(this.table.nativeElement);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, 'Validation_Error.xlsx');
  }

  createApiErrorHandlerMethod(isConsumptionApiError?: boolean): void {
    console.log(this.executionErrorList);
    this.planModuleService.executionError = false;
    if (isConsumptionApiError === true) {
      // this.executeBtn = true;
      this.planModuleService.moduleWiseTab = false;
    }
    const executionResponseMainDTO = {
      mpsDate: this.planModuleService.mpsState.mpsDateStr,
      mpsName: this.planModuleService.mpsState.mpsName,
      executionResponseDTOs: this.executionErrorList
    }
    this.planModuleService.apiExecutionErrorExcelCreation(executionResponseMainDTO).subscribe(value => {
    }, (error) => {
      console.log(error);
    });
    document.getElementById("openCreateApiErrorModal").click();
  }

  consumptionApis(): void {
    this.spinner.show();
    this.planModuleService.getPpHeader(this.planModuleService.mpsState.mpsName, this.planModuleService.mpsState.mpsDateStr)
      .pipe(
        switchMap((res: any) => {
          this.calculationService.selectedPpHeaderId = res.id;
          this.calculationService.ppHeader = res;
          this.calculationService.otifDate = res.otifDate;
          return this.calculationService.getLogicalWip();
        })).pipe(
      switchMap((logicalWipRes: any) => {
        logicalWipRes = JSON.parse(logicalWipRes);
        console.log(logicalWipRes);
        if (logicalWipRes.isSuccess === true) {
          return this.calculationService.getGrossWip();
        } else {
          this.spinner.hide();
          this.executionErrorList.push(logicalWipRes);
          this.createApiErrorHandlerMethod(true);
          return;
        }
      })
    ).pipe(
      switchMap((grossWipRes: any) => {
        grossWipRes = JSON.parse(grossWipRes);
        console.log(grossWipRes);
        if (grossWipRes.isSuccess === true) {
          return this.calculationService.getLogical();
        } else {
          this.spinner.hide();
          this.executionErrorList.push(grossWipRes);
          this.createApiErrorHandlerMethod(true);
          return;
        }
      })
    )
      .pipe(
        switchMap((logicalRes: any) => {
          logicalRes = JSON.parse(logicalRes);
          console.log(logicalRes);
          if (logicalRes.isSuccess === true) {
            return this.calculationService.getGross();
          } else {
            this.spinner.hide();
            this.executionErrorList.push(logicalRes);
            this.createApiErrorHandlerMethod(true);
            return;
          }
        })
      )
      .pipe(
        switchMap((grossRes: any) => {
          grossRes = JSON.parse(grossRes);
          console.log(grossRes);
          if (grossRes.isSuccess === true) {
            return this.calculationService.getLogicalBp();
          } else {
            this.spinner.hide();
            this.executionErrorList.push(grossRes);
            this.createApiErrorHandlerMethod(true);
            return;
          }
        })
      )
      .pipe(
        switchMap((logicalBpRes: any) => {
          logicalBpRes = JSON.parse(logicalBpRes);
          console.log(logicalBpRes);
          if (logicalBpRes.isSuccess === true) {
            return this.calculationService.getGrossBp();
          } else {
            this.spinner.hide();
            this.executionErrorList.push(logicalBpRes);
            this.createApiErrorHandlerMethod(true);
            return;
          }
        })
      )
      .pipe(
        switchMap((grossBpRes: any) => {
          grossBpRes = JSON.parse(grossBpRes);
          console.log(grossBpRes);
          if (grossBpRes.isSuccess === true) {
            return this.calculationService.getLogicalSubBase();
          } else {
            this.spinner.hide();
            this.executionErrorList.push(grossBpRes);
            this.createApiErrorHandlerMethod(true);
            return;
          }
        })
      )
      .pipe(
        switchMap((logicalSubBaseRes: any) => {
          logicalSubBaseRes = JSON.parse(logicalSubBaseRes);
          console.log(logicalSubBaseRes);
          if (logicalSubBaseRes.isSuccess === true) {
            return this.calculationService.getGrossSubBase();
          } else {
            this.spinner.hide();
            this.executionErrorList.push(logicalSubBaseRes);
            this.createApiErrorHandlerMethod(true);
            return;
          }
        })
      )
      .pipe(
        switchMap((grossBpRes: any) => {
          grossBpRes = JSON.parse(grossBpRes);
          console.log(grossBpRes);
          if (grossBpRes.isSuccess === true) {
            return this.calculationService.getLogicalRm();
          } else {
            this.spinner.hide();
            this.executionErrorList.push(grossBpRes);
            this.createApiErrorHandlerMethod(true);
            return;
          }
        })
      )
      .pipe(
        switchMap((logicalRmRes: any) => {
          logicalRmRes = JSON.parse(logicalRmRes);
          console.log(logicalRmRes);
          if (logicalRmRes.isSuccess === true) {
            return this.calculationService.getGrossRm();
          } else {
            this.spinner.hide();
            this.executionErrorList.push(logicalRmRes);
            this.createApiErrorHandlerMethod(true);
            return;
          }
        })
      )
      .pipe(
        switchMap((grossRmRes: any) => {
          grossRmRes = JSON.parse(grossRmRes);
          console.log(grossRmRes);
          if (grossRmRes.isSuccess === true) {
            return this.calculationService.getPivotConsumptionRm();
          } else {
            this.spinner.hide();
            this.createApiErrorHandlerMethod(true);
            this.executionErrorList.push(grossRmRes);
            return;
          }
        })
      ).subscribe((pivotConsumptionRes: any) => {
      pivotConsumptionRes = JSON.parse(pivotConsumptionRes);
      console.log(pivotConsumptionRes);
      if (pivotConsumptionRes.isSuccess === true) {
        this.rmPlanApis();
        this.pmPlanApis();
        this.fgSlobApis();
      } else {
        this.spinner.hide();
        this.executionErrorList.push(pivotConsumptionRes);
        this.createApiErrorHandlerMethod(true);
        return;
      }
    }, (error) => {
      console.log(error);
    });
    ;
  }

  rmPlanApis()
    :
    void {
    this.calculationService.getRmPurchaseCalculation()
      .pipe(
        switchMap((rmPurchaseCalculationRes: any) => {
          rmPurchaseCalculationRes = JSON.parse(rmPurchaseCalculationRes);
          console.log(rmPurchaseCalculationRes);
          if (rmPurchaseCalculationRes.isSuccess === false) {
            this.executionErrorList.push(rmPurchaseCalculationRes);
          }
          return this.calculationService.getRmPurchasePlan();
        })
      )
      .pipe(
        switchMap((rmPurchaseCalculationRes: any) => {
          rmPurchaseCalculationRes = JSON.parse(rmPurchaseCalculationRes);
          console.log(rmPurchaseCalculationRes);
          if (rmPurchaseCalculationRes.isSuccess === false) {
            this.executionErrorList.push(rmPurchaseCalculationRes);
          }
          return this.calculationService.getPurchasePlanCreateFRm();
        })
      ).pipe(
      switchMap((rmPurchaseCalculationFRmRes: any) => {
        rmPurchaseCalculationFRmRes = JSON.parse(rmPurchaseCalculationFRmRes);
        console.log(rmPurchaseCalculationFRmRes);
        if (rmPurchaseCalculationFRmRes.isSuccess === false) {
          this.executionErrorList.push(rmPurchaseCalculationFRmRes);
        }
        return this.calculationService.getRmSlob();
      })
    )
      .pipe(
        switchMap((rmSlobRes: any) => {
            rmSlobRes = JSON.parse(rmSlobRes);
            console.log(rmSlobRes);
            if (rmSlobRes.isSuccess === false) {
              this.executionErrorList.push(rmSlobRes);
            }
            return this.calculationService.getRmSlobSummary();
          }
        )
      )
      .pipe(
        switchMap((rmSlobSummaryRes: any) => {
          rmSlobSummaryRes = JSON.parse(rmSlobSummaryRes);
          console.log(rmSlobSummaryRes);
          if (rmSlobSummaryRes.isSuccess === false) {
            this.executionErrorList.push(rmSlobSummaryRes);
          }
          return this.calculationService.getRmCoverDaysCalculate();
        })
      )
      .pipe(
        switchMap((rmCoverDaysRes: any) => {
          rmCoverDaysRes = JSON.parse(rmCoverDaysRes);
          console.log(rmCoverDaysRes);
          if (rmCoverDaysRes.isSuccess === false) {
            this.executionErrorList.push(rmCoverDaysRes);
          }
          return this.calculationService.getRmCoverDaysSummary();
        })
      )
      .pipe(
        switchMap((rmCoverDaysSummaryRes: any) => {
          rmCoverDaysSummaryRes = JSON.parse(rmCoverDaysSummaryRes);
          console.log(rmCoverDaysSummaryRes);
          if (rmCoverDaysSummaryRes.isSuccess === false) {
            this.executionErrorList.push(rmCoverDaysSummaryRes);
          }
          return this.calculationService.getRmCoverDaysClassSummary();
        })
      )
      .subscribe((rmCoverDaysClassSummaryRes) => {
        rmCoverDaysClassSummaryRes = JSON.parse(rmCoverDaysClassSummaryRes);
        console.log(rmCoverDaysClassSummaryRes);
        if (rmCoverDaysClassSummaryRes.isSuccess === false) {
          this.executionErrorList.push(rmCoverDaysClassSummaryRes);
        }
        this.subs++;
        this.exportExcelApi();
      }, (e) => console.log(e));
  }

  pmPlanApis()
    :
    void {
    this.calculationService.getPmPurchaseCalculation()
      .pipe(
        switchMap((pmPurchaseCalculationRes: any) => {
          pmPurchaseCalculationRes = JSON.parse(pmPurchaseCalculationRes);
          console.log(pmPurchaseCalculationRes);
          if (pmPurchaseCalculationRes.isSuccess === false) {
            this.executionErrorList.push(pmPurchaseCalculationRes);
          }
          return this.calculationService.getPmPurchasePlan();
        })
      ).pipe(switchMap((pmPurchasePlanRes: any) => {
        pmPurchasePlanRes = JSON.parse(pmPurchasePlanRes);
        console.log(pmPurchasePlanRes);
        if (pmPurchasePlanRes.isSuccess === false) {
          this.executionErrorList.push(pmPurchasePlanRes);
        }
        return this.calculationService.getPmSlob();
      })
    ).pipe(
      switchMap((pmSlobRes: any) => {
        pmSlobRes = JSON.parse(pmSlobRes);
        console.log(pmSlobRes);
        if (pmSlobRes.isSuccess === false) {
          this.executionErrorList.push(pmSlobRes);
        }
        return this.calculationService.getPmSlobSummary();
      })
    )
      .pipe(
        switchMap((pmSlobSummery: any) => {
          pmSlobSummery = JSON.parse(pmSlobSummery);
          console.log(pmSlobSummery);
          if (pmSlobSummery.isSuccess === false) {
            this.executionErrorList.push(pmSlobSummery);
          }
          return this.calculationService.getMould();
        })
      )
      .pipe(
        switchMap((mould: any) => {
          mould = JSON.parse(mould);
          console.log(mould);
          if (mould.isSuccess === false) {
            this.executionErrorList.push(mould);
          }
          return this.calculationService.getTotalMould();
        })
      )
      .pipe(
        switchMap((totalMouldRes: any) => {
          totalMouldRes = JSON.parse(totalMouldRes);
          console.log(totalMouldRes);
          if (totalMouldRes.isSuccess === false) {
            this.executionErrorList.push(totalMouldRes);
          }
          return this.calculationService.getTotalMouldSummary();
        })
      )
      .pipe(
        switchMap((totalMouldSummaryRes: any) => {
          totalMouldSummaryRes = JSON.parse(totalMouldSummaryRes);
          console.log(totalMouldSummaryRes);
          if (totalMouldSummaryRes.isSuccess === false) {
            this.executionErrorList.push(totalMouldSummaryRes);
          }
          return this.calculationService.getPmCoverDaysCalculate();
        })
      )
      .pipe(
        switchMap((pmCoverDaysRes: any) => {
          pmCoverDaysRes = JSON.parse(pmCoverDaysRes);
          console.log(pmCoverDaysRes);
          if (pmCoverDaysRes.isSuccess === false) {
            this.executionErrorList.push(pmCoverDaysRes);
          }
          return this.calculationService.getPmCoverDaysSummary();
        })
      )
      .pipe(
        switchMap((pmCoverDaysSummaryRes: any) => {
          pmCoverDaysSummaryRes = JSON.parse(pmCoverDaysSummaryRes);
          console.log(pmCoverDaysSummaryRes);
          if (pmCoverDaysSummaryRes.isSuccess === false) {
            this.executionErrorList.push(pmCoverDaysSummaryRes);
          }
          return this.calculationService.getOtifCalculation();
        })
      )
      .pipe(
        switchMap((otifCalculationRes: any) => {
          otifCalculationRes = JSON.parse(otifCalculationRes);
          console.log(otifCalculationRes);
          if (otifCalculationRes.isSuccess === false) {
            this.executionErrorList.push(otifCalculationRes);
          }
          return this.calculationService.getOtifCalculationSummary();
        })
      )
      .pipe(
        switchMap((otifCalculationSummaryRes: any) => {
          otifCalculationSummaryRes = JSON.parse(otifCalculationSummaryRes);
          console.log(otifCalculationSummaryRes);
          if (otifCalculationSummaryRes.isSuccess === false) {
            this.executionErrorList.push(otifCalculationSummaryRes);
          }
          return this.calculationService.calculatePmOtifCumulativeScore();
        })
      )
      .pipe(
        switchMap((pmOtifCumulativeScoreRes: any) => {
          pmOtifCumulativeScoreRes = JSON.parse(pmOtifCumulativeScoreRes);
          console.log(pmOtifCumulativeScoreRes);
          if (pmOtifCumulativeScoreRes.isSuccess === false) {
            this.executionErrorList.push(pmOtifCumulativeScoreRes);
          }
          return this.calculationService.getPurchasePlanCreateFPm();
        })
      )
      .subscribe((purchasePlanCreateFPmRes: any) => {
        purchasePlanCreateFPmRes = JSON.parse(purchasePlanCreateFPmRes);
        console.log(purchasePlanCreateFPmRes);
        if (purchasePlanCreateFPmRes.isSuccess === false) {
          this.executionErrorList.push(purchasePlanCreateFPmRes);
        }
        this.subs++;
        this.exportExcelApi();
      }, (e) => console.log(e));
  }

  fgSlobApis()
    :
    void {
    this.calculationService.getFgLineSaturation()
      .pipe(switchMap((fgLineSaturationRes: any) => {
        fgLineSaturationRes = JSON.parse(fgLineSaturationRes);
        console.log(fgLineSaturationRes);
        if (fgLineSaturationRes.isSuccess === false) {
          this.executionErrorList.push(fgLineSaturationRes);
        }
        return this.calculationService.getFgLineSaturationSummary();
      }))
      .pipe(switchMap((fgLineSaturationSummaryRes: any) => {
        fgLineSaturationSummaryRes = JSON.parse(fgLineSaturationSummaryRes);
        console.log(fgLineSaturationSummaryRes);
        if (fgLineSaturationSummaryRes.isSuccess === false) {
          this.executionErrorList.push(fgLineSaturationSummaryRes);
        }
        return this.calculationService.getFgSkidSaturation();
      }))
      .pipe(switchMap((fgSkidSaturationRes: any) => {
        fgSkidSaturationRes = JSON.parse(fgSkidSaturationRes);
        console.log(fgSkidSaturationRes);
        if (fgSkidSaturationRes.isSuccess === false) {
          this.executionErrorList.push(fgSkidSaturationRes);
        }
        return this.calculationService.getFgSkidSaturationSummary();
      }))
      .pipe(switchMap((fgSkidSaturationSummaryRes: any) => {
        fgSkidSaturationSummaryRes = JSON.parse(fgSkidSaturationSummaryRes);
        console.log(fgSkidSaturationSummaryRes);
        if (fgSkidSaturationSummaryRes.isSuccess === false) {
          this.executionErrorList.push(fgSkidSaturationSummaryRes);
        }
        return this.calculationService.getSlobCreateFg();
      }))
      .pipe(switchMap((fgSlobRes: any) => {
        fgSlobRes = JSON.parse(fgSlobRes);
        console.log(fgSlobRes);
        if (fgSlobRes.isSuccess === false) {
          this.executionErrorList.push(fgSlobRes);
        }
        return this.calculationService.getSlobSummaryFgDivisionWise();
      }))
      .pipe(switchMap((slobSummaryFgDivisionWiseRes: any) => {
        slobSummaryFgDivisionWiseRes = JSON.parse(slobSummaryFgDivisionWiseRes);
        console.log(slobSummaryFgDivisionWiseRes);
        if (slobSummaryFgDivisionWiseRes.isSuccess === false) {
          this.executionErrorList.push(slobSummaryFgDivisionWiseRes);
        }
        return this.calculationService.getSlobSummaryFgTypeWise();
      }))
      .pipe(switchMap((slobSummaryFgTypeWiseRes: any) => {
        slobSummaryFgTypeWiseRes = JSON.parse(slobSummaryFgTypeWiseRes);
        console.log(slobSummaryFgTypeWiseRes);
        if (slobSummaryFgTypeWiseRes.isSuccess === false) {
          this.executionErrorList.push(slobSummaryFgTypeWiseRes);
        }
        return this.calculationService.getSlobSummaryCreateFg();
      }))
      .subscribe((fgSlobSummaryRes: any) => {
        fgSlobSummaryRes = JSON.parse(fgSlobSummaryRes);
        console.log(fgSlobSummaryRes);
        if (fgSlobSummaryRes.isSuccess === false) {
          this.executionErrorList.push(fgSlobSummaryRes);
        }
        this.subs++;
        this.exportExcelApi();
      }, (e) => {
        console.log(e);
        this.spinner.hide();
        /* this.idpService.spinnerOn = false;*/
      });
  }

  exportExcelApi()
    :
    void {
    if (this.subs === 3) {
      console.log(this.executionErrorList);
      const requestMpsAndFg = this.calculationService.excelMpsAndFgExport();
      const requestPm = this.calculationService.excelPmExport();
      const requestRm = this.calculationService.excelRmExport();
      const requestAnnualExcelAnalysis = this.calculationService.excelAllAnnualAnalysisExport();
      const requestArray = [];
      if (this.planModuleService.mpsState.isExecutionDone === null || this.planModuleService.mpsState.isExecutionDone === false) {
        requestArray.push(requestPm);
        requestArray.push(requestRm);
        requestArray.push(requestAnnualExcelAnalysis);
      }
      requestArray.push(requestMpsAndFg);
      this.spinner.show();
      forkJoin(requestArray).pipe(
        switchMap((res: any) => {
          console.log(res);
          const ppHeader = this.calculationService.ppHeader;
          ppHeader[`isExecutionDone`] = true;
          this.authService.idle.watch();
          return this.planModuleService.savePpHeader(ppHeader);
        })
      ).pipe(switchMap(
        (res: any) => {
          console.log(res);
          this.tagNameIsValidated = res.tagDescription;
         /* if (this.tagNameIsValidated === 'Validated') {
            this.pmVendorMangementApis();
            this.rmVendorMangementApis();
          }*/
          this.spinner.hide();
          if (this.executionErrorList.length > 0) {
            this.createApiErrorHandlerMethod();
          }
          return this.planModuleService.setExecutionStatusInRscIitIdpStatus(this.mpsState2);
        })
      ).pipe(switchMap(
        (res: any) => {
          console.log(res);
          this.subs = 0;
          /* this.idpService.spinnerOn = false;*/
          document.getElementById("openExecutionModal").click();
          return this.authService.showWarning;
        })
      )/*.pipe(switchMap(
        (res: any) => {
          console.log(res);
          if (res === 1) {
            document.getElementById('closemodelbuttoncsp').click();
          }
          return this.planModuleService.setExecutionStatusInRscIitIdpStatus(this.mpsState2);
        })
      )*/.subscribe(
        (res: any) => {
          console.log(res);
          if (res === 1) {
            document.getElementById('closemodelbuttoncsp').click();
          }
          this.subs = 0;
          this.spinner.hide();
          /* this.idpService.spinnerOn = false;*/
        },
        (error) => {
          /*   this.idpService.spinnerOn = false;*/
          this.spinner.hide();
          this.errorMsg = 'Something Went Wrong While Creating Reports.';
          console.log(error);
        });

    }
  }

  /*callApi(): void {
    this.consumptionApis();
  }*/
}
