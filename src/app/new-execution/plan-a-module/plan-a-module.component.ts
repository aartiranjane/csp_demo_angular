import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {ExecutionFlowPageServiceService} from "../execution-flow-page-service.service";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {NgxSpinnerService} from "ngx-spinner";
import {PlanModuleService} from "./plan-module.service";
import {switchMap} from "rxjs/operators";
import {AuthService} from "../../auth.service";

@Component({
  selector: 'app-plan-a-module',
  templateUrl: './plan-a-module.component.html',
  styleUrls: ['./plan-a-module.component.css']
})
export class PlanAModuleComponent implements OnInit, AfterViewChecked {
  planNameList: any = [];
  editPlanManagementForm: FormGroup;
  orgList: any = [];
  // duplicateName = false;
  planDataForm = new FormGroup({
    planDataFormArray: new FormArray([])
  });
  planNameListToSave: Record<string, any>[] = [];
  isEdit = false;
  isPlanNameValid = true;
  successMessage = '';
  errorMessage = '';
  invalidPlanId;
  duplicatePlanName = false;
  loginModuleName: any;
  tagNameList: any = [];
  changedRowPlanList: string[] = [];
  highlightEditValue = {
    editSelection: false
  };
  saveButtonDisabled = true;
  userName: any;
  moduleName: any;
  adminName: any;
  rowIndexValue: any;
  selectedTag: any;
  selectedRow: any;
  duplicateName: any;
  selectedDocumentType = [];
  singleSelectDropdownSettings: any;
  tagNameAllList: any = [];
  editMode = false;
  defaultSelectedTag: any;
  tagStatusChange=false;
  tagListBasedOnCon:any=[];
  duplicateEditName:any;
  error='';

  constructor(private authService: AuthService,
              private router: Router,
              public planModuleService: PlanModuleService,
              private spinner: NgxSpinnerService, private changeRef: ChangeDetectorRef,
              private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
    this.userName = this.executionFlowPageService.userName;
    this.moduleName = this.executionFlowPageService.selectedModuleName;
    this.spinner.show();
    this.planModuleService.getPlanModuleTagData().pipe(
      switchMap((tagDataList: any) => {
        console.log(tagDataList);
        this.tagNameList = tagDataList;
        /* this.tagNameList = tagDataList;
         this.tagNameList.forEach(value => {
           this.onChange(value);
         });*/
        /* if (tagDataList.length !== 0) {
           tagDataList.forEach((orgType, index) => {
             this.tagNameAllList.push({id: index, itemName: orgType});
           });
         }*/
        if (tagDataList !== null && tagDataList !== undefined) {
          tagDataList.forEach((res: any) => {
            this.tagNameAllList.push({id: res.id, itemName: res.description});
          });
          console.log(this.tagNameAllList);
        }

        return this.executionFlowPageService.getPlanMpsDataWithoutBackup();
      })).subscribe((planNameList: any) => {
      if (planNameList !== null && planNameList !== undefined) {
        this.planNameList = planNameList.reverse();
        console.log( this.planNameList[0]);
      }

      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
   /*  Validators.pattern(/^[\w\s]+$/), Validators.pattern(/^\S*$/),   Validators.pattern(/^[a-zA-Z][a-zA-Z0-9\s]*$/) */
    this.editPlanManagementForm = new FormGroup({
      id: new FormControl(''),
      mpsName: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[a-zA-Z][a-zA-Z0-9\s_]*$/),
        Validators.maxLength(25), Validators.minLength(5)]),
      mpsDate: new FormControl(''),
      tagDescription: new FormControl(''),
    });

    this.singleSelectDropdownSettings = {
      text: 'Select',
      enableSearchFilter: false,
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class'
    };


    /*  this.orgList = this.originalPlanNameList;*/

  }
  onItemSelect(selected:any){
    console.log(selected);
  }
  OnItemDeSelect(evt:any){
    console.log(evt);
    this.selectedDocumentType.push(this.tagNameAllList.find(type => type.itemName === evt.tagDescription));

    console.log(this.selectedDocumentType);
  }
getPlanDataList(){
   this.planNameList=[];
  this.spinner.show();
  this.executionFlowPageService.getPlanMpsDataWithoutBackup().subscribe((res: any) => {
    if (res !== null && res !== undefined) {
      this.planNameList = res.reverse();
      console.log(this.planNameList[0]);
  }
   this.spinner.hide();
  }, (error) => {
  this.spinner.hide();
  console.log(error);
});
}
  /*isEditing: boolean = false;
  enableEditIndex = null;

  switchEditMode(i) {
    this.selectedRow=i;
    this.isEditing = true;
    this.enableEditIndex = i;
  }

  save() {
    this.isEditing = false;
    this.enableEditIndex = null;
  }

  cancel() {
    this.isEditing = false;
    this.enableEditIndex = null;
  }

  planValidate(planName: any): boolean {
    return planName.length >= 5;
  }

  checkUniqueName(evt:any){
    console.log(this.planNameList);
    console.log(this.orgList);
    console.log(this.orgList[this.selectedRow].mpsName);
    console.log(evt);
  /!*  console.log(this.planNameList[this.selectedRow].mpsName)*!/
    if (evt === this.orgList[this.selectedRow].mpsName) {
      return;
    } else {
      this.planModuleService
        .isUniquePlanNameForEdit(evt)
        .subscribe(
          (res) => {
            this.duplicateName = res;
            this.checkName(evt);
          },
          (e) => console.log(e)
        )
    }
  }

  checkName(planName) {
    if (this.planValidate(planName) === false) {
      this.errorMessage = "minimum 5 characters required";
      // this.errorMessage = "already taken";
    } else if (this.duplicateName === true) {
      this.errorMessage = "already taken";
    } else {
      this.saveButtonDisabled = false;
    }
  }

*/
  /* checkDuplicatePlanName(event: any) {
     console.log(event);
     if (event.mpsName.value !== event.originalPlanName.value) {
       this.editDuplicateName(event.mpsName.value);
       if (this.duplicatePlanName === false) {
         this.errorMessage = '';
         if (this.planNameListToSave.length > 0) {
           this.saveEnable = false;
         }
       } else {
         this.errorMessage = 'Plan name already exist.';
         this.saveEnable = true;
       }
     } else {
       this.errorMessage = 'Plan name not changed.';
       this.saveEnable = true;
     }
   }*/

  /*editDuplicateName(planName: any) {
    console.log(planName);
    this.planModuleService
      .isUniquePlanName(planName.mpsName)
      .subscribe(
        (res) => {
          console.log(res);
          console.log(this.planDataForm);
          this.duplicatePlanName = res;
          // this.checkName();
        },
        (e) => console.log(e)
      );
  }

  onChange(newValue) {
    console.log(newValue);
    this.selectedTag = newValue;
  }*/


  onSave() {
    this.spinner.show();
    this.executionFlowPageService.getPlanMpsDataWithoutBackup().subscribe((res: any) => {
      if (res !== null && res !== undefined && res.length>0) {
        if (this.selectedDocumentType.length !== 0) {
          this.editPlanManagementForm.value.tagDescription = this.selectedDocumentType[0].itemName;
        }
          const temp = this.editPlanManagementForm.value.id;
          const index = this.planNameList.findIndex(documentFromList => {
            return documentFromList.id === temp;
          });
          if (index !== -1) {
            this.planNameList[index].mpsName = this.editPlanManagementForm.value.mpsName;
            if( this.planNameList[index].tagDescription !==this.editPlanManagementForm.value.tagDescription){
              this.tagStatusChange=true;
              this.planNameList[index].tagDescription = this.editPlanManagementForm.value.tagDescription;
              this.tagNameList.findIndex(value=>{
                if( value.description=== this.editPlanManagementForm.value.tagDescription){
                  this.planNameList[index].tagId = value.id;
                  this.planNameList[index].tagName = value.name;
                }
              })
            }
          }
          console.log(index);
          console.log("plan to be save."+ JSON.stringify( this.planNameList[index] ) );
          this.planModuleService.updatePlanManagement( this.tagStatusChange,this.planNameList[index]).subscribe((res: any) => {
            console.log(res);
            this.tagStatusChange = false;
            this.getPlanDataList();

          }, (error) => {
            console.log(error);
            this.spinner.hide();
          });
        }
    });
    this.successMessage = this.editMode ? 'Plan updated successfully' : '';
    setTimeout(() => {
      this.successMessage = '';
    }, 2000);

    this.editMode = false;


   /* if( this.editPlanValidate()=== false){
      this.error = 'min 5 character is required'}
      else{
      this.error = ' ' ;
    }*/
  }

  editDocument(document: any, i) {
    this.duplicateEditName=false;
    this.selectedRow=i;
    this.tagListBasedOnCon=[];
    console.log(document);
    this.defaultSelectedTag = document.tagDescription;
    this.editMode = true;
    this.getTagListBasedOnCondition();
    this.selectedDocumentType = [];
    this.selectedDocumentType.push(this.tagNameAllList.find(type => type.itemName === document.tagDescription));
    console.log(this.selectedDocumentType);
    this.editPlanManagementForm.patchValue({
      id: document.id,
      mpsName: document.mpsName

    });
    /* this.editPlanManagementForm.value.mpsName.setValue(document.mpsName);*/
    /* if (this.editPlanManagementForm !== undefined) {
       console.log(document);

      /!* *!/


     }*/
    console.log(this.editPlanManagementForm.value.mpsName);
  }

  getTagListBasedOnCondition(){
    if(this.defaultSelectedTag==="Pseudo First Cut")
    {
      this.tagNameList.forEach((res: any) => {
        if(res.description==="First Cut"){
          this.tagListBasedOnCon.push({id: res.id, itemName: res.description});
        }
      });
    }

    else if(this.defaultSelectedTag==="Pseudo Validated")
    {
      this.tagNameList.forEach((res: any) => {
        if(res.description==="Validated"){
          this.tagListBasedOnCon.push({id: res.id, itemName: res.description});
        }
      });
    }
   /* else{
      this.tagNameList.forEach((res: any) => {
          this.tagListBasedOnCon.push({id: res.id, itemName: res.description});
      });
    }*/
  }

  closeEditBtn(){
    this.tagListBasedOnCon=[];
  }


  editDuplicateName() {
    this.planModuleService
      .isUniqueMpsPlanManagementName(this.editPlanManagementForm.value.mpsName,this.editPlanManagementForm.value.id)
      .subscribe(
        (res) => {
          console.log(res);
          this.duplicateEditName = res;
        },
        (e) => console.log(e)
      );
  }
  editPlanValidate(): boolean {

    return this.editPlanManagementForm.value.mpsName.length >= 5;

  }

  logOut() {
    this.router.navigateByUrl('/login');
    this.executionFlowPageService.selectedModuleName = undefined;
    this.authService.idle.stop();
  }
}
