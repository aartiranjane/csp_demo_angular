import { Component, OnInit } from '@angular/core';
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-go-to-plan-module',
  templateUrl: './go-to-plan-module.component.html',
  styleUrls: ['./go-to-plan-module.component.css']
})
export class GoToPlanModuleComponent implements OnInit {
  model: NgbDateStruct;
  currentdate = new Date();

  singleselectedItems: any = [];
  singledropdownSettings = {};
  tagList:any=[];
  constructor() { }

  ngOnInit(): void {
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };
  }
  getTagData(item:any){}
  onItemDeSelect(){}

  getDate(date:any){}
}
