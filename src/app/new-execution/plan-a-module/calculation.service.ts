import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UriService} from "../../uri.service";
import {Observable} from 'rxjs';
import {ExecutionFlowPageServiceService} from "../execution-flow-page-service.service";

@Injectable({
  providedIn: 'root'
})
export class CalculationService {
  ppHeader = {};
  selectedPpHeaderId = 1;
  otifDate = "";
  endpoint = this.uriService.getResourceServerUri();

  constructor(private http: HttpClient, private uriService: UriService, private executionFlowPageService: ExecutionFlowPageServiceService) {
  }

  getLogicalSubBase(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/logical-consumption/create/sub-base/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getGrossSubBase(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/gross-consumption/create/sub-base/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getLogicalWip(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/logical-consumption/create/wip/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getGrossWip(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/gross-consumption/create/wip/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getLogical(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/logical-consumption/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getGross(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/gross-consumption/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getLogicalBp(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/logical-consumption/create/bp/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getGrossBp(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/gross-consumption/create/bp/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getLogicalRm(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/logical-consumption/create/rm/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getGrossRm(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/gross-consumption/create/rm/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPmPurchaseCalculation(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-purchase-plan-calculations/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPmPurchasePlan(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-purchase-plan/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getRmPurchaseCalculation(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-purchase-plan-calculations/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getRmPurchasePlan(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-purchase-plan/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPmSlob(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-slob/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPmSlobSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-slob-summary/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getOriginalMould(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/original-mould-saturation/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getOriginalTotalMould(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rsc-iot-original-total-mould-saturation/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getMould(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/latest-mould-saturation/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getTotalMould(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/latest-total-mould-saturation/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getTotalMouldSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/latest-total-mould-saturation-summary/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPmCoverDaysCalculate(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-cover-days/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPmCoverDaysSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-cover-days-summary/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getRmSlob(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-slob/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getRmSlobSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-slob-summary/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  excelMpsAndFgExport(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/all/excel/export/mpsAndFg/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  excelPmExport(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/all/excel/export/pm/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  excelRmExport(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/all/excel/export/rm/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }
  excelAllAnnualAnalysisExport(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/all/excel/export/mpsTriangleAnalysisExcel/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getFgSkidSaturation(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/fg-skid-saturation/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getFgLineSaturation(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/fg-line-saturation/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getFgSkidSaturationSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/fg-skid-saturation-summary/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getFgLineSaturationSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/fg-line-saturation-summary/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getOtifCalculation(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-otif-calculation/calculate/' + this.otifDate}`, {responseType: 'text'});
  }

  getOtifCalculationSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/pm-otif-summary/calculate/' + this.otifDate}`, {responseType: 'text'});
  }

  calculatePmOtifCumulativeScore(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/otif-cumulative/create'}`, {responseType: 'text'});
  }

  getRmCoverDaysCalculate(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-cover-days/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getRmCoverDaysSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-cover-days-summary/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getRmCoverDaysClassSummary(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-cover-days-class-summary/calculate/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getSlobCreateFg(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/slob/create/fg/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getSlobSummaryFgDivisionWise(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/slob-summary/fg/division-wise/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getSlobSummaryFgTypeWise(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/slob-summary/fg/type-wise/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getSlobSummaryCreateFg(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/slob-summary/fg/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPurchasePlanCreateFPm(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/purchase-plan/cover-days/pm/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPurchasePlanCreateFRm(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/purchase-plan/cover-days/rm/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getPivotConsumptionRm(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/rm-pivot-consumption/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

  getMpsPlanTotalQty(): Observable<any> {
    return this.http.get(`${this.endpoint + '/api/mps-plan-total-quantity/create/' + this.selectedPpHeaderId}`, {responseType: 'text'});
  }

}
