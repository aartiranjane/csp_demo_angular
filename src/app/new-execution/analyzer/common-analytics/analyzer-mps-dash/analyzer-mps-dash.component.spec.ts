import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyzerMpsDashComponent } from './analyzer-mps-dash.component';

describe('AnalyzerMpsDashComponent', () => {
  let component: AnalyzerMpsDashComponent;
  let fixture: ComponentFixture<AnalyzerMpsDashComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalyzerMpsDashComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyzerMpsDashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
