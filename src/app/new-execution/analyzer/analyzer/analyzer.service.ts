import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {UriService} from "../../../uri.service";
import {catchError, map} from "rxjs/operators";
import {BehaviorSubject, Subject, throwError} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AnalyzerService {
  headers = [];
  selectedMonth!:string;
  monthNameOnZip:string;
  /*analyzerMpsId!:number;*/
  endpoint = this.uriService.getResourceServerUri();
  public subject = new Subject<any>();
  constructor(private http: HttpClient, private uriService: UriService) {
  }

  setselectedMonth(selectedMonth: string) {
    this.updatedMonth.next(selectedMonth);
  }

  private updatedMonth = new BehaviorSubject(this.selectedMonth);
  getMonth = this.updatedMonth.asObservable();

  getAllBackUpFileNamesData(monthName: string) {
    return this.http.get(this.endpoint + '/api/all/backup/export/filenames/' + monthName, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllBackUpExcelExportDownload(monthYearValue: string, firstCutCheckedList: any, validatedCheckedList: any,pmOtifCheckedList: any, allModuleCheckedList: any) {
    return this.http.get(`${this.endpoint + '/api/download/backup-files/zip/' + monthYearValue + "/" + firstCutCheckedList.toString() + "/" + validatedCheckedList.toString() + "/"  + pmOtifCheckedList.toString() + "/" + allModuleCheckedList.toString()}`, {responseType: 'blob'});
  }


  getAllExcelExportFileNamesData(analyzerMpsId: any) {
    return this.http.get(this.endpoint + '/api/all/excel/export/filenames/'  + analyzerMpsId, httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  getAllSelectedExcelExport(analyzerMpsId: any, fgCheckedList: any, pmCheckedList: any,pmOtifCheckedList: any, rmCheckedList: any) {
    console.log(fgCheckedList);
    return this.http.get(`${this.endpoint + '/api/download/files/' + analyzerMpsId + "/" + fgCheckedList.toString() + "/" + pmCheckedList.toString() + "/"  + pmOtifCheckedList.toString() + "/" + rmCheckedList.toString()}`, {responseType: 'blob'});
  }

  getBackMonthNamesList(){
    return this.http.get(this.endpoint + '/api/all-month-year/backup', httpOptions).pipe(map((response: object) => response),
      catchError(this.errorHandler));
  }

  errorHandler(error: Response) {
    return throwError(error || 'Server Error');
  }
}
