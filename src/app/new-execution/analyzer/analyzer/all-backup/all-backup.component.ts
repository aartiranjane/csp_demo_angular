import {AfterViewChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {AnalyzerService} from "../analyzer.service";
import {PmServiceService} from "../../../new-pm/pm-service.service";
import {saveAs} from "file-saver";

@Component({
  selector: 'app-all-backup',
  templateUrl: './all-backup.component.html',
  styleUrls: ['./all-backup.component.css']
})
export class AllBackupComponent implements OnInit , AfterViewChecked {

  firstTabClicked = true;
  secondTabClicked = false;
  thirdTabClicked = false;

  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private spinner: NgxSpinnerService, private analyzerService: AnalyzerService, private pmServiceService: PmServiceService,
              private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {

    document.getElementById('nav-org-tabb').click();
    /*   this.spinner.show();*/
  }






}
