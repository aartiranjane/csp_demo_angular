import {Component, OnDestroy, OnInit} from '@angular/core';
import {AnalyzerService} from "../analyzer.service";
import {NgxSpinnerService} from "ngx-spinner";
import {saveAs} from "file-saver";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-backup-pm-otif',
  templateUrl: './backup-pm-otif.component.html',
  styleUrls: ['./backup-pm-otif.component.css']
})
export class BackupPmOtifComponent implements OnInit,OnDestroy  {
  $subs:Subscription;
  isDownloadOtif = true;
  otifSelectedAll: any;
  pmOtifZipList: any=[];
  otifSelectedAll2: any;
  otifCheckedAllList = {
    firstCutCheckedList: [],
    validatedCheckedList: [],
    pmOtifCheckedList:[],
    allModuleCheckedList:[]
  };
  otifSelectedExports: any[] = [];
  constructor(private analyzerService: AnalyzerService,private spinner:NgxSpinnerService) { }

  ngOnInit(): void {

    this.$subs=this.analyzerService.getMonth.subscribe(resValue => {
    if (resValue !== undefined && resValue !== null) {
      this.analyzerService.selectedMonth = resValue;
    }
    this.spinner.show();
    this.pmOtifZipList = [];
    this.analyzerService.getAllBackUpFileNamesData(this.analyzerService.selectedMonth).subscribe((allExcelExportFileNamesRes: any) => {
      if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
        console.log(allExcelExportFileNamesRes);
        for (let i = 0; i < allExcelExportFileNamesRes.pmOtifZipList.length; i++) {
          this.pmOtifZipList.push({name: allExcelExportFileNamesRes.pmOtifZipList[i], selected: false});
        }
        console.log(this.pmOtifZipList);
      }
      this.spinner.hide();
    }, (error) => {
      this.spinner.hide();
      console.log(error);
    });
  });

}
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  otifSelectAll() {
    for (var i = 0; i < this.pmOtifZipList.length; i++) {
      this.pmOtifZipList[i].selected = this.otifSelectedAll;
    }
    console.log("selectedAll 1 =" + this.otifSelectedAll);
    this.getCheckedItemListOtif();
  }

  checkIfAllSelectedOtif() {
    this.otifSelectedAll2 = this.pmOtifZipList.every((items: any) => {
      return items.selected === true;
    });
    if (this.otifSelectedAll2 === true) {
      this.otifSelectedAll = true;
    } else {
      this.otifSelectedAll = false;
    }
    this.getCheckedItemListOtif();
  }

  getCheckedItemListOtif() {
    this.otifCheckedAllList.pmOtifCheckedList = [];
    this.isDownloadOtif = true;
    for (var i = 0; i < this.pmOtifZipList.length; i++) {
      if (this.pmOtifZipList[i].selected) {
        this.otifCheckedAllList.pmOtifCheckedList.push(this.pmOtifZipList[i].name);
        this.isDownloadOtif = false;
      }
    }
  }

  downloadOtif() {
    let fileName;
    if (this.otifSelectedExports.length === 1) {
      fileName = this.otifSelectedExports.toString();
    }
    if (this.otifCheckedAllList.pmOtifCheckedList.length === 1) {
      if (this.otifCheckedAllList.pmOtifCheckedList.length === 1) {
        fileName = this.otifCheckedAllList.pmOtifCheckedList[0];
      }
    }
    else{
      fileName = this.analyzerService.monthNameOnZip;
    }
    if (this.otifCheckedAllList.firstCutCheckedList.length <= 0) {
      this.otifCheckedAllList.firstCutCheckedList.push("null");
    }
    if (this.otifCheckedAllList.allModuleCheckedList.length <= 0) {
      this.otifCheckedAllList.allModuleCheckedList.push("null");
    }
    if (this.otifCheckedAllList.validatedCheckedList.length <= 0) {
      this.otifCheckedAllList.validatedCheckedList.push("null");
    }

    this.analyzerService.getAllBackUpExcelExportDownload(this.analyzerService.selectedMonth, this.otifCheckedAllList.firstCutCheckedList, this.otifCheckedAllList.validatedCheckedList,
      this.otifCheckedAllList.pmOtifCheckedList, this.otifCheckedAllList.allModuleCheckedList).subscribe((updatedPmSlobListResponse: any) => {
      this.spinner.hide();
      console.log(updatedPmSlobListResponse);
      if (updatedPmSlobListResponse.type === "application/vnd.zip") {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
        saveAs(blob, fileName);
      } else {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
        saveAs(blob, fileName);
      }
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
    this.otifSelectedExports = [];
  }


}
