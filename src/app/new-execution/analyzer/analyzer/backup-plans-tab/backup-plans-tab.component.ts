import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {AnalyzerService} from "../analyzer.service";
import {PmServiceService} from "../../../new-pm/pm-service.service";
import {saveAs} from "file-saver";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-backup-plans-tab',
  templateUrl: './backup-plans-tab.component.html',
  styleUrls: ['./backup-plans-tab.component.css']
})
export class BackupPlansTabComponent implements OnInit , AfterViewChecked,OnDestroy {
  $subs:Subscription;
  isDisabled = true;
  folders = [
    "First Cut Plan",
    "Validated Plan",
  ];

  selectedAll: any;
  names: any;
  monthSelectList: any = [];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  analyzerMpsId: any;
  selected: boolean = false;
  checkedAllList = {
    firstCutCheckedList: [],
    validatedCheckedList: [],
    pmOtifCheckedList:[],
    allModuleCheckedList:[]
  };
  selectedExports: any[] = [];
  firstCutFileList:any=[];
  validatedFileList:any=[];
  selectedAll1: any;
  selectedAll2: any;

  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private spinner: NgxSpinnerService, private analyzerService: AnalyzerService, private pmServiceService: PmServiceService,
              private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {

    document.getElementById('nav-org-tabb').click();
    /*   this.spinner.show();*/
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };
    this.$subs=this.analyzerService.getMonth.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.analyzerService.selectedMonth = resValue;
      }
      this.spinner.show();
      this.firstCutFileList = [];
      this.validatedFileList=[];
      this.analyzerService.getAllBackUpFileNamesData(this.analyzerService.selectedMonth).subscribe((allExcelExportFileNamesRes: any) => {
        if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
          console.log(allExcelExportFileNamesRes);
          for (let i = 0; i < allExcelExportFileNamesRes.planZipList.firstCutFileList.length; i++) {
            this.firstCutFileList.push({name: allExcelExportFileNamesRes.planZipList.firstCutFileList[i], selected: false});
          }
          for (let i = 0; i < allExcelExportFileNamesRes.planZipList.validatedFileList.length; i++) {
            this.validatedFileList.push({name: allExcelExportFileNamesRes.planZipList.validatedFileList[i], selected: false});
          }
          console.log(this.firstCutFileList);
        }
        this.spinner.hide();
      }, (error) => {
        this.spinner.hide();
        console.log(error);
      });
    });
  }
  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }
  /*onItemSelect(item: any) {
    /!*  this.analyzerMpsId = item.id;*!/
    /!*    this.fgFileDataList = [];
        this.pmFileDataList = [];
        this.rmFileDataList = [];
        this.analyzerService.getAllExcelExportFileNamesData(this.analyzerMpsId).subscribe((allExcelExportFileNamesRes: any) => {
          if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
            /!*this.fgList = allExcelExportFileNamesRes?.fgFileList;*!/
            for (let i = 0; i < allExcelExportFileNamesRes.fgFileList.length; i++) {
              this.fgFileDataList.push({name: allExcelExportFileNamesRes.fgFileList[i], selected: false});
            }
            /!*  this.pmList = allExcelExportFileNamesRes?.pmFileList;*!/
            for (let i = 0; i < allExcelExportFileNamesRes.pmFileList.length; i++) {
              this.pmFileDataList.push({name: allExcelExportFileNamesRes.pmFileList[i], selected: false});
            }
            /!* this.rmList = allExcelExportFileNamesRes?.rmFileList;*!/
            for (let i = 0; i < allExcelExportFileNamesRes.rmFileList.length; i++) {
              this.rmFileDataList.push({name: allExcelExportFileNamesRes.rmFileList[i], selected: false});
            }

          }
        }, (error) => {
          console.log(error);
        });*!/
  }*/

  /*onItemDeSelect() {
    /!*  this.fgFileDataList = [];
      this.pmFileDataList = [];
      this.rmFileDataList = [];*!/
  }*/

  selectAll() {
    for (var i = 0; i < this.firstCutFileList.length; i++) {
      this.firstCutFileList[i].selected = this.selectedAll;
    }
    console.log("selectedAll 1 =" + this.selectedAll);
    for (var i = 0; i < this.validatedFileList.length; i++) {
      this.validatedFileList[i].selected = this.selectedAll;
    }
    this.getCheckedItemList();
  }

  checkIfAllSelected() {
    this.selectedAll1 = this.firstCutFileList.every((items: any) => {
      return items.selected === true;
    });
    this.selectedAll2 = this.validatedFileList.every((items: any) => {
      return items.selected === true;
    });
    if (this.selectedAll1 === true && this.selectedAll2 === true) {
      this.selectedAll = true;
    } else {
      this.selectedAll = false;
    }
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedAllList.firstCutCheckedList = [];
    this.checkedAllList.validatedCheckedList = [];

    this.isDisabled = true;
    for (var i = 0; i < this.firstCutFileList.length; i++) {
      if (this.firstCutFileList[i].selected) {
        this.checkedAllList.firstCutCheckedList.push(this.firstCutFileList[i].name);
        this.isDisabled = false;
      }
    }
    for (var i = 0; i < this.validatedFileList.length; i++) {
      if (this.validatedFileList[i].selected) {
        this.checkedAllList.validatedCheckedList.push(this.validatedFileList[i].name);
        this.isDisabled = false;
      }
    }

  }

  download() {
    let fileName;
    if (this.selectedExports.length === 1) {
      fileName = this.selectedExports.toString();
    }
    if (this.checkedAllList.firstCutCheckedList.length + this.checkedAllList.validatedCheckedList.length == 1) {
      if (this.checkedAllList.firstCutCheckedList.length === 1) {
        fileName = this.checkedAllList.firstCutCheckedList[0];
      }
      if (this.checkedAllList.validatedCheckedList.length === 1) {
        fileName = this.checkedAllList.validatedCheckedList[0];
      }
    }
    else{
      fileName = this.analyzerService.monthNameOnZip;
    }
    if (this.checkedAllList.firstCutCheckedList.length <= 0) {
      this.checkedAllList.firstCutCheckedList.push("null");
    }
    if (this.checkedAllList.validatedCheckedList.length <= 0) {
      this.checkedAllList.validatedCheckedList.push("null");
    }
    if (this.checkedAllList.pmOtifCheckedList.length <= 0) {
      this.checkedAllList.pmOtifCheckedList.push("null");
    }
    if (this.checkedAllList.allModuleCheckedList.length <= 0) {
      this.checkedAllList.allModuleCheckedList.push("null");
    }
    this.analyzerService.getAllBackUpExcelExportDownload(this.analyzerService.selectedMonth, this.checkedAllList.firstCutCheckedList, this.checkedAllList.validatedCheckedList,
      this.checkedAllList.pmOtifCheckedList, this.checkedAllList.allModuleCheckedList).subscribe((updatedPmSlobListResponse: any) => {
      this.spinner.hide();
      console.log(updatedPmSlobListResponse);
      if (updatedPmSlobListResponse.type === "application/vnd.zip") {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
        saveAs(blob, fileName);
      } else {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
        saveAs(blob, fileName);
      }
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
    this.selectedExports = [];
  }

  fillList(list: any) {
    list.forEach((value) => {
      if (value.selected) {
        const temp = value.name;
        this.selectedExports.push(temp)
      }
    });
  }
}
