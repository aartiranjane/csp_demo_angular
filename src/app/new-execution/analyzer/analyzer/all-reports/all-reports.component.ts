import {AfterViewChecked, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {ExecutionFlowPageServiceService} from "../../../execution-flow-page-service.service";
import {NgxSpinnerService} from "ngx-spinner";
import {AnalyzerService} from "../analyzer.service";
import {PmServiceService} from "../../../new-pm/pm-service.service";
import {saveAs} from "file-saver";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-all-reports',
  templateUrl: './all-reports.component.html',
  styleUrls: ['./all-reports.component.css']
})
export class AllReportsComponent implements OnInit, AfterViewChecked ,OnDestroy {
  $subs:Subscription;
  isDisabled = true;
  folders = [
    "FG",
    "PM",
    "RM",
  ];
  fgList: any = [];
  pmList: any = [];
  rmList: any = [];
  selectedAll: any;
  selectedAll1: any;
  selectedAll2: any;
  selectedAll3: any;
  names: any;
  mpsSelectList: any = [];
  singleselectedItems: any = [];
  singledropdownSettings = {};
  analyzerMpsId: any;
  pmFileDataList = [];
  fgFileDataList = [];
  rmFileDataList = [];
  selected: boolean = false;
  checkedAllList = {
    fgCheckedList: [],
    pmCheckedList: [],
    pmOtifCheckedList:[],
    rmCheckedList: []
  };
  selectedExports: any[] = [];
  selectedPlanName:any;

  constructor(private executionFlowPageService: ExecutionFlowPageServiceService,
              private spinner: NgxSpinnerService, private analyzerService: AnalyzerService, private pmServiceService: PmServiceService,
              private changeRef: ChangeDetectorRef) {
  }

  ngAfterViewChecked(): void {
    this.changeRef.detectChanges();
  }

  ngOnInit(): void {
   /* this.spinner.show();
    this.singledropdownSettings = {
      text: 'Select',
      showCheckbox: false,
      singleSelection: true,
      enableFilterSelectAll: false,
      classes: 'myclass custom-class',
      position: 'bottom',
      autoPosition: false,
      lazyLoading: false,
      maxHeight: 200,
      width: 50,
    };
    this.executionFlowPageService.getMpsHeaderData().subscribe((mpsHeaderResponse: any) => {
      if (mpsHeaderResponse !== null && mpsHeaderResponse !== undefined && mpsHeaderResponse.length !== 0) {
        mpsHeaderResponse.reverse().forEach((res: any) => {
          this.mpsSelectList.push({id: res.id, itemName: res.mpsName});
          this.singleselectedItems[0] = this.mpsSelectList[0];
        });
        this.onItemSelect(this.mpsSelectList[0]);
        this.spinner.hide();
      }
    }, (error) => {
      this.spinner.hide();
    });*/

    this.$subs=this.executionFlowPageService.getPpHeaderId.subscribe(resValue => {
      if (resValue !== undefined && resValue !== null) {
        this.executionFlowPageService.selectedHeaderId = resValue;
      }
      this.spinner.show();
      this.fgFileDataList = [];
      this.pmFileDataList = [];
      this.rmFileDataList = [];
      this.analyzerService.getAllExcelExportFileNamesData(this.executionFlowPageService.selectedHeaderId).subscribe((allExcelExportFileNamesRes: any) => {
        if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
          /*this.fgList = allExcelExportFileNamesRes?.fgFileList;*/
          for (let i = 0; i < allExcelExportFileNamesRes.fgFileList.length; i++) {
            this.fgFileDataList.push({name: allExcelExportFileNamesRes.fgFileList[i], selected: false});
          }
          /*  this.pmList = allExcelExportFileNamesRes?.pmFileList;*/
          for (let i = 0; i < allExcelExportFileNamesRes.pmFileList.length; i++) {
            this.pmFileDataList.push({name: allExcelExportFileNamesRes.pmFileList[i], selected: false});
          }
          /* this.rmList = allExcelExportFileNamesRes?.rmFileList;*/
          for (let i = 0; i < allExcelExportFileNamesRes.rmFileList.length; i++) {
            this.rmFileDataList.push({name: allExcelExportFileNamesRes.rmFileList[i], selected: false});
          }
        }
        this.spinner.hide();
      }, (error) => {
        console.log(error);
        this.spinner.hide();
      });

    });



  }

  ngOnDestroy(): void{
    this.$subs.unsubscribe();
  }

  /*onItemSelect(item: any) {
    this.selectedPlanName = item.itemName;
    this.analyzerMpsId = item.id;
    this.fgFileDataList = [];
    this.pmFileDataList = [];
    this.rmFileDataList = [];
    this.analyzerService.getAllExcelExportFileNamesData(this.analyzerMpsId).subscribe((allExcelExportFileNamesRes: any) => {
      if (allExcelExportFileNamesRes !== null && allExcelExportFileNamesRes !== undefined) {
        /!*this.fgList = allExcelExportFileNamesRes?.fgFileList;*!/
        for (let i = 0; i < allExcelExportFileNamesRes.fgFileList.length; i++) {
          this.fgFileDataList.push({name: allExcelExportFileNamesRes.fgFileList[i], selected: false});
        }
        /!*  this.pmList = allExcelExportFileNamesRes?.pmFileList;*!/
        for (let i = 0; i < allExcelExportFileNamesRes.pmFileList.length; i++) {
          this.pmFileDataList.push({name: allExcelExportFileNamesRes.pmFileList[i], selected: false});
        }
        /!* this.rmList = allExcelExportFileNamesRes?.rmFileList;*!/
        for (let i = 0; i < allExcelExportFileNamesRes.rmFileList.length; i++) {
          this.rmFileDataList.push({name: allExcelExportFileNamesRes.rmFileList[i], selected: false});
        }
      }
    }, (error) => {
      console.log(error);
    });
  }

  onItemDeSelect() {
    this.fgFileDataList = [];
    this.pmFileDataList = [];
    this.rmFileDataList = [];
  }*/

  selectAll() {
    for (var i = 0; i < this.fgFileDataList.length; i++) {
      this.fgFileDataList[i].selected = this.selectedAll;
    }
    console.log("selectedAll 1 =" + this.selectedAll);
    for (var i = 0; i < this.pmFileDataList.length; i++) {
      this.pmFileDataList[i].selected = this.selectedAll;
    }
    for (var i = 0; i < this.rmFileDataList.length; i++) {
      this.rmFileDataList[i].selected = this.selectedAll;
    }
    this.getCheckedItemList();
  }

  checkIfAllSelected() {
    this.selectedAll1 = this.fgFileDataList.every((items: any) => {
      return items.selected === true;
    });
    this.selectedAll2 = this.pmFileDataList.every((items: any) => {
      return items.selected === true;
    });
    this.selectedAll3 = this.rmFileDataList.every((items: any) => {
      return items.selected === true;
    });
    if (this.selectedAll1 === true && this.selectedAll2 === true && this.selectedAll3 === true) {
      this.selectedAll = true;
    } else {
      this.selectedAll = false;
    }
    this.getCheckedItemList();
  }

  getCheckedItemList() {
    this.checkedAllList.fgCheckedList = [];
    this.checkedAllList.pmCheckedList = [];
    this.checkedAllList.rmCheckedList = [];

    this.isDisabled = true;
    for (var i = 0; i < this.fgFileDataList.length; i++) {
      if (this.fgFileDataList[i].selected) {
        this.checkedAllList.fgCheckedList.push(this.fgFileDataList[i].name);
        this.isDisabled = false;
      }
    }
    for (var i = 0; i < this.pmFileDataList.length; i++) {
      if (this.pmFileDataList[i].selected) {
        this.checkedAllList.pmCheckedList.push(this.pmFileDataList[i].name);
        this.isDisabled = false;
      }
    }
    for (var i = 0; i < this.rmFileDataList.length; i++) {
      if (this.rmFileDataList[i].selected) {
        this.checkedAllList.rmCheckedList.push(this.rmFileDataList[i].name);
        this.isDisabled = false;
      }
    }
  }

  download() {
    let fileName;
    if (this.selectedExports.length === 1) {
      fileName = this.selectedExports.toString();
    }
    if (this.checkedAllList.fgCheckedList.length + this.checkedAllList.pmCheckedList.length + this.checkedAllList.rmCheckedList.length == 1) {
      if (this.checkedAllList.fgCheckedList.length === 1) {
        fileName = this.checkedAllList.fgCheckedList[0];
      }
      if (this.checkedAllList.pmCheckedList.length === 1) {
        fileName = this.checkedAllList.pmCheckedList[0];
      }
      if (this.checkedAllList.rmCheckedList.length === 1) {
        fileName = this.checkedAllList.rmCheckedList[0];
      }
    }
    if (this.checkedAllList.fgCheckedList.length <= 0) {
      this.checkedAllList.fgCheckedList.push("null");
    }
    if (this.checkedAllList.pmCheckedList.length <= 0) {
      this.checkedAllList.pmCheckedList.push("null");
    }
    if (this.checkedAllList.pmOtifCheckedList.length <= 0) {
      this.checkedAllList.pmOtifCheckedList.push("null");
    }
    if (this.checkedAllList.rmCheckedList.length <= 0) {
      this.checkedAllList.rmCheckedList.push("null");
    }
    this.analyzerService.getAllSelectedExcelExport(this.executionFlowPageService.selectedHeaderId, this.checkedAllList.fgCheckedList, this.checkedAllList.pmCheckedList,
      this.checkedAllList.pmOtifCheckedList, this.checkedAllList.rmCheckedList).subscribe((updatedPmSlobListResponse: any) => {
      this.spinner.hide();
      console.log(updatedPmSlobListResponse);
      if (updatedPmSlobListResponse.type === "application/vnd.xlsx") {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/xlsx;'});
        saveAs(blob, fileName);
      } else {
        const blob = new Blob([updatedPmSlobListResponse], {type: 'application/zip;'});
        saveAs(blob, this.executionFlowPageService.selectedPlanName);
      }
    }, (error) => {
      console.log(error);
      this.spinner.hide();
    });
    this.selectedExports = [];
  }

  fillList(list: any) {
    list.forEach((value) => {
      if (value.selected) {
        const temp = value.name;
        this.selectedExports.push(temp)
      }
    });
  }
}
