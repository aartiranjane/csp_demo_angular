import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-reports-page',
  templateUrl: './all-reports-page.component.html',
  styleUrls: ['./all-reports-page.component.css']
})
export class AllReportsPageComponent implements OnInit {
  firstTabClicked = true;
  secondTabClicked = false;
  constructor() { }

  ngOnInit(): void {
    document.getElementById('nav-org-tabb').click();
  }

}
