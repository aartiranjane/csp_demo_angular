import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonMpsDeltaAnalyticsComponent } from './common-mps-delta-analytics.component';

describe('CommonMpsDeltaAnalyticsComponent', () => {
  let component: CommonMpsDeltaAnalyticsComponent;
  let fixture: ComponentFixture<CommonMpsDeltaAnalyticsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommonMpsDeltaAnalyticsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonMpsDeltaAnalyticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
