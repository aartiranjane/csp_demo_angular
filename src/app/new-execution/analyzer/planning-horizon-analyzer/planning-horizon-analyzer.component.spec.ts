import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningHorizonAnalyzerComponent } from './planning-horizon-analyzer.component';

describe('PlanningHorizonAnalyzerComponent', () => {
  let component: PlanningHorizonAnalyzerComponent;
  let fixture: ComponentFixture<PlanningHorizonAnalyzerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanningHorizonAnalyzerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningHorizonAnalyzerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
