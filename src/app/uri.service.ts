import {Injectable} from '@angular/core';
import {SocketIoConfig} from 'ngx-socket-io';

@Injectable()
export class UriService {
    private docPath = 'http://localhost:3000';
    static socket = {url: 'http://localhost:4500'};
    /*https://docupload.lorealc2net.com*/

    

    getResourceServerUri() {
        return 'http://localhost:8085';
        /*https://resource.lorealc2net.com*/
    }

    getAuthServerUri() {
        return 'http://localhost:9000';
        /*https://auth.lorealc2net.com*/
    }

    getWebServerUri() {
        return 'https://www.lorealc2net.com';
    }

    getDocUploadUri() {
        return this.docPath + '/upload';
    }

    getDocPath() {
        return this.docPath;
    }
    getSocketConfig(): SocketIoConfig {
        return {url: 'http://localhost:4500'};
        // 'https://stream-chat.sanjalidemo.com'
        // 'http://localhost:4444'
    }

}
